package com.mybookingpal.dataaccesslayer.handlers;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UTCDateTypeHandler implements TypeHandler {

	@Override
	public void setParameter(final PreparedStatement ps, final int i, Object timestamp, final JdbcType jdbcType) throws SQLException {
		if (timestamp == null) {
			ps.setNull(i, jdbcType.TYPE_CODE);
		} else {
			ps.setTimestamp(i, (Timestamp) timestamp, getUTCCalendar());
		}
	}

	@Override
	public Date getResult(final ResultSet rs, final String columnName) throws SQLException {
		try {
			String timestamp = rs.getString(columnName);
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timestamp);
		} catch (Exception e) {
			return null;
		}
	}

	public Object getResult(final ResultSet rs, final int columnIndex) throws SQLException {
		return rs.getTimestamp(columnIndex, getUTCCalendar());
	}

	@Override
	public Object getResult(final CallableStatement cs, final int columnIndex) throws SQLException {
		return cs.getTimestamp(columnIndex, getUTCCalendar());
	}

	private static Calendar getUTCCalendar() {
		return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	}
}
