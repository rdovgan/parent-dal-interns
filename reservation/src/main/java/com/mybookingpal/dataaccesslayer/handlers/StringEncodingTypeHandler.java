package com.mybookingpal.dataaccesslayer.handlers;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.StringTypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StringEncodingTypeHandler extends StringTypeHandler {

	public StringEncodingTypeHandler() {
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
		String[] toEncode = new String[] { (String) parameter };
		String p = StringEscapeUtils.escapeHtml4(parameter);
		ps.setString(i, p);
	}

	@Override
	public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String resuString = rs.getString(columnName);
		if (resuString != null) {
			resuString = StringEscapeUtils.unescapeHtml4(resuString);
		}
		return resuString;
	}

	@Override
	public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String resuString = cs.getString(columnIndex);
		if (resuString != null) {
			resuString = StringEscapeUtils.unescapeHtml4(resuString);
		}
		return resuString;
	}

}
