package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.Reservation;
import com.mybookingpal.dataaccesslayer.mappers.ReservationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ReservationDao {

	@Autowired
	private ReservationMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Reservation read() {
		return mapper.read();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Reservation> readAll() {
		return mapper.readAll();
	}
}
