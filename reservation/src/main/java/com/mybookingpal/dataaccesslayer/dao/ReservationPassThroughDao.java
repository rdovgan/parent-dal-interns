package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ReservationPassThroughModel;
import com.mybookingpal.dataaccesslayer.mappers.ReservationPassThroughMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ReservationPassThroughDao {

	@Autowired
	private ReservationPassThroughMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ReservationPassThroughModel action) {
		if (action.getReservationId() == null) {
			return;
		}
		mapper.create(action);
	}
}
