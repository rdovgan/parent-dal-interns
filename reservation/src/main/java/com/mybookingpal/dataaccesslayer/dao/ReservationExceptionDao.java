package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ReservationExceptionModel;
import com.mybookingpal.dataaccesslayer.mappers.ReservationExceptionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Repository
public class ReservationExceptionDao {

	@Autowired
	private ReservationExceptionMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ReservationExceptionModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ReservationExceptionModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		mapper.update(action);
	}

	public ReservationExceptionModel read(ReservationExceptionModel action) {
		if (Objects.isNull(action)) {
			return null;
		}
		return mapper.read(action);
	}

	public ReservationExceptionModel exists(ReservationExceptionModel action) {
		if (Objects.isNull(action)) {
			return null;
		}
		return mapper.exists(action);
	}
}
