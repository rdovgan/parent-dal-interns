package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ReservationExceptionModel;

public interface ReservationExceptionMapper {

	ReservationExceptionModel exists(ReservationExceptionModel action);

	ReservationExceptionModel read(ReservationExceptionModel action);

	void create(ReservationExceptionModel action);

	void update(ReservationExceptionModel action);
}
