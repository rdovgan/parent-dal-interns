package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ReservationPassThroughModel;

public interface ReservationPassThroughMapper {

	void create(ReservationPassThroughModel action);
}
