package com.mybookingpal.dataaccesslayer.entity.enums;

public enum State {
	Active, Cleared
}
