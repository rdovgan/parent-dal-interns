package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.Price;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PriceMapper {

	List<Price> readByProductId(@Param("productId") Integer productId);
}
