package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.Price;
import com.mybookingpal.dataaccesslayer.mappers.PriceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PriceDao {

	@Autowired
	private PriceMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW) List<Price> readByProductId(Integer id) {
		return mapper.readByProductId(id);
	}
}
