package com.mybookingpal.dataaccesslayer.handlers;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@MappedTypes(LocalDate.class)
public class LocalDateTypeHandler extends BaseTypeHandler<LocalDate> {

	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, LocalDate localDate, JdbcType jdbcType) throws SQLException {
		Date date = Date.valueOf(localDate);
		preparedStatement.setString(i, date.toString());
	}

	@Override
	public LocalDate getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		Date date = resultSet.getDate(columnName);
		if (date == null) {
			return null;
		}
		return getLocalDate(date);
	}

	@Override
	public LocalDate getNullableResult(ResultSet resultSet, int i) throws SQLException {
		return LocalDate.parse(resultSet.getString(i));
	}

	@Override
	public LocalDate getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		Date date = callableStatement.getDate(columnIndex);
		if (date == null) {
			return null;
		}
		return getLocalDate(date);
	}

	private LocalDate getLocalDate(Date date) {
		if (date != null) {
			return date.toLocalDate();
		}
		return null;
	}
}
