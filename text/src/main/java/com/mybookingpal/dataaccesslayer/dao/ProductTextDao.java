package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ProductTextModel;
import com.mybookingpal.dataaccesslayer.mappers.ProductTextMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class ProductTextDao {
	@Autowired
	ProductTextMapper productTextMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		productTextMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertOrUpdateList(@Param("list") List<ProductTextModel> productTextList) {
		if (!CollectionUtils.isEmpty(productTextList)) {
			productTextMapper.insertOrUpdateList(productTextList.stream()
					.filter(Objects::nonNull)
					.collect(Collectors.toList()));
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertOrUpdateProductText(ProductTextModel productText) {
		if (productText != null) {
			productTextMapper.insertOrUpdateProductText(productText);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelProductText(List<Integer> productTextIds) {
		if (!CollectionUtils.isEmpty(productTextIds)) {
			productTextMapper.cancelProductText(productTextIds);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> getByProductId(Integer productId) {
		return productTextMapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> getByProductIdList(List<Integer> productId) {
		if (CollectionUtils.isEmpty(productId)) {
			return Collections.emptyList();
		}
		return productTextMapper.getByProductIdList(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> readByProductIdWithAllTypes(Integer productId) {
		return productTextMapper.readByProductIdWithAllTypes(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyProductTexts(Integer fromProductId, Integer toProductId) {
		productTextMapper.copyProductTexts(fromProductId, toProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> getProductTextFromLastTimeStamp(String productId, Date version) {
		return productTextMapper.getProductTextFromLastTimeStamp(productId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> readByProductIdAndTypeAndLanguage(Integer productId, String type, String language) {
		return productTextMapper.readByProductIdAndTypeAndLanguage(productId, type, language);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> readByProductIdAndTypeAndLanguageAndState(Integer productId, String type, String language, String state) {
		return productTextMapper.readByProductIdAndTypeAndLanguageAndState(productId, type, language, state);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductTextModel> readByProductIdAndLanguage(Integer productId, String language) {
		return productTextMapper.readByProductIdAndLanguage(productId, language);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateProductText(ProductTextModel productText) {
		if (productText != null) {
			productTextMapper.updateProductText(productText);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ProductTextModel productText) {
		if (productText != null) {
			productTextMapper.update(productText);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void saveListProductText(List<ProductTextModel> productTexts) {
		if (!CollectionUtils.isEmpty(productTexts)) {
			productTextMapper.saveListProductText(productTexts.stream()
					.filter(Objects::nonNull)
					.collect(Collectors.toList()));
		}
	}

}
