package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.TextLanguageAction;
import com.mybookingpal.dataaccesslayer.entity.TextModel;
import com.mybookingpal.dataaccesslayer.mappers.TextMapper;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
public class TextDao {
	@Autowired
	TextMapper textMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TextModel readByID(String id) {
		return textMapper.readByID(id);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(TextModel action) {
		if (action != null) {
			textMapper.create(action);
		}
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TextModel read(String id) {
		return textMapper.read(id);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(TextModel action) {
		if (action != null) {
			textMapper.update(action);
		}
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TextModel readbyexample(TextModel action) {
		if (action == null) {
			return null;
		}
		return textMapper.readbyexample(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deletebyexample(TextModel action) {
		if (action != null) {
			textMapper.deletebyexample(action);
		}
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String lastimage(String root) {
		return textMapper.lastimage(root);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> imagesbynameid(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return textMapper.imagesbynameid(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> imagesbyurl(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return textMapper.imagesbyurl(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> imageidsbynameid(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return textMapper.imageidsbynameid(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TextModel readbyNameAndID(TextModel action) {
		if (action == null) {
			return null;
		}
		return textMapper.readbyNameAndID(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TextModel> readallbyid(String id) {
		return textMapper.readallbyid(id);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TextModel> readListByIds(TextLanguageAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return textMapper.readListByIds(action);
	}
}
