package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PartyTextModel;
import com.mybookingpal.dataaccesslayer.mappers.PartyTextMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
public class PartyTextDao {
	@Autowired
	PartyTextMapper partyTextMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PartyTextModel action) {
		if (action != null) {
			partyTextMapper.create(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyTextModel> readByPartyId(String id) {
		return partyTextMapper.readByPartyId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PartyTextModel action) {
		if (action != null) {
			partyTextMapper.update(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyTextModel> readByExample(PartyTextModel example) {
		if (example == null) {
			return Collections.emptyList();
		}
		return partyTextMapper.readByExample(example);
	}
}
