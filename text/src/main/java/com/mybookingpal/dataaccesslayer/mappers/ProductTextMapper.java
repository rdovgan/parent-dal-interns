package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ProductTextModel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ProductTextMapper {

	void deleteByProductId(String productId);

	void insertOrUpdateList(@Param("list") List<ProductTextModel> productTextList);

	void insertOrUpdateProductText(ProductTextModel productText);

	void cancelProductText(List<Integer> productTextIds);

	List<ProductTextModel> getByProductId(Integer productId);

	List<ProductTextModel> getByProductIdList(List<Integer> productId);

	List<ProductTextModel> readByProductIdWithAllTypes(Integer productId);

	void copyProductTexts(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	List<ProductTextModel> getProductTextFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);

	List<ProductTextModel> readByProductIdAndTypeAndLanguage(@Param("productId") Integer productId, @Param("type") String type, @Param("language") String language);

	List<ProductTextModel> readByProductIdAndTypeAndLanguageAndState(@Param("productId") Integer productId, @Param("type") String type,
			@Param("language") String language, @Param("state") String state);

	List<ProductTextModel> readByProductIdAndLanguage(@Param("productId") Integer productId, @Param("language") String language);

	void updateProductText(ProductTextModel productText);

	void update(ProductTextModel productText);

	void saveListProductText(@Param("list") List<ProductTextModel> productTexts);
}
