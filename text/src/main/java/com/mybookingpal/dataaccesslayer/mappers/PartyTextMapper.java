package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PartyTextModel;

import java.util.List;

public interface PartyTextMapper {

	void create(PartyTextModel action);

	List<PartyTextModel> readByPartyId(String id);

	void update(PartyTextModel action);

	List<PartyTextModel> readByExample(PartyTextModel example);

}
