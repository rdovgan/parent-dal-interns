package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.TextLanguageAction;
import com.mybookingpal.dataaccesslayer.entity.TextModel;
import com.mybookingpal.utils.entity.NameId;

import java.util.List;

public interface TextMapper {

	TextModel readByID(String id);

	@Deprecated
	void create(TextModel action);

	@Deprecated
	TextModel read(String id);

	@Deprecated
	void update(TextModel action);

	@Deprecated
	TextModel readbyexample(TextModel action);

	@Deprecated
	void deletebyexample(TextModel action);

	@Deprecated
	String lastimage(String root);

	@Deprecated
	List<NameId> imagesbynameid(NameId action);

	@Deprecated
	List<NameId> imagesbyurl(NameId action);

	@Deprecated
	List<String> imageidsbynameid(NameId action);

	//Added for Flipkey integration
	@Deprecated
	TextModel readbyNameAndID(TextModel action);

	@Deprecated
	List<TextModel> readallbyid(String id);

	@Deprecated
	List<TextModel> readListByIds(TextLanguageAction action);
}
