package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.ProductTextState;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductTextStateTypeHandler extends BaseTypeHandler<ProductTextState> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ProductTextState parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getValue());
	}

	@Override
	public ProductTextState getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return ProductTextState.getByInt(rs.getInt(columnName));
	}

	@Override
	public ProductTextState getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return ProductTextState.getByInt(rs.getInt(columnIndex));
	}

	@Override
	public ProductTextState getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return ProductTextState.getByInt(cs.getInt(columnIndex));
	}
}
