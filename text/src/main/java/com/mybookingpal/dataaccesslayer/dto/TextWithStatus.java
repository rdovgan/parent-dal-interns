package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.TextModel;

import javax.xml.bind.annotation.XmlTransient;

public class TextWithStatus extends TextModel {
	private int status = 0;

	@XmlTransient
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Text [status=");
		builder.append(status);
		builder.append(", id=");
		builder.append(id);
		builder.append(", language=");
		builder.append(language);
		builder.append(", name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", state=");
		builder.append(state);
		builder.append(", date=");
		builder.append(date);
		builder.append(", notes=");
		builder.append(notes);
		//		builder.append(", data=");
		//		builder.append(Arrays.toString(data));
		builder.append("]");
		return builder.toString();
	}
}
