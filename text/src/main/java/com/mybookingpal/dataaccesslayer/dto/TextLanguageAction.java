package com.mybookingpal.dataaccesslayer.dto;

import java.util.List;

public class TextLanguageAction {
	private List<String> listText;
	private String language;

	public TextLanguageAction() {
	}

	public TextLanguageAction(List<String> listText, String language) {
		this.listText = listText;
		this.language = language;
	}

	public List<String> getListText() {
		return listText;
	}

	public void setListText(List<String> listText) {
		this.listText = listText;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
