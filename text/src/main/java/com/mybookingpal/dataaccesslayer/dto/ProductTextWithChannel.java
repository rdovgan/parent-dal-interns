package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.ProductTextType;
import com.mybookingpal.dataaccesslayer.entity.ProductTextModel;

public class ProductTextWithChannel extends ProductTextModel {
	private Integer channelId;

	public ProductTextWithChannel() {
	}

	public ProductTextWithChannel(String productId, Integer partyId, String value, ProductTextType type, String language, String htmlValue, Integer channelId) {
		super(productId, partyId, value, type, language, htmlValue);
		this.channelId = channelId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
}
