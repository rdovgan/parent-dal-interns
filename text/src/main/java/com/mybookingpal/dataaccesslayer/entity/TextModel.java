package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.TextState;
import com.mybookingpal.dataaccesslayer.constants.TextType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
public class TextModel {
	protected String id;
	protected String name;
	protected String state;
	protected String type;
	protected String notes;
	protected String language;
	protected Date date;
	protected Byte[] data;
	protected Date version; //latest change

	public TextModel() {
	}

	public TextModel(String id, String language) {
		this.id = id;
		this.language = language;
	}

	public TextModel(String id, String name, TextType type, Date date, String notes, String language) {
		this.id = id;
		this.name = name;
		this.state = TextState.Created.name();
		this.type = type == null ? null : type.name();
		this.date = date;
		this.notes = notes;
		this.language = language;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? " " : name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setType(TextType type) {
		this.type = type == null ? null : type.name();
	}


	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes == null ? " " : notes;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? "EN" : language;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date == null ? new Date() : date;
	}

	public Byte[] getData() {
		return data;
	}

	public void setData(Byte[] data) {
		this.data = data;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Text [");
		builder.append("id=");
		builder.append(id);
		builder.append(", language=");
		builder.append(language);
		builder.append(", name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", state=");
		builder.append(state);
		builder.append(", date=");
		builder.append(date);
		builder.append(", notes=");
		builder.append(notes);
		//		builder.append(", data=");
		//		builder.append(Arrays.toString(data));
		builder.append("]");
		return builder.toString();
	}
}