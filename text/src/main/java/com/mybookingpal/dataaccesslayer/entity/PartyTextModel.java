package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class PartyTextModel {
	private Integer partyId;
	private String type;
	private String language;
	private String value;
	private Date createdDate;
	private Date version;

	public PartyTextModel() {
	}

	public PartyTextModel(Integer partyId, String type, String language, String value, Date createdDate, Date version) {
		this.partyId = partyId;
		this.type = type;
		this.language = language;
		this.value = value;
		this.createdDate = createdDate;
		this.version = version;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}
