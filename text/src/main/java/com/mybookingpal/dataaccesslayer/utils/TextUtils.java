package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.constants.TextConstants;
import com.mybookingpal.dataaccesslayer.constants.TextType;
import com.mybookingpal.dataaccesslayer.dto.TextWithStatus;
import com.mybookingpal.dataaccesslayer.entity.TextModel;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class TextUtils {
	public static boolean noId(TextModel text) {
		return StringUtils.isBlank(text.getId()) || "0".equals(text.getId());
	}

	public static boolean hasId(TextModel text) {
		return !noId(text);
	}

	public static boolean notType(TextType type, TextModel text) {
		return text.getType() == null || type == null || !text.getType()
				.equalsIgnoreCase(type.name());
	}

	public static boolean hasType(TextType type, TextModel text) {
		return !notType(type, text);
	}

	public static boolean noStatus(TextWithStatus text) {
		return text.getStatus() == 0;
	}

	public static boolean noNotes(TextModel text) {
		return StringUtils.isBlank(text.getNotes());
	}

	public static String getPlainText(TextModel text) {
		if (text.getNotes() == null) {
			return "";
		}
		return text.getNotes()
				.replaceAll("\\<.*?>", "");
	}

	public static boolean noLanguage(TextModel text) {
		return StringUtils.isBlank(text.getLanguage());
	}

	public static boolean hasLanguage(String language, TextModel text) {
		return text.getLanguage() != null && text.getLanguage()
				.equalsIgnoreCase(language);
	}

	public static boolean noDate(TextModel text) {
		return text.getDate() == null;
	}

	public static String getDataString(TextModel text) {
		if (text.getData() == null) {
			return null;
		}
		String value = new String();
		for (byte datum : text.getData()) {
			value += (char) datum;
		}
		return value;
	}

	public static boolean noData(long min, long max, TextModel text) {
		return text.getData() == null || text.getData().length < min || text.getData().length > max;
	}

	/**
	 * Checks if the URL is a valid image file type, otherwise false.
	 *
	 * @param url the URL of the file.
	 * @return true, if a valid image file type, otherwise false.
	 */
	public static boolean isImageFile(String url) {
		if (url == null) {
			return false;
		}
		String filename = url.trim()
				.toLowerCase();
		return filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".bmp") || filename.endsWith(".gif") || filename.endsWith(".png");
	}

	/**
	 * Checks if the URL is not a valid image file type, otherwise false.
	 *
	 * @param url the URL of the file.
	 * @return true, if not a valid image file type, otherwise false.
	 */
	public static boolean notImageFile(String url) {
		return !isImageFile(url);
	}

	/**
	 * Checks if the URL is not a valid audio file type, otherwise false.
	 *
	 * @param url the URL of the file.
	 * @return true if a valid audio file type, otherwise false.
	 */
	public static boolean isAudioFile(String url) {
		if (url == null) {
			return false;
		}
		String filename = url.trim()
				.toLowerCase();
		return filename.endsWith(".au") || filename.endsWith(".mp3") || filename.endsWith(".ogg") || filename.endsWith(".wav");
	}

	public static boolean notAudioFile(String url) {
		return !isAudioFile(url);
	}

	/**
	 * Gets the extension type of the specified filename.
	 *
	 * @param filename the specified filename.
	 */
	public static final String getExtension(String filename) {
		String[] args = filename.split("\\.");
		if (args.length < 2) {
			return "";
		} else {
			return "." + args[args.length - 1].toLowerCase();
		}
	}

	/**
	 * Gets the specified file name without its extension.
	 *
	 * @param value the specified file name with extension.
	 * @return the file name without extension.
	 */
	public static String trimExtension(String value) {
		if (value == null || value.lastIndexOf('.') <= 0) {
			return (value);
		} else {
			return value.substring(0, value.lastIndexOf('.'));
		}
	}

	/**
	 * Gets the specified file name without its extension.
	 *
	 * @param value the specified file name with extension.
	 * @return the file name only last part of URL.
	 */
	public static String getS3ImageFilename(String value) {
		String filename = "";
		if (value == null || value.lastIndexOf('/') <= 0) {
			filename = value;
		} else {
			filename = value.substring(value.lastIndexOf('/') + 1);
		}
		int index = filename.indexOf(TextConstants.THUMBNAIL_INFIX);
		if (index > 0) {
			filename = filename.substring(0, index) + filename.substring(index + TextConstants.THUMBNAIL_INFIX.length());
		}
		return filename;
	}

	/**
	 * Gets the specified file name without its extension.
	 *
	 * @param value the specified file name with extension.
	 * @return part of URL without filename.
	 */
	public static String getProductURL(String value) {
		if (value == null || value.lastIndexOf('/') <= 0) {
			return (value);
		} else {
			return value.substring(0, value.lastIndexOf('/') + 1);
		}
	}

	/**
	 * @param imageurls
	 * @return part of URL without filename
	 */
	public static String getProductImageURL(List<String> imageurls) {
		String url = "";
		if (imageurls.size() > 0) {
			url = imageurls.get(0);
			url = url.substring(0, url.lastIndexOf('/') + 1);
		}
		return url;
	}

	/**
	 * Gets a string with any HTML mark up removed.
	 *
	 * @param html the text from which HTML mark up is to be removed.
	 * @return text without mark up.
	 * @see <pre>http://developer.ean.com/faqs/Development</pre>
	 * TODO:
	 * xmlText=Replace(xmlText,"&amp;lt;","&lt;")
	 * xmlText=Replace(xmlText,"&amp;gt;","&gt;")
	 * xmlText=Replace(xmlText,"&amp;apos;","&apos;")
	 * xmlText=Replace(xmlText,"&amp;#x0A","")
	 * xmlText=Replace(xmlText,"&amp;#x0D","")
	 * xmlText=Replace(xmlText,"&#x0D","")
	 * xmlText=Replace(xmlText,"&#x0A","")
	 * xmlText=Replace(xmlText,"&amp;#x09","")
	 * xmlText=Replace(xmlText,"&amp;amp;amp;","&amp;")
	 * xmlText=Replace(xmlText,"&lt;br&gt;","<br />")
	 */
	public static final String stripHTML(String html) {
		if (html == null || html.isEmpty()) {
			return html;
		}
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&amp;lt;", "&lt;");
		html = html.replaceAll("&amp;gt;", "&gt;");
		html = html.replaceAll("&amp;apos;", "&apos;");
		html = html.replaceAll("&amp;#x0A", "");
		html = html.replaceAll("&amp;#x0D", "");
		html = html.replaceAll("&#x0D", "");
		html = html.replaceAll("&#x0A", "");
		html = html.replaceAll("&amp;#x09", "");
		html = html.replaceAll("&amp;amp;amp;", "&amp;");
		html = html.replaceAll("&lt;br&gt;", "<br />");
		return html.replaceAll("\\<.*?>", ""); // alt return html.replaceAll("\\<.*?\\>", "");
	}

	/**
	 * Splits the specified text into separate lines where the line separators characters are
	 * newline (hex 0x0a) or carriage return (hex 0x0d) characters.
	 *
	 * @param text the specified text.
	 * @return the text split into separate lines
	 */
	public static final String[] getSentences(String text) {
		if (text == null || text.isEmpty()) {
			return null;
		}
		return text.split("\\r?\\n");
	}

	public static String getLogo(String organizationid) {
		return "Logo." + organizationid;
	}//NO TYPE EXTENSION!
}
