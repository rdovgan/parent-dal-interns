package com.mybookingpal.dataaccesslayer.constants;

public enum TextState {
	Initial, Created, Final
}