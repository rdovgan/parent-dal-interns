package com.mybookingpal.dataaccesslayer.constants;

public enum PartyTextEnum {
	OWNER_INFO("OWNER_INFO"),
	NEIGHBORHOOD_OVERVIEW("NEIGHBORHOOD_OVERVIEW");

	private String value;

	PartyTextEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static PartyTextEnum getByName(String value) {
		for (PartyTextEnum v : values()) {
			if (v.value.equals(value)) {
				return v;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}
}