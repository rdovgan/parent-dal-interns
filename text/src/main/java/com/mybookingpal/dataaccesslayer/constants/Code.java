package com.mybookingpal.dataaccesslayer.constants;

import org.apache.commons.lang3.StringUtils;

public enum Code {
	Checkin, Contents, Condition, Contract, Contact, File,
	Inside, Options, Outside, Pictures, Private, Public,
	Service, Url, Special, Rules;

	public static final Code getCode(String type) {
		if (StringUtils.isBlank(type)) {
			return Code.Public;
		}
		try {
			return Code.valueOf(type);
		} catch (Throwable x) {
			return Code.Public;
		}
	}
}