package com.mybookingpal.dataaccesslayer.constants;

public enum TextType {
	Blob, File, Image, ImageBlob, PublicFile, Text, HTML
}