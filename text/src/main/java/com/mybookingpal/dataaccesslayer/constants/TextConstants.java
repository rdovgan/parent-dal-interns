package com.mybookingpal.dataaccesslayer.constants;

public class TextConstants {
	//form upload
	public static final String THUMBNAIL_PIXELS = "Thumbnail Pixels";
	public static final String FULLSIZE_PIXELS = "Fullsize Pixels";
	public static final String FILE_NAME = "File Name";
	public static final String FILE_NOTES = "File Notes";
	public static final String FILE_TYPE = "File Type"; //Upload file type
	public static final int THUMBNAIL_PIXELS_VALUE = 100;
	public static final int FULLSIZE_PIXELS_VALUE = 500;
	public static final int LOGO_PIXELS_VALUE = 100; //high
	public static final int MAX_BLOB_SIZE = 200000;
	public static final int MAX_TEXT_SIZE = 20000;
	public static final String TEMP_FILE = "Temp";

	public static final String IMAGE = "Image";

	public static final String AUDIO_OGG = ".ogg"; //qualifies text id
	public static final String AUDIO_MP3 = ".mp3";
	public static final String AUDIO_WAV = ".wav";

	public static final String VIDEO_OGG = ".ogg";
	public static final String VIDEO_MP4 = ".mp4";
	public static final String VIDEO_WEBM = ".webm";

	public static final String DEFAULT_IMAGE = "Product144-000";
	public static final String FULLSIZE_JPG = ".jpg";
	public static final String THUMBNAIL_JPG = "Thumb.jpg";
	public static final String THUMBNAIL_INFIX = "Thumb";
	public static final String IMAGE_PREFIX = "Product";
	public static final String DEFAULT_IMAGE_URL = "https://s3.amazonaws.com/mybookingpal/pictures/Product144-000Thumb.jpg";

}
