package com.mybookingpal.dataaccesslayer.constants;

public enum ProductTextType {
	Description, ShortDescription, HouseRules, Name, Access, Interaction, NeighborhoodOverview, Transit, Notes, FinePrint, DisplayName, ChannelFullDescription, ChannelShortDescription, AboutTheProperty;

	public String getStringValue() {
		return this.name();
	}

	public int getValue() {
		throw new UnsupportedOperationException();
	}
}
