package com.mybookingpal.dataaccesslayer.constants;

public enum ProductTextState {
	CREATED(2), FINAL(3);

	private Integer value;

	ProductTextState(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static ProductTextState getByInt(int value) {
		for (ProductTextState v : values()) {
			if (v.value == value) {
				return v;
			}
		}
		return CREATED;
	}

	public static String getNameByInt(Integer value) {
		return getByInt(value).name();
	}
}
