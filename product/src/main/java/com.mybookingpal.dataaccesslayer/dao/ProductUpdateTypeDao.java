package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ProductUpdateTypeModel;
import com.mybookingpal.dataaccesslayer.mappers.ProductUpdateTypeMapper;
import com.mybookingpal.dataaccesslayer.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ProductUpdateTypeDao {

	@Autowired
	private ProductUpdateTypeMapper productUpdateTypeMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductUpdateTypeModel getByProductId(Integer id) {
		return productUpdateTypeMapper.getByProductId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductUpdateTypeModel> getListByProductIds(List<Integer> productIds) {
		return productUpdateTypeMapper.getListByProductIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getAllProductIdsByParty(String partyId) {
		return productUpdateTypeMapper.getAllProductIdsByParty(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insert(List<ProductUpdateTypeModel> productUpdateTypeModelList) {
		if (CollectionUtils.isEmpty(productUpdateTypeModelList)) {
			return;
		}
		productUpdateTypeMapper.insert(productUpdateTypeModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ProductUpdateTypeModel productUpdateTypeModel) {
		productUpdateTypeMapper.update(productUpdateTypeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ProductUpdateTypeModel productUpdateTypeModel) {
		productUpdateTypeMapper.create(productUpdateTypeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertSelective(ProductUpdateTypeModel productUpdateTypeModel) {
		productUpdateTypeMapper.insertSelective(productUpdateTypeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateProductUpdateType(ProductUpdateTypeModel productUpdateTypeModel) {
		productUpdateTypeMapper.updateProductUpdateType(productUpdateTypeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateProductUpdateTypeForProducts(ProductUpdateTypeModel productUpdateTypeModel, List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return;
		}
		productUpdateTypeMapper.updateProductUpdateTypeForProducts(productUpdateTypeModel, productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createProductUpdateTypeForProducts(ProductUpdateTypeModel productUpdateTypeModel, List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return;
		}
		productUpdateTypeMapper.createProductUpdateTypeForProducts(productUpdateTypeModel, productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean showFeeTaxDisclaimer(Integer productId) {
		return productUpdateTypeMapper.showFeeTaxDisclaimer(productId);
	}

}
