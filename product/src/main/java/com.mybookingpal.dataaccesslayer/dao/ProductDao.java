package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.Area;
import com.mybookingpal.dataaccesslayer.dto.AttributeAction;
import com.mybookingpal.dataaccesslayer.dto.BeststayzProductFilterDto;
import com.mybookingpal.dataaccesslayer.dto.BeststayzProductReport;
import com.mybookingpal.dataaccesslayer.dto.BookingPalValidationInfo;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalIncomingListing;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalIncomingListingFilterDto;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalProductFilterDto;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalProductReport;
import com.mybookingpal.dataaccesslayer.dto.GuidelineProduct;
import com.mybookingpal.dataaccesslayer.dto.GuidelineProductRequestDto;
import com.mybookingpal.dataaccesslayer.dto.ImageWidgetItem;
import com.mybookingpal.dataaccesslayer.dto.MarriottApiValidationInfo;
import com.mybookingpal.dataaccesslayer.dto.MarriottValidationRequestDto;
import com.mybookingpal.dataaccesslayer.dto.MedalliaOrganizationalData;
import com.mybookingpal.dataaccesslayer.dto.NameIdCountOffset;
import com.mybookingpal.dataaccesslayer.dto.OtaSearchParameter;
import com.mybookingpal.dataaccesslayer.dto.ProductAction;
import com.mybookingpal.dataaccesslayer.dto.ProductChannel;
import com.mybookingpal.dataaccesslayer.dto.ProductChannelRelation;
import com.mybookingpal.dataaccesslayer.dto.ProductIdWithNameItem;
import com.mybookingpal.dataaccesslayer.dto.ProductIdWithPhysicalAddressDto;
import com.mybookingpal.dataaccesslayer.dto.ProductIdentification;
import com.mybookingpal.dataaccesslayer.dto.ProductInfo;
import com.mybookingpal.dataaccesslayer.dto.ProductWidgetItem;
import com.mybookingpal.dataaccesslayer.dto.Property;
import com.mybookingpal.dataaccesslayer.dto.PropertyInfo;
import com.mybookingpal.dataaccesslayer.dto.SearchListingModelRequest;
import com.mybookingpal.dataaccesslayer.dto.SearchListingModelResponse;
import com.mybookingpal.dataaccesslayer.entity.ProductModel;
import com.mybookingpal.dataaccesslayer.mappers.ProductMapper;
import com.mybookingpal.dataaccesslayer.utils.CollectionUtils;
import com.mybookingpal.utils.entity.Parameter;
import com.mybookingpal.utils.entity.IdVersion;
import com.mybookingpal.utils.entity.BigDecimalExt;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ProductDao {

	@Autowired
	private ProductMapper productMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ProductModel action) {
		if (action != null) {
			productMapper.create(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createList(List<ProductModel> products) {
		if (CollectionUtils.isEmpty(products)) {
			return;
		}
		productMapper.createList(products);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel read(Integer id) {
		return productMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ProductModel action) {
		if (action != null) {
			productMapper.update(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateLinkedIds(List<ProductModel> products) {
		if (CollectionUtils.isEmpty(products)) {
			return;
		}
		productMapper.updateLinkedIds(products);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateNotCheckValue(ProductModel action) {
		if (action != null) {
			productMapper.updateNotCheckValue(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel exists(ProductModel action) {
		if (action == null) {
			return null;
		}
		return productMapper.exists(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double commission(String supplierId) {
		return productMapper.commission(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double discount(String supplierId) {
		return productMapper.discount(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> list(ProductModel action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.list(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getPrimaryAndSingleProduct(ProductModel action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.getPrimaryAndSingleProduct(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getPrimarySingleAndMultiRepProduct(ProductModel action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.getPrimarySingleAndMultiRepProduct(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readProductCollectionByIdCollection(List<Integer> productIdCollection) {
		if (CollectionUtils.isEmpty(productIdCollection)) {
			return Collections.emptyList();
		}
		return productMapper.readProductCollectionByIdCollection(productIdCollection);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readProductsByLocationIds(List<Integer> locationIds) {
		if (CollectionUtils.isEmpty(locationIds)) {
			return Collections.emptyList();
		}
		return productMapper.readProductsByLocationIds(locationIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> altlist(String altPartyId) {
		return productMapper.altlist(altPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> preparedAltList(String altPartyId) {
		return productMapper.preparedAltList(altPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> componentlist(String productid) {
		return productMapper.componentlist(productid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readByCoordinatesAndRadius(List<Integer> locationParentIds, Long latitude, Long longitude, Integer guests, Long radius,
			Integer limit, List<Integer> propertyManagerIds, List<Integer> productsForIgnoring) {
		return productMapper.readByCoordinatesAndRadius(locationParentIds, latitude, longitude, guests, radius, limit, propertyManagerIds, productsForIgnoring);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> idsByParentLocationAndPrice(String locationId, String fromDate, String toDate, String agentId, Boolean inquireOnly, String pmId) {
		return productMapper.idsByParentLocationAndPrice(locationId, fromDate, toDate, agentId, inquireOnly, pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> idsByLocationIdAndAgentId(String locationId, String fromDate, String toDate, String agentId, Boolean inquireOnly) {
		return productMapper.idsByLocationIdAndAgentId(locationId, fromDate, toDate, agentId, inquireOnly);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> idswithreservationcollision(String locationId, String fromDate, String toDate) {
		return productMapper.idswithreservationcollision(locationId, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String productCountBySupplier(String partyId) {
		return productMapper.productCountBySupplier(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String selectedProductCountBySupplier(String partyId) {
		return productMapper.selectedProductCountBySupplier(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String productCountByAltSupplier(String altPmId, String pmsId) {
		return productMapper.productCountByAltSupplier(altPmId, pmsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateSupplierIdByAltSupplier(ProductModel action) {
		if (action != null) {
			productMapper.updateSupplierIdByAltSupplier(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> flipkeyproducts(String supplierId) {
		return productMapper.flipkeyproducts(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel altread(NameId action) {
		if (action == null) {
			return null;
		}
		return productMapper.altread(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> altreadList(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.altreadList(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel altreadbysupplier(NameId action) {
		if (action == null) {
			return null;
		}
		return productMapper.altreadbysupplier(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> altPartyread(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.altPartyread(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getActiveIdsByAltParty(NameId action, Boolean findAllStatesExceptFinal) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.getActiveIdsByAltParty(action, findAllStatesExceptFinal);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getActiveIdsByAltPartyList(List<String> partyIds) {
		if (CollectionUtils.isEmpty(partyIds)) {
			return Collections.emptyList();
		}
		return productMapper.getActiveIdsByAltPartyList(partyIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductNamesByIds(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return Collections.emptyList();
		}
		return productMapper.getProductNamesByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readByNameAndPartyID(String name, String partyId, boolean compressName) {
		return productMapper.readByNameAndPartyID(name, partyId, compressName);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelversion(ProductModel action) {
		if (action != null) {
			productMapper.cancelversion(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void activateProducts(List<Integer> ids) {
		if (!CollectionUtils.isEmpty(ids)) {
			productMapper.activateProducts(ids);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setStateCompleteByProductIds(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			productMapper.setStateCompleteByProductIds(ids);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readToBeSuspendedProducts(ProductModel action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.readToBeSuspendedProducts(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void initializeProductsNotInFeed(ProductModel action) {
		if (action != null) {
			productMapper.initializeProductsNotInFeed(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeproductid() {
		return productMapper.activeproductid();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListBySupplierForChannel(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListBySupplierForChannel(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListByPartyForChannel(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListByPartyForChannel(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsForChannel(String id) {
		return productMapper.getProductsForChannel(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveOrUndistributedProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveOrUndistributedProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveProductIdListByPartyForChannel(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductIdListByPartyForChannel(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> activeListOfPropertyManagersAndCommision() {
		return productMapper.activeListOfPropertyManagersAndCommision();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListBySupplierForApartmentsApart(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListBySupplierForApartmentsApart(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeAndInactiveProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeAndInactiveProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductAltIdListBySupplier(String partyId) {
		return productMapper.activeProductAltIdListBySupplier(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeproductidListbyChannelPartner(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeproductidListbyChannelPartner(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> productIdListWithNullLocationBySupplierId(String supplierId) {
		return productMapper.productIdListWithNullLocationBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> activeProductsList(List<Integer> excludedPMs) {
		if (CollectionUtils.isEmpty(excludedPMs)) {
			return Collections.emptyList();
		}
		return productMapper.activeProductsList(excludedPMs);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> activeProductsByVersion(Date version, List<Integer> excludedPMs) {
		if (CollectionUtils.isEmpty(excludedPMs)) {
			return Collections.emptyList();
		}
		return productMapper.activeProductsByVersion(version, excludedPMs);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> activeExcludedProductsList(List<Integer> excludedPMs) {
		if (CollectionUtils.isEmpty(excludedPMs)) {
			return Collections.emptyList();
		}
		return productMapper.activeExcludedProductsList(excludedPMs);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> activeExcludedProductsByVersion(Date version, List<Integer> excludedPMs) {
		if (CollectionUtils.isEmpty(excludedPMs)) {
			return Collections.emptyList();
		}
		return productMapper.activeExcludedProductsByVersion(version, excludedPMs);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertRoomIds(List<NameId> roomIds) {
		if (CollectionUtils.isEmpty(roomIds)) {
			return;
		}
		productMapper.insertRoomIds(roomIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> selectRoomsForProduct(String productId) {
		return productMapper.selectRoomsForProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteRoom(NameId roomId) {
		if (roomId != null) {
			productMapper.deleteRoom(roomId);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer countPropertyByChannel(Integer channelId) {
		return productMapper.countPropertyByChannel(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readProductIdListForChannelPartner(Integer channelId) {
		return productMapper.readProductIdListForChannelPartner(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readPropertyIdListForAgents() {
		return productMapper.readPropertyIdListForAgents();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readByExample(ProductModel product) {
		if (product == null) {
			return Collections.emptyList();
		}
		return productMapper.readByExample(product);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readBySupplierId(String supplierId) {
		return productMapper.readBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readProductListNotFinalOrSuspendedBySupplierId(String supplierId) {
		return productMapper.readProductListNotFinalOrSuspendedBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readBySupplierIdAndState(String supplierId, List<String> states) {
		if (CollectionUtils.isEmpty(states)) {
			return Collections.emptyList();
		}
		return productMapper.readBySupplierIdAndState(supplierId, states);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readProductsIdsByAltId(String altPartyId, List<String> altIds) {
		if (CollectionUtils.isEmpty(altIds)) {
			return Collections.emptyList();
		}
		return productMapper.readProductsIdsByAltId(altPartyId, altIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductIdsStillActive(Integer supplierId, List<Integer> listSuspendedProducts) {
		if (CollectionUtils.isEmpty(listSuspendedProducts)) {
			return Collections.emptyList();
		}
		return productMapper.getProductIdsStillActive(supplierId, listSuspendedProducts);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelProducts(String altPartyId, List<String> listProductIdsTobeSuspended, Date version) {
		if (CollectionUtils.isEmpty(listProductIdsTobeSuspended)) {
			return;
		}
		productMapper.cancelProducts(altPartyId, listProductIdsTobeSuspended, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateProductStandardPerson(ProductModel product, Integer standardPerson) {
		if (product != null) {
			productMapper.updateProductStandardPerson(product, standardPerson);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> productsIdsByParty(String partyId) {
		return productMapper.productsIdsByParty(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getSubProductIds(Integer parentProductId) {
		return productMapper.getSubProductIds(parentProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsByParentId(String id) {
		return productMapper.getProductsByParentId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getCreatedProductsByParentId(Long id) {
		return productMapper.getCreatedProductsByParentId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getSubUnitsByMasterId(String id) {
		return productMapper.getSubUnitsByMasterId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getParentsByLocationId(String id) {
		return productMapper.getParentsByLocationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void convertMultiUnitToSingle(String id) {
		productMapper.convertMultiUnitToSingle(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer numberOfActiveProducts(String altPartyId) {
		return productMapper.numberOfActiveProducts(altPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsFinal(Integer id) {
		productMapper.markAsFinal(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsSuspended(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return;
		}
		productMapper.markAsSuspended(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsIncompleteProducts(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return;
		}
		productMapper.markAsIncompleteProducts(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setStateById(Integer id, String state) {
		productMapper.setStateById(id, state);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setStateByIds(List<Integer> productIdList, String state) {
		if (CollectionUtils.isEmpty(productIdList)) {
			return;
		}
		productMapper.setStateByIds(productIdList, state);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getEmptyParent(Integer altPartyId) {
		return productMapper.getEmptyParent(altPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> findMultiUnitsByPropertyManagerAndType(Long propertyManagerId, String multiUnitType) {
		return productMapper.findMultiUnitsByPropertyManagerAndType(propertyManagerId, multiUnitType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getAllRoomsForRoot(Long rootId) {
		return productMapper.getAllRoomsForRoot(rootId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getAllAvailableRoomsForRoot(Long rootId) {
		return productMapper.getAllAvailableRoomsForRoot(rootId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateCurrencyForAllChildProducts(@NonNull Long parentId, @NonNull String currency) {
		productMapper.updateCurrencyForAllChildProducts(parentId, currency);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createModel(ProductModel productModely) {
		if (productModely != null) {
			productMapper.createModel(productModely);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateModel(ProductModel productModely) {
		if (productModely != null) {
			productMapper.updateModel(productModely);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getMultiUnitPropertyByProductId(Integer productId) {
		return productMapper.getMultiUnitPropertyByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readNonFinalSingleProductsBySupplier(Long supplierId) {
		return productMapper.readNonFinalSingleProductsBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readNonFinalProducts(Long organizationId) {
		return productMapper.readNonFinalProducts(organizationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateProductLocation(String productId, String locationId) {
		productMapper.updateProductLocation(productId, locationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateLocationForProductList(List<String> productIds, String locationId) {
		if (CollectionUtils.isEmpty(productIds)) {
			return;
		}
		productMapper.updateLocationForProductList(productIds, locationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> activeMultiUnitProductIdListByParentId(Integer productId, Integer supplierId) {
		return productMapper.activeMultiUnitProductIdListByParentId(productId, supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getProductFromLastTimeStamp(ProductModel productModel) {
		if (productModel == null) {
			return null;
		}
		return productMapper.getProductFromLastTimeStamp(productModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readDistinctCurrencyList() {
		return productMapper.readDistinctCurrencyList();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getAllProductsOfFinalAndSuspended(String partyId) {
		return productMapper.getAllProductsOfFinalAndSuspended(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getParentByMltProductId(String parentId) {
		return productMapper.getParentByMltProductId(parentId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> fetchActiveSingleUnitProductListByIds(List<String> products) {
		if (CollectionUtils.isEmpty(products)) {
			return Collections.emptyList();
		}
		return productMapper.fetchActiveSingleUnitProductListByIds(products);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getProductIDBySupplier(Integer supplierId) {
		return productMapper.getProductIDBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getActiveProductIDBySupplier(Integer supplierId) {
		return productMapper.getActiveProductIDBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getProductIdsBySupplierAndMultiUnits(Integer supplierId, List<String> multiUnits) {
		return productMapper.getProductIdsBySupplierAndMultiUnits(supplierId, multiUnits);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsBySupplier(Integer supplierId) {
		return productMapper.getProductsBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getActiveProductsBySupplier(Integer supplierId) {
		return productMapper.getActiveProductsBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsBySupplierTypesAssignedToManager(Integer supplierId, List<String> types) {
		if (CollectionUtils.isEmpty(types)) {
			return Collections.emptyList();
		}
		return productMapper.getProductsBySupplierTypesAssignedToManager(supplierId, types);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductDisplayName() {
		return productMapper.getProductDisplayName();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getOwnChildren(String productId) {
		return productMapper.getOwnChildren(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getMltChildren(String productId) {
		return productMapper.getMltChildren(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Long> getChildrenWithParents(String productId) {
		return productMapper.getChildrenWithParents(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductByCode(String altPartyId, String code) {
		return productMapper.getProductByCode(altPartyId, code);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getProductByNameAndAltPartyID(String altPartyId, String code) {
		return productMapper.getProductByNameAndAltPartyID(altPartyId, code);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelMltAndChildren(Long productId) {
		productMapper.cancelMltAndChildren(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getMltRepFromSub(String productId) {
		return productMapper.getMltRepFromSub(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getMltForRoot(Long rootId) {
		return productMapper.getMltForRoot(rootId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getPrmByMlt(Integer rootId) {
		return productMapper.getPrmByMlt(rootId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> selectKeyOwns() {
		return productMapper.selectKeyOwns();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel readByAddressComponent(Integer addressComponentsId) {
		return productMapper.readByAddressComponent(addressComponentsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllOldRelations(Date date) {
		return productMapper.readAllOldRelations(date);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getPersonByProductId(Integer productId) {
		return productMapper.getPersonByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readProductIdsBySupplierIdsAndMultiUnits(List<Integer> pmIds, List<String> multiUnits) {
		return productMapper.readProductIdsBySupplierIdsAndMultiUnits(pmIds, multiUnits);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getGuidelinesProductIds(Integer pmId) {
		return productMapper.getGuidelinesProductIds(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getAllRestrictedPropertiesIds() {
		return productMapper.getAllRestrictedPropertiesIds();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsBySupplierAndType(Integer supplierId, String type) {
		return productMapper.getProductsBySupplierAndType(supplierId, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readMarriottStudioProperties() {
		return productMapper.readMarriottStudioProperties();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getMarriottApprovedProductsByHmcId(Integer hmcId) {
		return productMapper.getMarriottApprovedProductsByHmcId(hmcId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel readByAltId(String altId) {
		return productMapper.readByAltId(altId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel readById(Integer productId) {
		return productMapper.readById(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel getProductAvoidNPE(Integer id) {
		return productMapper.getProductAvoidNPE(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveProductIdListByOwnerForChannelByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductIdListByOwnerForChannelByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllSupplier(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.readAllSupplier(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllActiveProperties(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.readAllActiveProperties(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllActiveOrSuspendedProperties(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.readAllActiveOrSuspendedProperties(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeSingleUnitProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeSingleUnitProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeMultiUnitSubProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeMultiUnitSubProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeMultiUnitKeyRootProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.allMultiUnitKeyRootProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> activeMultiUnitRepRootProductListBySupplier(String supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeMultiUnitRepRootProductListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> activeMultiUnitProductsByRootId(String hotelId) {
		return productMapper.activeMultiUnitProductsByRootId(hotelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListBySupplierForChannelWithoutOWN(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListBySupplierForChannelWithoutOWN(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeSGLProductIdListBySupplierForChannel(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.activeSGLProductIdListBySupplierForChannel(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allProductIdListBySupplierForChannel(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.allProductIdListBySupplierForChannel(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListByOwnerForChannel(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListByOwnerForChannel(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveProductIdListBySupplierForChannel(NameIdAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductIdListBySupplierForChannel(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allMultiUnitKeyProductIdListByRootId(NameIdAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.allMultiUnitKeyProductIdListByRootId(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveOrUndistributedProductIdListBySupplierWithoutOWN(NameIdAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveOrUndistributedProductIdListBySupplierWithoutOWN(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveSGLProductIdListBySupplierByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveSGLProductIdListBySupplierByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListBySupplierByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListBySupplierByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeSGLProductIdListBySupplierByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeSGLProductIdListBySupplierByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllMasterForASupplier(String supplierId) {
		return productMapper.fetchAllMasterForASupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductIdsByParentIds(List<String> productIdList) {
		if (CollectionUtils.isEmpty(productIdList)) {
			return Collections.emptyList();
		}
		return productMapper.getProductIdsByParentIds(productIdList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductsByParentIds(List<String> productIdList) {
		if (CollectionUtils.isEmpty(productIdList)) {
			return Collections.emptyList();
		}
		return productMapper.getProductsByParentIds(productIdList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllParentForASupplier(String supplierId) {
		return productMapper.fetchAllParentForASupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> findAllFinalizedMLTsForASupplierByVersion(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.findAllFinalizedMLTsForASupplierByVersion(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllFinalizedParentForASupplierByVersion(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.fetchAllFinalizedParentForASupplierByVersion(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> findAllReactivatedParentsForASupplierByVersion(ProductAction supplier) {
		if (supplier == null) {
			return Collections.emptyList();
		}
		return productMapper.findAllReactivatedParentsForASupplierByVersion(supplier);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> findAllFinalizedKeysForMLTByVersion(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.findAllFinalizedKeysForMLTByVersion(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllCreatedChildByParentIdByVersion(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.fetchAllCreatedChildByParentIdByVersion(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllCreatedMasterBySupplierIdByVersion(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.fetchAllCreatedMasterBySupplierIdByVersion(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getProductByName(String hotelName) {
		return productMapper.getProductByName(hotelName);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllProductsByPartyForChannelFromChannelParty(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.getAllProductsByPartyForChannelFromChannelParty(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllProductsBySupplierForChannel(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.getAllProductsBySupplierForChannel(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getOWNPropertiesBySupplierId(String supplierId) {
		return productMapper.getOWNPropertiesBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updatePhone(ProductModel product) {
		if (product != null) {
			productMapper.updatePhone(product);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> fetchActiveMultiUnitKeyProductListByIds(List<String> products) {
		if (CollectionUtils.isEmpty(products)) {
			return Collections.emptyList();
		}
		return productMapper.fetchActiveMultiUnitKeyProductListByIds(products);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> fetchActiveMultiUnitRepProductListByIds(List<String> products) {
		if (CollectionUtils.isEmpty(products)) {
			return Collections.emptyList();
		}
		return productMapper.fetchActiveMultiUnitRepProductListByIds(products);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getFinilizedMlts(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.getFinilizedMlts(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getFinilizedMltsByMLTList(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.getFinilizedMltsByMLTList(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllCreatedMasterForOwnListByVersion(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.fetchAllCreatedMasterForOwnListByVersion(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> fetchAllCreatedMasterByVersion(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.fetchAllCreatedMasterByVersion(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> findKeyProductsForMlt(String parentId) {
		return productMapper.findKeyProductsForMlt(parentId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> inactiveProductListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeMuliUnitProductIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeMuliUnitProductIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allSinglePropertiesIdListBySupplier(NameIdAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.allSinglePropertiesIdListBySupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> rootProducts(String supplierId) {
		return productMapper.rootProducts(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> allRootProducts(String supplierId, Date version) {
		return productMapper.allRootProducts(supplierId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> allRootProductsFromChannelProductMap(String supplierId, Integer channelId, Date version) {
		return productMapper.allRootProductsFromChannelProductMap(supplierId, channelId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allOWNProductsFromChannelProductMap(String supplierId, Integer channelId) {
		return productMapper.allOWNProductsFromChannelProductMap(supplierId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allMLTProductsFromChannelProductMap(String supplierId, Integer channelId) {
		return productMapper.allMLTProductsFromChannelProductMap(supplierId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> distributedRootProducts(String supplierId, Integer channelId) {
		return productMapper.distributedRootProducts(supplierId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readRootProducts(List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.readRootProducts(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllActivePropertiesByPropertyIds(List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.readAllActivePropertiesByPropertyIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllPropertiesByPropertyIds(List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.readAllPropertiesByPropertyIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllActivePropertiesByPmIds(List<String> pmIds) {
		if (CollectionUtils.isEmpty(pmIds)) {
			return Collections.emptyList();
		}
		return productMapper.readAllActivePropertiesByPmIds(pmIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductChannel> getMappedProducts(String abbreviation, Integer pmId, List<String> multiUnits) {
		if (CollectionUtils.isEmpty(multiUnits)) {
			return Collections.emptyList();
		}
		return productMapper.getMappedProducts(abbreviation, pmId, multiUnits);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductChannel getMbpProductWithMTCCheck(String productId, String abbreviation) {
		return productMapper.getMbpProductWithMTCCheck(productId, abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductModel readByAltIdAndSupplierId(String altId, String supplierId) {
		return productMapper.readByAltIdAndSupplierId(altId, supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getAllNewProductsByVersionForChannelPortal(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.getAllNewProductsByVersionForChannelPortal(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getBoookingSettingForProduct(Integer id) {
		return productMapper.getBoookingSettingForProduct(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getFlagsForProduct(Integer id) {
		return productMapper.getFlagsForProduct(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readMarriottProductsForGoogleChannel(Integer marriottChannelId) {
		return productMapper.readMarriottProductsForGoogleChannel(marriottChannelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer countSearchProducts(SearchListingModelRequest searchListingModelRequest) {
		if (searchListingModelRequest == null) {
			return null;
		}
		return productMapper.countSearchProducts(searchListingModelRequest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<SearchListingModelResponse> searchProducts(SearchListingModelRequest searchListingModelRequest) {
		if (searchListingModelRequest == null) {
			return Collections.emptyList();
		}
		return productMapper.searchProducts(searchListingModelRequest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updatePhysicalAddresses(ProductIdWithPhysicalAddressDto dto) {
		if (dto != null) {
			productMapper.updatePhysicalAddresses(dto);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> productlistbyids(List<String> productids) {
		if (CollectionUtils.isEmpty(productids)) {
			return Collections.emptyList();
		}
		return productMapper.productlistbyids(productids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> hotelsearch(OtaSearchParameter action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.hotelsearch(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String valuenameid(AttributeAction action) {
		if (action == null) {
			return StringUtils.EMPTY;
		}
		return productMapper.valuenameid(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidbylocationid(String[] locationids) {
		if (locationids.length == 0) {
			return Collections.emptyList();
		}
		return productMapper.nameidbylocationid(locationids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readByCoordinatesAndRadius(List<Integer> locationParentIds, BigDecimalExt latitude, BigDecimalExt longitude, Integer guests,
			BigDecimalExt radius, Integer limit, List<Integer> propertyManagerIds, List<Integer> productsForIgnoring) {
		return productMapper.readByCoordinatesAndRadius(locationParentIds, latitude, longitude, guests, radius, limit, propertyManagerIds, productsForIgnoring);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidbyarea(Area action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.nameidbyarea(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameiddynamic() {
		return productMapper.nameiddynamic();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> type() {
		return productMapper.type();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Property property(String productid) {
		return productMapper.property(productid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Property propertyForMultiUnit(String productid) {
		return productMapper.propertyForMultiUnit(productid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductInfo> productInfoListBySupplier(NameIdAction altpartyid) {
		if (altpartyid == null) {
			return Collections.emptyList();
		}
		return productMapper.productInfoListBySupplier(altpartyid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> productotas(String list) {
		return productMapper.productotas(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> otatypes() {
		return productMapper.otatypes();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductWidgetItem productwidget(String productid) {
		return productMapper.productwidget(productid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidwidget(Parameter action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.nameidwidget(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> jsonnameids(String[] productids) {
		if (productids.length == 0) {
			return Collections.emptyList();
		}
		return productMapper.jsonnameids(productids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageWidgetItem> imagewidget(Parameter action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.imagewidget(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllActiveSupplier(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.readAllActiveSupplier(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readAllSupplierWithInactiveNoLimit(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.readAllSupplierWithInactiveNoLimit(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> channelProductsNameIdByName(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.channelProductsNameIdByName(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> travelAgentProductsNameIdByName(NameIdAction nameIdAction) {
		if (nameIdAction == null) {
			return Collections.emptyList();
		}
		return productMapper.travelAgentProductsNameIdByName(nameIdAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveProductIdListBySupplierByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductIdListBySupplierByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activeProductIdListByPartyForChannelByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.activeProductIdListByPartyForChannelByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> inactiveProductIdListByPartyForChannelByVersion(ProductAction supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return productMapper.inactiveProductIdListByPartyForChannelByVersion(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Property> propertyByChannelPage(NameIdCountOffset action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.propertyByChannelPage(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer countPropertyForAgents(NameIdCountOffset action) {
		if (action == null) {
			return null;
		}
		return productMapper.countPropertyForAgents(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Property> readPageOfProductsByListOfSupplier(NameIdCountOffset action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.readPageOfProductsByListOfSupplier(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductIdentification> readProductsNotInList(String altPartyId, List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.readProductsNotInList(altPartyId, productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductIdentification> readNotFinalProductsNotInList(String altPartyId, List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.readNotFinalProductsNotInList(altPartyId, productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getSupplierIDForProductId(NameIdAction id) {
		if (id == null) {
			return StringUtils.EMPTY;
		}
		return productMapper.getSupplierIDForProductId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidbyname(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return productMapper.nameidbyname(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdVersion> getMaxVersions(List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.getMaxVersions(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductIdWithNameItem> getAllSubunitsByPmAndProductName(Integer pmId, String productName, Integer productId) {
		return productMapper.getAllSubunitsByPmAndProductName(pmId, productName, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductChannel> getDistributedProducts(String abbreviation, Integer pmId, List<String> multiUnits) {
		if (CollectionUtils.isEmpty(multiUnits)) {
			return Collections.emptyList();
		}
		return productMapper.getDistributedProducts(abbreviation, pmId, multiUnits);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> getActiveProductsBySupplierAndMultiUnits(Integer supplierId, List<String> multiUnits) {
		if (CollectionUtils.isEmpty(multiUnits)) {
			return Collections.emptyList();
		}
		return productMapper.getActiveProductsBySupplierAndMultiUnits(supplierId, multiUnits);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductChannelRelation readProductNameAndChannelByChannelProductId(String channelProductId) {
		return productMapper.readProductNameAndChannelByChannelProductId(channelProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> unDistributedMLTProductIdListBySupplier(ProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return productMapper.unDistributedMLTProductIdListBySupplier(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> activePRMAndSUBProductIdListByParentId(ProductAction parentId) {
		if (parentId == null) {
			return Collections.emptyList();
		}
		return productMapper.activePRMAndSUBProductIdListByParentId(parentId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> allInactivePRMAndSUBProductIdListByParentId(ProductAction parentId) {
		if (parentId == null) {
			return Collections.emptyList();
		}
		return productMapper.allInactivePRMAndSUBProductIdListByParentId(parentId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<BeststayzProductReport> getBeststayzProducts(BeststayzProductFilterDto filter) {
		if (filter == null) {
			return Collections.emptyList();
		}
		return productMapper.getBeststayzProducts(filter);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyInfo> getPropertiesByPmId(Integer pmId) {
		return productMapper.getPropertiesByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPortalProductReport> getMarriottProducts(ChannelPortalProductFilterDto filter) {
		if (filter == null) {
			return Collections.emptyList();
		}
		return productMapper.getMarriottProducts(filter);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> getChannelPortalProductsIdName(Integer hmcId, String hmcName, Integer channelId) {
		return productMapper.getChannelPortalProductsIdName(hmcId, hmcName, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MedalliaOrganizationalData> getMedalliaOrganizationalData() {
		return productMapper.getMedalliaOrganizationalData();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<GuidelineProduct> getGuidelinesProducts(GuidelineProductRequestDto request) {
		if (request == null) {
			return Collections.emptyList();
		}
		return productMapper.getGuidelinesProducts(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getTotalGuidelinesProducts(GuidelineProductRequestDto request) {
		if (request == null) {
			return null;
		}
		return productMapper.getTotalGuidelinesProducts(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MarriottApiValidationInfo> getMarriottApiValidationInfo(MarriottValidationRequestDto request) {
		if (request == null) {
			return Collections.emptyList();
		}
		return productMapper.getMarriottApiValidationInfo(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<GuidelineProduct> getGuidelinesProductsForChannelPortal(MarriottValidationRequestDto request) {
		if (request == null) {
			return Collections.emptyList();
		}
		return productMapper.getGuidelinesProductsForChannelPortal(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<BookingPalValidationInfo> getBookingPalValidationInfo(MarriottValidationRequestDto request) {
		if (request == null) {
			return Collections.emptyList();
		}
		return productMapper.getBookingPalValidationInfo(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> getLogPropertyValidationByProducts(List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productMapper.getLogPropertyValidationByProducts(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductModel> readExpediaActiveProductsBySupplierId(Integer supplierId) {
		return productMapper.readExpediaActiveProductsBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPortalIncomingListing> getIncomingListingsForChannelPortal(ChannelPortalIncomingListingFilterDto filter) {
		if (filter == null) {
			return Collections.emptyList();
		}
		return productMapper.getIncomingListingsForChannelPortal(filter);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getTotalIncomingListingsForChannelPortal(Integer channelId) {
		return productMapper.getTotalIncomingListingsForChannelPortal(channelId);
	}
}
