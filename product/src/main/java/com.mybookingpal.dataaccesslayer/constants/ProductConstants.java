package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;
import java.util.List;

public class ProductConstants {

	public static final Double DEFAULT_SECUIRTY_DEPOSIT = 0.0;

	public enum SpaceUnitEnum {
		SQ_M("m2"), SQ_FT("ft2");

		private String value;

		SpaceUnitEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static SpaceUnitEnum getByStr(String value) {
			for (SpaceUnitEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	public static enum MultiUnit {
		OWN,
		PRM,
		SGL,
		MLT,
		SUB;

		private MultiUnit() {
		}

		public static List<String> getMultiUnitRooms() {
			return Arrays.asList(PRM.name(), SGL.name(), SUB.name());
		}
	}

	public static enum ProductGroup {
		MULTI_REP,
		MULTI_KEY,
		KEY;

		private ProductGroup() {
		}
	}
}
