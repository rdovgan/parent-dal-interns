package com.mybookingpal.dataaccesslayer.constants;

public class ProductUpdateTypeConstants {

	public enum TypeEnum {
		Fees("Fees"),
		Tax("Tax"),
		Images("Image"),
		Prices("Price"),
		Address("Address"),
		Room("Room"),
		BedroomConfig("Bedroom config"),
		Policies("Policies"),
		PropertyName("Property name"),
		DisplayName("Display name"),
		Currency("Currency"),
		Rating("rating"),
		Rooms("Number of rooms"),
		Space("Property space"),
		Bathrooms("Number of bathrooms"),
		Toilet("Toilet"),
		Persons("Number of persons"),
		Children("Number of children"),
		Infant("Number of infant"),
		Baby("Number of baby"),
		CheckIn("Check in time"),
		CheckOut("Check out time"),
		ProductType("Product type"),
		MainDescription("Main description"),
		ShortDescription("Short description"),
		HouseRules("House rules"),
		FinePrint("Fine print"),
		TouristLicenseNumber("Tourist License Number"),
		LeadTime("Lead Time"),
		Bed("Bed"),
		BuiltDate("BuiltDate"),
		RenovatingDate("RenovatingDate"),
		RentingDate("RentingDate"),
		HostLocation("HostLocation");

		private String type;

		TypeEnum(String code) {
			this.type = code;
		}

		public String getType() {
			return type;
		}
	}
}
