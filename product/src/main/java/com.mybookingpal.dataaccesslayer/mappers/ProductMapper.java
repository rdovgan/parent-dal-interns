package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.Area;
import com.mybookingpal.dataaccesslayer.dto.AttributeAction;
import com.mybookingpal.dataaccesslayer.dto.BeststayzProductFilterDto;
import com.mybookingpal.dataaccesslayer.dto.BeststayzProductReport;
import com.mybookingpal.dataaccesslayer.dto.BookingPalValidationInfo;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalIncomingListing;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalIncomingListingFilterDto;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalProductFilterDto;
import com.mybookingpal.dataaccesslayer.dto.ChannelPortalProductReport;
import com.mybookingpal.dataaccesslayer.dto.GuidelineProduct;
import com.mybookingpal.dataaccesslayer.dto.GuidelineProductRequestDto;
import com.mybookingpal.dataaccesslayer.dto.ImageWidgetItem;
import com.mybookingpal.dataaccesslayer.dto.MarriottApiValidationInfo;
import com.mybookingpal.dataaccesslayer.dto.MarriottValidationRequestDto;
import com.mybookingpal.dataaccesslayer.dto.MedalliaOrganizationalData;
import com.mybookingpal.dataaccesslayer.dto.NameIdCountOffset;
import com.mybookingpal.dataaccesslayer.dto.OtaSearchParameter;
import com.mybookingpal.dataaccesslayer.dto.ProductAction;
import com.mybookingpal.dataaccesslayer.dto.ProductChannel;
import com.mybookingpal.dataaccesslayer.dto.ProductChannelRelation;
import com.mybookingpal.dataaccesslayer.dto.ProductIdWithNameItem;
import com.mybookingpal.dataaccesslayer.dto.ProductIdWithPhysicalAddressDto;
import com.mybookingpal.dataaccesslayer.dto.ProductIdentification;
import com.mybookingpal.dataaccesslayer.dto.ProductInfo;
import com.mybookingpal.dataaccesslayer.dto.ProductWidgetItem;
import com.mybookingpal.dataaccesslayer.dto.Property;
import com.mybookingpal.dataaccesslayer.dto.PropertyInfo;
import com.mybookingpal.dataaccesslayer.dto.SearchListingModelRequest;
import com.mybookingpal.dataaccesslayer.dto.SearchListingModelResponse;
import com.mybookingpal.dataaccesslayer.entity.ProductModel;
import com.mybookingpal.utils.entity.Parameter;
import com.mybookingpal.utils.entity.IdVersion;
import com.mybookingpal.utils.entity.BigDecimalExt;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ProductMapper {

	void create(ProductModel action);

	void createList(@Param("list") List<ProductModel> products);

	ProductModel read(Integer id);

	void update(ProductModel action);

	void updateLinkedIds(@Param("list") List<ProductModel> products);

	void updateNotCheckValue(ProductModel action);

	ProductModel exists(ProductModel action);

	Double commission(String supplierid);

	Double discount(String supplierid);

	List<ProductModel> list(ProductModel action);

	List<ProductModel> getPrimaryAndSingleProduct(ProductModel action);

	List<ProductModel> getPrimarySingleAndMultiRepProduct(ProductModel action);

	List<ProductModel> readProductCollectionByIdCollection(List<Integer> productIdCollection);

	List<ProductModel> readProductsByLocationIds(List<Integer> locationIds);

	List<ProductModel> altlist(String altpartyid);

	List<ProductModel> preparedAltList(String altpartyid);

	List<ProductModel> componentlist(String productid);

	List<ProductModel> readByCoordinatesAndRadius(@Param("locationParentIds") List<Integer> locationParentIds, @Param("latitude") Long latitude,
			@Param("longitude") Long longitude, @Param("guests") Integer guests, @Param("radius") Long radius, @Param("limit") Integer limit,
			@Param("propertyManagerIds") List<Integer> propertyManagerIds, @Param("productsForIgnoring") List<Integer> productsForIgnoring);

	// List of product in location with collisions.
	List<String> idsByParentLocationAndPrice(@Param("locationid") String locationid, @Param("fromdate") String fromdate, @Param("todate") String todate,
			@Param("channelid") String agentid, @Param("inquireonly") Boolean inquireonly, @Param("pmid") String pmId);

	// List of product in location with collisions.
	List<String> idsByLocationIdAndAgentId(@Param("locationId") String locationId, @Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("agentId") String agentId, @Param("inquireOnly") Boolean inquireOnly);

	// List of product in location with collisions.
	List<String> idswithreservationcollision(@Param("locationid") String locationid, @Param("fromdate") String fromdate, @Param("todate") String todate);


	String productCountBySupplier(String partyid);

	String selectedProductCountBySupplier(String partyid);

	String productCountByAltSupplier(@Param("altsupplierid") String altpmid, @Param("altpartyid") String pmsid);

	void updateSupplierIdByAltSupplier(ProductModel action);

	List<String> flipkeyproducts(String supplierid); //Flipkey IDs

	ProductModel altread(NameId action);

	List<ProductModel> altreadList(NameId action);

	ProductModel altreadbysupplier(NameId action);

	List<ProductModel> altPartyread(NameId action);

	List<String> getActiveIdsByAltParty(@Param("action") NameId action, @Param("findAllStatesExceptFinal") Boolean findAllStatesExceptFinal);

	List<String> getActiveIdsByAltPartyList(List<String> partyIds);

	List<String> getProductNamesByIds(@Param("list") List<Integer> ids);

	List<ProductModel> readByNameAndPartyID(@Param("name") String name, @Param("partyId") String partyId, @Param("compressName") boolean compressName);

	void cancelversion(ProductModel action);

	void activateProducts(List<Integer> id);

	void setStateCompleteByProductIds(List<Integer> id);

	List<ProductModel> readToBeSuspendedProducts(ProductModel action);

	void initializeProductsNotInFeed(ProductModel action);

	List<String> activeproductid();

	List<String> activeProductIdListBySupplier(NameIdAction supplierId);

	List<String> activeProductIdListBySupplierForChannel(NameIdAction supplierId);

	List<String> activeProductIdListByPartyForChannel(NameIdAction supplierId);

	List<ProductModel> getProductsForChannel(String id);

	List<String> inactiveOrUndistributedProductIdListBySupplier(NameIdAction supplierId);

	List<String> inactiveProductIdListByPartyForChannel(NameIdAction supplierId);

	List<ProductModel> activeListOfPropertyManagersAndCommision();

	List<String> activeProductIdListBySupplierForApartmentsApart(NameIdAction supplierId);

	List<String> activeAndInactiveProductIdListBySupplier(NameIdAction supplierId);

	List<String> activeProductAltIdListBySupplier(String partyId);

	List<String> activeproductidListbyChannelPartner(NameIdAction channelPartnerId);

	//Location check
	List<ProductModel> productIdListWithNullLocationBySupplierId(String supplierId);

	List<Integer> activeProductsList(@Param("excludedPMs") List<Integer> excludedPMs);

	List<Integer> activeProductsByVersion(@Param("version") Date version, @Param("excludedPMs") List<Integer> excludedPMs);

	List<Integer> activeExcludedProductsList(@Param("excludedPMs") List<Integer> excludedPMs);

	List<Integer> activeExcludedProductsByVersion(@Param("version") Date version, @Param("excludedPMs") List<Integer> excludedPMs);

	// roomids
	void insertRoomIds(List<NameId> roomIds);

	List<String> selectRoomsForProduct(String productid);

	void deleteRoom(NameId roomId);

	// XMLs to FTP mappers
	Integer countPropertyByChannel(Integer channelId);

	List<Integer> readProductIdListForChannelPartner(Integer channelId);

	List<Integer> readPropertyIdListForAgents();

	List<ProductModel> readByExample(ProductModel product);

	List<ProductModel> readBySupplierId(String supplierId);

	List<ProductModel> readProductListNotFinalOrSuspendedBySupplierId(String supplierId);

	List<ProductModel> readBySupplierIdAndState(@Param("supplierId") String supplierId, @Param("states") List<String> states);

	ProductModel readByAltId(String altId);

	List<String> readProductsIdsByAltId(@Param("altPartyId") String altPartyId, @Param("altIds") List<String> altIds);

	List<String> getProductIdsStillActive(@Param("supplierId") Integer supplierId, @Param("productIds") List<Integer> listSuspendedProducts);

	void cancelProducts(@Param("altPartyId") String altPartyId, @Param("productIds") List<String> listProductIdsTobeSuspended, @Param("version") Date version);

	/**
	 * Updates standard person for certain product.
	 *
	 * @param product        current product for which we update standard person value.
	 * @param standardPerson value of standard person per product.
	 */
	void updateProductStandardPerson(@Param("product") ProductModel product, @Param("standardPerson") Integer standardPerson);

	List<Integer> productsIdsByParty(String partyId);

	List<Integer> getSubProductIds(Integer parentProductId);

	List<ProductModel> getProductsByParentId(String id);

	List<ProductModel> getCreatedProductsByParentId(Long id);

	List<ProductModel> getSubUnitsByMasterId(String id);

	List<String> getParentsByLocationId(String id);

	void convertMultiUnitToSingle(String id);

	int numberOfActiveProducts(String altPartyId);

	void markAsFinal(Integer id);

	void markAsSuspended(@Param("ids") List<Integer> ids);

	void markAsIncompleteProducts(@Param("ids") List<Integer> ids);

	void setStateById(@Param("id") Integer id, @Param("state") String state);

	void setStateByIds(@Param("list") List<Integer> productIdList, @Param("state") String state);

	List<Integer> getEmptyParent(@Param("altpartyId") Integer altpartyId);

	List<ProductModel> findMultiUnitsByPropertyManagerAndType(@Param("pmId") Long propertyManagerId, @Param("type") String multiUnitType);

	List<ProductModel> getAllRoomsForRoot(Long rootId);

	List<ProductModel> getAllAvailableRoomsForRoot(Long rootId);

	void updateCurrencyForAllChildProducts(@Param("parentId") Long parentId, @Param("currency") String currency);

	void createModel(ProductModel productModel);

	void updateModel(ProductModel productModel);

	String getMultiUnitPropertyByProductId(Integer productId);

	List<ProductModel> readNonFinalSingleProductsBySupplier(@Param("supplierId") Long supplierId);

	List<ProductModel> readNonFinalProducts(@Param("organizationId") Long organizationId);

	void updateProductLocation(@Param("productId") String productId, @Param("locationId") String locationId);

	void updateLocationForProductList(@Param("list") List<String> productIds, @Param("locationId") String locationId);

	List<Integer> activeMultiUnitProductIdListByParentId(@Param("productId") Integer productId, @Param("supplierId") Integer supplierId);

	ProductModel getProductFromLastTimeStamp(ProductModel product);

	List<String> readDistinctCurrencyList();

	List<ProductModel> getAllProductsOfFinalAndSuspended(String partyId);

	ProductModel getParentByMltProductId(String parentId);

	List<String> fetchActiveSingleUnitProductListByIds(@Param("list") List<String> products);

	List<Integer> getProductIDBySupplier(@Param("supplierId") Integer supplierId);

	List<Integer> getActiveProductIDBySupplier(@Param("supplierId") Integer supplierId);

	List<Integer> getProductIdsBySupplierAndMultiUnits(@Param("supplierId") Integer supplierId, @Param("multiUnits") List<String> multiUnits);

	List<ProductModel> getProductsBySupplier(@Param("supplierId") Integer supplierId);

	List<ProductModel> getActiveProductsBySupplier(@Param("supplierId") Integer supplierId);

	List<ProductModel> getProductsBySupplierTypesAssignedToManager(@Param("supplierId") Integer supplierId, @Param("list") List<String> types);

	List<String> getProductDisplayName();

	List<ProductModel> getOwnChildren(@Param("productId") String productId);

	List<ProductModel> getMltChildren(@Param("productId") String productId);

	List<Long> getChildrenWithParents(@Param("productId") String productId);

	List<ProductModel> getProductByCode(@Param("altPartyId") String altPartyId, @Param("code") String code);

	ProductModel getProductByNameAndAltPartyID(@Param("altPartyId") String altPartyId, @Param("name") String code);

	void cancelMltAndChildren(@Param("productId") Long productId);

	ProductModel getMltRepFromSub(String productId);

	List<ProductModel> getMltForRoot (Long rootId);

	ProductModel getPrmByMlt (Integer rootId);

	List<Integer> selectKeyOwns();

	ProductModel readByAddressComponent(@Param("addressComponentsID") Integer addressComponentsID);

	List<ProductModel> readAllOldRelations(@Param("date") Date date);

	Integer getPersonByProductId(@Param("productId") Integer productId);

	List<Integer> readProductIdsBySupplierIdsAndMultiUnits(@Param("ids") List<Integer> pmIds, @Param("multiUnits") List<String> multiUnits);

	List<Integer> getGuidelinesProductIds(@Param("pmId") Integer pmId);

	List<Integer> getAllRestrictedPropertiesIds();

	List<ProductModel> getProductsBySupplierAndType(@Param("supplierId") Integer supplierId, @Param("type") String type);

	List<Integer> readMarriottStudioProperties();

	List<Integer> getMarriottApprovedProductsByHmcId(@Param("hmcId") Integer hmcId);

	ProductModel readById(Integer productId);

	ProductModel getProductAvoidNPE(Integer id);

	List<String> inactiveProductIdListByOwnerForChannelByVersion(ProductAction supplierId);

	List<ProductModel> readAllSupplier(NameIdAction nameIdAction);

	List<ProductModel> readAllActiveProperties(NameIdAction nameIdAction);

	List<ProductModel> readAllActiveOrSuspendedProperties(NameIdAction nameIdAction);

	List<String> activeSingleUnitProductIdListBySupplier(NameIdAction supplierId);

	List<String> activeMultiUnitSubProductIdListBySupplier(NameIdAction supplierId);

	List<String> activeMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	List<String> allMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	List<String> activeMultiUnitRepRootProductIdListBySupplier(NameIdAction supplierId);

	List<ProductModel> activeMultiUnitRepRootProductListBySupplier(String supplierId);

	List<ProductModel> activeMultiUnitProductsByRootId(String hotelId);

	List<String> activeProductIdListBySupplierForChannelWithoutOWN(ProductAction supplier);

	List<String> activeSGLProductIdListBySupplierForChannel(ProductAction supplier);

	List<String> allProductIdListBySupplierForChannel(ProductAction supplier);

	List<String> activeProductIdListByOwnerForChannel(ProductAction supplierId);

	List<String> inactiveProductIdListBySupplierForChannel(NameIdAction supplierId);

	List<String> allMultiUnitKeyProductIdListByRootId(NameIdAction supplierId);

	List<String> inactiveOrUndistributedProductIdListBySupplierWithoutOWN(NameIdAction supplierId);

	List<String> inactiveSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	List<String> activeProductIdListBySupplierByVersion(ProductAction supplierId);

	List<String> activeSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	List<ProductModel> fetchAllMasterForASupplier(String supplierId);

	List<String> getProductIdsByParentIds(@Param("listId") List<String> productIdList);

	List<ProductModel> getProductsByParentIds(@Param("listId") List<String> productIdList);

	List<ProductModel> fetchAllParentForASupplier(String supplierId);

	List<ProductModel> findAllFinalizedMLTsForASupplierByVersion(ProductAction supplier);

	List<ProductModel> fetchAllFinalizedParentForASupplierByVersion(ProductAction supplier);

	List<ProductModel> findAllReactivatedParentsForASupplierByVersion(ProductAction supplier);

	List<ProductModel> findAllFinalizedKeysForMLTByVersion(ProductAction productAction);

	List<ProductModel> fetchAllCreatedChildByParentIdByVersion(ProductAction productAction);

	List<ProductModel> fetchAllCreatedMasterBySupplierIdByVersion(ProductAction productAction);

	List<ProductModel> getProductByName(@Param("hotelName") String hotelName);

	List<String> getAllProductsByPartyForChannelFromChannelParty(NameIdAction supplierId);

	List<String> getAllProductsBySupplierForChannel(NameIdAction supplierId);

	List<String> getOWNPropertiesBySupplierId(String supplierId);

	void updatePhone(ProductModel product);

	List<String> fetchActiveMultiUnitKeyProductListByIds(@Param("list") List<String> products);

	List<String> fetchActiveMultiUnitRepProductListByIds(@Param("list") List<String> products);

	//TODO: rootIds?
	List<ProductModel> getFinilizedMlts(ProductAction productAction);

	//TODO: rootIds?
	List<ProductModel> getFinilizedMltsByMLTList(ProductAction productAction);

	List<ProductModel> fetchAllCreatedMasterForOwnListByVersion(ProductAction productAction);

	//TODO: rootIds?
	List<ProductModel> fetchAllCreatedMasterByVersion(ProductAction productAction);

	List<String> findKeyProductsForMlt(String parentId);

	List<ProductModel> inactiveProductListBySupplier(NameIdAction supplierId);

	List<String> activeMuliUnitProductIdListBySupplier(NameIdAction supplierId);

	List<String> allSinglePropertiesIdListBySupplier(NameIdAction supplierId);

	List<ProductModel> rootProducts(String supplierId);

	List<ProductModel> allRootProducts(@Param("supplierId") String supplierId, @Param("version") Date version);

	List<ProductModel> allRootProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId, @Param("version") Date version);

	List<String> allOWNProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);

	List<String> allMLTProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);

	List<ProductModel> distributedRootProducts(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);

	List<ProductModel> readRootProducts(@Param("list") List<String> productIds);

	List<ProductModel> readAllActivePropertiesByPropertyIds(List<String> productIds);

	List<ProductModel> readAllPropertiesByPropertyIds(List<String> productIds);

	List<ProductModel> readAllActivePropertiesByPmIds(List<String> pmIds);

	List<ProductChannel> getMappedProducts(@Param("abbreviation") String abbreviation, @Param("pmId") Integer pmId,
			@Param("multiUnitCodes") List<String> multiUnits);

	ProductChannel getMbpProductWithMTCCheck(@Param("productId") String productId, @Param("abbreviation") String abbreviation);

	ProductModel readByAltIdAndSupplierId(@Param("altId") String altId, @Param("supplierId") String supplierId);

	List<Integer> getAllNewProductsByVersionForChannelPortal(ProductAction productAction);

	Integer getBoookingSettingForProduct(Integer id);

	List<String> getFlagsForProduct(Integer id);

	List<ProductModel> readMarriottProductsForGoogleChannel(@Param("marriottChannelId") Integer marriottChannelId);

	Integer countSearchProducts(SearchListingModelRequest searchListingModelRequest);

	List<SearchListingModelResponse> searchProducts(SearchListingModelRequest searchListingModelRequest);

	void updatePhysicalAddresses(@Param("dto") ProductIdWithPhysicalAddressDto dto);

	@Deprecated
	/**
	 * use readProductCollectionByIdCollection instead of this
	 */
	List<ProductModel> productlistbyids(List<String> productids);


	List<ProductModel> hotelsearch(OtaSearchParameter action); //HotelSearchRS

	String valuenameid(AttributeAction action);//Ota

	List<NameId> nameidbylocationid(String[] locationids); //NameIdByLocation

	List<ProductModel> readByCoordinatesAndRadius(@Param("locationParentIds") List<Integer> locationParentIds, @Param("latitude") BigDecimalExt latitude,
			@Param("longitude") BigDecimalExt longitude, @Param("guests") Integer guests, @Param("radius") BigDecimalExt radius, @Param("limit") Integer limit,
			@Param("propertyManagerIds") List<Integer> propertyManagerIds, @Param("productsForIgnoring") List<Integer> productsForIgnoring);

	List<NameId> nameidbyarea(Area action);

	List<NameId> nameiddynamic(); //dynamic price products

	List<NameId> type(); //product type

	/**
	 * Get property by it ID without any additional checks
	 */
	Property property(String productid);

	Property propertyForMultiUnit(String productid);

	List<ProductInfo> productInfoListBySupplier(NameIdAction altpartyid);

	List<NameId> productotas(String list); //ProductOtas

	List<NameId> otatypes(); //OtaTypes

	//JSON SQL queries
	ProductWidgetItem productwidget(String productid);

	List<NameId> nameidwidget(Parameter action);

	List<NameId> jsonnameids(String[] productids);

	List<ImageWidgetItem> imagewidget(Parameter action);

	List<ProductModel> readAllActiveSupplier(NameIdAction nameIdAction);

	List<ProductModel> readAllSupplierWithInactiveNoLimit(NameIdAction nameIdAction);

	List<NameId> channelProductsNameIdByName(NameIdAction nameIdAction);

	List<NameId> travelAgentProductsNameIdByName(NameIdAction nameIdAction);

	List<String> inactiveProductIdListBySupplierByVersion(ProductAction supplierId);

	List<String> activeProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	List<String> inactiveProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	List<Property> propertyByChannelPage(NameIdCountOffset action);

	Integer countPropertyForAgents(NameIdCountOffset action);

	List<Property> readPageOfProductsByListOfSupplier(NameIdCountOffset action);

	List<ProductIdentification> readProductsNotInList(@Param("altPartyId") String altPartyId, @Param("productIds") List<String> productIds);

	List<ProductIdentification> readNotFinalProductsNotInList(@Param("altPartyId") String altPartyId, @Param("productIds") List<String> productIds);

	String getSupplierIDForProductId(NameIdAction id);

	List<NameId> nameidbyname(NameIdAction action);

	List<IdVersion> getMaxVersions(List<Integer> productIds);

	List<ProductIdWithNameItem> getAllSubunitsByPmAndProductName(@Param("pmId") Integer pmId, @Param("productName") String productName,
			@Param("productId") Integer productId);

	List<ProductChannel> getDistributedProducts(@Param("abbreviation") String abbreviation, @Param("pmId") Integer pmId,
			@Param("multiUnitCodes") List<String> multiUnits);

	List<ProductModel> getActiveProductsBySupplierAndMultiUnits(@Param("supplierId") Integer supplierId, @Param("multiUnits") List<String> multiUnits);

	ProductChannelRelation readProductNameAndChannelByChannelProductId(@Param("channelProductId") String channelProductId);

	List<String> unDistributedMLTProductIdListBySupplier(ProductAction productAction);

	List<String> activePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<String> allInactivePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<BeststayzProductReport> getBeststayzProducts(@Param("filter") BeststayzProductFilterDto filter);

	List<PropertyInfo> getPropertiesByPmId(@Param("pmId") Integer pmId);

	List<ChannelPortalProductReport> getMarriottProducts(@Param("filter") ChannelPortalProductFilterDto filter);

	List<NameId> getChannelPortalProductsIdName(@Param("hmcId") Integer hmcId, @Param("hmcName") String hmcName, @Param("channelId") Integer channelId);

	List<MedalliaOrganizationalData> getMedalliaOrganizationalData();

	List<GuidelineProduct> getGuidelinesProducts(@Param("request") GuidelineProductRequestDto request);

	Integer getTotalGuidelinesProducts(@Param("request") GuidelineProductRequestDto request);

	List<MarriottApiValidationInfo> getMarriottApiValidationInfo(@Param("request") MarriottValidationRequestDto request);

	List<GuidelineProduct> getGuidelinesProductsForChannelPortal(@Param("request") MarriottValidationRequestDto request);

	List<BookingPalValidationInfo> getBookingPalValidationInfo(@Param("request") MarriottValidationRequestDto request);

	List<NameId> getLogPropertyValidationByProducts(@Param("productIds") List<Integer> productIds);

	List<ProductModel> readExpediaActiveProductsBySupplierId(@Param("supplierId") Integer supplierId);

	List<ChannelPortalIncomingListing> getIncomingListingsForChannelPortal(@Param("filter") ChannelPortalIncomingListingFilterDto filter);

	Integer getTotalIncomingListingsForChannelPortal(@Param("channelId") Integer channelId);
}