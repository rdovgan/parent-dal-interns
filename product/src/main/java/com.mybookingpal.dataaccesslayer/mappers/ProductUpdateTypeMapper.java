package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ProductUpdateTypeModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductUpdateTypeMapper {
	ProductUpdateTypeModel getByProductId(Integer id);

	List<ProductUpdateTypeModel> getListByProductIds(@Param("list") List<Integer> productIds);

	List<Integer> getAllProductIdsByParty(String partyId);

	void insert(@Param("list") List<ProductUpdateTypeModel> productUpdateTypeModelList);

	void update(ProductUpdateTypeModel productUpdateTypeModel);

	void create(ProductUpdateTypeModel productUpdateTypeModel);

	void insertSelective(ProductUpdateTypeModel productUpdateTypeModel);

	void updateProductUpdateType(ProductUpdateTypeModel productUpdateTypeModel);

	void updateProductUpdateTypeForProducts(@Param("model") ProductUpdateTypeModel productUpdateTypeModel, @Param("list") List<Integer> productIds);

	void createProductUpdateTypeForProducts(@Param("model") ProductUpdateTypeModel productUpdateTypeModel, @Param("list") List<Integer> productIds);

	Boolean showFeeTaxDisclaimer(Integer productId);
}
