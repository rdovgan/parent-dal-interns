package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.constants.ProductConstants;
import com.mybookingpal.dataaccesslayer.entity.ProductModel;

public class ProductUtils {
	public static ProductModel makeClone(ProductModel productModel) throws CloneNotSupportedException {
		return (ProductModel) productModel.clone();
	}

	public static boolean isMultiUnitParent(ProductModel productModel) {
		return ProductConstants.MultiUnit.MLT.name().equals(productModel.getMultiUnit())
				&& (ProductConstants.ProductGroup.MULTI_REP.name().equals(productModel.getProductGroup())
				|| ProductConstants.ProductGroup.MULTI_KEY.name().equals(productModel.getProductGroup()));
	}

	public static boolean isProductMultiUnit(ProductModel productModel) {
		return isMultiUnitKeyLevel(productModel) || isMultiUnitRepLevel(productModel);
	}

	public static boolean isMultiUnitKeyLevel(ProductModel productModel) {
		return ProductConstants.ProductGroup.MULTI_KEY.name()
				.equals(productModel.getProductGroup());
	}

	public static boolean isMultiUnitRepLevel(ProductModel productModel) {
		return ProductConstants.ProductGroup.MULTI_REP.name()
				.equals(productModel.getProductGroup());
	}
}
