package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductModel;
import com.mybookingpal.utils.entity.NameId;

import java.util.List;
import java.util.TimeZone;

public class ProductWithAdditionalFields extends ProductModel {
	protected Long organizationId;
	protected Long actorId;
	protected String pmAbbrev;
	protected String pmsAbbrev;
	protected String orderBy;
	protected Long startRow;
	protected Long numRows;
	protected String location;
	protected String owner;
	private Integer leadTime;
	private TimeZone timeZone;
	private List<NameId> files;
	private String productImageRootLocation;

	public ProductWithAdditionalFields() {
	}

	public ProductWithAdditionalFields(ProductModel productModel, Long organizationId, Long actorId, String pmAbbrev, String pmsAbbrev, String orderBy,
			Long startRow, Long numRows, String location, String owner, Integer leadTime, TimeZone timeZone, List<NameId> files,
			String productImageRootLocation) {
		super(productModel);
		this.organizationId = organizationId;
		this.actorId = actorId;
		this.pmAbbrev = pmAbbrev;
		this.pmsAbbrev = pmsAbbrev;
		this.orderBy = orderBy;
		this.startRow = startRow;
		this.numRows = numRows;
		this.location = location;
		this.owner = owner;
		this.leadTime = leadTime;
		this.timeZone = timeZone;
		this.files = files;
		this.productImageRootLocation = productImageRootLocation;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getActorId() {
		return actorId;
	}

	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}

	public String getPmAbbrev() {
		return pmAbbrev;
	}

	public void setPmAbbrev(String pmAbbrev) {
		this.pmAbbrev = pmAbbrev;
	}

	public String getPmsAbbrev() {
		return pmsAbbrev;
	}

	public void setPmsAbbrev(String pmsAbbrev) {
		this.pmsAbbrev = pmsAbbrev;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Long getStartRow() {
		return startRow;
	}

	public void setStartRow(Long startRow) {
		this.startRow = startRow;
	}

	public Long getNumRows() {
		return numRows;
	}

	public void setNumRows(Long numRows) {
		this.numRows = numRows;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(Integer leadTime) {
		this.leadTime = leadTime;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public List<NameId> getFiles() {
		return files;
	}

	public void setFiles(List<NameId> files) {
		this.files = files;
	}

	public String getProductImageRootLocation() {
		return productImageRootLocation;
	}

	public void setProductImageRootLocation(String productImageRootLocation) {
		this.productImageRootLocation = productImageRootLocation;
	}
}
