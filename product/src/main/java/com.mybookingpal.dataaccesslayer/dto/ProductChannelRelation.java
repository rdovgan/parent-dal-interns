package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ProductChannelRelation {
	private String channelPartyId;
	private String productId;

	public ProductChannelRelation() {
	}

	public ProductChannelRelation(String channelPartyId, String productId) {
		this.channelPartyId = channelPartyId;
		this.productId = productId;
	}

	public String getChannelPartyId() {
		return channelPartyId;
	}

	public ProductChannelRelation setChannelPartyId(String channelPartyId) {
		this.channelPartyId = channelPartyId;
		return this;
	}

	public String getProductId() {
		return productId;
	}

	public ProductChannelRelation setProductId(String productId) {
		this.productId = productId;
		return this;
	}
}
