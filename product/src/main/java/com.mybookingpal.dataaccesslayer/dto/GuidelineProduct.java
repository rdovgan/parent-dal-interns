package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class GuidelineProduct {
	private Integer id;
	private Integer supplierId;
	private String name;
	private String displayName;
	private String channelDisplayName;
	private Boolean useDisplayName;
	private String state;
	private String pmId;
	private Integer photos;
	private Integer amenities;
	private Boolean myOptimizeCriteria;
	private Boolean validationJobResult;
	private Boolean validationAddressResult;
	private Date lastJobRun;
	private String message;
	private String hmcProductId;
	private Integer availabilityDays;
	private Boolean productTypeValidation;

	public GuidelineProduct() {
	}

	public GuidelineProduct(Integer id, Integer supplierId, String name, String displayName, String channelDisplayName, Boolean useDisplayName, String state,
			String pmId, Integer photos, Integer amenities, Boolean myOptimizeCriteria, Boolean validationJobResult, Boolean validationAddressResult,
			Date lastJobRun, String message, String hmcProductId, Integer availabilityDays, Boolean productTypeValidation) {
		this.id = id;
		this.supplierId = supplierId;
		this.name = name;
		this.displayName = displayName;
		this.channelDisplayName = channelDisplayName;
		this.useDisplayName = useDisplayName;
		this.state = state;
		this.pmId = pmId;
		this.photos = photos;
		this.amenities = amenities;
		this.myOptimizeCriteria = myOptimizeCriteria;
		this.validationJobResult = validationJobResult;
		this.validationAddressResult = validationAddressResult;
		this.lastJobRun = lastJobRun;
		this.message = message;
		this.hmcProductId = hmcProductId;
		this.availabilityDays = availabilityDays;
		this.productTypeValidation = productTypeValidation;
	}

	public Integer getId() {
		return id;
	}

	public GuidelineProduct setId(Integer id) {
		this.id = id;
		return this;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public GuidelineProduct setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
		return this;
	}

	public String getName() {
		return name;
	}

	public GuidelineProduct setName(String name) {
		this.name = name;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public GuidelineProduct setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public String getChannelDisplayName() {
		return channelDisplayName;
	}

	public GuidelineProduct setChannelDisplayName(String channelDisplayName) {
		this.channelDisplayName = channelDisplayName;
		return this;
	}

	public Boolean getUseDisplayName() {
		return useDisplayName;
	}

	public GuidelineProduct setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
		return this;
	}

	public String getState() {
		return state;
	}

	public GuidelineProduct setState(String state) {
		this.state = state;
		return this;
	}

	public String getPmId() {
		return pmId;
	}

	public GuidelineProduct setPmId(String pmId) {
		this.pmId = pmId;
		return this;
	}

	public Integer getPhotos() {
		return photos;
	}

	public GuidelineProduct setPhotos(Integer photos) {
		this.photos = photos;
		return this;
	}

	public Integer getAmenities() {
		return amenities;
	}

	public GuidelineProduct setAmenities(Integer amenities) {
		this.amenities = amenities;
		return this;
	}

	public Boolean getMyOptimizeCriteria() {
		return myOptimizeCriteria;
	}

	public GuidelineProduct setMyOptimizeCriteria(Boolean myOptimizeCriteria) {
		this.myOptimizeCriteria = myOptimizeCriteria;
		return this;
	}

	public Boolean getValidationJobResult() {
		return validationJobResult;
	}

	public GuidelineProduct setValidationJobResult(Boolean validationJobResult) {
		this.validationJobResult = validationJobResult;
		return this;
	}

	public Boolean getValidationAddressResult() {
		return validationAddressResult;
	}

	public GuidelineProduct setValidationAddressResult(Boolean validationAddressResult) {
		this.validationAddressResult = validationAddressResult;
		return this;
	}

	public Date getLastJobRun() {
		return lastJobRun;
	}

	public GuidelineProduct setLastJobRun(Date lastJobRun) {
		this.lastJobRun = lastJobRun;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public GuidelineProduct setMessage(String message) {
		this.message = message;
		return this;
	}

	public String getHmcProductId() {
		return hmcProductId;
	}

	public GuidelineProduct setHmcProductId(String hmcProductId) {
		this.hmcProductId = hmcProductId;
		return this;
	}

	public Integer getAvailabilityDays() {
		return availabilityDays;
	}

	public GuidelineProduct setAvailabilityDays(Integer availabilityDays) {
		this.availabilityDays = availabilityDays;
		return this;
	}

	public Boolean getProductTypeValidation() {
		return productTypeValidation;
	}

	public GuidelineProduct setProductTypeValidation(Boolean productTypeValidation) {
		this.productTypeValidation = productTypeValidation;
		return this;
	}
}
