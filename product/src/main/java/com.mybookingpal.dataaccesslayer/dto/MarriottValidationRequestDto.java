package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class MarriottValidationRequestDto {
	private Integer hmcId;
	private String hmcName;
	private Integer productId;
	private Integer channelProductId;

	public MarriottValidationRequestDto() {
	}

	public MarriottValidationRequestDto(Integer hmcId, String hmcName, Integer productId, Integer channelProductId) {
		this.hmcId = hmcId;
		this.hmcName = hmcName;
		this.productId = productId;
		this.channelProductId = channelProductId;
	}

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Integer channelProductId) {
		this.channelProductId = channelProductId;
	}
}
