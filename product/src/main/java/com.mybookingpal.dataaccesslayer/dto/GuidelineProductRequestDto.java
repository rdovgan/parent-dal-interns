package com.mybookingpal.dataaccesslayer.dto;

import java.util.List;

//TODO: temporary item for query
public class GuidelineProductRequestDto {
	private Integer pmId;
	private String report;
	private List<String> search;
	private String sortBy;
	private String sortType;
	private Integer page;
	private Integer limit;

	public GuidelineProductRequestDto() {
	}

	public GuidelineProductRequestDto(Integer pmId, String report, List<String> search, String sortBy, String sortType, Integer page, Integer limit) {
		this.pmId = pmId;
		this.report = report;
		this.search = search;
		this.sortBy = sortBy;
		this.sortType = sortType;
		this.page = page;
		this.limit = limit;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public List<String> getSearch() {
		return search;
	}

	public void setSearch(List<String> search) {
		this.search = search;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
}
