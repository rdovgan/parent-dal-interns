package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class MarriottApiValidationInfo {
	private String productName;
	private String displayName;
	private Boolean useDisplayName;
	private String hmcProductId;
	private Integer productId;
	private String productState;
	private Boolean supplierId;
	private Boolean cancellationPolicyValidation;
	private Boolean productNameValidation;
	private Boolean productTypeValidation;

	public MarriottApiValidationInfo() {
	}

	public MarriottApiValidationInfo(String productName, String displayName, Boolean useDisplayName, String hmcProductId, Integer productId,
			String productState, Boolean supplierId, Boolean cancellationPolicyValidation, Boolean productNameValidation, Boolean productTypeValidation) {
		this.productName = productName;
		this.displayName = displayName;
		this.useDisplayName = useDisplayName;
		this.hmcProductId = hmcProductId;
		this.productId = productId;
		this.productState = productState;
		this.supplierId = supplierId;
		this.cancellationPolicyValidation = cancellationPolicyValidation;
		this.productNameValidation = productNameValidation;
		this.productTypeValidation = productTypeValidation;
	}

	public String getProductName() {
		return productName;
	}

	public MarriottApiValidationInfo setProductName(String productName) {
		this.productName = productName;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public MarriottApiValidationInfo setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public Boolean getUseDisplayName() {
		return useDisplayName;
	}

	public MarriottApiValidationInfo setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
		return this;
	}

	public String getHmcProductId() {
		return hmcProductId;
	}

	public MarriottApiValidationInfo setHmcProductId(String hmcProductId) {
		this.hmcProductId = hmcProductId;
		return this;
	}

	public Integer getProductId() {
		return productId;
	}

	public MarriottApiValidationInfo setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public String getProductState() {
		return productState;
	}

	public MarriottApiValidationInfo setProductState(String productState) {
		this.productState = productState;
		return this;
	}

	public Boolean getSupplierId() {
		return supplierId;
	}

	public MarriottApiValidationInfo setSupplierId(Boolean supplierId) {
		this.supplierId = supplierId;
		return this;
	}

	public Boolean getCancellationPolicyValidation() {
		return cancellationPolicyValidation;
	}

	public MarriottApiValidationInfo setCancellationPolicyValidation(Boolean cancellationPolicyValidation) {
		this.cancellationPolicyValidation = cancellationPolicyValidation;
		return this;
	}

	public Boolean getProductNameValidation() {
		return productNameValidation;
	}

	public MarriottApiValidationInfo setProductNameValidation(Boolean productNameValidation) {
		this.productNameValidation = productNameValidation;
		return this;
	}

	public Boolean getProductTypeValidation() {
		return productTypeValidation;
	}

	public MarriottApiValidationInfo setProductTypeValidation(Boolean productTypeValidation) {
		this.productTypeValidation = productTypeValidation;
		return this;
	}
}
