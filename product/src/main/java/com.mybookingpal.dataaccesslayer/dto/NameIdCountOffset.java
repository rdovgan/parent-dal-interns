package com.mybookingpal.dataaccesslayer.dto;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

//TODO: temporary item for query
@Component
@Scope(value = "prototype")
public class NameIdCountOffset {
	private int id;
	private int count;
	private int offset;
	private List<Integer> idList;

	public int getCount() {
		return count;
	}

	public int getId() {
		return id;
	}

	public int getOffset() {
		return offset;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<Integer> getIdList() {
		return idList;
	}

	public void setIdList(List<Integer> idList) {
		this.idList = idList;
	}
}