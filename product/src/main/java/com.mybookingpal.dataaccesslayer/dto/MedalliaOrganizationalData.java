package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class MedalliaOrganizationalData {
	private Integer productId;
	private String space;
	private String spaceUnit;
	private String productState;
	private String hmcName;
	private String productName;
	private String hmcContact;
	private String hmcEmail;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String country;
	private Integer bedrooms;
	private Double bathrooms;
	private String locationType;
	private Boolean patioBalcony;
	private Boolean airConditioning;
	private Boolean freeParking;
	private Boolean buildingElevator;
	private Boolean petsAllowed;
	private Boolean petsDeny;
	private Boolean waterfront;
	private Boolean bathtub;
	private Boolean pool;
	private Boolean butler;
	private Boolean conciergeServices;
	private Boolean dailyHousekeeping;
	private Boolean inPersonCheckIn;
	private Date maxVersion;
	private Date lastExportTime;
	private Integer marriottProductState;

	public MedalliaOrganizationalData() {
	}

	public MedalliaOrganizationalData(Integer productId, String space, String spaceUnit, String productState, String hmcName, String productName,
			String hmcContact, String hmcEmail, String address, String city, String state, String zip, String country, Integer bedrooms, Double bathrooms,
			String locationType, Boolean patioBalcony, Boolean airConditioning, Boolean freeParking, Boolean buildingElevator, Boolean petsAllowed,
			Boolean petsDeny, Boolean waterfront, Boolean bathtub, Boolean pool, Boolean butler, Boolean conciergeServices, Boolean dailyHousekeeping,
			Boolean inPersonCheckIn, Date maxVersion, Date lastExportTime, Integer marriottProductState) {
		this.productId = productId;
		this.space = space;
		this.spaceUnit = spaceUnit;
		this.productState = productState;
		this.hmcName = hmcName;
		this.productName = productName;
		this.hmcContact = hmcContact;
		this.hmcEmail = hmcEmail;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
		this.bedrooms = bedrooms;
		this.bathrooms = bathrooms;
		this.locationType = locationType;
		this.patioBalcony = patioBalcony;
		this.airConditioning = airConditioning;
		this.freeParking = freeParking;
		this.buildingElevator = buildingElevator;
		this.petsAllowed = petsAllowed;
		this.petsDeny = petsDeny;
		this.waterfront = waterfront;
		this.bathtub = bathtub;
		this.pool = pool;
		this.butler = butler;
		this.conciergeServices = conciergeServices;
		this.dailyHousekeeping = dailyHousekeeping;
		this.inPersonCheckIn = inPersonCheckIn;
		this.maxVersion = maxVersion;
		this.lastExportTime = lastExportTime;
		this.marriottProductState = marriottProductState;
	}

	public Integer getProductId() {
		return productId;
	}

	public MedalliaOrganizationalData setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public String getSpace() {
		return space;
	}

	public MedalliaOrganizationalData setSpace(String space) {
		this.space = space;
		return this;
	}

	public String getSpaceUnit() {
		return spaceUnit;
	}

	public MedalliaOrganizationalData setSpaceUnit(String spaceUnit) {
		this.spaceUnit = spaceUnit;
		return this;
	}

	public String getProductState() {
		return productState;
	}

	public MedalliaOrganizationalData setProductState(String productState) {
		this.productState = productState;
		return this;
	}

	public String getHmcName() {
		return hmcName;
	}

	public MedalliaOrganizationalData setHmcName(String hmcName) {
		this.hmcName = hmcName;
		return this;
	}

	public String getProductName() {
		return productName;
	}

	public MedalliaOrganizationalData setProductName(String productName) {
		this.productName = productName;
		return this;
	}

	public String getHmcContact() {
		return hmcContact;
	}

	public MedalliaOrganizationalData setHmcContact(String hmcContact) {
		this.hmcContact = hmcContact;
		return this;
	}

	public String getHmcEmail() {
		return hmcEmail;
	}

	public MedalliaOrganizationalData setHmcEmail(String hmcEmail) {
		this.hmcEmail = hmcEmail;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public MedalliaOrganizationalData setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getCity() {
		return city;
	}

	public MedalliaOrganizationalData setCity(String city) {
		this.city = city;
		return this;
	}

	public String getState() {
		return state;
	}

	public MedalliaOrganizationalData setState(String state) {
		this.state = state;
		return this;
	}

	public String getZip() {
		return zip;
	}

	public MedalliaOrganizationalData setZip(String zip) {
		this.zip = zip;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public MedalliaOrganizationalData setCountry(String country) {
		this.country = country;
		return this;
	}

	public Integer getBedrooms() {
		return bedrooms;
	}

	public MedalliaOrganizationalData setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
		return this;
	}

	public Double getBathrooms() {
		return bathrooms;
	}

	public MedalliaOrganizationalData setBathrooms(Double bathrooms) {
		this.bathrooms = bathrooms;
		return this;
	}

	public String getLocationType() {
		return locationType;
	}

	public MedalliaOrganizationalData setLocationType(String locationType) {
		this.locationType = locationType;
		return this;
	}

	public Boolean getPatioBalcony() {
		return patioBalcony;
	}

	public MedalliaOrganizationalData setPatioBalcony(Boolean patioBalcony) {
		this.patioBalcony = patioBalcony;
		return this;
	}

	public Boolean getAirConditioning() {
		return airConditioning;
	}

	public MedalliaOrganizationalData setAirConditioning(Boolean airConditioning) {
		this.airConditioning = airConditioning;
		return this;
	}

	public Boolean getFreeParking() {
		return freeParking;
	}

	public MedalliaOrganizationalData setFreeParking(Boolean freeParking) {
		this.freeParking = freeParking;
		return this;
	}

	public Boolean getBuildingElevator() {
		return buildingElevator;
	}

	public MedalliaOrganizationalData setBuildingElevator(Boolean buildingElevator) {
		this.buildingElevator = buildingElevator;
		return this;
	}

	public Boolean getPetsAllowed() {
		return petsAllowed;
	}

	public MedalliaOrganizationalData setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
		return this;
	}

	public Boolean getPetsDeny() {
		return petsDeny;
	}

	public MedalliaOrganizationalData setPetsDeny(Boolean petsDeny) {
		this.petsDeny = petsDeny;
		return this;
	}

	public Boolean getWaterfront() {
		return waterfront;
	}

	public MedalliaOrganizationalData setWaterfront(Boolean waterfront) {
		this.waterfront = waterfront;
		return this;
	}

	public Boolean getBathtub() {
		return bathtub;
	}

	public MedalliaOrganizationalData setBathtub(Boolean bathtub) {
		this.bathtub = bathtub;
		return this;
	}

	public Boolean getPool() {
		return pool;
	}

	public MedalliaOrganizationalData setPool(Boolean pool) {
		this.pool = pool;
		return this;
	}

	public Boolean getButler() {
		return butler;
	}

	public MedalliaOrganizationalData setButler(Boolean butler) {
		this.butler = butler;
		return this;
	}

	public Boolean getConciergeServices() {
		return conciergeServices;
	}

	public MedalliaOrganizationalData setConciergeServices(Boolean conciergeServices) {
		this.conciergeServices = conciergeServices;
		return this;
	}

	public Boolean getDailyHousekeeping() {
		return dailyHousekeeping;
	}

	public MedalliaOrganizationalData setDailyHousekeeping(Boolean dailyHousekeeping) {
		this.dailyHousekeeping = dailyHousekeeping;
		return this;
	}

	public Boolean getInPersonCheckIn() {
		return inPersonCheckIn;
	}

	public MedalliaOrganizationalData setInPersonCheckIn(Boolean inPersonCheckIn) {
		this.inPersonCheckIn = inPersonCheckIn;
		return this;
	}

	public Date getMaxVersion() {
		return maxVersion;
	}

	public MedalliaOrganizationalData setMaxVersion(Date maxVersion) {
		this.maxVersion = maxVersion;
		return this;
	}

	public Date getLastExportTime() {
		return lastExportTime;
	}

	public MedalliaOrganizationalData setLastExportTime(Date lastExportTime) {
		this.lastExportTime = lastExportTime;
		return this;
	}

	public Integer getMarriottProductState() {
		return marriottProductState;
	}

	public MedalliaOrganizationalData setMarriottProductState(Integer marriottProductState) {
		this.marriottProductState = marriottProductState;
		return this;
	}
}
