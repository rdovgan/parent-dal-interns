package com.mybookingpal.dataaccesslayer.dto;

import java.sql.Time;
import java.util.Date;

//TODO: temporary item for query
public class PropertyInfo {
	private Integer id;
	private String name;
	private String displayName;
	private String image;
	private Double latitude;
	private Double longitude;
	private Double baths;
	private Time checkIn;
	private Time checkOut;
	private Integer persons;
	private Integer beds;
	private String countryCode;
	private String city;
	private String postalCode;
	private Boolean assignedToManager;
	private String status;
	private Integer cpCount;
	private Integer cpState;
	private Date lastUpdate;
	private String address;
	private String multiUnit;
	private Integer rating;
	private String imagePreview;
	private Integer bedRooms;

	public PropertyInfo() {
	}

	public PropertyInfo(Integer id, String name, String displayName, String image, Double latitude, Double longitude, Double baths, Time checkIn, Time checkOut,
			Integer persons, Integer beds, String countryCode, String city, String postalCode, Boolean assignedToManager, String status, Integer cpCount,
			Integer cpState, Date lastUpdate, String address, String multiUnit, Integer rating, String imagePreview, Integer bedRooms) {
		this.id = id;
		this.name = name;
		this.displayName = displayName;
		this.image = image;
		this.latitude = latitude;
		this.longitude = longitude;
		this.baths = baths;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.persons = persons;
		this.beds = beds;
		this.countryCode = countryCode;
		this.city = city;
		this.postalCode = postalCode;
		this.assignedToManager = assignedToManager;
		this.status = status;
		this.cpCount = cpCount;
		this.cpState = cpState;
		this.lastUpdate = lastUpdate;
		this.address = address;
		this.multiUnit = multiUnit;
		this.rating = rating;
		this.imagePreview = imagePreview;
		this.bedRooms = bedRooms;
	}

	public Integer getId() {
		return id;
	}

	public PropertyInfo setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public PropertyInfo setName(String name) {
		this.name = name;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public PropertyInfo setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public String getImage() {
		return image;
	}

	public PropertyInfo setImage(String image) {
		this.image = image;
		return this;
	}

	public Double getLatitude() {
		return latitude;
	}

	public PropertyInfo setLatitude(Double latitude) {
		this.latitude = latitude;
		return this;
	}

	public Double getLongitude() {
		return longitude;
	}

	public PropertyInfo setLongitude(Double longitude) {
		this.longitude = longitude;
		return this;
	}

	public Double getBaths() {
		return baths;
	}

	public PropertyInfo setBaths(Double baths) {
		this.baths = baths;
		return this;
	}

	public Date getCheckIn() {
		return checkIn;
	}

	public PropertyInfo setCheckIn(Time checkIn) {
		this.checkIn = checkIn;
		return this;
	}

	public Date getCheckOut() {
		return checkOut;
	}

	public PropertyInfo setCheckOut(Time checkOut) {
		this.checkOut = checkOut;
		return this;
	}

	public Integer getPersons() {
		return persons;
	}

	public PropertyInfo setPersons(Integer persons) {
		this.persons = persons;
		return this;
	}

	public Integer getBeds() {
		return beds;
	}

	public PropertyInfo setBeds(Integer beds) {
		this.beds = beds;
		return this;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public PropertyInfo setCountryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public String getCity() {
		return city;
	}

	public PropertyInfo setCity(String city) {
		this.city = city;
		return this;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public PropertyInfo setPostalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public Boolean getAssignedToManager() {
		return assignedToManager;
	}

	public PropertyInfo setAssignedToManager(Boolean assignedToManager) {
		this.assignedToManager = assignedToManager;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public PropertyInfo setStatus(String status) {
		this.status = status;
		return this;
	}

	public Integer getCpCount() {
		return cpCount;
	}

	public PropertyInfo setCpCount(Integer cpCount) {
		this.cpCount = cpCount;
		return this;
	}

	public Integer getCpState() {
		return cpState;
	}

	public PropertyInfo setCpState(Integer cpState) {
		this.cpState = cpState;
		return this;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public PropertyInfo setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public PropertyInfo setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getMultiUnit() {
		return multiUnit;
	}

	public PropertyInfo setMultiUnit(String multiUnit) {
		this.multiUnit = multiUnit;
		return this;
	}

	public Integer getRating() {
		return rating;
	}

	public PropertyInfo setRating(Integer rating) {
		this.rating = rating;
		return this;
	}

	public String getImagePreview() {
		return imagePreview;
	}

	public PropertyInfo setImagePreview(String imagePreview) {
		this.imagePreview = imagePreview;
		return this;
	}

	public Integer getBedRooms() {
		return bedRooms;
	}

	public PropertyInfo setBedRooms(Integer bedRooms) {
		this.bedRooms = bedRooms;
		return this;
	}
}
