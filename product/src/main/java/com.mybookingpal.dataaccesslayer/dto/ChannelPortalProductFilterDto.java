package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class ChannelPortalProductFilterDto {
	private Integer productId;
	private Integer channelProductId;
	private String productName;
	private String displayName;
	private String HMCName;
	private Integer HMCId;
	private String country;
	private String city;
	private Integer score;
	private Date filterDate;
	private Date startDate;
	private Date endDate;

	public ChannelPortalProductFilterDto() {
	}

	public ChannelPortalProductFilterDto(Integer productId, Integer channelProductId, String productName, String displayName, String HMCName, Integer HMCId,
			String country, String city, Integer score, Date filterDate, Date startDate, Date endDate) {
		this.productId = productId;
		this.channelProductId = channelProductId;
		this.productName = productName;
		this.displayName = displayName;
		this.HMCName = HMCName;
		this.HMCId = HMCId;
		this.country = country;
		this.city = city;
		this.score = score;
		this.filterDate = filterDate;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Integer channelProductId) {
		this.channelProductId = channelProductId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getHMCName() {
		return HMCName;
	}

	public void setHMCName(String HMCName) {
		this.HMCName = HMCName;
	}

	public Integer getHMCId() {
		return HMCId;
	}

	public void setHMCId(Integer HMCId) {
		this.HMCId = HMCId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Date getFilterDate() {
		return filterDate;
	}

	public void setFilterDate(Date filterDate) {
		this.filterDate = filterDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
