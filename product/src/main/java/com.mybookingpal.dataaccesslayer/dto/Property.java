package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class Property {
	private Integer id;
	private String name;
	protected String altId;
	private String type;
	private Integer locationID;
	private String currency;
	private Double latitude;
	private Double longitude;
	private Integer supplierId;
	private String physicaladdress;
	private String city;
	private String region;
	private String country;
	protected Integer bed;
	protected Integer room;
	protected Double bathroom;
	protected Integer toilet;
	protected Integer person;
	protected Integer child;
	protected Integer infant;
	protected Double discount;
	private String inquireState;
	private Date version;
	private Boolean useonepricerow;
}
