package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class ChannelPortalIncomingListingFilterDto {
	private Integer channelId;
	private Integer supplierId;
	private String country;
	private String city;
	private Integer score;
	private Integer bedrooms;
	private Date startDate;
	private Date endDate;

	public ChannelPortalIncomingListingFilterDto() {
	}

	public ChannelPortalIncomingListingFilterDto(Integer channelId, Integer supplierId, String country, String city, Integer score, Integer bedrooms,
			Date startDate, Date endDate) {
		this.channelId = channelId;
		this.supplierId = supplierId;
		this.country = country;
		this.city = city;
		this.score = score;
		this.bedrooms = bedrooms;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
