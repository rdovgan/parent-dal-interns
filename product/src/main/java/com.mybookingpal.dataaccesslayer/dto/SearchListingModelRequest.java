package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class SearchListingModelRequest {
	private Integer supplierId;
	private String propertyId;
	private String propertyName;
	private Integer startPagination;
	private Integer limit;

	public SearchListingModelRequest() {
	}

	public SearchListingModelRequest(Integer supplierId, String propertyId, String propertyName, Integer startPagination, Integer limit) {
		this.supplierId = supplierId;
		this.propertyId = propertyId;
		this.propertyName = propertyName;
		this.startPagination = startPagination;
		this.limit = limit;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Integer getStartPagination() {
		return startPagination;
	}

	public void setStartPagination(Integer startPagination) {
		this.startPagination = startPagination;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
}
