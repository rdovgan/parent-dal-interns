package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class BeststayzProductFilterDto {
	protected Integer productId;
	protected String productName;
	protected String displayName;
	protected String ownerName;
	protected Integer ownerId;
	protected String channelProductId;
	protected Integer channelId;

	public BeststayzProductFilterDto() {
	}

	public BeststayzProductFilterDto(Integer productId, String productName, String displayName, String ownerName, Integer ownerId, String channelProductId,
			Integer channelId) {
		this.productId = productId;
		this.productName = productName;
		this.displayName = displayName;
		this.ownerName = ownerName;
		this.ownerId = ownerId;
		this.channelProductId = channelProductId;
		this.channelId = channelId;
	}

	public Integer getProductId() {
		return productId;
	}

	public BeststayzProductFilterDto setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public String getProductName() {
		return productName;
	}

	public BeststayzProductFilterDto setProductName(String productName) {
		this.productName = productName;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public BeststayzProductFilterDto setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public BeststayzProductFilterDto setOwnerName(String ownerName) {
		this.ownerName = ownerName;
		return this;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public BeststayzProductFilterDto setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
		return this;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public BeststayzProductFilterDto setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
		return this;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public BeststayzProductFilterDto setChannelId(Integer channelId) {
		this.channelId = channelId;
		return this;
	}
}
