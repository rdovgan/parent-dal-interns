package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class SearchListingModelResponse {
	private Integer id;
	private Integer altId;
	private String name;
	private String region;
	private String country;

	public SearchListingModelResponse() {
	}

	public SearchListingModelResponse(Integer id, Integer altId, String name, String region, String country) {
		this.id = id;
		this.altId = altId;
		this.name = name;
		this.region = region;
		this.country = country;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAltId() {
		return altId;
	}

	public void setAltId(Integer altId) {
		this.altId = altId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
