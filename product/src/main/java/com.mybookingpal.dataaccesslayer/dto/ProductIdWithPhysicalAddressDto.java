package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ProductIdWithPhysicalAddressDto {
	private Integer productId;
	private String physicalAddress;

	public ProductIdWithPhysicalAddressDto() {
	}

	public ProductIdWithPhysicalAddressDto(Integer productId, String physicalAddress) {
		this.productId = productId;
		this.physicalAddress = physicalAddress;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}
}
