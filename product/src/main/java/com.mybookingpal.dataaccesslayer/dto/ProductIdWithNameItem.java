package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ProductIdWithNameItem {
	private Integer productId;
	private String productName;
	private String parentName;

	public ProductIdWithNameItem() {
	}

	public ProductIdWithNameItem(Integer productId, String productName, String parentName) {
		this.productId = productId;
		this.productName = productName;
		this.parentName = parentName;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
