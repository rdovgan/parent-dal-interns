package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ChannelPortalIncomingListing {
	private Integer productId;
	private String supplierName;
	private String dateAdded;
	private Integer score;
	private String country;
	private Integer numberOfBedrooms;
	private String displayName;
	private String productState;
	private Integer channelPortalState;
	private String city;

	public ChannelPortalIncomingListing() {
	}

	public ChannelPortalIncomingListing(Integer productId, String supplierName, String dateAdded, Integer score, String country, Integer numberOfBedrooms,
			String displayName, String productState, Integer channelPortalState, String city) {
		this.productId = productId;
		this.supplierName = supplierName;
		this.dateAdded = dateAdded;
		this.score = score;
		this.country = country;
		this.numberOfBedrooms = numberOfBedrooms;
		this.displayName = displayName;
		this.productState = productState;
		this.channelPortalState = channelPortalState;
		this.city = city;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	public void setNumberOfBedrooms(Integer numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProductState() {
		return productState;
	}

	public void setProductState(String productState) {
		this.productState = productState;
	}

	public Integer getChannelPortalState() {
		return channelPortalState;
	}

	public void setChannelPortalState(Integer channelPortalState) {
		this.channelPortalState = channelPortalState;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
