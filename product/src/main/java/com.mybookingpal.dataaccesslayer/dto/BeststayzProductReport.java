package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class BeststayzProductReport extends BeststayzProductFilterDto{
	private String productState;
	private String address;
	private Integer addressComponentId;
	private String pmName;
	private Integer channelState;
	private Boolean assignedToManager;

	public BeststayzProductReport() {
	}

	public BeststayzProductReport(Integer productId, String productName, String displayName, String ownerName, Integer ownerId, String channelProductId,
			Integer channelId, String productState, String address, Integer addressComponentId, String pmName, Integer channelState,
			Boolean assignedToManager) {
		super(productId, productName, displayName, ownerName, ownerId, channelProductId, channelId);
		this.productState = productState;
		this.address = address;
		this.addressComponentId = addressComponentId;
		this.pmName = pmName;
		this.channelState = channelState;
		this.assignedToManager = assignedToManager;
	}

	public String getProductState() {
		return productState;
	}

	public void setProductState(String productState) {
		this.productState = productState;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getAddressComponentId() {
		return addressComponentId;
	}

	public void setAddressComponentId(Integer addressComponentId) {
		this.addressComponentId = addressComponentId;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public Integer getChannelState() {
		return channelState;
	}

	public void setChannelState(Integer channelState) {
		this.channelState = channelState;
	}

	public Boolean getAssignedToManager() {
		return assignedToManager;
	}

	public void setAssignedToManager(Boolean assignedToManager) {
		this.assignedToManager = assignedToManager;
	}
}
