package com.mybookingpal.dataaccesslayer.dto;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

//TODO: temporary item for query
@Component("ProductAction")
@Scope(value = "prototype")
public class ProductAction {
	protected String countryCode;
	private boolean suggested;
	private int numrows = Integer.MAX_VALUE;
	private int offsetrows = 0;
	protected String organizationid;
	protected String id;
	protected String name = StringUtils.EMPTY;
	protected String state;
	protected String type;
	protected String ids;
	protected String productids;
	protected Date version;
	protected Double rank;
	protected String supplierid;
	protected String parentId;
	protected List<String> rootIds;
	protected int channelId;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public boolean isSuggested() {
		return suggested;
	}

	public void setSuggested(boolean suggested) {
		this.suggested = suggested;
	}

	public int getNumrows() {
		return numrows;
	}

	public void setNumrows(int numrows) {
		this.numrows = numrows;
	}

	public int getOffsetrows() {
		return offsetrows;
	}

	public void setOffsetrows(int offsetrows) {
		this.offsetrows = offsetrows;
	}

	public String getOrganizationid() {
		return organizationid;
	}

	public void setOrganizationid(String organizationid) {
		this.organizationid = organizationid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getProductids() {
		return productids;
	}

	public void setProductids(String productids) {
		this.productids = productids;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Double getRank() {
		return rank;
	}

	public void setRank(Double rank) {
		this.rank = rank;
	}

	public String getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(String supplierid) {
		this.supplierid = supplierid;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public List<String> getRootIds() {
		return rootIds;
	}

	public void setRootIds(List<String> rootIds) {
		this.rootIds = rootIds;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
}
