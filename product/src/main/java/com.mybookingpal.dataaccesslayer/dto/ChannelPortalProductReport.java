package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class ChannelPortalProductReport {
	private Integer productId;
	private String channelProductId;
	private String productAltId;
	private Integer numberOfBedrooms;
	private Date dateAdded;
	private Date dateApproved;
	private Date dateRejected;
	private String productName;
	private String displayName;
	private Integer channelProductState;
	private String productState;
	private String  address;
	private String country;
	private String state;
	private String city;
	private String HMCName;
	private Boolean channelSpecificValidation;
	private Integer score;
	private Boolean live;

	public ChannelPortalProductReport() {
	}

	public ChannelPortalProductReport(Integer productId, String channelProductId, String productAltId, Integer numberOfBedrooms, Date dateAdded,
			Date dateApproved, Date dateRejected, String productName, String displayName, Integer channelProductState, String productState, String address,
			String country, String state, String city, String HMCName, Boolean channelSpecificValidation, Integer score, Boolean live) {
		this.productId = productId;
		this.channelProductId = channelProductId;
		this.productAltId = productAltId;
		this.numberOfBedrooms = numberOfBedrooms;
		this.dateAdded = dateAdded;
		this.dateApproved = dateApproved;
		this.dateRejected = dateRejected;
		this.productName = productName;
		this.displayName = displayName;
		this.channelProductState = channelProductState;
		this.productState = productState;
		this.address = address;
		this.country = country;
		this.state = state;
		this.city = city;
		this.HMCName = HMCName;
		this.channelSpecificValidation = channelSpecificValidation;
		this.score = score;
		this.live = live;
	}

	public Integer getProductId() {
		return productId;
	}

	public ChannelPortalProductReport setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public ChannelPortalProductReport setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
		return this;
	}

	public String getProductAltId() {
		return productAltId;
	}

	public ChannelPortalProductReport setProductAltId(String productAltId) {
		this.productAltId = productAltId;
		return this;
	}

	public Integer getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	public ChannelPortalProductReport setNumberOfBedrooms(Integer numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
		return this;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public ChannelPortalProductReport setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
		return this;
	}

	public Date getDateApproved() {
		return dateApproved;
	}

	public ChannelPortalProductReport setDateApproved(Date dateApproved) {
		this.dateApproved = dateApproved;
		return this;
	}

	public Date getDateRejected() {
		return dateRejected;
	}

	public ChannelPortalProductReport setDateRejected(Date dateRejected) {
		this.dateRejected = dateRejected;
		return this;
	}

	public String getProductName() {
		return productName;
	}

	public ChannelPortalProductReport setProductName(String productName) {
		this.productName = productName;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public ChannelPortalProductReport setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public Integer getChannelProductState() {
		return channelProductState;
	}

	public ChannelPortalProductReport setChannelProductState(Integer channelProductState) {
		this.channelProductState = channelProductState;
		return this;
	}

	public String getProductState() {
		return productState;
	}

	public ChannelPortalProductReport setProductState(String productState) {
		this.productState = productState;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public ChannelPortalProductReport setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public ChannelPortalProductReport setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getState() {
		return state;
	}

	public ChannelPortalProductReport setState(String state) {
		this.state = state;
		return this;
	}

	public String getCity() {
		return city;
	}

	public ChannelPortalProductReport setCity(String city) {
		this.city = city;
		return this;
	}

	public String getHMCName() {
		return HMCName;
	}

	public ChannelPortalProductReport setHMCName(String HMCName) {
		this.HMCName = HMCName;
		return this;
	}

	public Boolean getChannelSpecificValidation() {
		return channelSpecificValidation;
	}

	public ChannelPortalProductReport setChannelSpecificValidation(Boolean channelSpecificValidation) {
		this.channelSpecificValidation = channelSpecificValidation;
		return this;
	}

	public Integer getScore() {
		return score;
	}

	public ChannelPortalProductReport setScore(Integer score) {
		this.score = score;
		return this;
	}

	public Boolean getLive() {
		return live;
	}

	public ChannelPortalProductReport setLive(Boolean live) {
		this.live = live;
		return this;
	}
}
