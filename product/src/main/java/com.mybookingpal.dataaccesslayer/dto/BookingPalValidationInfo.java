package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

//TODO: temporary item for query
public class BookingPalValidationInfo {
	private String productName;
	private String address;
	private String hmcProductId;
	private Integer productId;
	private Date createdDate;
	private String productState;

	public BookingPalValidationInfo() {
	}

	public BookingPalValidationInfo(String productName, String address, String hmcProductId, Integer productId, Date createdDate, String productState) {
		this.productName = productName;
		this.address = address;
		this.hmcProductId = hmcProductId;
		this.productId = productId;
		this.createdDate = createdDate;
		this.productState = productState;
	}

	public String getProductName() {
		return productName;
	}

	public BookingPalValidationInfo setProductName(String productName) {
		this.productName = productName;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public BookingPalValidationInfo setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getHmcProductId() {
		return hmcProductId;
	}

	public BookingPalValidationInfo setHmcProductId(String hmcProductId) {
		this.hmcProductId = hmcProductId;
		return this;
	}

	public Integer getProductId() {
		return productId;
	}

	public BookingPalValidationInfo setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public BookingPalValidationInfo setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public String getProductState() {
		return productState;
	}

	public BookingPalValidationInfo setProductState(String productState) {
		this.productState = productState;
		return this;
	}
}
