package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ImageWidgetItem {
	private String url;
	private String notes;

	public ImageWidgetItem() {
	}

	public ImageWidgetItem(String url, String notes) {
		this.url = url;
		this.notes = notes;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
