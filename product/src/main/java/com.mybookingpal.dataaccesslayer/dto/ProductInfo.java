package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ProductInfo {
	private String id;
	private String location;
	private String country;
	private String address;
	private String description;
	private String bathrooms;
	private String commission;
	private String securityDeposit;
	private Boolean selected;

	public ProductInfo() {
	}

	public ProductInfo(String id, String location, String country, String address, String description, String bathrooms, String commission,
			String securityDeposit, Boolean selected) {
		this.id = id;
		this.location = location;
		this.country = country;
		this.address = address;
		this.description = description;
		this.bathrooms = bathrooms;
		this.commission = commission;
		this.securityDeposit = securityDeposit;
		this.selected = selected;
	}

	public String getId() {
		return id;
	}

	public ProductInfo setId(String id) {
		this.id = id;
		return this;
	}

	public String getLocation() {
		return location;
	}

	public ProductInfo setLocation(String location) {
		this.location = location;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public ProductInfo setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public ProductInfo setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public ProductInfo setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getBathrooms() {
		return bathrooms;
	}

	public ProductInfo setBathrooms(String bathrooms) {
		this.bathrooms = bathrooms;
		return this;
	}

	public String getCommission() {
		return commission;
	}

	public ProductInfo setCommission(String commission) {
		this.commission = commission;
		return this;
	}

	public String getSecurityDeposit() {
		return securityDeposit;
	}

	public ProductInfo setSecurityDeposit(String securityDeposit) {
		this.securityDeposit = securityDeposit;
		return this;
	}

	public Boolean getSelected() {
		return selected;
	}

	public ProductInfo setSelected(Boolean selected) {
		this.selected = selected;
		return this;
	}
}
