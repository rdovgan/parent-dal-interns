package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class OtaSearchParameter {
	private Integer productid;
	private String productname;
	private Integer supplierid;
	private Integer ownerid;
	private Integer guestcount;
	private Double maxrate;
	private Double nelatitude;
	private Double swlatitude;
	private Double nelongitude;
	private Double swlongitude;

	public OtaSearchParameter() {
	}

	public OtaSearchParameter(Integer productid, String productname, Integer supplierid, Integer ownerid, Integer guestcount, Double maxrate, Double nelatitude,
			Double swlatitude, Double nelongitude, Double swlongitude) {
		this.productid = productid;
		this.productname = productname;
		this.supplierid = supplierid;
		this.ownerid = ownerid;
		this.guestcount = guestcount;
		this.maxrate = maxrate;
		this.nelatitude = nelatitude;
		this.swlatitude = swlatitude;
		this.nelongitude = nelongitude;
		this.swlongitude = swlongitude;
	}

	public Integer getProductid() {
		return productid;
	}

	public void setProductid(Integer productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public Integer getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(Integer supplierid) {
		this.supplierid = supplierid;
	}

	public Integer getOwnerid() {
		return ownerid;
	}

	public void setOwnerid(Integer ownerid) {
		this.ownerid = ownerid;
	}

	public Integer getGuestcount() {
		return guestcount;
	}

	public void setGuestcount(Integer guestcount) {
		this.guestcount = guestcount;
	}

	public Double getMaxrate() {
		return maxrate;
	}

	public void setMaxrate(Double maxrate) {
		this.maxrate = maxrate;
	}

	public Double getNelatitude() {
		return nelatitude;
	}

	public void setNelatitude(Double nelatitude) {
		this.nelatitude = nelatitude;
	}

	public Double getSwlatitude() {
		return swlatitude;
	}

	public void setSwlatitude(Double swlatitude) {
		this.swlatitude = swlatitude;
	}

	public Double getNelongitude() {
		return nelongitude;
	}

	public void setNelongitude(Double nelongitude) {
		this.nelongitude = nelongitude;
	}

	public Double getSwlongitude() {
		return swlongitude;
	}

	public void setSwlongitude(Double swlongitude) {
		this.swlongitude = swlongitude;
	}
}
