package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductModel;

//TODO: temporary item for query
public class ProductChannel extends ProductModel {
	private String channelProductId;
	private String parentChannelProductId;

	public ProductChannel() {
	}

	public ProductChannel(ProductModel productModel, String channelProductId, String parentChannelProductId) {
		super(productModel);
		this.channelProductId = channelProductId;
		this.parentChannelProductId = parentChannelProductId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public ProductChannel setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
		return this;
	}

	public String getParentChannelProductId() {
		return parentChannelProductId;
	}

	public ProductChannel setParentChannelProductId(String parentChannelProductId) {
		this.parentChannelProductId = parentChannelProductId;
		return this;
	}
}
