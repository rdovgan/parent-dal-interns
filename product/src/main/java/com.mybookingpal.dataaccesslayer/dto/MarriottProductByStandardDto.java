package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class MarriottProductByStandardDto {
	private Integer productId;
	private String imageUrl;
	private Boolean isSelected;
	private String location;
	private String name;

	public MarriottProductByStandardDto() {
	}

	public MarriottProductByStandardDto(Integer productId, String imageUrl, Boolean isSelected, String location, String name) {
		this.productId = productId;
		this.imageUrl = imageUrl;
		this.isSelected = isSelected;
		this.location = location;
		this.name = name;
	}

	public Integer getProductId() {
		return productId;
	}

	public MarriottProductByStandardDto setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public MarriottProductByStandardDto setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
		return this;
	}

	public Boolean getSelected() {
		return isSelected;
	}

	public MarriottProductByStandardDto setSelected(Boolean selected) {
		isSelected = selected;
		return this;
	}

	public String getLocation() {
		return location;
	}

	public MarriottProductByStandardDto setLocation(String location) {
		this.location = location;
		return this;
	}

	public String getName() {
		return name;
	}

	public MarriottProductByStandardDto setName(String name) {
		this.name = name;
		return this;
	}
}
