package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductUpdateTypeModel;

public class ProductUpdateTypeWithAdditionalFields extends ProductUpdateTypeModel {
	private Boolean builtDate;
	private Boolean renovatingDate;
	private Boolean rentingDate;
	private Boolean hostLocation;
	private Boolean basePrice;

	public ProductUpdateTypeWithAdditionalFields() {
		super();
		this.builtDate = false;
		this.renovatingDate = false;
		this.rentingDate = false;
		this.hostLocation = false;
		this.basePrice = false;
	}

	public ProductUpdateTypeWithAdditionalFields(Boolean type, Integer productId, Boolean builtDate, Boolean renovatingDate, Boolean rentingDate,
			Boolean hostLocation, Boolean basePrice) {
		super(type, productId);
		this.builtDate = builtDate;
		this.renovatingDate = renovatingDate;
		this.rentingDate = rentingDate;
		this.hostLocation = hostLocation;
		this.basePrice = basePrice;
	}

	public Boolean getBuiltDate() {
		return builtDate;
	}

	public void setBuiltDate(Boolean builtDate) {
		this.builtDate = builtDate;
	}

	public Boolean getRenovatingDate() {
		return renovatingDate;
	}

	public void setRenovatingDate(Boolean renovatingDate) {
		this.renovatingDate = renovatingDate;
	}

	public Boolean getRentingDate() {
		return rentingDate;
	}

	public void setRentingDate(Boolean rentingDate) {
		this.rentingDate = rentingDate;
	}

	public Boolean getHostLocation() {
		return hostLocation;
	}

	public void setHostLocation(Boolean hostLocation) {
		this.hostLocation = hostLocation;
	}

	public Boolean getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(Boolean basePrice) {
		this.basePrice = basePrice;
	}
}
