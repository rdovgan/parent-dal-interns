package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class Area {
	private Double nelatitude;
	private Double swlatitude;
	private Double nelongitude;
	private Double swlongitude;

	public Area() {
	}

	public Area(Double nelatitude, Double swlatitude, Double nelongitude, Double swlongitude) {
		this.nelatitude = nelatitude;
		this.swlatitude = swlatitude;
		this.nelongitude = nelongitude;
		this.swlongitude = swlongitude;
	}

	public Double getNelatitude() {
		return nelatitude;
	}

	public void setNelatitude(Double nelatitude) {
		this.nelatitude = nelatitude;
	}

	public Double getSwlatitude() {
		return swlatitude;
	}

	public void setSwlatitude(Double swlatitude) {
		this.swlatitude = swlatitude;
	}

	public Double getNelongitude() {
		return nelongitude;
	}

	public void setNelongitude(Double nelongitude) {
		this.nelongitude = nelongitude;
	}

	public Double getSwlongitude() {
		return swlongitude;
	}

	public void setSwlongitude(Double swlongitude) {
		this.swlongitude = swlongitude;
	}
}
