package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.ProductConstants;

//TODO: temporary item for query
public class ProductWidgetItem {
	private Long id;
	private Long locationId;
	private Long ownerId;
	private Long partOfId;
	private Long supplierId;
	private String name;
	private String ownername;
	private String partofname;
	private String suppliername;
	private String code;
	private Double commission;
	private String currency;
	private Double discount;
	private Double ownerDiscount;
	private Double altitude;
	private Double latitude;
	private Double longitude;
	private Integer linenChange;
	private Integer person;
	private Integer child;
	private Integer infant;
	private Integer quantity;
	private String physicalAddress;
	private Double rank;
	private Integer rating;
	private Integer refresh;
	private Integer bed;
	private Integer room;
	private Double bathroom;
	private Integer toilet;
	private Integer floor;
	private String state;
	private String space;
	private ProductConstants.SpaceUnitEnum spaceUnit;
	private String serviceDays;
	private String tax;
	private String unit;
	private String unspsc;
	private String webAddress;

	public ProductWidgetItem() {
	}

	public ProductWidgetItem(Long id, Long locationId, Long ownerId, Long partOfId, Long supplierId, String name, String ownername, String partofname,
			String suppliername, String code, Double commission, String currency, Double discount, Double ownerDiscount, Double altitude, Double latitude,
			Double longitude, Integer linenChange, Integer person, Integer child, Integer infant, Integer quantity, String physicalAddress, Double rank,
			Integer rating, Integer refresh, Integer bed, Integer room, Double bathroom, Integer toilet, Integer floor, String state, String space,
			ProductConstants.SpaceUnitEnum spaceUnit, String serviceDays, String tax, String unit, String unspsc, String webAddress) {
		this.id = id;
		this.locationId = locationId;
		this.ownerId = ownerId;
		this.partOfId = partOfId;
		this.supplierId = supplierId;
		this.name = name;
		this.ownername = ownername;
		this.partofname = partofname;
		this.suppliername = suppliername;
		this.code = code;
		this.commission = commission;
		this.currency = currency;
		this.discount = discount;
		this.ownerDiscount = ownerDiscount;
		this.altitude = altitude;
		this.latitude = latitude;
		this.longitude = longitude;
		this.linenChange = linenChange;
		this.person = person;
		this.child = child;
		this.infant = infant;
		this.quantity = quantity;
		this.physicalAddress = physicalAddress;
		this.rank = rank;
		this.rating = rating;
		this.refresh = refresh;
		this.bed = bed;
		this.room = room;
		this.bathroom = bathroom;
		this.toilet = toilet;
		this.floor = floor;
		this.state = state;
		this.space = space;
		this.spaceUnit = spaceUnit;
		this.serviceDays = serviceDays;
		this.tax = tax;
		this.unit = unit;
		this.unspsc = unspsc;
		this.webAddress = webAddress;
	}

	public Long getId() {
		return id;
	}

	public ProductWidgetItem setId(Long id) {
		this.id = id;
		return this;
	}

	public Long getLocationId() {
		return locationId;
	}

	public ProductWidgetItem setLocationId(Long locationId) {
		this.locationId = locationId;
		return this;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public ProductWidgetItem setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
		return this;
	}

	public Long getPartOfId() {
		return partOfId;
	}

	public ProductWidgetItem setPartOfId(Long partOfId) {
		this.partOfId = partOfId;
		return this;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public ProductWidgetItem setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
		return this;
	}

	public String getName() {
		return name;
	}

	public ProductWidgetItem setName(String name) {
		this.name = name;
		return this;
	}

	public String getOwnername() {
		return ownername;
	}

	public ProductWidgetItem setOwnername(String ownername) {
		this.ownername = ownername;
		return this;
	}

	public String getPartofname() {
		return partofname;
	}

	public ProductWidgetItem setPartofname(String partofname) {
		this.partofname = partofname;
		return this;
	}

	public String getSuppliername() {
		return suppliername;
	}

	public ProductWidgetItem setSuppliername(String suppliername) {
		this.suppliername = suppliername;
		return this;
	}

	public String getCode() {
		return code;
	}

	public ProductWidgetItem setCode(String code) {
		this.code = code;
		return this;
	}

	public Double getCommission() {
		return commission;
	}

	public ProductWidgetItem setCommission(Double commission) {
		this.commission = commission;
		return this;
	}

	public String getCurrency() {
		return currency;
	}

	public ProductWidgetItem setCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	public Double getDiscount() {
		return discount;
	}

	public ProductWidgetItem setDiscount(Double discount) {
		this.discount = discount;
		return this;
	}

	public Double getOwnerDiscount() {
		return ownerDiscount;
	}

	public ProductWidgetItem setOwnerDiscount(Double ownerDiscount) {
		this.ownerDiscount = ownerDiscount;
		return this;
	}

	public Double getAltitude() {
		return altitude;
	}

	public ProductWidgetItem setAltitude(Double altitude) {
		this.altitude = altitude;
		return this;
	}

	public Double getLatitude() {
		return latitude;
	}

	public ProductWidgetItem setLatitude(Double latitude) {
		this.latitude = latitude;
		return this;
	}

	public Double getLongitude() {
		return longitude;
	}

	public ProductWidgetItem setLongitude(Double longitude) {
		this.longitude = longitude;
		return this;
	}

	public Integer getLinenChange() {
		return linenChange;
	}

	public ProductWidgetItem setLinenChange(Integer linenChange) {
		this.linenChange = linenChange;
		return this;
	}

	public Integer getPerson() {
		return person;
	}

	public ProductWidgetItem setPerson(Integer person) {
		this.person = person;
		return this;
	}

	public Integer getChild() {
		return child;
	}

	public ProductWidgetItem setChild(Integer child) {
		this.child = child;
		return this;
	}

	public Integer getInfant() {
		return infant;
	}

	public ProductWidgetItem setInfant(Integer infant) {
		this.infant = infant;
		return this;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public ProductWidgetItem setQuantity(Integer quantity) {
		this.quantity = quantity;
		return this;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public ProductWidgetItem setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
		return this;
	}

	public Double getRank() {
		return rank;
	}

	public ProductWidgetItem setRank(Double rank) {
		this.rank = rank;
		return this;
	}

	public Integer getRating() {
		return rating;
	}

	public ProductWidgetItem setRating(Integer rating) {
		this.rating = rating;
		return this;
	}

	public Integer getRefresh() {
		return refresh;
	}

	public ProductWidgetItem setRefresh(Integer refresh) {
		this.refresh = refresh;
		return this;
	}

	public Integer getBed() {
		return bed;
	}

	public ProductWidgetItem setBed(Integer bed) {
		this.bed = bed;
		return this;
	}

	public Integer getRoom() {
		return room;
	}

	public ProductWidgetItem setRoom(Integer room) {
		this.room = room;
		return this;
	}

	public Double getBathroom() {
		return bathroom;
	}

	public ProductWidgetItem setBathroom(Double bathroom) {
		this.bathroom = bathroom;
		return this;
	}

	public Integer getToilet() {
		return toilet;
	}

	public ProductWidgetItem setToilet(Integer toilet) {
		this.toilet = toilet;
		return this;
	}

	public Integer getFloor() {
		return floor;
	}

	public ProductWidgetItem setFloor(Integer floor) {
		this.floor = floor;
		return this;
	}

	public String getState() {
		return state;
	}

	public ProductWidgetItem setState(String state) {
		this.state = state;
		return this;
	}

	public String getSpace() {
		return space;
	}

	public ProductWidgetItem setSpace(String space) {
		this.space = space;
		return this;
	}

	public ProductConstants.SpaceUnitEnum getSpaceUnit() {
		return spaceUnit;
	}

	public ProductWidgetItem setSpaceUnit(ProductConstants.SpaceUnitEnum spaceUnit) {
		this.spaceUnit = spaceUnit;
		return this;
	}

	public String getServiceDays() {
		return serviceDays;
	}

	public ProductWidgetItem setServiceDays(String serviceDays) {
		this.serviceDays = serviceDays;
		return this;
	}

	public String getTax() {
		return tax;
	}

	public ProductWidgetItem setTax(String tax) {
		this.tax = tax;
		return this;
	}

	public String getUnit() {
		return unit;
	}

	public ProductWidgetItem setUnit(String unit) {
		this.unit = unit;
		return this;
	}

	public String getUnspsc() {
		return unspsc;
	}

	public ProductWidgetItem setUnspsc(String unspsc) {
		this.unspsc = unspsc;
		return this;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public ProductWidgetItem setWebAddress(String webAddress) {
		this.webAddress = webAddress;
		return this;
	}
}
