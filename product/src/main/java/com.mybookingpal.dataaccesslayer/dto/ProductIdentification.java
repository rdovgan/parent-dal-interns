package com.mybookingpal.dataaccesslayer.dto;

//TODO: temporary item for query
public class ProductIdentification {
	private String id;
	private String altid;
	private String name;

	public ProductIdentification() {
	}

	public ProductIdentification(String id, String altid, String name) {
		this.id = id;
		this.altid = altid;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public ProductIdentification setId(String id) {
		this.id = id;
		return this;
	}

	public String getAltid() {
		return altid;
	}

	public ProductIdentification setAltid(String altid) {
		this.altid = altid;
		return this;
	}

	public String getName() {
		return name;
	}

	public ProductIdentification setName(String name) {
		this.name = name;
		return this;
	}
}
