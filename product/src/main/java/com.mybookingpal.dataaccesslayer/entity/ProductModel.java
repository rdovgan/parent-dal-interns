package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.ProductConstants;

import java.sql.Time;
import java.util.Date;

public class ProductModel {
	protected Long id;
	protected String altSupplierId;
	protected Long partOfId;
	//TODO: @XStreamOmitField
	protected Long ownerId;
	protected Long locationId;
	//TODO: @XStreamAlias("pm-id")
	protected Long supplierId;
	//TODO: @XStreamOmitField
	protected String unspsc;
	protected String name;
	protected String type;
	protected String currency;
	protected String unit;
	protected Double latitude;
	protected Double longitude;
	protected Double altitude;
	//TODO: @XStreamOmitField
	protected String code;
	protected String webAddress;
	protected String physicalAddress;
	protected Long addressComponentsId;
	//TODO: @XStreamOmitField
	protected String tax;
	protected String altId;
	protected Long altPartyId;
	protected String serviceDays; // service weekdays Sum = 0
	protected Integer bed;
	protected Integer room;
	protected Double bathroom;
	protected Integer toilet;
	protected Integer floor;
	protected String state;
	protected Boolean bpValidation;
	protected String space;
	protected ProductConstants.SpaceUnitEnum spaceUnit;
	protected String options;
	protected Integer quantity;
	protected Integer standardPerson;
	protected Integer person;
	protected Integer child;
	protected Integer infant;
	protected Integer baby;
	protected Integer rating;
	//TODO: @XmlTransient
	protected Integer linenChange;
	//TODO: @XmlTransient
	protected Integer refresh;
	//TODO: @XmlTransient
	protected Double commission;
	//TODO: @XmlTransient
	protected Double discount;
	//TODO: @Deprecated
	protected Double securityDeposit;
	//TODO: @XStreamOmitField
	protected Boolean useOnePriceRow;
	protected Integer bookingSetting;
	protected String inquireState;
	protected Boolean displayAddress = true;
	protected Time checkInTime;
	protected Time checkInToTime;
	protected Time checkOutTime;
	protected String displayName;
	protected Boolean useDisplayName;
	protected String parentId;
	protected Long linkedId;
	protected String multiUnit;
	protected Boolean assignedToManager;
	//TODO: @XmlTransient
	protected Double ownerDiscount;
	//TODO: @XmlTransient
	protected Double rank;
	//TODO: @XmlTransient
	protected Boolean dynamicPricingEnabled;
	protected Date version;
	protected Boolean roomType;
	protected String productGroup;
	protected String phone;
	protected Double cleaningFee;
	protected String taxNumber;
	protected Integer livingPlace;
	protected Date touristLicenseExpiryDate;
	protected Integer ratingNumber;
	protected Date builtDate;
	protected Date renovatingDate;
	protected Date rentingDate;
	protected Boolean hostLocation;
	protected Double basePrice;


	public ProductModel() {
	}

	public ProductModel(ProductModel productModel) {
		if (productModel == null) {
			return;
		}
		this.id = productModel.getId();
		this.altSupplierId = productModel.getAltSupplierId();
		this.partOfId = productModel.getPartOfId();
		this.ownerId = productModel.getOwnerId();
		this.locationId = productModel.getLocationId();
		this.supplierId = productModel.getSupplierId();
		this.unspsc = productModel.getUnspsc();
		this.name = productModel.getName();
		this.type = productModel.getType();
		this.currency = productModel.getCurrency();
		this.unit = productModel.getUnit();
		this.latitude = productModel.getLatitude();
		this.longitude = productModel.getLongitude();
		this.altitude = productModel.getAltitude();
		this.code = productModel.getCode();
		this.webAddress = productModel.getWebAddress();
		this.physicalAddress = productModel.getPhysicalAddress();
		this.tax = productModel.getTax();
		this.altId = productModel.getAltId();
		this.altPartyId = productModel.getAltPartyId();
		this.serviceDays = productModel.getServiceDays();
		this.room = productModel.getRoom();
		this.bathroom = productModel.getBathroom();
		this.toilet = productModel.getToilet();
		this.floor = productModel.getFloor();
		this.state = productModel.getState();
		this.space = productModel.getSpace();
		this.options = productModel.getOptions();
		this.quantity = productModel.getQuantity();
		this.person = productModel.getPerson();
		this.standardPerson = productModel.getStandardPerson();
		this.child = productModel.getChild();
		this.baby = productModel.getBaby();
		this.infant = productModel.getInfant();
		this.rating = productModel.getRating();
		this.linenChange = productModel.getLinenChange();
		this.refresh = productModel.getRefresh();
		this.commission = productModel.getCommission();
		this.discount = productModel.getDiscount();
		this.securityDeposit = productModel.getSecurityDeposit();
		this.useOnePriceRow = productModel.getUseOnePriceRow();
		this.bookingSetting = productModel.getBookingSetting();
		this.inquireState = productModel.getInquireState();
		this.displayAddress = productModel.getDisplayAddress();
		this.checkInTime = productModel.getCheckInTime();
		this.checkOutTime = productModel.getCheckOutTime();
		this.assignedToManager = productModel.getAssignedToManager();
		this.ownerDiscount = productModel.getOwnerDiscount();
		this.rank = productModel.getRank();
		this.dynamicPricingEnabled = productModel.getDynamicPricingEnabled();
		this.version = productModel.getVersion();
		this.roomType = productModel.getRoomType();
		this.phone = productModel.getPhone();
		this.useDisplayName = productModel.isUseDisplayName();
		this.displayName = productModel.getDisplayName();
		this.addressComponentsId = productModel.getAddressComponentsId();
		this.bed = productModel.getBed();
		this.parentId = productModel.getParentId();
		this.linkedId = productModel.getLinkedId();
		this.multiUnit = productModel.getMultiUnit();
		this.productGroup = productModel.getProductGroup();
		this.spaceUnit = productModel.getSpaceUnit();
		this.livingPlace = productModel.getLivingPlace();
		this.touristLicenseExpiryDate = productModel.getTouristLicenseExpiryDate();
		this.builtDate = productModel.getBuiltDate();
		this.renovatingDate = productModel.getRenovatingDate();
		this.rentingDate = productModel.getRentingDate();
		this.hostLocation = productModel.getHostLocation();
		this.basePrice = productModel.basePrice;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getRoomType() {
		return roomType;
	}

	public void setRoomType(Boolean roomType) {
		this.roomType = roomType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAltSupplierId() {
		return altSupplierId;
	}

	public void setAltSupplierId(String altSupplierId) {
		this.altSupplierId = altSupplierId;
	}

	public Long getPartOfId() {
		return partOfId;
	}

	public void setPartOfId(Long partOfId) {
		this.partOfId = partOfId;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	//TODO: @Deprecated
	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getUnspsc() {
		return unspsc;
	}

	public void setUnspsc(String unspsc) {
		this.unspsc = unspsc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public Long getAltPartyId() {
		return altPartyId;
	}

	public void setAltPartyId(Long altPartyId) {
		this.altPartyId = altPartyId;
	}

	public String getServiceDays() {
		return serviceDays;
	}

	public void setServiceDays(String serviceDays) {
		this.serviceDays = serviceDays;
	}

	public Integer getBed() {
		return bed;
	}

	public void setBed(Integer bed) {
		this.bed = bed;
	}

	public Integer getRoom() {
		return room;
	}

	public void setRoom(Integer room) {
		this.room = room;
	}

	public Double getBathroom() {
		return bathroom;
	}

	public void setBathroom(Double bathroom) {
		this.bathroom = bathroom;
	}

	public Integer getToilet() {
		return toilet;
	}

	public void setToilet(Integer toilet) {
		this.toilet = toilet;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPerson() {
		return person;
	}

	public void setPerson(Integer person) {
		this.person = person;
	}

	public Integer getStandardPerson() {
		return standardPerson;
	}

	public void setStandardPerson(Integer standardPerson) {
		this.standardPerson = standardPerson;
	}

	public Integer getChild() {
		return child;
	}

	public void setChild(Integer child) {
		this.child = child;
	}

	public Integer getInfant() {
		return infant;
	}

	public void setInfant(Integer infant) {
		this.infant = infant;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getLinenChange() {
		return linenChange;
	}

	public void setLinenChange(Integer linenChange) {
		this.linenChange = linenChange;
	}

	public Integer getRefresh() {
		return refresh;
	}

	public void setRefresh(Integer refresh) {
		this.refresh = refresh;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	//TODO: @Derpecated
	public Double getSecurityDeposit() {
		return securityDeposit;
	}

	//TODO: @Derpecated
	public void setSecurityDeposit(Double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public Boolean getUseOnePriceRow() {
		return useOnePriceRow;
	}

	public void setUseOnePriceRow(Boolean useOnePriceRow) {
		this.useOnePriceRow = useOnePriceRow;
	}

	public Integer getBookingSetting() {
		return bookingSetting;
	}

	public void setBookingSetting(Integer bookingSetting) {
		this.bookingSetting = bookingSetting;
	}

	public String getInquireState() {
		return inquireState;
	}

	public void setInquireState(String inquireState) {
		this.inquireState = inquireState;
	}


	public Boolean getDisplayAddress() {
		return displayAddress;
	}

	public void setDisplayAddress(Boolean displayAddress) {
		this.displayAddress = displayAddress;
	}

	public Time getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Time checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Time getCheckInToTime() {
		return checkInToTime;
	}

	public void setCheckInToTime(Time checkInToTime) {
		this.checkInToTime = checkInToTime;
	}

	public Time getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Time checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Boolean isUseDisplayName() {
		return useDisplayName;
	}

	public void setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Long getLinkedId() {
		return linkedId;
	}

	public void setLinkedId(Long linkedId) {
		this.linkedId = linkedId;
	}

	public String getMultiUnit() {
		return multiUnit;
	}

	public void setMultiUnit(String multiUnit) {
		this.multiUnit = multiUnit;
	}

	public Boolean getAssignedToManager() {
		return assignedToManager;
	}

	public void setAssignedToManager(Boolean assignedToManager) {
		this.assignedToManager = assignedToManager;
	}

	public Double getOwnerDiscount() {
		return ownerDiscount;
	}

	public void setOwnerDiscount(Double ownerDiscount) {
		this.ownerDiscount = ownerDiscount;
	}

	public Double getRank() {
		return rank;
	}

	public void setRank(Double rank) {
		this.rank = rank;
	}

	public Boolean getDynamicPricingEnabled() {
		return dynamicPricingEnabled;
	}

	public void setDynamicPricingEnabled(Boolean dynamicPricingEnabled) {
		this.dynamicPricingEnabled = dynamicPricingEnabled;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public ProductConstants.SpaceUnitEnum getSpaceUnit() {
		return spaceUnit;
	}

	public void setSpaceUnit(ProductConstants.SpaceUnitEnum spaceUnit) {
		this.spaceUnit = spaceUnit;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public Long getAddressComponentsId() {
		return addressComponentsId;
	}

	public void setAddressComponentsId(Long addressComponentsId) {
		this.addressComponentsId = addressComponentsId;
	}

	public Integer getBaby() {
		return baby;
	}

	public void setBaby(Integer baby) {
		this.baby = baby;
	}

	public void setCleaningFee(Double cleaningFee) {
		this.cleaningFee = cleaningFee;
	}

	public Double getCleaningFee() {
		return cleaningFee;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public Integer getLivingPlace() {
		return livingPlace;
	}

	public void setLivingPlace(Integer livingPlace) {
		this.livingPlace = livingPlace;
	}

	public Date getTouristLicenseExpiryDate() {
		return touristLicenseExpiryDate;
	}

	public void setTouristLicenseExpiryDate(Date touristLicenseExpiryDate) {
		this.touristLicenseExpiryDate = touristLicenseExpiryDate;
	}

	public Integer getRatingNumber() {
		return ratingNumber;
	}

	public void setRatingNumber(Integer ratingNumber) {
		this.ratingNumber = ratingNumber;
	}

	public Date getBuiltDate() {
		return builtDate;
	}

	public void setBuiltDate(Date builtDate) {
		this.builtDate = builtDate;
	}

	public Date getRenovatingDate() {
		return renovatingDate;
	}

	public void setRenovatingDate(Date renovatingDate) {
		this.renovatingDate = renovatingDate;
	}

	public Date getRentingDate() {
		return rentingDate;
	}

	public void setRentingDate(Date rentingDate) {
		this.rentingDate = rentingDate;
	}

	public Boolean getHostLocation() {
		return hostLocation;
	}

	public void setHostLocation(Boolean hostLocation) {
		this.hostLocation = hostLocation;
	}

	public Double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}

	public Boolean getBpValidation() {
		return bpValidation;
	}

	public void setBpValidation(Boolean bpValidation) {
		this.bpValidation = bpValidation;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ProductModel{");
		sb.append("id=")
				.append(id);
		sb.append(", altSupplierId='")
				.append(altSupplierId)
				.append('\'');
		sb.append(", partOfId=")
				.append(partOfId);
		sb.append(", ownerId=")
				.append(ownerId);
		sb.append(", locationId=")
				.append(locationId);
		sb.append(", supplierId=")
				.append(supplierId);
		sb.append(", unspsc='")
				.append(unspsc)
				.append('\'');
		sb.append(", name='")
				.append(name)
				.append('\'');
		sb.append(", type='")
				.append(type)
				.append('\'');
		sb.append(", currency='")
				.append(currency)
				.append('\'');
		sb.append(", unit='")
				.append(unit)
				.append('\'');
		sb.append(", latitude=")
				.append(latitude);
		sb.append(", longitude=")
				.append(longitude);
		sb.append(", altitude=")
				.append(altitude);
		sb.append(", code='")
				.append(code)
				.append('\'');
		sb.append(", webAddress='")
				.append(webAddress)
				.append('\'');
		sb.append(", physicalAddress='")
				.append(physicalAddress)
				.append('\'');
		sb.append(", addressComponentsId=")
				.append(addressComponentsId);
		sb.append(", tax='")
				.append(tax)
				.append('\'');
		sb.append(", altId='")
				.append(altId)
				.append('\'');
		sb.append(", altPartyId=")
				.append(altPartyId);
		sb.append(", serviceDays='")
				.append(serviceDays)
				.append('\'');
		sb.append(", bed=")
				.append(bed);
		sb.append(", room=")
				.append(room);
		sb.append(", bathroom=")
				.append(bathroom);
		sb.append(", toilet=")
				.append(toilet);
		sb.append(", floor=")
				.append(floor);
		sb.append(", state='")
				.append(state)
				.append('\'');
		sb.append(", bpValidation=")
				.append(bpValidation);
		sb.append(", space='")
				.append(space)
				.append('\'');
		sb.append(", spaceUnit=")
				.append(spaceUnit);
		sb.append(", options='")
				.append(options)
				.append('\'');
		sb.append(", quantity=")
				.append(quantity);
		sb.append(", person=")
				.append(person);
		sb.append(", standardPerson=")
				.append(standardPerson);
		sb.append(", child=")
				.append(child);
		sb.append(", infant=")
				.append(infant);
		sb.append(", baby=")
				.append(baby);
		sb.append(", rating=")
				.append(rating);
		sb.append(", linenChange=")
				.append(linenChange);
		sb.append(", refresh=")
				.append(refresh);
		sb.append(", commission=")
				.append(commission);
		sb.append(", discount=")
				.append(discount);
		sb.append(", securityDeposit=")
				.append(securityDeposit);
		sb.append(", useOnePriceRow=")
				.append(useOnePriceRow);
		sb.append(", bookingSetting=")
				.append(bookingSetting);
		sb.append(", inquireState='")
				.append(inquireState)
				.append('\'');
		sb.append(", displayAddress=")
				.append(displayAddress);
		sb.append(", checkInTime=")
				.append(checkInTime);
		sb.append(", checkInToTime=")
				.append(checkInToTime);
		sb.append(", checkOutTime=")
				.append(checkOutTime);
		sb.append(", displayName='")
				.append(displayName)
				.append('\'');
		sb.append(", useDisplayName=")
				.append(useDisplayName);
		sb.append(", parentId='")
				.append(parentId)
				.append('\'');
		sb.append(", linkedId=")
				.append(linkedId);
		sb.append(", multiUnit='")
				.append(multiUnit)
				.append('\'');
		sb.append(", assignedToManager=")
				.append(assignedToManager);
		sb.append(", ownerDiscount=")
				.append(ownerDiscount);
		sb.append(", rank=")
				.append(rank);
		sb.append(", dynamicPricingEnabled=")
				.append(dynamicPricingEnabled);
		sb.append(", version=")
				.append(version);
		sb.append(", roomType=")
				.append(roomType);
		sb.append(", productGroup='")
				.append(productGroup)
				.append('\'');
		sb.append(", phone='")
				.append(phone)
				.append('\'');
		sb.append(", cleaningFee=")
				.append(cleaningFee);
		sb.append(", taxNumber='")
				.append(taxNumber)
				.append('\'');
		sb.append(", livingPlace=")
				.append(livingPlace);
		sb.append(", touristLicenseExpiryDate=")
				.append(touristLicenseExpiryDate);
		sb.append(", ratingNumber=")
				.append(ratingNumber);
		sb.append(", builtDate=")
				.append(builtDate);
		sb.append(", renovatingDate=")
				.append(renovatingDate);
		sb.append(", rentingDate=")
				.append(rentingDate);
		sb.append(", hostLocation=")
				.append(hostLocation);
		sb.append(", basePrice=")
				.append(basePrice);
		sb.append('}');
		return sb.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((altSupplierId == null) ? 0 : altSupplierId.hashCode());
		result = prime * result + ((assignedToManager == null) ? 0 : assignedToManager.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((multiUnit == null) ? 0 : multiUnit.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((person == null) ? 0 : person.hashCode());
		result = prime * result + ((physicalAddress == null) ? 0 : physicalAddress.hashCode());
		result = prime * result + ((productGroup == null) ? 0 : productGroup.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		result = prime * result + ((standardPerson == null) ? 0 : standardPerson.hashCode());
		result = prime * result + ((supplierId == null) ? 0 : supplierId.hashCode());
		result = prime * result + ((useDisplayName == null) ? 0 : useDisplayName.hashCode());
		result = prime * result + ((useOnePriceRow == null) ? 0 : useOnePriceRow.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductModel other = (ProductModel) obj;
		if (altSupplierId == null) {
			if (other.altSupplierId != null)
				return false;
		} else if (!altSupplierId.equals(other.altSupplierId))
			return false;
		if (assignedToManager == null) {
			if (other.assignedToManager != null)
				return false;
		} else if (!assignedToManager.equals(other.assignedToManager))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (multiUnit == null) {
			if (other.multiUnit != null)
				return false;
		} else if (!multiUnit.equals(other.multiUnit))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (person == null) {
			if (other.person != null)
				return false;
		} else if (!person.equals(other.person))
			return false;
		if (physicalAddress == null) {
			if (other.physicalAddress != null)
				return false;
		} else if (!physicalAddress.equals(other.physicalAddress))
			return false;
		if (productGroup == null) {
			if (other.productGroup != null)
				return false;
		} else if (!productGroup.equals(other.productGroup))
			return false;
		if (room == null) {
			if (other.room != null)
				return false;
		} else if (!room.equals(other.room))
			return false;
		if (standardPerson == null) {
			if (other.standardPerson != null)
				return false;
		} else if (!standardPerson.equals(other.standardPerson))
			return false;
		if (supplierId == null) {
			if (other.supplierId != null)
				return false;
		} else if (!supplierId.equals(other.supplierId))
			return false;
		if (useDisplayName == null) {
			if (other.useDisplayName != null)
				return false;
		} else if (!useDisplayName.equals(other.useDisplayName))
			return false;
		if (useOnePriceRow == null) {
			if (other.useOnePriceRow != null)
				return false;
		} else if (!useOnePriceRow.equals(other.useOnePriceRow))
			return false;
		return true;
	}
}