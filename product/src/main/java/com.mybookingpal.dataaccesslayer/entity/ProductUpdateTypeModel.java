package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class ProductUpdateTypeModel {

	protected Integer id;
	protected Integer productId;
	protected Boolean fees;
	protected Boolean tax;
	protected Boolean image;
	protected Boolean price;
	protected Boolean address;
	protected Date createdDate;
	protected Date version;
	protected Boolean rooms;
	protected Boolean bedroomConfig;
	protected Boolean policies;
	protected Boolean propertyName;
	protected Boolean displayName;
	protected Boolean currency;
	protected Boolean rating;
	protected Boolean space;
	protected Boolean bathrooms;
	protected Boolean toilet;
	protected Boolean persons;
	protected Boolean children;
	protected Boolean infant;
	protected Boolean baby;
	protected Boolean checkInTime;
	protected Boolean checkOutTime;
	protected Boolean mainDescription;
	protected Boolean shortDescription;
	protected Boolean houseRules;
	protected Boolean finePrint;
	protected Boolean bed;
	protected Boolean leadTime;
	protected Boolean productType;
	protected Boolean touristLicenseNumber;

	public ProductUpdateTypeModel() {
		this.fees = false;
		this.tax = false;
		this.image = false;
		this.price = false;
		this.address = false;
		this.rooms = false;
		this.bedroomConfig = false;
		this.createdDate = new Date();
		this.bedroomConfig = false;
		this.policies = false;
		this.propertyName = false;
		this.displayName = false;
		this.currency = false;
		this.rating = false;
		this.space = false;
		this.bathrooms = false;
		this.toilet = false;
		this.persons = false;
		this.children = false;
		this.infant = false;
		this.baby = false;
		this.checkInTime = false;
		this.checkOutTime = false;
		this.mainDescription = false;
		this.shortDescription = false;
		this.houseRules = false;
		this.bed = false;
		this.finePrint = false;
		this.bed = false;
		this.version = new Date();
		this.propertyName = false;
		this.leadTime = false;
		this.productType = false;
		this.touristLicenseNumber = false;
	}

	public ProductUpdateTypeModel(Boolean type, Integer productId) {
		this.productId = productId;
		this.fees = type;
		this.tax = type;
		this.image = type;
		this.price = type;
		this.address = type;
		this.rooms = type;
		this.bedroomConfig = type;
		this.createdDate = new Date();
		this.policies = type;
		this.propertyName = type;
		this.displayName = type;
		this.currency = type;
		this.rating = type;
		this.space = type;
		this.bathrooms = type;
		this.toilet = type;
		this.persons = type;
		this.children = type;
		this.infant = type;
		this.baby = type;
		this.checkInTime = type;
		this.checkOutTime = type;
		this.mainDescription = type;
		this.shortDescription = type;
		this.houseRules = type;
		this.bed = type;
		this.finePrint = type;
		this.version = new Date();
		this.productType = type;
		this.touristLicenseNumber = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Boolean getFees() {
		return fees;
	}

	public void setFees(Boolean fees) {
		this.fees = fees;
	}

	public Boolean getTax() {
		return tax;
	}

	public void setTax(Boolean tax) {
		this.tax = tax;
	}

	public Boolean getImage() {
		return image;
	}

	public void setImage(Boolean image) {
		this.image = image;
	}

	public Boolean getPrice() {
		return price;
	}

	public void setPrice(Boolean price) {
		this.price = price;
	}

	public Boolean getAddress() {
		return address;
	}

	public void setAddress(Boolean address) {
		this.address = address;
	}

	public Boolean getPolicies() {
		return policies;
	}

	public void setPolicies(Boolean policies) {
		this.policies = policies;
	}

	public Boolean getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(Boolean propertyName) {
		this.propertyName = propertyName;
	}

	public Boolean getDisplayName() {
		return displayName;
	}

	public void setDisplayName(Boolean displayName) {
		this.displayName = displayName;
	}

	public Boolean getCurrency() {
		return currency;
	}

	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}

	public Boolean getRating() {
		return rating;
	}

	public void setRating(Boolean rating) {
		this.rating = rating;
	}

	public Boolean getSpace() {
		return space;
	}

	public void setSpace(Boolean space) {
		this.space = space;
	}

	public Boolean getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(Boolean bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Boolean getToilet() {
		return toilet;
	}

	public void setToilet(Boolean toilet) {
		this.toilet = toilet;
	}

	public Boolean getPersons() {
		return persons;
	}

	public void setPersons(Boolean persons) {
		this.persons = persons;
	}

	public Boolean getChildren() {
		return children;
	}

	public void setChildren(Boolean children) {
		this.children = children;
	}

	public Boolean getInfant() {
		return infant;
	}

	public void setInfant(Boolean infant) {
		this.infant = infant;
	}

	public Boolean getBaby() {
		return baby;
	}

	public void setBaby(Boolean baby) {
		this.baby = baby;
	}

	public Boolean getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Boolean checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Boolean getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Boolean checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public Boolean getMainDescription() {
		return mainDescription;
	}

	public void setMainDescription(Boolean mainDescription) {
		this.mainDescription = mainDescription;
	}

	public Boolean getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(Boolean shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Boolean getHouseRules() {
		return houseRules;
	}

	public void setHouseRules(Boolean houseRules) {
		this.houseRules = houseRules;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Boolean getRooms() {
		return rooms;
	}

	public void setRooms(Boolean rooms) {
		this.rooms = rooms;
	}

	public Boolean getBedroomConfig() {
		return bedroomConfig;
	}

	public void setBedroomConfig(Boolean bedroomConfig) {
		this.bedroomConfig = bedroomConfig;
	}

	public Boolean getFinePrint() {
		return finePrint;
	}

	public void setFinePrint(Boolean finePrint) {
		this.finePrint = finePrint;
	}

	public Boolean getBed() {
		return bed;
	}

	public void setBed(Boolean bed) {
		this.bed = bed;
	}

	public Boolean getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(Boolean leadTime) {
		this.leadTime = leadTime;
	}

	public Boolean getProductType() {
		return productType;
	}

	public void setProductType(Boolean productType) {
		this.productType = productType;
	}

	public Boolean getTouristLicenseNumber() {
		return touristLicenseNumber;
	}

	public void setTouristLicenseNumber(Boolean touristLicenseNumber) {
		this.touristLicenseNumber = touristLicenseNumber;
	}


}
