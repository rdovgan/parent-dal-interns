package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.dataaccesslayer.constants.ProductConstants.SpaceUnitEnum;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(SpaceUnitEnum.class)
public class SpaceUnitEnumHandler extends EnumTypeHandler<SpaceUnitEnum> {

	public SpaceUnitEnumHandler() {
		super(SpaceUnitEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, SpaceUnitEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public SpaceUnitEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : SpaceUnitEnum.getByStr(value);
	}

	public SpaceUnitEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : SpaceUnitEnum.getByStr(value);
	}
}