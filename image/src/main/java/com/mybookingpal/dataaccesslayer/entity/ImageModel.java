package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.ImageProperties;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ImageModel {
	protected Integer id;
	protected String name;
	protected String oldName;
	protected String state;
	protected String type;
	protected String notes;
	protected String language;
	protected Integer productId;
	protected Byte[] data;
	protected String url;
	// this url is for hosted images
	private String urlMBP;
	protected Boolean standard = false;
	protected Boolean thumbnail = false;
	// 1 - image has type (e.g. "Main Image", "Exterior", "Interior" picture etc.)
	// 2 - default number for images which do not have any type.
	protected Integer sort;
	private Integer height;
	private Integer width;
	private String tags;
	private LocalDateTime version;
	private LocalDateTime createdDate;
	private String hash;
	private Integer partyId;
	private Integer size;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? " " : name;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlMBP() {
		return urlMBP;
	}

	public void setUrlMBP(String urlMBP) {
		this.urlMBP = urlMBP;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type.toLowerCase();
	}

	public void setType(ImageProperties.Type type) {
		this.type = type == null ? null : type.name();
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes == null ? " " : notes;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? "EN" : language;
	}

	public Byte[] getData() {
		return data;
	}

	public void setData(Byte[] data) {
		this.data = data;
	}

	public Boolean isStandard() {
		return standard;
	}

	public void setStandard(Boolean standard) {
		this.standard = standard;
	}

	public Boolean isThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Boolean thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setId(String id) {
		if (id != null) {
			this.id = Integer.valueOf(id);
		}
	}

	public String getId() {
		return (id != null) ? id.toString() : null;
	}

	public void setVersion(LocalDateTime inVersion) {
		version = inVersion;
	}

	public LocalDateTime getVersion() {
		return version;
	}

	public void setCreatedDate(LocalDateTime inCreatedDate) {
		createdDate = inCreatedDate;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setHash(String inHash) {
		hash = inHash;
	}

	public String getHash() {
		return hash;
	}

	public void setPartyId(Integer inPartyId) {
		partyId = inPartyId;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setSize(Integer inSize) {
		size = inSize;
	}

	public Integer getSize() {
		return size;
	}

	public void setState(String inState) {
		state = inState;
	}

	public String getState() {
		return state;
	}
}
