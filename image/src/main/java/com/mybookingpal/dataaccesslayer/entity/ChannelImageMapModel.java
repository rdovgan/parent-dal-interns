package com.mybookingpal.dataaccesslayer.entity;

import java.time.LocalDateTime;

public class ChannelImageMapModel {
	private Integer id;
	private String channelImageId;
	private String channelId;
	private String channelProductId;
	private String channelRoomId;
	private String productId;
	private Integer imageId;
	private Boolean status;
	private LocalDateTime version;
	private LocalDateTime created;

	public Integer getId() {
		return id;
	}

	public void setId(Integer inId) {
		id = inId;
	}

	public String getChannelImageId() {
		return channelImageId;
	}

	public void setChannelImageId(String inChannelImageId) {
		channelImageId = inChannelImageId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String inChannelId) {
		channelId = inChannelId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String inChannelProductId) {
		channelProductId = inChannelProductId;
	}

	public String getChannelRoomId() {
		return channelRoomId;
	}

	public void setChannelRoomId(String inChannelRoomId) {
		channelRoomId = inChannelRoomId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String inProductId) {
		productId = inProductId;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer inImageId) {
		imageId = inImageId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean inStatus) {
		status = inStatus;
	}

	public LocalDateTime getVersion() {
		return version;
	}

	public void setVersion(LocalDateTime inVersion) {
		version = inVersion;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime inCreated) {
		created = inCreated;
	}
}
