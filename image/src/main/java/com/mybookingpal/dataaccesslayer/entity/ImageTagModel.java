package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ImageTagModel {

	private Integer id;
	private String name;
	private Integer bookingComId;
	private String agodaId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBookingComId() {
		return bookingComId;
	}

	public void setBookingComId(Integer bookingComId) {
		this.bookingComId = bookingComId;
	}

	public String getAgodaId() {
		return agodaId;
	}

	public void setAgodaId(String agodaId) {
		this.agodaId = agodaId;
	}

	@Override
	public String toString() {
		return "ImageTag [id=" + id + ", name=" + name + ", bookingComId=" + bookingComId + ", agodaId= " + agodaId + "]";
	}

}
