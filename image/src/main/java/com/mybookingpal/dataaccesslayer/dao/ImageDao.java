package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ImageModel;
import com.mybookingpal.dataaccesslayer.dto.ImageSearch;
import com.mybookingpal.dataaccesslayer.entity.ImageTagModel;
import com.mybookingpal.dataaccesslayer.mappers.ImageMapper;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ImageDao {
	@Autowired
	ImageMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ImageModel action) {
		if (action != null) {
			mapper.create(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageModel read(String id) {
		if (StringUtils.isBlank(id)) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ImageModel action) {
		if (action != null) {
			mapper.update(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateTags(ImageModel action) {
		if (action != null) {
			mapper.updateTags(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateImageDimensions(List<ImageModel> images) {
		if (!CollectionUtils.isEmpty(images)) {
			mapper.updateImageDimensions(images);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> readByExample(ImageModel action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return mapper.readByExample(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelByExample(ImageModel action) {
		if (action != null) {
			mapper.cancelByExample(action);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelByProductId(String id) {
		if (!StringUtils.isBlank(id)) {
			mapper.cancelByProductId(id);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageModel defaultImageByProductId(NameId action) {
		if (action == null) {
			return null;
		}
		return mapper.defaultImageByProductId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> imagesByProductId(NameId action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return mapper.imagesByProductId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> imagesByProductIdSortOrder(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return mapper.imagesByProductIdSortOrder(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> productIdsOfImages() {
		return mapper.productIdsOfImages();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageSearch> listByProductIds(ImageSearch action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return mapper.listByProductIds(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageTagModel> readAllImageTags() {
		return mapper.readAllImageTags();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageTagModel> readImageTags(Integer id) {
		if (id == null) {
			return Collections.emptyList();
		}
		return mapper.readImageTags(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> readImagesWithoutDimensions(Integer count) {
		if (count == null) {
			return Collections.emptyList();
		}
		return mapper.readImagesWithoutDimensions(count);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyImages(Integer fromProductId, Integer toProductId) {
		if (fromProductId != null && toProductId != null) {
			mapper.copyImages(fromProductId, toProductId);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> readImageListByIdList(List<Integer> imageIdList) {
		if (CollectionUtils.isEmpty(imageIdList)) {
			return Collections.emptyList();
		}
		return mapper.readImageListByIdList(imageIdList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> getImagesById(Integer productId) {
		if (productId == null) {
			return Collections.emptyList();
		}
		return mapper.getImagesById(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> getAllImagesById(Integer productId) {
		if (productId == null) {
			return Collections.emptyList();
		}
		return mapper.getAllImagesById(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getTags(Integer imageId) {
		if (imageId == null) {
			return null;
		}
		return mapper.getTags(imageId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageTagModel> getTagsByIds(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return Collections.emptyList();
		}
		return mapper.getTagsByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getProductIdByImageId(Integer imageId) {
		if (imageId == null) {
			return null;
		}
		return mapper.getProductIdByImageId(imageId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageTagModel> getAllTags() {
		return mapper.getAllTags();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> getImagesFromLastTimeStamp(String productId, Date version) {
		if (StringUtils.isBlank(productId) || version == null) {
			return Collections.emptyList();
		}
		return mapper.getImagesFromLastTimeStamp(productId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> getImagesByProductIds(List<Integer> productId) {
		if (CollectionUtils.isEmpty(productId)) {
			return Collections.emptyList();
		}
		return mapper.getImagesByProductIds(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> getImagesByProductId(Integer productId) {
		if (productId == null) {
			return Collections.emptyList();
		}
		return mapper.getImagesByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageModel getImagesByUrl(Integer productId, String url) {
		if (productId == null || StringUtils.isBlank(url)) {
			return null;
		}
		return mapper.getImagesByUrl(productId, url);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getNumberImagesBySize(Integer productId, Integer height, Integer width) {
		if (productId == null || height == null || width == null) {
			return null;
		}
		return mapper.getNumberImagesBySize(productId, height, width);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void moveToFinalPartyImages(Integer partyId) {
		if (partyId != null) {
			mapper.moveToFinalPartyImages(partyId);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageModel readCurrentImageByPartyId(Integer partyId) {
		if (partyId == null) {
			return null;
		}
		return mapper.readCurrentImageByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateSize(List<ImageModel> images) {
		if (!CollectionUtils.isEmpty(images)) {
			mapper.updateSize(images);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createImageTag(ImageTagModel imageTagModel) {
		if (imageTagModel != null) {
			mapper.createImageTag(imageTagModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateImageTag(ImageTagModel imageTagModel) {
		if (imageTagModel != null) {
			mapper.updateImageTag(imageTagModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageTagModel readImageTag(String imageTagId) {
		if (StringUtils.isBlank(imageTagId)) {
			return null;
		}
		return mapper.readImageTag(imageTagId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ImageModel> imagesByProductIdRegardlessOfState(String productId) {
		if (StringUtils.isBlank(productId)) {
			return Collections.emptyList();
		}
		return mapper.imagesByProductIdRegardlessOfState(productId);
	}
}
