package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelImageMapModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelImageMapMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ChannelImageMapDao {
	@Autowired
	ChannelImageMapMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createImageMap(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel != null) {
			mapper.createImageMap(channelImageMapModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMap(Integer imageId) {
		if (imageId == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMap(imageId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelImageMapModel readImageMapByImageId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return null;
		}
		return mapper.readImageMapByImageId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelImageMapModel readImageMapByImageIdRoomId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return null;
		}
		return mapper.readImageMapByImageIdRoomId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateChannelImageId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel != null) {
			mapper.updateChannelImageId(channelImageMapModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateChannelImageMap(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel != null) {
			mapper.updateChannelImageMap(channelImageMapModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteById(String id) {
		if (!StringUtils.isBlank(id)) {
			mapper.deleteById(id);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteImageByImageId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel != null) {
			mapper.deleteImageByImageId(channelImageMapModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateByChannelImageIdChannelProductId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel != null) {
			mapper.updateStateByChannelImageIdChannelProductId(channelImageMapModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateByChannelImageIdsChannelProductId(String channelProductId, List<String> channelImageIds) {
		if (!StringUtils.isBlank(channelProductId) && channelImageIds != null)
			mapper.updateStateByChannelImageIdsChannelProductId(channelProductId, channelImageIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getPropertiesForUploadForChannel(Integer channelId) {
		if (channelId == null) {
			return Collections.emptyList();
		}
		return mapper.getPropertiesForUploadForChannel(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMapsByChannelProductIdAndChannelImageIdAndChannelId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMapsByChannelProductIdAndChannelImageIdAndChannelId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMapByProductIdChannelId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMapByProductIdChannelId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readAllImageMapByProductIdChannelId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readAllImageMapByProductIdChannelId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelImageMapModel readImageMapByProductIdChannelIdRoomIdImageId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return null;
		}
		return mapper.readImageMapByProductIdChannelIdRoomIdImageId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMapWithoutChannelImage(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMapWithoutChannelImage(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMapInactive(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMapInactive(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readAllImageMapByChannelProductIdRoomIdChannelId(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readAllImageMapByChannelProductIdRoomIdChannelId(channelImageMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelImageMapModel> readImageMapActive(ChannelImageMapModel channelImageMapModel) {
		if (channelImageMapModel == null) {
			return Collections.emptyList();
		}
		return mapper.readImageMapActive(channelImageMapModel);
	}
}
