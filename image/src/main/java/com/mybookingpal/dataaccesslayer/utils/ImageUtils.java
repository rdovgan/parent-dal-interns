package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.constants.ImageProperties;

public class ImageUtils {

	public static ImageProperties.Code getCode(String type) {
		if (type == null || type.isEmpty()) {
			return ImageProperties.Code.Public;
		}
		try {
			return ImageProperties.Code.valueOf(type);
		} catch (Throwable x) {
			return ImageProperties.Code.Public;
		}
	}

	/**
	 * Checks if the URL is a valid image file type, otherwise false.
	 *
	 * @param url the URL of the file.
	 * @return true, if a valid image file type, otherwise false.
	 */
	public static boolean isImageFile(String url) {
		if (url == null) {
			return false;
		}
		String filename = url.trim()
				.toLowerCase();
		return filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".bmp") || filename.endsWith(".gif") || filename.endsWith(".png");
	}

	/**
	 * Checks if the URL is not a valid image file type, otherwise false.
	 *
	 * @param url the URL of the file.
	 * @return true, if not a valid image file type, otherwise false.
	 */
	public static boolean notImageFile(String url) {
		return !isImageFile(url);
	}

	/**
	 * Gets the extension type of the specified filename.
	 *
	 * @param filename the specified filename.
	 */
	public static String getExtension(String filename) {
		String[] args = filename.split("\\.");
		return args[1].toLowerCase();
	}

	/*
	 * Get the specified image name for UI imported images
	 */
	public static String trimUIName(String value) {
		if (value == null || value.lastIndexOf('/') <= 0) {
			return (value);
		}
		return value.substring(value.lastIndexOf('/') + 1, value.length());
	}

	/**
	 * Gets a string with any HTML mark up removed.
	 *
	 * @param html the text from which HTML mark up is to be removed.
	 * @return text without mark up.
	 * @see <pre>http://developer.ean.com/faqs/Development</pre>
	 * TODO:
	 * xmlText=Replace(xmlText,"&amp;lt;","&lt;")
	 * xmlText=Replace(xmlText,"&amp;gt;","&gt;")
	 * xmlText=Replace(xmlText,"&amp;apos;","&apos;")
	 * xmlText=Replace(xmlText,"&amp;#x0A","")
	 * xmlText=Replace(xmlText,"&amp;#x0D","")
	 * xmlText=Replace(xmlText,"&#x0D","")
	 * xmlText=Replace(xmlText,"&#x0A","")
	 * xmlText=Replace(xmlText,"&amp;#x09","")
	 * xmlText=Replace(xmlText,"&amp;amp;amp;","&amp;")
	 * xmlText=Replace(xmlText,"&lt;br&gt;","<br />")
	 */
	public static String stripHTML(String html) {
		if (html == null || html.isEmpty()) {
			return html;
		}
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&amp;lt;", "&lt;");
		html = html.replaceAll("&amp;gt;", "&gt;");
		html = html.replaceAll("&amp;apos;", "&apos;");
		html = html.replaceAll("&amp;#x0A", "");
		html = html.replaceAll("&amp;#x0D", "");
		html = html.replaceAll("&#x0D", "");
		html = html.replaceAll("&#x0A", "");
		html = html.replaceAll("&amp;#x09", "");
		html = html.replaceAll("&amp;amp;amp;", "&amp;");
		html = html.replaceAll("&lt;br&gt;", "<br />");
		return html.replaceAll("\\<.*?>", ""); // alt return html.replaceAll("\\<.*?\\>", "");
	}

	 /*
	 *TODO
	public String getName(int length) {
		return NameIdUtils.trim(name, ",", length);
	}
	 */
}
