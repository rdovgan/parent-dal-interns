package com.mybookingpal.dataaccesslayer.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dataaccesslayer.entity.ChannelImageMapModel;

public interface ChannelImageMapMapper {

	void createImageMap(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readImageMap(Integer imageId);

	ChannelImageMapModel readImageMapByImageId(ChannelImageMapModel channelImageMapModel);

	ChannelImageMapModel readImageMapByImageIdRoomId(ChannelImageMapModel channelImageMapModel);

	void updateChannelImageId(ChannelImageMapModel channelImageMapModel);

	void updateChannelImageMap(ChannelImageMapModel channelImageMapModel);

	void deleteById(@Param("id") String id);

	void deleteImageByImageId(ChannelImageMapModel channelImageMapModel);

	void updateStateByChannelImageIdChannelProductId(ChannelImageMapModel channelImageMapModel);

	void updateStateByChannelImageIdsChannelProductId(@Param("channelProductId") String channelProductId, @Param("list") List<String> channelImageIds);

	List<Integer> getPropertiesForUploadForChannel(@Param("channelId") Integer channelId);

	List<ChannelImageMapModel> readImageMapsByChannelProductIdAndChannelImageIdAndChannelId(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readImageMapByProductIdChannelId(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readAllImageMapByProductIdChannelId(ChannelImageMapModel channelImageMapModel);

	ChannelImageMapModel readImageMapByProductIdChannelIdRoomIdImageId(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readImageMapWithoutChannelImage(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readImageMapInactive(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readAllImageMapByChannelProductIdRoomIdChannelId(ChannelImageMapModel channelImageMapModel);

	List<ChannelImageMapModel> readImageMapActive(ChannelImageMapModel channelImageMapModel);

}