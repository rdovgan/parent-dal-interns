package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;

import com.mybookingpal.dataaccesslayer.entity.ImageModel;
import com.mybookingpal.dataaccesslayer.dto.ImageSearch;
import com.mybookingpal.dataaccesslayer.entity.ImageTagModel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ImageMapper {
	void create(ImageModel action);

	ImageModel read(String id);

	void update(ImageModel action);

	void updateTags(ImageModel action);

	void updateImageDimensions(@Param("list") List<ImageModel> images);

	List<ImageModel> readByExample(ImageModel action);

	void cancelByExample(ImageModel action);

	void cancelByProductId(String id);

	ImageModel defaultImageByProductId(NameId action);

	List<ImageModel> imagesByProductId(NameId action);

	List<ImageModel> imagesByProductIdSortOrder(NameIdAction action);

	List<String> productIdsOfImages();

	List<ImageSearch> listByProductIds(ImageSearch action);

	List<ImageTagModel> readAllImageTags();

	List<ImageTagModel> readImageTags(Integer id);

	List<ImageModel> readImagesWithoutDimensions(@Param("count") Integer count);

	void copyImages(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	List<ImageModel> readImageListByIdList(List<Integer> imageIdList);

	List<ImageModel> getImagesById(Integer productId);

	List<ImageModel> getAllImagesById(Integer productId);

	String getTags(Integer imageId);

	List<ImageTagModel> getTagsByIds(List<Integer> ids);

	Integer getProductIdByImageId(Integer imageId);

	List<ImageTagModel> getAllTags();

	List<ImageModel> getImagesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);

	List<ImageModel> getImagesByProductIds(@Param("list") List<Integer> productId);

	List<ImageModel> getImagesByProductId(@Param("productId") Integer productId);

	ImageModel getImagesByUrl(@Param("productId") Integer productId, @Param("url") String url);

	Integer getNumberImagesBySize(@Param("productId") Integer productId, @Param("height") Integer height, @Param("width") Integer width);

	void moveToFinalPartyImages(@Param("id") Integer partyId);

	ImageModel readCurrentImageByPartyId(@Param("id") Integer partyId);

	void updateSize(@Param("list") List<ImageModel> images);

	void createImageTag(ImageTagModel imageTagModel);

	void updateImageTag(ImageTagModel imageTagModel);

	ImageTagModel readImageTag(String imageTagId);

	List<ImageModel> imagesByProductIdRegardlessOfState(String productId);
}
