package com.mybookingpal.dataaccesslayer.constants;

public class ImageProperties {
	public enum Code {Checkin, Contents, Condition, Contract, Contact, File, Inside, Options, Outside, Pictures, Private, Public, Service, Url}

	public enum State {Initial, Created, Final}

	public enum Type {Hosted, Linked, Blob}

	public enum Size {REGULAR, STANDARD, THUMB}
}
