package com.mybookingpal.dataaccesslayer.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.mybookingpal.dataaccesslayer.constants.ImageProperties;
import com.mybookingpal.dataaccesslayer.entity.ImageModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@XmlRootElement(name = "text")
@Component("Image")
@Scope(value = "prototype")
public class Image extends ImageModel {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Image other = (Image) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (productId == null) {
			if (other.productId != null) {
				return false;
			}
		} else if (!productId.equals(other.productId)) {
			return false;
		}
		return true;
	}

	protected Integer status = 0;
	private Byte[] imageData;
	private Boolean isValid;

	public Image() {
	}

	public Image(Integer id, String language) {
		this.id = id;
		this.language = language;
	}

	public Image(Integer id, String name, ImageProperties.Type type, String notes, String language) {
		this.id = id;
		this.name = name;
		this.state = ImageProperties.State.Created.name();
		this.type = type == null ?
				null :
				type.name()
						.toLowerCase();
		this.notes = notes;
		this.language = language;
	}

	public Image(String name, Integer productId, ImageProperties.Type type, String notes, String language, String url) {
		this.name = name;
		this.state = ImageProperties.State.Created.name();
		this.type = type == null ?
				null :
				type.name()
						.toLowerCase();
		this.notes = notes;
		this.language = language;
		this.productId = productId;
		this.url = url;
	}

	public Boolean isValid() {
		return isValid;
	}

	public void setValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public Boolean notType(ImageProperties.Type type) {
		return this.type == null || type == null || !this.type.equalsIgnoreCase(type.name());
	}

	public Boolean hasType(ImageProperties.Type type) {
		return !notType(type);
	}

	public String getPlainText() {
		if (notes == null) {
			return "";
		}
		return notes.replaceAll("\\<.*?>", "");
	}

	public Byte[] getImageData() {
		return imageData;
	}

	public void setImageData(Byte[] imageData) {
		this.imageData = imageData;
	}

	@Override
	public String toString() {
		return "Image [" + "status=" + status + ", id=" + id + ", name=" + name + ", oldName=" + oldName + ", state=" + state + ", type=" + type + ", notes="
				+ notes + ", language=" + language + ", productId=" + productId + ", url=" + url + ", urlMBP=" + getUrlMBP() + ", standard=" + standard
				+ ", thumbnail=" + thumbnail + ", sort=" + sort + ", height=" + getHeight() + ", width=" + getWidth() + ", tags=" + getTags() + ']';
	}
}

