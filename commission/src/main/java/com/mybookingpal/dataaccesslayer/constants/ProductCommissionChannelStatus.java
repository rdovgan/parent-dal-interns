package com.mybookingpal.dataaccesslayer.constants;

public enum ProductCommissionChannelStatus {
	Active(true), Inactive(false);

	private boolean value;

	ProductCommissionChannelStatus(boolean value) {
		this.value = value;
	}

	public boolean value() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}
}
