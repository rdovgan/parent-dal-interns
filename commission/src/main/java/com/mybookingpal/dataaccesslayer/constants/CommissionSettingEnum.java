package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum CommissionSettingEnum {

	Default(0, "Default"), NightlyRate(1, "NightlyRate"), NightlyRateWithFees(2, "NightlyRateWithFees");

	private Integer settingValue;
	private String settingName;

	CommissionSettingEnum(Integer settingValue, String settingName) {
		this.settingValue = settingValue;
		this.settingName = settingName;
	}

	public Integer getSettingValue() {
		return this.settingValue;
	}

	public String getSettingName() {
		return this.settingName;
	}

	public static CommissionSettingEnum getByValue(Integer value) {
		return Arrays.stream(CommissionSettingEnum.values())
				.filter(item -> item.getSettingValue().equals(value))
				.findFirst().orElse(Default);
	}
}
