package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.dataaccesslayer.constants.CommissionSettingEnum;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CommissionSettingEnumHandler extends EnumTypeHandler<CommissionSettingEnum> {

	public CommissionSettingEnumHandler() {
		super(CommissionSettingEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, CommissionSettingEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getSettingValue());
	}

	public CommissionSettingEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : CommissionSettingEnum.getByValue(value);
	}

	public CommissionSettingEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : CommissionSettingEnum.getByValue(value);
	}
}
