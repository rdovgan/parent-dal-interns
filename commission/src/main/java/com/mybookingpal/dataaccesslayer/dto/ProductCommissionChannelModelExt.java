package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductCommissionChannelModel;

public class ProductCommissionChannelModelExt extends ProductCommissionChannelModel {
	private Double defaultCommissionValue;

	public ProductCommissionChannelModelExt(Long productId, Integer channelPartnerId, Double commissionDelta, Boolean status, Double defaultCommissionValue) {
		super(productId, channelPartnerId, commissionDelta, status);
		this.defaultCommissionValue = defaultCommissionValue;
	}

	public Double getDefaultCommissionValue() {
		return defaultCommissionValue;
	}

	public void setDefaultCommissionValue(Double defaultCommissionValue) {
		this.defaultCommissionValue = defaultCommissionValue;
	}
}
