package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(value = "prototype")
public class CommissionModel {

	private Integer id;
	private Integer partyId;
	private Integer toPartyId;
	private Date startDate;
	private Date endDate;
	private Double value;
	private Boolean typeValue;
	private Integer type;
	private Date version;
	private Integer commissionSetting;

	public Integer getCommissionSetting() {
		return commissionSetting;
	}

	public void setCommissionSetting(Integer commissionSetting) {
		this.commissionSetting = commissionSetting;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public Integer getToPartyId() {
		return toPartyId;
	}

	public void setToPartyId(Integer toPartyId) {
		this.toPartyId = toPartyId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Boolean isTypeValue() {
		return typeValue;
	}

	public void setTypeValue(Boolean typeValue) {
		this.typeValue = typeValue;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

}
