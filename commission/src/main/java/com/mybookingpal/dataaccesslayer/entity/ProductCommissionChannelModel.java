package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class ProductCommissionChannelModel {
	private Long productId;
	private Integer channelPartnerId;
	private Double commissionDelta;
	private Boolean status;
	private Date version;
	private Date createdDate;

	public ProductCommissionChannelModel(Long productId, Integer channelPartnerId, Double commissionDelta, Boolean status) {
		this.productId = productId;
		this.channelPartnerId = channelPartnerId;
		this.commissionDelta = commissionDelta;
		this.status = status;
	}

	public ProductCommissionChannelModel() {
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public Double getCommissionDelta() {
		return commissionDelta;
	}

	public void setCommissionDelta(Double comissionDelta) {
		this.commissionDelta = comissionDelta;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	//TODO: rename methods
	public Date getVersin() {
		return version;
	}

	public void setVersin(Date version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
