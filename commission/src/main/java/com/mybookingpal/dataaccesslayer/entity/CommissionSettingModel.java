package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.CommissionSettingEnum;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class CommissionSettingModel {

	private Long id;
	private Integer propertyManagerId;
	private Integer channelPartnerPartyId;
	private Integer productId;
	private CommissionSettingEnum setting;
	private Integer type;

	public CommissionSettingModel() {
	}

	public CommissionSettingModel(Integer propertyManagerId, Integer channelPartnerPartyId, Integer productId, CommissionSettingEnum setting, Integer type) {
		this.propertyManagerId = propertyManagerId;
		this.channelPartnerPartyId = channelPartnerPartyId;
		this.productId = productId;
		this.setting = setting;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getChannelPartnerPartyId() {
		return channelPartnerPartyId;
	}

	public void setChannelPartnerPartyId(Integer channelPartnerPartyId) {
		this.channelPartnerPartyId = channelPartnerPartyId;
	}

	public CommissionSettingEnum getSetting() {
		return setting;
	}

	public void setSetting(CommissionSettingEnum setting) {
		this.setting = setting;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
