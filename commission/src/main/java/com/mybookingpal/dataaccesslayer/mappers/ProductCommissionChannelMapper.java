package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ProductCommissionChannelModel;
import com.mybookingpal.utils.entity.IdVersion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductCommissionChannelMapper {

	void createUpdateList(@Param("list") List<ProductCommissionChannelModel> productCommissionChannel);

	List<ProductCommissionChannelModel> read(@Param("productId") Long productId);

	ProductCommissionChannelModel readByProductAndChannelWithActiveStatus(@Param("productId") Long productId, @Param("channelId") Integer channelId);

	ProductCommissionChannelModel readActiveByProductAndChannelPartner(ProductCommissionChannelModel tempProductCommissionChannel);

	List<IdVersion> getMaxVersionForHACommissionPerProduct(
			@Param("channelId")Integer channelId, @Param("channelPartnerId") Integer channelPartnerId, @Param("productIds") List<Integer> productIds);
}
