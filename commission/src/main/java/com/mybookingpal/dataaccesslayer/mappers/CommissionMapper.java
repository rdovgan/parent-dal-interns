package com.mybookingpal.dataaccesslayer.mappers;

import java.util.Date;
import java.util.List;

import com.mybookingpal.dataaccesslayer.entity.CommissionModel;
import org.apache.ibatis.annotations.Param;

public interface CommissionMapper {

	void create(CommissionModel action);

	CommissionModel read(Integer id);

	void update(CommissionModel action);

	void updateCommissionByTypeAndPmId(CommissionModel action);

	void delete(Integer id);

	List<CommissionModel> readCommissionsListByPm(Integer pmId);

	List<CommissionModel> readBpCommissionsForPm(Integer pmId);

	List<CommissionModel> readBpSeasonalCommissionsForPmInDateRange(CommissionModel commission);

	List<CommissionModel> readPmIncludedCommissionsForPmInDateRange(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId,
			@Param("bookingDate") String bookingDate);

	List<CommissionModel> readPmIncludedCommissionsForPm(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId);

	List<CommissionModel> readCommissionsForPmInDateRange(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId,
			@Param("bookingDate") Date bookingDate, @Param("type") Integer type);

	List<CommissionModel> readCommissions(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId, @Param("type") Integer type);

	List<CommissionModel> readCommissionListByPmAndChannelPartnerPartyId(@Param("pmId") Integer propertyManagerId,
			@Param("channelPartnerPartyId") Integer channelPartnerPartyId);
}
