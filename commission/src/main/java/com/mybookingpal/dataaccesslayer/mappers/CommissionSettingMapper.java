package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.constants.CommissionSettingEnum;
import com.mybookingpal.dataaccesslayer.entity.CommissionSettingModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommissionSettingMapper {

	void create(CommissionSettingModel commissionSetting);

	void createList(@Param("list") List<CommissionSettingModel> commissionSettings);

	CommissionSettingModel read(Long id);

	CommissionSettingModel readBpCommissionByPmIdAndCpPartyId(CommissionSettingModel commissionSetting);

	CommissionSettingModel readBpCommissionByPmIdAndChannelPartyId(@Param("propertyManagerId") Integer pmId, @Param("channelPartnerPartyId") Integer channelId);

	void changeCommissionSetting(@Param("commissionSetting") CommissionSettingEnum commissionSettingEnum, @Param("pmId") Integer partyId,
			@Param("channelPartnerPartyId") Integer channelPartnerPartyId);

	CommissionSettingModel readChannelCommissionByPmIdChannelPartyIdAndProductId(@Param("propertyManagerId") Integer pmId,
			@Param("channelPartnerPartyId") Integer channelId, @Param("productId") Integer productId);

	List<CommissionSettingModel> readChannelCommissionByPmIdAndProductId(@Param("propertyManagerId") Integer pmId, @Param("productId") Integer productId);

	void deleteChannelCommissionSettingByProductId(@Param("productId") Integer productId);

}
