package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.CommissionModel;
import com.mybookingpal.dataaccesslayer.mappers.CommissionMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CommissionDao {
	private final CommissionMapper mapper;

	public CommissionDao(CommissionMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CommissionModel commissionModel) {
		if (Objects.isNull(commissionModel)) {
			return;
		}
		mapper.create(commissionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CommissionModel read(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CommissionModel commissionModel) {
		if (Objects.isNull(commissionModel)) {
			return;
		}
		mapper.update(commissionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateCommissionByTypeAndPmId(CommissionModel commissionModel) {
		if (Objects.isNull(commissionModel)) {
			return;
		}
		mapper.updateCommissionByTypeAndPmId(commissionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (Objects.isNull(id)) {
			return;
		}
		mapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionModel> readCommissionsListByPm(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return mapper.readCommissionsListByPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionModel> readBpCommissionsForPm(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return mapper.readBpCommissionsForPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionModel> readBpSeasonalCommissionsForPmInDateRange(CommissionModel commissionModel) {
		if (Objects.isNull(commissionModel)) {
			return Collections.emptyList();
		}
		return mapper.readBpSeasonalCommissionsForPmInDateRange(commissionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionModel> readPmIncludedCommissionsForPmInDateRange(
			Integer partyId, Integer toPartyId, String bookingDate) {
		if (Objects.isNull(partyId) || Objects.isNull(toPartyId)) {
			return Collections.emptyList();
		}
		return mapper.readPmIncludedCommissionsForPmInDateRange(partyId, toPartyId, bookingDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionModel> readPmIncludedCommissionsForPm(Integer partyId, Integer toPartyId) {
		if (Objects.isNull(partyId) || Objects.isNull(toPartyId)) {
			return Collections.emptyList();
		}
		return mapper.readPmIncludedCommissionsForPm(partyId, toPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<CommissionModel> readCommissionsForPmInDateRange(
			Integer partyId, Integer toPartyId, Date bookingDate, Integer type) {
		if (Objects.isNull(partyId) || Objects.isNull(toPartyId) || Objects.isNull(type)) {
			return Collections.emptyList();
		}
		return mapper.readCommissionsForPmInDateRange(partyId, toPartyId, bookingDate, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<CommissionModel> readCommissions(Integer partyId, Integer toPartyId, Integer type) {
		if (Objects.isNull(partyId) || Objects.isNull(toPartyId) || Objects.isNull(type)) {
			return Collections.emptyList();
		}
		return mapper.readCommissions(partyId, toPartyId, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<CommissionModel> readCommissionListByPmAndChannelPartnerPartyId(
			Integer propertyManagerId, Integer channelPartnerPartyId) {
		if (Objects.isNull(propertyManagerId) || Objects.isNull(channelPartnerPartyId)) {
			return Collections.emptyList();
		}
		return mapper.readCommissionListByPmAndChannelPartnerPartyId(propertyManagerId, channelPartnerPartyId);
	}
}
