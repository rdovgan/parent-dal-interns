package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ProductCommissionChannelModel;
import com.mybookingpal.dataaccesslayer.mappers.ProductCommissionChannelMapper;
import com.mybookingpal.utils.entity.IdVersion;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ProductCommissionChannelDao {
	private final ProductCommissionChannelMapper mapper;

	public ProductCommissionChannelDao(ProductCommissionChannelMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductCommissionChannelModel> read(Long productId) {
		if (Objects.isNull(productId)) {
			return null;
		}
		return mapper.read(productId);
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createUpdateList(List<ProductCommissionChannelModel> list) {
		if (CollectionUtils.isEmpty(list) || list.contains(null)) {
			return;
		}
		mapper.createUpdateList(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductCommissionChannelModel readByProductAndChannelWithActiveStatus(Long productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return null;
		}
		return mapper.readByProductAndChannelWithActiveStatus(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductCommissionChannelModel readActiveByProductAndChannelPartner(ProductCommissionChannelModel productCommissionChannel) {
		if (Objects.isNull(productCommissionChannel)) {
			return null;
		}
		return mapper.readActiveByProductAndChannelPartner(productCommissionChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdVersion> getMaxVersionForHACommissionPerProduct(Integer channelId, Integer channelPartnerId, List<Integer> productIds) {
		if (Objects.isNull(channelId) || Objects.isNull(channelPartnerId) || CollectionUtils.isEmpty(productIds)) {
			return null;
		}
		return mapper.getMaxVersionForHACommissionPerProduct(channelId, channelPartnerId, productIds);
	}
}
