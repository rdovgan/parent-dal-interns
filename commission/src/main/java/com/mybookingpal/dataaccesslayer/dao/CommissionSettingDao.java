package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.constants.CommissionSettingEnum;
import com.mybookingpal.dataaccesslayer.entity.CommissionSettingModel;
import com.mybookingpal.dataaccesslayer.mappers.CommissionSettingMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CommissionSettingDao {
	private final CommissionSettingMapper mapper;

	public CommissionSettingDao(CommissionSettingMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CommissionSettingModel commissionSettingModel) {
		if (Objects.isNull(commissionSettingModel)) {
			return;
		}
		mapper.create(commissionSettingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createList(List<CommissionSettingModel> list) {
		if (CollectionUtils.isEmpty(list)) {
			return;
		}
		mapper.createList(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CommissionSettingModel read(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CommissionSettingModel readBpCommissionByPmIdAndCpPartyId(CommissionSettingModel commissionSettingModel) {
		if (Objects.isNull(commissionSettingModel)) {
			return null;
		}
		return mapper.readBpCommissionByPmIdAndCpPartyId(commissionSettingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CommissionSettingModel readBpCommissionByPmIdAndChannelPartyId(Integer pmId, Integer channelId) {
		return mapper.readBpCommissionByPmIdAndChannelPartyId(pmId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void changeCommissionSetting(CommissionSettingEnum settingEnum, Integer pmId, Integer channelPartnerPartyId) {
		if (Objects.isNull(settingEnum) || Objects.isNull(pmId) || Objects.isNull(channelPartnerPartyId)) {
			return;
		}
		mapper.changeCommissionSetting(settingEnum, pmId, channelPartnerPartyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CommissionSettingModel readChannelCommissionByPmIdChannelPartyIdAndProductId(Integer pmId, Integer channelId, Integer productId) {
		return mapper.readChannelCommissionByPmIdChannelPartyIdAndProductId(pmId, channelId, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CommissionSettingModel> readChannelCommissionByPmIdAndProductId(Integer pmId, Integer productId) {
		if (Objects.isNull(pmId)) {
			return null;
		}
		return mapper.readChannelCommissionByPmIdAndProductId(pmId, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteChannelCommissionSettingByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return;
		}
		mapper.deleteChannelCommissionSettingByProductId(productId);
	}
}
