package com.mybookingpal.dataaccesslayer.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface IsPrice {

	Long getId();

	Long getAltId();

	String getEntityType();

	Long getEntityId();

	Long getPartyId();

	String getName();

	String getState();

	String getType();

	LocalDate getDate();

	LocalDate getToDate();

	Double getQuantity();

	String getUnit();

	BigDecimal getValue();

	BigDecimal getMinimum();

	BigDecimal getFactor();

	String getCurrency();

	BigDecimal getCost();

	Integer getAvailable();

	Integer getMinStay();

	Integer getMaxStay();

	Long getSupplierId();

	String getPayer();

	String getRule();

	LocalDateTime getVersion();

	LocalDateTime getCreated();

	Long getRatePlanId();
}
