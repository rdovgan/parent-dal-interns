package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.IsPrice;
import com.mybookingpal.dataaccesslayer.entity.RatePlanModel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface RatePlanMapper {

	void create(RatePlanModel ratePlanModel);

	RatePlanModel read(Long id);

	void update(RatePlanModel ratePlanModel);

	void deleteById(Long ratePlanId);

	RatePlanModel readByRateCode(String rateCode);

	List<RatePlanModel> getRatePlansForProductId(Long productId);

	List<RatePlanModel> listByProductIds(@Param("productIds") List<Long> productIds);

	RatePlanModel readCreatedRatePlan(Long id);

	void makeToFinalById(Long ratePlanId);

	RatePlanModel readByRateCodeAndPmId(@Param("rateCode") String rateCode, @Param("pmId") Long pmId);

	List<IsPrice> getRatePlansWithDatesForProductId(Long productId);

	List<RatePlanModel> listByProductIdAndDates(@Param("productId") Long productIds, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

}
