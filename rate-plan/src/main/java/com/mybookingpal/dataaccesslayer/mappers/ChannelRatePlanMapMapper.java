package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelRatePlanMapModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChannelRatePlanMapMapper {

	void create(ChannelRatePlanMapModel ratePlanMap);

	/**
	 * @deprecated use {@link #readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(String channelId, String ratePlanId, String productId)} instead
	 */
	@Deprecated
	ChannelRatePlanMapModel readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ChannelRatePlanMapModel ratePlanMap);

	ChannelRatePlanMapModel readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(@Param("channelId") String channelId,
			@Param("ratePlanId") String ratePlanId, @Param("productId") String productId);

	void updateState(ChannelRatePlanMapModel ratePlanMap);

	/**
	 * @deprecated use {@link #readByChannelIdAndProductId(String productId, String channelId)} instead
	 */
	@Deprecated
	List<ChannelRatePlanMapModel> readByChannelIdAndProductId(ChannelRatePlanMapModel ratePlanMap);

	List<ChannelRatePlanMapModel> readByChannelIdAndProductId(@Param("productId") String productId, @Param("channelId") String channelId);

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelProductId(String channelId, String channelProductId)} instead
	 */
	@Deprecated
	List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductId(ChannelRatePlanMapModel ratePlanMap);

	List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductId(@Param("channelId") String channelId, @Param("channelProductId") String channelProductId);

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelProductAndRoomId(String channelId, String channelProductId, String channelRoomId)} instead
	 */
	@Deprecated
	List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductAndRoomId(ChannelRatePlanMapModel ratePlanMap);

	List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductAndRoomId(@Param("channelId") String channelId,
			@Param("channelProductId") String channelProductId, @Param("channelRoomId") String channelRoomId);

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelRatePlanId(String channelId, String channelRatePlanId)} instead
	 */
	@Deprecated
	ChannelRatePlanMapModel readByChannelIdAndChannelRatePlanId(ChannelRatePlanMapModel ratePlanMap);

	ChannelRatePlanMapModel readByChannelIdAndChannelRatePlanId(@Param("channelId") String channelId, @Param("channelRatePlanId") String channelRatePlanId);

	/**
	 * @deprecated use {@link #findByChannelProductIdAndRateId(String channelProductId, String channelRatePlanId)} instead
	 */
	@Deprecated
	ChannelRatePlanMapModel findByChannelProductIdAndRateId(ChannelRatePlanMapModel channelProductMap);

	ChannelRatePlanMapModel findByChannelProductIdAndRateId(@Param("channelProductId") String channelProductId,
			@Param("channelRatePlanId") String channelRatePlanId);

	/**
	 * @deprecated use {@link #findByBPProductAndChannelForExpedia(String productId, List list)} instead
	 */
	@Deprecated
	ChannelRatePlanMapModel findByBPProductAndChannelForExpedia(ChannelRatePlanMapModel channelProductMap);

	ChannelRatePlanMapModel findByBPProductAndChannelForExpedia(@Param("productId") String productId, @Param("list") List<String> list);

	void update(ChannelRatePlanMapModel ratePlanMap);

	/**
	 * @deprecated use {@link #readByProductIdChannelId(String channelId, String productId)} instead
	 */
	@Deprecated
	List<ChannelRatePlanMapModel> readByProductIdChannelId(ChannelRatePlanMapModel ratePlanMap);

	List<ChannelRatePlanMapModel> readByProductIdChannelId(@Param("channelId") String channelId, @Param("productId") String productId);

	//TODO: Fix naming
	ChannelRatePlanMapModel findByProductIdAndChannelIdsAndRateplanIdNull(@Param("productId") String productId, @Param("list") List<Integer> channelIds);

	List<ChannelRatePlanMapModel> getAllProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);

	List<Integer> getChannelIdsByProductId(@Param("productId") String productId);

	List<Integer> getChannelIdsBySupplierId(@Param("supplierId") String supplierId);

	ChannelRatePlanMapModel findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds);

	List<ChannelRatePlanMapModel> findByProductIdsChannelIds(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);

}
