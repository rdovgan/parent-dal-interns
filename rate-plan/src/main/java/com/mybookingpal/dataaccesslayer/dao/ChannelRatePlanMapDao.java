package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelRatePlanMapModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelRatePlanMapMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelRatePlanMapDao {

	private final ChannelRatePlanMapMapper mapper;

	public ChannelRatePlanMapDao(ChannelRatePlanMapMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return;
		}

		mapper.create(ratePlanMap);
	}

	/**
	 * @deprecated use {@link #readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(String channelId, String ratePlanId, String productId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return null;
		}

		return readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ratePlanMap.getChannelId(), ratePlanMap.getRatePlanId(), ratePlanMap.getProductId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(String channelId, String ratePlanId, String productId) {
		if (Objects.isNull(channelId) || Objects.isNull(ratePlanId) || Objects.isNull(productId)) {
			return null;
		}

		return mapper.readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(channelId, ratePlanId, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateState(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return;
		}

		mapper.updateState(ratePlanMap);
	}

	/**
	 * @deprecated use {@link #readByChannelIdAndProductId(String productId, String channelId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndProductId(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return Collections.emptyList();
		}

		return readByChannelIdAndProductId(ratePlanMap.getProductId(), ratePlanMap.getChannelId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndProductId(String productId, String channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}

		return mapper.readByChannelIdAndProductId(productId, channelId);
	}

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelProductId(String channelId, String channelProductId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductId(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return Collections.emptyList();
		}

		return readByChannelIdAndChannelProductId(ratePlanMap.getChannelId(), ratePlanMap.getChannelProductId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductId(String channelId, String channelProductId) {
		if (Objects.isNull(channelId) || Objects.isNull(channelProductId)) {
			return Collections.emptyList();
		}

		return mapper.readByChannelIdAndChannelProductId(channelId, channelProductId);
	}

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelProductAndRoomId(String channelId, String channelProductId, String channelRoomId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductAndRoomId(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return Collections.emptyList();
		}
		return readByChannelIdAndChannelProductAndRoomId(ratePlanMap.getChannelId(), ratePlanMap.getChannelProductId(), ratePlanMap.getChannelRoomId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByChannelIdAndChannelProductAndRoomId(String channelId, String channelProductId, String channelRoomId) {
		if (Objects.isNull(channelId) || Objects.isNull(channelProductId) || Objects.isNull(channelRoomId)) {
			return Collections.emptyList();
		}
		return mapper.readByChannelIdAndChannelProductAndRoomId(channelId, channelProductId, channelRoomId);
	}

	/**
	 * @deprecated use {@link #readByChannelIdAndChannelRatePlanId(String channelId, String channelRatePlanId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel readByChannelIdAndChannelRatePlanId(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return null;
		}

		return readByChannelIdAndChannelRatePlanId(ratePlanMap.getChannelId(), ratePlanMap.getChannelRatePlanId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel readByChannelIdAndChannelRatePlanId(String channelId, String channelRatePlanId) {
		if (Objects.isNull(channelId) || Objects.isNull(channelRatePlanId)) {
			return null;
		}

		return mapper.readByChannelIdAndChannelRatePlanId(channelId, channelRatePlanId);
	}

	/**
	 * @deprecated use {@link #findByChannelProductIdAndRateId(String channelProductId, String channelRatePlanId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel findByChannelProductIdAndRateId(ChannelRatePlanMapModel channelProductMap) {
		if (Objects.isNull(channelProductMap)) {
			return null;
		}

		return findByChannelProductIdAndRateId(channelProductMap.getChannelProductId(), channelProductMap.getChannelRatePlanId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel findByChannelProductIdAndRateId(String channelProductId, String channelRatePlanId) {
		if (Objects.isNull(channelProductId) || Objects.isNull(channelRatePlanId)) {
			return null;
		}

		return mapper.findByChannelProductIdAndRateId(channelProductId, channelRatePlanId);
	}

	/**
	 * @deprecated use {@link #findByBPProductAndChannelForExpedia(String productId, List list)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel findByBPProductAndChannelForExpedia(ChannelRatePlanMapModel channelProductMap) {
		if (Objects.isNull(channelProductMap)) {
			return null;
		}

		return findByBPProductAndChannelForExpedia(channelProductMap);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRatePlanMapModel findByBPProductAndChannelForExpedia(String productId, List<String> list) {
		if (Objects.isNull(productId) || Objects.isNull(list)) {
			return null;
		}

		return mapper.findByBPProductAndChannelForExpedia(productId, list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return;
		}

		mapper.update(ratePlanMap);
	}

	/**
	 * @deprecated use {@link #readByProductIdChannelId(String channelId, String productId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByProductIdChannelId(ChannelRatePlanMapModel ratePlanMap) {
		if (Objects.isNull(ratePlanMap)) {
			return Collections.emptyList();
		}

		return readByProductIdChannelId(ratePlanMap.getChannelId(), ratePlanMap.getProductId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> readByProductIdChannelId(String channelId, String productId) {
		if (Objects.isNull(channelId) || Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.readByProductIdChannelId(channelId, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public ChannelRatePlanMapModel findByProductIdAndChannelIdsAndRateplanIdNull(String productId, List<Integer> channelIds) {
		if (Objects.isNull(productId) || Objects.isNull(channelIds)) {
			return null;
		}

		return mapper.findByProductIdAndChannelIdsAndRateplanIdNull(productId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> getAllProductByPmAndByChannel(String supplierId, List<Integer> channelIds) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}

		return mapper.getAllProductByPmAndByChannel(supplierId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getChannelIdsByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getChannelIdsByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getChannelIdsBySupplierId(String supplierId) {
		if (Objects.isNull(supplierId)) {
			return Collections.emptyList();
		}

		return mapper.getChannelIdsBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public ChannelRatePlanMapModel findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(List<String> productIds,
			List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return null;
		}

		return mapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(productIds, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRatePlanMapModel> findByProductIdsChannelIds(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}

		return mapper.findByProductIdsChannelIds(productIds, channelIds);
	}

}
