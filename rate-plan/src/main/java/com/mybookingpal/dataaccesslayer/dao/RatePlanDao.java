package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.IsPrice;
import com.mybookingpal.dataaccesslayer.entity.RatePlanModel;
import com.mybookingpal.dataaccesslayer.mappers.RatePlanMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class RatePlanDao {

	private final RatePlanMapper mapper;

	public RatePlanDao(RatePlanMapper ratePlanMapper) {
		this.mapper = ratePlanMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(RatePlanModel ratePlanModel) {
		if (Objects.isNull(ratePlanModel)) {
			return;
		}

		mapper.create(ratePlanModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RatePlanModel read(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}

		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(RatePlanModel ratePlanModel) {
		if (Objects.isNull(ratePlanModel) || Objects.isNull(ratePlanModel.getId())) {
			return;
		}

		mapper.update(ratePlanModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteById(Long ratePlanId) {
		if (Objects.isNull(ratePlanId)) {
			return;
		}

		mapper.deleteById(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RatePlanModel readByRateCode(String rateCode) {
		if (Objects.isNull(rateCode)) {
			return null;
		}

		return mapper.readByRateCode(rateCode);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RatePlanModel> getRatePlansForProductId(Long productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getRatePlansForProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RatePlanModel> listByProductIds(List<Long> productIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}

		List<Long> clearProductIdList = productIds.stream().filter(Objects::nonNull).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(clearProductIdList)) {
			return null;
		}

		return mapper.listByProductIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RatePlanModel readCreatedRatePlan(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}

		return mapper.readCreatedRatePlan(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void makeToFinalById(Long ratePlanId) {
		if (Objects.isNull(ratePlanId)) {
			return;
		}

		mapper.makeToFinalById(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RatePlanModel readByRateCodeAndPmId(String rateCode, Long pmId) {
		if (Objects.isNull(rateCode) || Objects.isNull(pmId)) {
			return null;
		}

		return mapper.readByRateCodeAndPmId(rateCode, pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsPrice> getRatePlansWithDatesForProductId(Long productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getRatePlansWithDatesForProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RatePlanModel> listByProductIdAndDates(Long productIds, Date fromDate, Date toDate) {
		if (Objects.isNull(productIds) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}

		return mapper.listByProductIdAndDates(productIds, fromDate, toDate);
	}

}
