package com.mybookingpal.dataaccesslayer.constants;

public enum ChannelRatePlanMapState {
	CREATED, FINAL, SUSPENDED
}
