package com.mybookingpal.dataaccesslayer.constants;

public enum RatePlanModelState {
	CREATED, FINAL
}
