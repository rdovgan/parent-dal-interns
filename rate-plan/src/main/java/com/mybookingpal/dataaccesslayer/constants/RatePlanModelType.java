package com.mybookingpal.dataaccesslayer.constants;

public enum RatePlanModelType {
	BASE_RATE, CHANNEL_RATE
}
