package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.RatePlanModelState;
import com.mybookingpal.dataaccesslayer.constants.RatePlanModelType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;

public class RatePlanModel {
	private Long id;
	private String code;
	private String name;
	private RatePlanModelState state;
	private RatePlanModelType type;
	private Integer channelId;
	private LocalDate created;
	private LocalDate version;

	public RatePlanModel() {
	}

	public RatePlanModel(RatePlanModel ratePlan) {
		this.id = ratePlan.getId();
		this.code = ratePlan.getCode();
		this.name = ratePlan.getName();
		this.state = ratePlan.getState();
		this.type = ratePlan.getType();
		this.channelId = ratePlan.getChannelId();
		this.created = ratePlan.getCreated();
		this.version = ratePlan.getVersion();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RatePlanModelState getState() {
		return state;
	}

	public void setState(RatePlanModelState state) {
		this.state = state;
	}

	public RatePlanModelType getType() {
		return type;
	}

	public void setType(RatePlanModelType type) {
		this.type = type;
	}

	public LocalDate getCreated() {
		return created;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getVersion() {
		return version;
	}

	public void setVersion(LocalDate version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof RatePlanModel))
			return false;

		RatePlanModel that = (RatePlanModel) o;

		return new EqualsBuilder().append(getId(), that.getId()).append(getCode(), that.getCode()).append(getName(), that.getName())
				.append(getState(), that.getState()).append(getChannelId(), that.getChannelId()).append(getCreated(), that.getCreated())
				.append(getVersion(), that.getVersion()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(getId()).append(getCode()).append(getName()).append(getState()).append(getChannelId()).append(getCreated())
				.append(getVersion()).toHashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("RatePlanModel{");
		sb.append("id=").append(id);
		sb.append(", code=").append(code);
		sb.append(", name=").append(name);
		sb.append(", state=").append(state);
		sb.append(", channelId=").append(channelId);
		sb.append(", created=").append(created);
		sb.append(", version=").append(version);
		sb.append('}');
		return sb.toString();
	}
}
