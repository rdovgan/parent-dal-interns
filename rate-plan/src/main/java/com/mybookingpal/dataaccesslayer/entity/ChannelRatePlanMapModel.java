package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.ChannelRatePlanMapState;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Scope(value = "prototype")
public class ChannelRatePlanMapModel {

	private Long id;
	private String productId;
	private String channelProductId;
	private String ratePlanId;
	private String channelRatePlanId;
	private String channelId;
	private String channelRoomId;
	private ChannelRatePlanMapState state;
	private Date created;
	private Date version;
	private String ratePlanModel;
	private List<String> list;

	public ChannelRatePlanMapModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public String getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(String ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public String getChannelRatePlanId() {
		return channelRatePlanId;
	}

	public void setChannelRatePlanId(String channelRatePlanId) {
		this.channelRatePlanId = channelRatePlanId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public ChannelRatePlanMapState getState() {
		return state;
	}

	public void setState(ChannelRatePlanMapState state) {
		this.state = state;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public String getChannelRoomId() {
		return channelRoomId;
	}

	public void setChannelRoomId(String channelRoomId) {
		this.channelRoomId = channelRoomId;
	}

	public String getRatePlanModel() {
		return ratePlanModel;
	}

	public void setRatePlanModel(String ratePlanModel) {
		this.ratePlanModel = ratePlanModel;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("productId", productId).append("channelProductId", channelProductId)
				.append("ratePlanId", ratePlanId).append("channelRatePlanId", channelRatePlanId).append("channelRoomId", channelRoomId)
				.append("channelId", channelId).append("state", state).append("created", created).append("version", version)
				.append("ratePlanModel", ratePlanModel).toString();
	}
}
