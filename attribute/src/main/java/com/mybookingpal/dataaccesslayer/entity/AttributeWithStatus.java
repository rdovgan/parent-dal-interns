package com.mybookingpal.dataaccesslayer.entity;


public class AttributeWithStatus extends AttributeModel{
	private Integer status = 0;

	public AttributeWithStatus() {
	}

	public AttributeWithStatus(String list, String id, Integer status) {
		super(list, id);
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
