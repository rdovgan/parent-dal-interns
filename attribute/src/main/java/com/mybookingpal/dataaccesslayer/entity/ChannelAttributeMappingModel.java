package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelAttributeMappingModel {
	private String id;
	private String attributeCode;
	private String channelId;
	private Integer channelAttributeLevel;
	private String channelCode;
	private String channelAttributeCategory;
	private Integer defaultValue;
	private Boolean valueRequired;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public Integer getChannelAttributeLevel() {
		return channelAttributeLevel;
	}

	public void setChannelAttributeLevel(int channelAttributeLevel) {
		this.channelAttributeLevel = channelAttributeLevel;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getChannelAttributeCategory() {
		return channelAttributeCategory;
	}

	public void setChannelAttributeCategory(String channelAttributeCategory) {
		this.channelAttributeCategory = channelAttributeCategory;
	}

	public Integer getDefaultValue() {
		return defaultValue;
	}

	public Boolean getValueRequired() {
		return valueRequired;
	}

	public void setValueRequired(Boolean valueRequired) {
		this.valueRequired = valueRequired;
	}

	public void setChannelAttributeLevel(Integer channelAttributeLevel) {
		this.channelAttributeLevel = channelAttributeLevel;
	}

	public void setDefaultValue(Integer defaultValue) {
		this.defaultValue = defaultValue;
	}

}