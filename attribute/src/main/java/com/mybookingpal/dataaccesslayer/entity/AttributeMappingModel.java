package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.AttributeType;

import java.util.Date;

public class AttributeMappingModel {
	private Integer id;
	private String name;
	private String code;
	private AttributeType type;
	private Integer partyId;
	private Date version;

	public AttributeMappingModel() {
	}

	public AttributeMappingModel(String name, String code, AttributeType type, Integer partyId) {
		this.name = name;
		this.code = code;
		this.type = type;
		this.partyId = partyId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AttributeType getType() {
		return type;
	}

	public void setType(AttributeType type) {
		this.type = type;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	@Override
	public String toString() {
		return "AttributeMapping [id=" + id + ", name=" + name + ", code=" + code + ", type=" + type + ", partyId=" + partyId + ", version=" + version + "]";
	}

}