package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class ProductAttributeModel {

	protected String id;
	protected String productId;
	protected String attributeId;
	protected Date createdDate;
	protected Integer quantity;
	protected Boolean updateType;
	protected Boolean deleteAutoUpdate;
	protected Integer distance = 0;
	protected Date version;

	public ProductAttributeModel() {
		this.createdDate = new Date();
		this.quantity = 1;
		this.updateType = false;
		this.deleteAutoUpdate = false;
	}

	public ProductAttributeModel(String id, String productId, String attributeId, Integer quantity) {
		this();
		this.id = id;
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
	}

	public ProductAttributeModel(String id, String productId, String attributeId, Integer quantity, Integer distance) {
		this();
		this.id = id;
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
		this.distance = distance;
	}

	public ProductAttributeModel(String productId, String attributeId) {
		this();
		this.productId = productId;
		this.attributeId = attributeId;
	}

	public ProductAttributeModel(String productId, String attributeId, Boolean updateType) {
		this();
		this.productId = productId;
		this.attributeId = attributeId;
		this.updateType = updateType;
	}

	public ProductAttributeModel(String productId, String attributeId, Integer quantity, Integer distance) {
		this();
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
		this.distance = distance;
	}

	public ProductAttributeModel(String productId, String attributeId, Integer quantity) {
		this();
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
	}

	public ProductAttributeModel(String productId, String attributeId, Integer quantity, Boolean updateType) {
		this();
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
		this.updateType = updateType;
	}

	public ProductAttributeModel(String productId, String attributeId, Date createdDate, Integer quantity, Integer distance) {
		this.productId = productId;
		this.attributeId = attributeId;
		this.createdDate = createdDate;
		this.quantity = quantity;
		this.distance = distance;
		this.updateType = false;
		this.deleteAutoUpdate = false;
	}

	public ProductAttributeModel(String productId, String attributeId, Date createdDate, Integer quantity, Boolean updateType) {
		this.productId = productId;
		this.attributeId = attributeId;
		this.createdDate = createdDate;
		this.quantity = quantity;
		this.updateType = updateType;
		this.deleteAutoUpdate = false;
	}

	public ProductAttributeModel(String id, String productId, String attributeId, Integer quantity, Boolean updateType, Boolean deleteAutoUpdate) {
		this();
		this.id = id;
		this.productId = productId;
		this.attributeId = attributeId;
		this.quantity = quantity;
		this.updateType = updateType;
		this.deleteAutoUpdate = deleteAutoUpdate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Boolean getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Boolean updateType) {
		this.updateType = updateType;
	}

	public Boolean getDeleteAutoUpdate() {
		return deleteAutoUpdate;
	}

	public void setDeleteAutoUpdate(Boolean deleteAutoUpdate) {
		this.deleteAutoUpdate = deleteAutoUpdate;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeId == null) ? 0 : attributeId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductAttributeModel other = (ProductAttributeModel) obj;
		if (attributeId == null) {
			if (other.attributeId != null) {
				return false;
			}
		} else if (!attributeId.equals(other.attributeId)) {
			return false;
		}
		if (productId == null) {
			if (other.productId != null) {
				return false;
			}
		} else if (!productId.equals(other.productId)) {
			return false;
		}
		return true;
	}

}