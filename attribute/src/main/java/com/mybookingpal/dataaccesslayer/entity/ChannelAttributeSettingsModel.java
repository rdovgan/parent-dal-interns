package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.AcceptsPropertyType;
import com.mybookingpal.dataaccesslayer.constants.BookingType;
import com.mybookingpal.dataaccesslayer.constants.ChannelAttributeSettingsState;
import com.mybookingpal.dataaccesslayer.constants.NativePropertyType;
import com.mybookingpal.dataaccesslayer.constants.RatesAndAvailabilityMapping;

import java.util.Date;

public class ChannelAttributeSettingsModel {
	private Integer channelPartnerId;
	private NativePropertyType nativePropertyType;
	private AcceptsPropertyType acceptsPropertyType;
	private RatesAndAvailabilityMapping ratesAndAvailabilityMapping;
	private Integer ratesQuantity;
	private BookingType bookingType;
	private Boolean supportMarkUp;
	private String minimumRequirementsText;
	private Integer minimumProperties;
	private String channelInstructionsPdf;
	private String signupPdf;
	private String channelInformation;
	private String distributionStepsPdf;
	private Boolean supportSynchronization;
	private Boolean supportsCreateProperty;
	private Boolean supportsSwitchOver;
	private Boolean supportsOpenClose;
	private Boolean supportValidation;
	private Boolean supportUpdateStatic;
	private Boolean supportUpdateImage;
	private Boolean supportUpdateDynamic;
	private Boolean supportCreateChannelAccount;
	private Boolean supportAuthorization;
	private Boolean supportGather;
	private Boolean supportChannelCancellationPolicy;
	private Boolean supportCancelFromPm;
	private Boolean supportModificationFromPm;
	private Date createdDate;
	private Date version;
	private ChannelAttributeSettingsState state;

	public ChannelAttributeSettingsModel(Integer channelPartnerId, NativePropertyType nativePropertyType, AcceptsPropertyType acceptsPropertyType,
			RatesAndAvailabilityMapping ratesAndAvailabilityMapping, Integer ratesQuantity, BookingType bookingType, Boolean supportMarkUp, String minimumRequirementsText, Integer minimumProperties,
			String channelInstructionsPdf, String signupPdf, String channelInformation, String distributionStepsPdf, Boolean supportSynchronization, Boolean supportsCreateProperty, Boolean supportsSwitchOver, Boolean supportsOpenClose, Boolean supportValidation,
			Boolean supportUpdateStatic, Boolean supportUpdateImage, Boolean supportUpdateDynamic, Boolean supportCreateChannelAccount,
			Boolean supportAuthorization, Boolean supportGather, Boolean supportChannelCancellationPolicy, Boolean supportCancelFromPm,
			Boolean supportModificationFromPm, ChannelAttributeSettingsState state) {
		this.channelPartnerId = channelPartnerId;
		this.nativePropertyType = nativePropertyType;
		this.acceptsPropertyType = acceptsPropertyType;
		this.ratesAndAvailabilityMapping = ratesAndAvailabilityMapping;
		this.ratesQuantity = ratesQuantity;
		this.bookingType = bookingType;
		this.supportMarkUp = supportMarkUp;
		this.minimumRequirementsText = minimumRequirementsText;
		this.minimumProperties = minimumProperties;
		this.channelInstructionsPdf = channelInstructionsPdf;
		this.signupPdf = signupPdf;
		this.channelInformation = channelInformation;
		this.distributionStepsPdf = distributionStepsPdf;
		this.supportSynchronization = supportSynchronization;
		this.supportsCreateProperty = supportsCreateProperty;
		this.supportsSwitchOver = supportsSwitchOver;
		this.supportsOpenClose = supportsOpenClose;
		this.supportUpdateDynamic = supportUpdateDynamic;
		this.supportValidation = supportValidation;
		this.supportUpdateStatic= supportUpdateStatic;
		this.supportUpdateImage = supportUpdateImage;
		this.supportCreateChannelAccount = supportCreateChannelAccount;
		this.supportAuthorization = supportAuthorization;
		this.supportGather = supportGather;
		this.supportChannelCancellationPolicy = supportChannelCancellationPolicy;
		this.supportCancelFromPm = supportCancelFromPm;
		this.supportModificationFromPm = supportModificationFromPm;
		this.state = state;
	}

	public ChannelAttributeSettingsModel() {
	}

	public String getMinimumRequirementsText() {
		return minimumRequirementsText;
	}

	public void setMinimumRequirementsText(String minimumRequirementsText) {
		this.minimumRequirementsText = minimumRequirementsText;
	}

	public String getChannelInstructionsPdf() {
		return channelInstructionsPdf;
	}

	public void setChannelInstructionsPdf(String channelInstructionsPdf) {
		this.channelInstructionsPdf = channelInstructionsPdf;
	}

	public String getSignupPdf() {
		return signupPdf;
	}

	public void setSignupPdf(String signupPdf) {
		this.signupPdf = signupPdf;
	}

	public String getDistributionStepsPdf() {
		return distributionStepsPdf;
	}

	public void setDistributionStepsPdf(String distributionStepsPdf) {
		this.distributionStepsPdf = distributionStepsPdf;
	}

	public Boolean getSupportCancelFromPm() {
		return supportCancelFromPm;
	}

	public void setSupportCancelFromPm(Boolean supportCancelFromPm) {
		this.supportCancelFromPm = supportCancelFromPm;
	}

	public Boolean getSupportModificationFromPm() {
		return supportModificationFromPm;
	}

	public void setSupportModificationFromPm(Boolean supportModificationFromPm) {
		this.supportModificationFromPm = supportModificationFromPm;
	}

	public Boolean getSupportCreateChannelAccount() {
		return supportCreateChannelAccount;
	}

	public void setSupportCreateChannelAccount(Boolean supportCreateChannelAccount) {
		this.supportCreateChannelAccount = supportCreateChannelAccount;
	}

	public Boolean getSupportAuthorization() {
		return supportAuthorization;
	}

	public void setSupportAuthorization(Boolean supportAuthorization) {
		this.supportAuthorization = supportAuthorization;
	}

	public Boolean getSupportGather() {
		return supportGather;
	}

	public void setSupportGather(Boolean supportGather) {
		this.supportGather = supportGather;
	}

	public Boolean getSupportChannelCancellationPolicy() {
		return supportChannelCancellationPolicy;
	}

	public void setSupportChannelCancellationPolicy(Boolean supportChannelCancellationPolicy) {
		this.supportChannelCancellationPolicy = supportChannelCancellationPolicy;
	}

	public ChannelAttributeSettingsState getState() {
		return state;
	}

	public void setState(ChannelAttributeSettingsState state) {
		this.state = state;
	}

	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setPartyId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public NativePropertyType getNativePropertyType() {
		return nativePropertyType;
	}

	public void setNativePropertyType(NativePropertyType nativePropertyType) {
		this.nativePropertyType = nativePropertyType;
	}

	public AcceptsPropertyType getAcceptsPropertyType() {
		return acceptsPropertyType;
	}

	public void setAcceptsPropertyType(AcceptsPropertyType acceptsPropertyType) {
		this.acceptsPropertyType = acceptsPropertyType;
	}

	public RatesAndAvailabilityMapping getRatesAndAvailabilityMapping() {
		return ratesAndAvailabilityMapping;
	}

	public void setRatesAndAvailabilityMapping(RatesAndAvailabilityMapping ratesAndAvailabilityMapping) {
		this.ratesAndAvailabilityMapping = ratesAndAvailabilityMapping;
	}

	public Boolean getSupportSynchronization() {
		return supportSynchronization;
	}

	public void setSupportSynchronization(Boolean supportSynchronization) {
		this.supportSynchronization = supportSynchronization;
	}

	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public Boolean getSupportsCreateProperty() {
		return supportsCreateProperty;
	}

	public void setSupportsCreateProperty(Boolean supportsCreateProperty) {
		this.supportsCreateProperty = supportsCreateProperty;
	}

	public Boolean getSupportsSwitchOver() {
		return supportsSwitchOver;
	}

	public void setSupportsSwitchOver(Boolean supportsSwitchOver) {
		this.supportsSwitchOver = supportsSwitchOver;
	}

	public Boolean getSupportsOpenClose() {
		return supportsOpenClose;
	}

	public void setSupportsOpenClose(Boolean supportsOpenClose) {
		this.supportsOpenClose = supportsOpenClose;
	}

	public Boolean getSupportValidation() {
		return supportValidation;
	}

	public void setSupportValidation(Boolean supportValidation) {
		this.supportValidation = supportValidation;
	}

	public Boolean getSupportUpdateStatic() {
		return supportUpdateStatic;
	}

	public void setSupportUpdateStatic(Boolean supportUpdateStatic) {
		this.supportUpdateStatic = supportUpdateStatic;
	}

	public Boolean getSupportUpdateImage() {
		return supportUpdateImage;
	}

	public void setSupportUpdateImage(Boolean supportUpdateImage) {
		this.supportUpdateImage = supportUpdateImage;
	}

	public Boolean getSupportUpdateDynamic() {
		return supportUpdateDynamic;
	}

	public void setSupportUpdateDynamic(Boolean supportUpdateDynamic) {
		this.supportUpdateDynamic = supportUpdateDynamic;
	}

	public Integer getRatesQuantity() {
		return ratesQuantity;
	}

	public void setRatesQuantity(Integer ratesQuantity) {
		this.ratesQuantity = ratesQuantity;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public Boolean getSupportMarkUp() {
		return supportMarkUp;
	}

	public void setSupportMarkUp(Boolean supportMarkUp) {
		this.supportMarkUp = supportMarkUp;
	}

	public Integer getMinimumProperties() {
		return minimumProperties;
	}

	public void setMinimumProperties(Integer minimumProperties) {
		this.minimumProperties = minimumProperties;
	}

	public String getChannelInformation() {
		return channelInformation;
	}

	public void setChannelInformation(String channelInformation) {
		this.channelInformation = channelInformation;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}