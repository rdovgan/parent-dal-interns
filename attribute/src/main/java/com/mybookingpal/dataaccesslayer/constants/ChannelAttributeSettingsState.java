package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum ChannelAttributeSettingsState {
	NOT_VISIBLE(0, "Not visible"), VISIBLE(1, "Visible"), COMING_SOON(2, "Coming soon");

	private Integer id;
	private String name;

	ChannelAttributeSettingsState(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static ChannelAttributeSettingsState getByName(String name) {
		return Arrays.stream(ChannelAttributeSettingsState.values())
				.filter(textType -> textType.getId() != null && textType.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}

	public static ChannelAttributeSettingsState getById(Integer id) {
		return Arrays.stream(ChannelAttributeSettingsState.values())
				.filter(textType -> textType.getId() != null && textType.getId()
						.equals(id))
				.findFirst()
				.orElse(null);
	}
}