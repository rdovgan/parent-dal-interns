package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum BookingType {
	Instant(0, "Instant"), RequestToBook(1, "Request to book"), Both(2, "Both");

	private Integer id;
	private String name;

	BookingType(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static BookingType getByName(String name) {
		return Arrays.stream(BookingType.values())
				.filter(textType -> textType.getId() != null && textType.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}

	public static BookingType getById(Integer id) {
		return Arrays.stream(BookingType.values())
				.filter(textType -> textType.getId() != null && textType.getId()
						.equals(id))
				.findFirst()
				.orElse(null);
	}
}