package com.mybookingpal.dataaccesslayer.constants;

public enum AttributeType {
	UNKNOWN(0), AMENITY(1), PRODUCT_TYPE(2);

	private Integer value;

	AttributeType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static AttributeType getByInt(int value) {
		for (AttributeType v : values()) {
			if (v.value == value) {
				return v;
			}
		}
		return UNKNOWN;
	}

	public static String getNameByInt(Integer value) {
		return getByInt(value).name();
	}
}