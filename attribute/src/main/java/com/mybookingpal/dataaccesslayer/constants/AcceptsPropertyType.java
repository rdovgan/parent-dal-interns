package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum AcceptsPropertyType {
	HomesAndApartments(0, "Homes / apartments"), Hotels(1, "Hotels"), Both(2, "Both");

	private Integer id;
	private String name;

	AcceptsPropertyType(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static AcceptsPropertyType getByName(String name) {
		return Arrays.stream(AcceptsPropertyType.values())
				.filter(textType -> textType.getId() != null && textType.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}

	public static AcceptsPropertyType getById(Integer id) {
		return Arrays.stream(AcceptsPropertyType.values())
				.filter(textType -> textType.getId() != null && textType.getId()
						.equals(id))
				.findFirst()
				.orElse(null);
	}
}