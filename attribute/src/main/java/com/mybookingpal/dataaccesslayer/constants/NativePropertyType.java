package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum NativePropertyType {
	HomesAndApartments(0, "Homes / apartments"), Hotels(1, "Hotels");

	private Integer id;
	private String name;

	NativePropertyType(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static NativePropertyType getByName(String name) {
		return Arrays.stream(NativePropertyType.values())
				.filter(textType -> textType.getId() != null && textType.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}

	public static NativePropertyType getById(Integer id) {
		return Arrays.stream(NativePropertyType.values())
				.filter(textType -> textType.getId() != null && textType.getId()
						.equals(id))
				.findFirst()
				.orElse(null);
	}
}