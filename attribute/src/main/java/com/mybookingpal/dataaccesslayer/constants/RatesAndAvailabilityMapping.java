package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum RatesAndAvailabilityMapping {
	MapToProperty(0, "Map to property"), MapToRoom(1, "Map to room"), MapToRatePlan(2, "Map to rate plan");

	private Integer id;
	private String name;

	RatesAndAvailabilityMapping(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static RatesAndAvailabilityMapping getByName(String name) {
		return Arrays.stream(RatesAndAvailabilityMapping.values())
				.filter(textType -> textType.getId() != null && textType.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}

	public static RatesAndAvailabilityMapping getById(Integer id) {
		return Arrays.stream(RatesAndAvailabilityMapping.values())
				.filter(textType -> textType.getId() != null && textType.getId()
						.equals(id))
				.findFirst()
				.orElse(null);
	}
}
