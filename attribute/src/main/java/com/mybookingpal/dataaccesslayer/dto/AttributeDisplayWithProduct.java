package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.AttributeDisplayModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class AttributeDisplayWithProduct extends AttributeDisplayModel {
	private Long productId;

	private Boolean active = true;

	private Integer quantity;

	private Boolean updateType;

	private Boolean deleteAutoUpdate;

	private Integer distance;

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Boolean getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Boolean updateType) {
		this.updateType = updateType;
	}

	public Boolean getDeleteAutoUpdate() {
		return deleteAutoUpdate;
	}

	public void setDeleteAutoUpdate(Boolean deleteAutoUpdate) {
		this.deleteAutoUpdate = deleteAutoUpdate;
	}
}
