package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductAttributeModel;

public class ProductAttributeWithName extends ProductAttributeModel {

	private String displayName;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ProductAttributeWithName{");
		sb.append("displayName='").append(displayName).append('\'');
		sb.append(super.toString());
		sb.append('}');
		return sb.toString();
	}
}
