package com.mybookingpal.dataaccesslayer.dto;

import java.time.LocalDateTime;

public class Attribute {
	private String name;
	private String code;
	private LocalDateTime version;

	public Attribute() {
	}

	public Attribute(String name, String code, LocalDateTime version) {
		this.name = name;
		this.code = code;
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDateTime getVersion() {
		return version;
	}

	public void setVersion(LocalDateTime version) {
		this.version = version;
	}
}
