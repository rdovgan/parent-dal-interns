package com.mybookingpal.dataaccesslayer.dto;

public class Amenities {
	private String attributeName;
	private String attributeList;
	private String parent;
	private String attributeID;
	private String checked;

	public Amenities() {
	}

	public Amenities(String attributeName, String attributeList, String parent, String attributeID, String checked) {
		this.attributeName = attributeName;
		this.attributeList = attributeList;
		this.parent = parent;
		this.attributeID = attributeID;
		this.checked = checked;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(String attributeList) {
		this.attributeList = attributeList;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getAttributeID() {
		return attributeID;
	}

	public void setAttributeID(String attributeID) {
		this.attributeID = attributeID;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}
}
