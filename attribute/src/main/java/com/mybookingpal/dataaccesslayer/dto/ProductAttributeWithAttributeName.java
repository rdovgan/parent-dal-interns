package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductAttributeModel;

public class ProductAttributeWithAttributeName extends ProductAttributeModel {
	private String attributeName;

	public ProductAttributeWithAttributeName() {
	}

	public ProductAttributeWithAttributeName(String id, String productId, String attributeId, Integer quantity, Boolean updateType, Boolean deleteAutoUpdate,
			String attributeName) {
		super(id, productId, attributeId, quantity, updateType, deleteAutoUpdate);
		this.attributeName = attributeName;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
}
