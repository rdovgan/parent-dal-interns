package com.mybookingpal.dataaccesslayer.dto;

public class ProductManager {
	private Integer productId;
	private String productName;
	private Integer managerId;
	private String managerName;
	private String managerEmail;

	public ProductManager() {
	}

	public ProductManager(Integer productId, String productName, Integer managerId, String managerName, String managerEmail) {
		this.productId = productId;
		this.productName = productName;
		this.managerId = managerId;
		this.managerName = managerName;
		this.managerEmail = managerEmail;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}
}
