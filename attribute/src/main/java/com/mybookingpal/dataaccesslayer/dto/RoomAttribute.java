package com.mybookingpal.dataaccesslayer.dto;

public class RoomAttribute {
	private Integer roomId;
	private String roomName;
	private String attributeId;
	private String attributeName;

	public RoomAttribute() {
	}

	public RoomAttribute(Integer roomId, String roomName, String attributeId, String attributeName) {
		this.roomId = roomId;
		this.roomName = roomName;
		this.attributeId = attributeId;
		this.attributeName = attributeName;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
}
