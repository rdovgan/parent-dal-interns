package com.mybookingpal.dataaccesslayer.dto;

public class KeyCollection {
	private Integer id;
	private String productId;
	private String checkInMethod;
	private String additionCheckInDto;

	public KeyCollection() {
	}

	public KeyCollection(Integer id, String productId, String checkInMethod, String additionCheckInDto) {
		this.id = id;
		this.productId = productId;
		this.checkInMethod = checkInMethod;
		this.additionCheckInDto = additionCheckInDto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCheckInMethod() {
		return checkInMethod;
	}

	public void setCheckInMethod(String checkInMethod) {
		this.checkInMethod = checkInMethod;
	}

	public String getAdditionCheckInDto() {
		return additionCheckInDto;
	}

	public void setAdditionCheckInDto(String additionCheckInDto) {
		this.additionCheckInDto = additionCheckInDto;
	}
}
