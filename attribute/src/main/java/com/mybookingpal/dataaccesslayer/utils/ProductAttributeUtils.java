package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.ProductAttributeModel;
import org.springframework.stereotype.Component;

@Component
public class ProductAttributeUtils {
	public static ProductAttributeModel fillProductAttributeNearbyAmenities(String productId, String attributeId, Integer quantity, Integer distance) {
		ProductAttributeModel productAttributeModel = new ProductAttributeModel();
		productAttributeModel.setProductId(productId);
		productAttributeModel.setAttributeId(attributeId);
		productAttributeModel.setQuantity(quantity);
		if (distance > 0) {
			productAttributeModel.setDistance(distance);
		}
		return productAttributeModel;
	}

	public static ProductAttributeModel fillProductAttributeNearbyAmenities(String id, String productId, String attributeId, Integer quantity,
			Integer distance) {
		ProductAttributeModel productAttributeModel = new ProductAttributeModel();
		productAttributeModel.setId(id);
		productAttributeModel.setProductId(productId);
		productAttributeModel.setAttributeId(attributeId);
		productAttributeModel.setQuantity(quantity);
		if (distance > 0) {
			productAttributeModel.setDistance(distance);
		}
		return productAttributeModel;
	}

	public static boolean noProductId(ProductAttributeModel productAttributeModel) {
		return productAttributeModel.getProductId() == null;
	}

	public static boolean noAttributeId(ProductAttributeModel productAttributeModel) {
		return productAttributeModel.getAttributeId() == null || productAttributeModel.getAttributeId()
				.isEmpty();
	}

	public static boolean noProductIdOrAttributeId(ProductAttributeModel productAttributeModel) {
		return noProductId(productAttributeModel) && noAttributeId(productAttributeModel);
	}
}
