package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelAttributeSettingsModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChannelAttributeSettingsMapper {

	void create(ChannelAttributeSettingsModel channelAttributeSettings);

	ChannelAttributeSettingsModel readByChannelPartnerId(@Param("channelPartnerId") Integer channelPartnerId);

	List<ChannelAttributeSettingsModel> readAll();

	List<ChannelAttributeSettingsModel> readAllVisibleAndComingSoon();

	void deleteByChannelPartnerId(@Param("channelPartnerId") Integer channelPartnerId);

	void update(ChannelAttributeSettingsModel channelAttributeSettings);

}
