package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.utils.entity.IdVersion;

import com.mybookingpal.dataaccesslayer.dto.KeyCollection;
import com.mybookingpal.dataaccesslayer.dto.ProductAttributeWithName;
import com.mybookingpal.dataaccesslayer.dto.ProductManager;
import com.mybookingpal.dataaccesslayer.dto.RoomAttribute;
import com.mybookingpal.dataaccesslayer.entity.ProductAttributeModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ProductAttributeMapper {
	void create(ProductAttributeModel productAttribute);

	void updateByProductAndAttribute(ProductAttributeModel productAttribute);

	List<String> readAttributeByProductId(Integer id);

	void insertList(@Param("list") List<ProductAttributeModel> productAttributeList);

	void deleteByProductId(String productId);

	void deleteByProductIdAndAttribute(@Param("productId") String productId, @Param("attribute") String attribute);

	List<String> productIds(ProductAttributeModel productAttribute);

	List<String> attributeIds(ProductAttributeModel productAttribute);

	List<String> skipManualAttributeIds(ProductAttributeModel productAttribute);

	List<String> attributeIdsForPropertyValidator(@Param("productId") String productId);

	void delete(ProductAttributeModel productAttribute);

	List<ProductAttributeModel> getAllByProductId(String productId);

	List<ProductAttributeWithName> getAccommodationTypeWithNameByProductId(String productId);

	void updateQuantity(ProductAttributeModel productAttribute);

	void updateDistance(ProductAttributeModel productAttribute);

	void updateDeleteAutoUpdate(ProductAttributeModel productAttribute);

	ProductAttributeModel getByProductIdAndAttributeId(ProductAttributeModel productAttribute);

	void copyAttributes(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	List<ProductAttributeModel> readAttributesByProductIds(List<String> productIds);

	List<ProductAttributeModel> readAttributesStartWithPctForProducts(List<String> productIds);

	List<String> readAllAttributesBeginWithPrefixForProduct(@Param("productId") Integer productId, @Param("prefix") String prefix);

	List<ProductAttributeModel> getProductAttributesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);

	List<ProductAttributeWithName> readAttributesDisplayNameForProduct(Integer productId);

	ProductAttributeModel getByProductAndAttribute(@Param("productId") Long productId, @Param("attributes") List<String> attributes);

	void deleteByProductIdAndAttributes(@Param("productId") String productId, @Param("attributes") List<String> attributes);

	List<String> getMltProductIdsByAttributes(@Param("productId") Long productId, @Param("attributes") List<String> attributes);

	List<ProductAttributeModel> getListByProductAndAttributes(@Param("productId") Long productId, @Param("attributes") List<String> attributes);

	ProductAttributeModel getAttributeByDisplayCategory(@Param("productId") Long productId, @Param("displayName") String displayName);

	void updateProductAttributeCode(@Param("newDisplayCategory") String newDisplayCategory, @Param("id") Integer id);

	List<RoomAttribute> getMltProductRoomAttributes(@Param("productId") Long productId, @Param("attributes") List<String> attributes);

	void deleteByIdList(@Param("idList") List<Long> idList);

	void deleteMltProductRoomAttributes(@Param("productId") String productId, @Param("attributes") List<String> attributes);

	void updateProductAttributesUpdateType(@Param("productId") Integer productId, @Param("updateType") Boolean updateType);

	void updateProductAttributesDeleteAutoUpdate(@Param("productId") Integer productId, @Param("deleteAutoUpdate") Boolean deleteAutoUpdate);

	void updateProductAttributesUpdateTypeForProducts(@Param("list") List<Integer> productIds, @Param("updateType") Boolean updateType);

	List<ProductAttributeModel> readAllAttributesByProductIds(@Param("list") List<Integer> productIds);

	//TODO: Has to be moved to ProductMapper
	List<ProductManager> getAllByProductIds(@Param("listProductId") List<Integer> listProductId);

	List<ProductAttributeModel> isPetAllowed(@Param("productId") String productId);

	KeyCollection getKeyCollection(@Param("productId") Integer productId);

	void createKeyCollection(KeyCollection keyCollection);

	void updateKeyCollection(KeyCollection keyCollection);

	void deleteKeyCollection(@Param("id") Integer id);

	Integer getAttributesCountByProductIdAndAttributeId(ProductAttributeModel productAttribute);

	ProductAttributeModel readByProductIdAndAttribute(@Param("productId") Integer productId, @Param("attribute") String attribute);

	void deleteRoomCodesByProductId(Integer productId);

	void deleteHotelCodesByProductId(Integer productId);

	List<ProductAttributeWithName> getAllWithNamesByProductId(String productId);

	List<IdVersion> getLastUpdateByProductIds(@Param("productIds") List<Integer> productIds);

}