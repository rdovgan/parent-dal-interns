package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.Attribute;
import com.mybookingpal.dataaccesslayer.entity.AttributeMappingModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AttributeMappingMapper {
	List<AttributeMappingModel> selectByNameList(@Param("attributeNames") List<String> attributeNames, @Param("type") Integer type);

	void insert(AttributeMappingModel attributeMappingModel);

	void updateNotMapped(Attribute Attribute);
}
