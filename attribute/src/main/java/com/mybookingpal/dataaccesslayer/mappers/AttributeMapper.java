package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.Amenities;
import com.mybookingpal.dataaccesslayer.entity.AttributeModel;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AttributeMapper {
	AttributeModel read(AttributeModel attributeModel);

	List<String> readAttribute(List<String> list);

	void insert(AttributeModel attributeModel);

	void update(AttributeModel attributeModel);

	List<NameId> razorlist();

	@MapKey("ID")
	Map<String, String> getAttributeValues(List<String> list);

	String getAttributeGroupNameByList(String id);

	List<NameId> list();

	List<AttributeModel> listByExample(AttributeModel attributeModel);

	List<NameId> pctListValue(AttributeModel attributeModel);

	List<String> getBedType();

	List<AttributeModel> getAllAttributesByTypeProperty(@Param("isHomeowner") Boolean isHomeowner);

	List<AttributeModel> getAllAttributesByTypeBed();

	List<Amenities> getAmenities(@Param("productId") Integer productId, @Param("isHomeOwner") Boolean isHomeOwner);

	List<Amenities> getAmenitiesById(@Param("productId") Integer productId);

}
