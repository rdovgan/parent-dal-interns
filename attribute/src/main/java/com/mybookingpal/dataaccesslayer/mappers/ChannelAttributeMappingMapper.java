package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelAttributeMappingModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChannelAttributeMappingMapper {

	List<ChannelAttributeMappingModel> readbyCodes(@Param("list") List<String> attributeCodes, @Param("channelId") Integer channelId);

	void create(ChannelAttributeMappingModel channelAttributeMappingModel);

	List<ChannelAttributeMappingModel> readByExample(ChannelAttributeMappingModel example);

	List<ChannelAttributeMappingModel> readExistingListByCodesAndChannelId(@Param("list") List<String> attributeCodes, @Param("channelId") Integer channelId);

}
