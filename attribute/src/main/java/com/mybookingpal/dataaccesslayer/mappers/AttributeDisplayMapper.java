package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.AttributeDisplayWithProduct;
import com.mybookingpal.dataaccesslayer.entity.AttributeDisplayModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AttributeDisplayMapper {

	List<AttributeDisplayWithProduct> readAttributesForRoot(@Param("productId") Long productId);

	List<AttributeDisplayWithProduct> readAttributesForRoom(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	List<AttributeDisplayWithProduct> readAttributes(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	Integer getNumberOfAmenitiesByProduct(@Param("productId") Integer productId);

	String readDisplayNameByAttributeCode(@Param("attributeCode") String attributeCode);

	List<AttributeDisplayWithProduct> readAllPossibleAttributes();

	List<AttributeDisplayWithProduct> readAllAttributes();

	List<AttributeDisplayWithProduct> readAttributesForResort(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoot(List<String> activeAttributes);

	List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoom(List<String> activeAttributes);

	List<AttributeDisplayWithProduct> readDefiningAttributeForRoot();

	List<AttributeDisplayWithProduct> getAmenityByDisplayCategory(@Param("categoryType") String categoryType);

	List<AttributeDisplayWithProduct> readDefiningAttributesForRoom();

	List<AttributeDisplayWithProduct> readAllProductAttributes(@Param("productId") Long productId);

	List<AttributeDisplayWithProduct> readProductsAttributesWithCategories(@Param("productIds") List<Long> productIds,
			@Param("categoryType") String categoryType);

	List<AttributeDisplayWithProduct> readUniqueChildrenAttributes(@Param("ids") List<Long> productIds, @Param("size") Integer size);

	List<AttributeDisplayWithProduct> readBedTypeProductAttributes(@Param("productId") Long productId, @Param("categoryType") String categoryType);

	List<AttributeDisplayWithProduct> readAllPossibleBedTypes(@Param("categoryType") String categoryType);

	List<AttributeDisplayModel> readAttributeDisplaysByAttributeCodes(@Param("attributeCodes") List<String> attributeCodes);

	void updateAttributeDisplayCount();
}