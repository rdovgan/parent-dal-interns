package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.ChannelAttributeSettingsState;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChannelAttributeSettingsStateTypeHandler extends BaseTypeHandler<ChannelAttributeSettingsState> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ChannelAttributeSettingsState parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public ChannelAttributeSettingsState getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return ChannelAttributeSettingsState.getById(rs.getInt(columnName));
	}

	@Override
	public ChannelAttributeSettingsState getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return ChannelAttributeSettingsState.getById(rs.getInt(columnIndex));
	}

	@Override
	public ChannelAttributeSettingsState getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return ChannelAttributeSettingsState.getById(cs.getInt(columnIndex));
	}
}
