package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.BookingType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookingTypeTypeHandler extends BaseTypeHandler<BookingType> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, BookingType parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public BookingType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return BookingType.getById(rs.getInt(columnName));
	}

	@Override
	public BookingType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return BookingType.getById(rs.getInt(columnIndex));
	}

	@Override
	public BookingType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return BookingType.getById(cs.getInt(columnIndex));
	}
}
