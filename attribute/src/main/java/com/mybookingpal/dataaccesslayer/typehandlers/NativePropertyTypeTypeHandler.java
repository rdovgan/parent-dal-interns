package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.NativePropertyType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NativePropertyTypeTypeHandler extends BaseTypeHandler<NativePropertyType> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, NativePropertyType parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public NativePropertyType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return NativePropertyType.getById(rs.getInt(columnName));
	}

	@Override
	public NativePropertyType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return NativePropertyType.getById(rs.getInt(columnIndex));
	}

	@Override
	public NativePropertyType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return NativePropertyType.getById(cs.getInt(columnIndex));
	}
}
