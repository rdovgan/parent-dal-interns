package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.AcceptsPropertyType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AcceptsPropertyTypeTypeHandler extends BaseTypeHandler<AcceptsPropertyType> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, AcceptsPropertyType parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public AcceptsPropertyType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return AcceptsPropertyType.getById(rs.getInt(columnName));
	}

	@Override
	public AcceptsPropertyType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return AcceptsPropertyType.getById(rs.getInt(columnIndex));
	}

	@Override
	public AcceptsPropertyType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return AcceptsPropertyType.getById(cs.getInt(columnIndex));
	}
}
