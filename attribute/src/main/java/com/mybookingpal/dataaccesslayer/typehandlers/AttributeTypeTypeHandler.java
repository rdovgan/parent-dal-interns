package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.AttributeType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AttributeTypeTypeHandler extends BaseTypeHandler<AttributeType> {
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, AttributeType attributeType, JdbcType jdbcType) throws SQLException {
		preparedStatement.setInt(i, attributeType.getValue());
	}

	@Override
	public AttributeType getNullableResult(ResultSet resultSet, String s) throws SQLException {
		Integer attributeTypeValue = resultSet.getInt(s);
		return AttributeType.getByInt(attributeTypeValue);
	}

	@Override
	public AttributeType getNullableResult(ResultSet resultSet, int i) throws SQLException {
		Integer attributeTypeValue = resultSet.getInt(i);
		return AttributeType.getByInt(attributeTypeValue);
	}

	@Override
	public AttributeType getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
		Integer attributeTypeValue = callableStatement.getInt(i);
		return AttributeType.getByInt(attributeTypeValue);
	}
}
