package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.RatesAndAvailabilityMapping;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RatesAndAvailabilityMappingTypeHandler extends BaseTypeHandler<RatesAndAvailabilityMapping> {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, RatesAndAvailabilityMapping parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public RatesAndAvailabilityMapping getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return RatesAndAvailabilityMapping.getById(rs.getInt(columnName));
	}

	@Override
	public RatesAndAvailabilityMapping getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return RatesAndAvailabilityMapping.getById(rs.getInt(columnIndex));
	}

	@Override
	public RatesAndAvailabilityMapping getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return RatesAndAvailabilityMapping.getById(cs.getInt(columnIndex));
	}
}
