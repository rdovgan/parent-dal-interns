package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.constants.AttributeType;
import com.mybookingpal.dataaccesslayer.dto.Attribute;
import com.mybookingpal.dataaccesslayer.entity.AttributeMappingModel;
import com.mybookingpal.dataaccesslayer.mappers.AttributeMappingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
public class AttributeMappingDao {
	@Autowired
	private AttributeMappingMapper attributeMappingMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeMappingModel> selectByNameList(List<String> attributeNames, AttributeType type) {
		if (CollectionUtils.isEmpty(attributeNames)) {
			return Collections.emptyList();
		}
		return attributeMappingMapper.selectByNameList(attributeNames, type.getValue());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insert(AttributeMappingModel attributeMappingModel) {
		if (attributeMappingModel == null) {
			return;
		}
		attributeMappingMapper.insert(attributeMappingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateNotMapped(Attribute attribute) {
		if (attribute == null) {
			return;
		}
		attributeMappingMapper.updateNotMapped(attribute);
	}
}
