package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.Amenities;
import com.mybookingpal.dataaccesslayer.entity.AttributeModel;
import com.mybookingpal.dataaccesslayer.mappers.AttributeMapper;
import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
public class AttributeDao {
	@Autowired
	private AttributeMapper attributeMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AttributeModel read(String list, String id) {
		return attributeMapper.read(new AttributeModel(list, id));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readAttribute(List<String> list) {
		return CollectionUtils.isEmpty(list) ? Collections.emptyList() : attributeMapper.readAttribute(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insert(AttributeModel attributeModel) {
		if (attributeModel != null) {
			attributeMapper.insert(attributeModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(AttributeModel attributeModel) {
		if (attributeModel != null) {
			attributeMapper.update(attributeModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> razorlist() {
		return attributeMapper.razorlist();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getAttributeValues(List<String> list) {
		return CollectionUtils.isEmpty(list) ? null : StringUtils.join(attributeMapper.getAttributeValues(list));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getAttributeGroupNameByList(String id) {
		return attributeMapper.getAttributeGroupNameByList(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> list() {
		return attributeMapper.list();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeModel> listByExample(AttributeModel attributeModel) {
		return attributeModel == null ? null : attributeMapper.listByExample(attributeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> pctListValue(String id) {
		return StringUtils.isBlank(id) ? Collections.emptyList() : attributeMapper.pctListValue(new AttributeModel(null, id));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getBedType() {
		return attributeMapper.getBedType();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeModel> getAllAttributesByTypeProperty(Boolean isHomeowner) {
		return attributeMapper.getAllAttributesByTypeProperty(isHomeowner);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeModel> getAllAttributesByTypeBed() {
		return attributeMapper.getAllAttributesByTypeBed();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Amenities> getAmenities(Integer productId, Boolean isHomeOwner) {
		return attributeMapper.getAmenities(productId, isHomeOwner);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Amenities> getAmenitiesById(Integer productId) {
		return attributeMapper.getAmenitiesById(productId);
	}

}
