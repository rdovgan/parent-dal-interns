package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.AttributeDisplayWithProduct;
import com.mybookingpal.dataaccesslayer.entity.AttributeDisplayModel;
import com.mybookingpal.dataaccesslayer.mappers.AttributeDisplayMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
public class AttributeDisplayDao {
	@Autowired
	private AttributeDisplayMapper attributeDisplayMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAttributesForRoot(Long productId) {
		return attributeDisplayMapper.readAttributesForRoot(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAttributesForRoom(List<Long> productIds, Boolean updateType) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readAttributesForRoom(productIds, updateType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAttributes(List<Long> productIds, Boolean updateType) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readAttributes(productIds, updateType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getNumberOfAmenitiesByProduct(Integer productId) {
		return attributeDisplayMapper.getNumberOfAmenitiesByProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String readDisplayNameByAttributeCode(String attributeCode) {
		return attributeDisplayMapper.readDisplayNameByAttributeCode(attributeCode);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAllPossibleAttributes() {
		return attributeDisplayMapper.readAllPossibleAttributes();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAllAttributes() {
		return attributeDisplayMapper.readAllAttributes();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAttributesForResort(List<Long> productIds, Boolean updateType) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readAttributesForResort(productIds, updateType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoot(List<String> activeAttributes) {
		if (CollectionUtils.isEmpty(activeAttributes)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readDefiningAttributesNotIncludeCurrentForRoot(activeAttributes);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoom(List<String> activeAttributes) {
		if (CollectionUtils.isEmpty(activeAttributes)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readDefiningAttributesNotIncludeCurrentForRoom(activeAttributes);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readDefiningAttributeForRoot() {
		return attributeDisplayMapper.readDefiningAttributeForRoot();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> getAmenityByDisplayCategory(String categoryType) {
		return attributeDisplayMapper.getAmenityByDisplayCategory(categoryType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readDefiningAttributesForRoom() {
		return attributeDisplayMapper.readDefiningAttributesForRoom();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAllProductAttributes(Long productId) {
		return attributeDisplayMapper.readAllProductAttributes(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readProductsAttributesWithCategories(List<Long> productIds, String categoryType) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readProductsAttributesWithCategories(productIds, categoryType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readUniqueChildrenAttributes(List<Long> productIds, Integer size) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readUniqueChildrenAttributes(productIds, size);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readBedTypeProductAttributes(Long productId, String categoryType) {
		return attributeDisplayMapper.readBedTypeProductAttributes(productId, categoryType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayWithProduct> readAllPossibleBedTypes(String categoryType) {
		return attributeDisplayMapper.readAllPossibleBedTypes(categoryType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AttributeDisplayModel> readAttributeDisplaysByAttributeCodes(List<String> attributeCodes) {
		if (CollectionUtils.isEmpty(attributeCodes)) {
			return Collections.emptyList();
		}
		return attributeDisplayMapper.readAttributeDisplaysByAttributeCodes(attributeCodes);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateAttributeDisplayCount() {
		attributeDisplayMapper.updateAttributeDisplayCount();
	}
}
