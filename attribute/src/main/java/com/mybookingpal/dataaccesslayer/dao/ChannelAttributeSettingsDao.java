package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelAttributeSettingsModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelAttributeSettingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ChannelAttributeSettingsDao {
	@Autowired
	ChannelAttributeSettingsMapper channelAttributeSettingsMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)

	public void create(ChannelAttributeSettingsModel channelAttributeSettings) {
		if (channelAttributeSettings != null) {
			channelAttributeSettingsMapper.create(channelAttributeSettings);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelAttributeSettingsModel readByChannelPartnerId(Integer channelPartnerId) {
		return channelAttributeSettingsMapper.readByChannelPartnerId(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelAttributeSettingsModel> readAll() {
		return channelAttributeSettingsMapper.readAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelAttributeSettingsModel> readAllVisibleAndComingSoon() {
		return channelAttributeSettingsMapper.readAllVisibleAndComingSoon();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByChannelPartnerId(Integer channelPartnerId) {
		channelAttributeSettingsMapper.deleteByChannelPartnerId(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelAttributeSettingsModel channelAttributeSettings) {
		if (channelAttributeSettings != null) {
			channelAttributeSettingsMapper.update(channelAttributeSettings);
		}
	}
}
