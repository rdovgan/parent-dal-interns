package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.utils.entity.IdVersion;

import com.mybookingpal.dataaccesslayer.dto.KeyCollection;
import com.mybookingpal.dataaccesslayer.dto.ProductAttributeWithName;
import com.mybookingpal.dataaccesslayer.dto.ProductManager;
import com.mybookingpal.dataaccesslayer.dto.RoomAttribute;
import com.mybookingpal.dataaccesslayer.entity.ProductAttributeModel;
import com.mybookingpal.dataaccesslayer.mappers.ProductAttributeMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class ProductAttributeDao {
	@Autowired
	ProductAttributeMapper productAttributeMapper;

	public void create(ProductAttributeModel productAttribute) {
		if (productAttribute != null) {
			productAttributeMapper.create(productAttribute);
		}
	}

	public void updateByProductAndAttribute(ProductAttributeModel productAttribute) {
		if (productAttribute != null) {
			productAttributeMapper.updateByProductAndAttribute(productAttribute);
		}
	}

	public List<String> readAttributeByProductId(Integer id) {
		return productAttributeMapper.readAttributeByProductId(id);
	}

	public void insertList(List<ProductAttributeModel> productAttributeList) {
		if (!CollectionUtils.isEmpty(productAttributeList)) {
			productAttributeMapper.insertList(productAttributeList);
		}
	}

	public void deleteByProductId(String productId) {
		productAttributeMapper.deleteByProductId(productId);
	}

	public void deleteByProductIdAndAttribute(String productId, String attribute) {
		productAttributeMapper.deleteByProductIdAndAttribute(productId, attribute);
	}

	public List<String> productIds(ProductAttributeModel productAttribute) {
		if (productAttribute == null) {
			return Collections.emptyList();
		}
		return productAttributeMapper.productIds(productAttribute);
	}

	public List<String> attributeIds(ProductAttributeModel productAttribute) {
		if (productAttribute == null) {
			return Collections.emptyList();
		}
		return productAttributeMapper.attributeIds(productAttribute);
	}

	public List<String> skipManualAttributeIds(ProductAttributeModel productAttribute) {
		if (productAttribute == null) {
			return Collections.emptyList();
		}
		return productAttributeMapper.skipManualAttributeIds(productAttribute);
	}

	public List<String> attributeIdsForPropertyValidator(String productId) {
		return productAttributeMapper.attributeIdsForPropertyValidator(productId);
	}

	/**
	 * @deprecated
	 * Use {@link ProductAttributeDao#deleteByProductIdAndAttribute(String, String)} instead
	 */
	@Deprecated
	public void delete(ProductAttributeModel productAttribute) {
		if (productAttribute != null && (StringUtils.isNotBlank(productAttribute.getAttributeId()) && StringUtils.isNotBlank(
				productAttribute.getProductId()))) {
			productAttributeMapper.delete(productAttribute);
		}
	}

	public List<ProductAttributeModel> getAllByProductId(String productId) {
		return productAttributeMapper.getAllByProductId(productId);
	}

	public List<ProductAttributeWithName> getAccommodationTypeWithNameByProductId(String productId) {
		return productAttributeMapper.getAccommodationTypeWithNameByProductId(productId);
	}

	public void updateQuantity(ProductAttributeModel productAttribute) {
		if (productAttribute != null) {
			productAttributeMapper.updateQuantity(productAttribute);
		}
	}

	public void updateDistance(ProductAttributeModel productAttribute) {
		if (productAttribute != null) {
			productAttributeMapper.updateDistance(productAttribute);
		}
	}

	public void updateDeleteAutoUpdate(ProductAttributeModel productAttribute) {
		if (productAttribute != null) {
			productAttributeMapper.updateDeleteAutoUpdate(productAttribute);
		}
	}

	public ProductAttributeModel getByProductIdAndAttributeId(ProductAttributeModel productAttribute) {
		if (productAttribute == null) {
			return null;
		}
		return productAttributeMapper.getByProductIdAndAttributeId(productAttribute);
	}

	public void copyAttributes(Integer fromProductId, Integer toProductId) {
		productAttributeMapper.copyAttributes(fromProductId, toProductId);
	}

	public List<ProductAttributeModel> readAttributesByProductIds(List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.readAttributesByProductIds(productIds);
	}

	public List<ProductAttributeModel> readAttributesStartWithPctForProducts(List<String> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.readAttributesStartWithPctForProducts(productIds);
	}

	public List<String> readAllAttributesBeginWithPrefixForProduct(Integer productId, String prefix) {
		return productAttributeMapper.readAllAttributesBeginWithPrefixForProduct(productId, prefix);
	}

	public List<ProductAttributeModel> getProductAttributesFromLastTimeStamp(String productId, Date version) {
		return productAttributeMapper.getProductAttributesFromLastTimeStamp(productId, version);
	}

	public List<ProductAttributeWithName> readAttributesDisplayNameForProduct(Integer productId) {
		return productAttributeMapper.readAttributesDisplayNameForProduct(productId);
	}

	public ProductAttributeModel getByProductAndAttribute(Long productId, List<String> attributes) {
		if (CollectionUtils.isEmpty(attributes)) {
			return null;
		}
		return productAttributeMapper.getByProductAndAttribute(productId, attributes);
	}

	public void deleteByProductIdAndAttributes(String productId, List<String> attributes) {
		if (!CollectionUtils.isEmpty(attributes)) {
			productAttributeMapper.deleteByProductIdAndAttributes(productId, attributes);
		}
	}

	public List<String> getMltProductIdsByAttributes(Long productId, List<String> attributes) {
		if (CollectionUtils.isEmpty(attributes)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.getMltProductIdsByAttributes(productId, attributes);
	}

	public List<ProductAttributeModel> getListByProductAndAttributes(Long productId, List<String> attributes) {
		if (CollectionUtils.isEmpty(attributes)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.getListByProductAndAttributes(productId, attributes);
	}

	public ProductAttributeModel getAttributeByDisplayCategory(Long productId, String displayName) {
		return productAttributeMapper.getAttributeByDisplayCategory(productId, displayName);
	}

	public void updateProductAttributeCode(String newDisplayCategory, Integer id) {
		if (StringUtils.isNotBlank(newDisplayCategory)) {
			productAttributeMapper.updateProductAttributeCode(newDisplayCategory, id);
		}
	}

	public List<RoomAttribute> getMltProductRoomAttributes(Long productId, List<String> attributes) {
		if (CollectionUtils.isEmpty(attributes)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.getMltProductRoomAttributes(productId, attributes);
	}

	public void deleteByIdList(List<Long> idList) {
		if (!CollectionUtils.isEmpty(idList)) {
			productAttributeMapper.deleteByIdList(idList);
		}
	}

	public void deleteMltProductRoomAttributes(String productId, List<String> attributes) {
		if (!CollectionUtils.isEmpty(attributes)) {
			productAttributeMapper.deleteMltProductRoomAttributes(productId, attributes);
		}
	}

	public void updateProductAttributesUpdateType(Integer productId, Boolean updateType) {
		productAttributeMapper.updateProductAttributesUpdateType(productId, updateType);
	}

	public void updateProductAttributesDeleteAutoUpdate(Integer productId, Boolean deleteAutoUpdate) {
		productAttributeMapper.updateProductAttributesDeleteAutoUpdate(productId, deleteAutoUpdate);
	}

	public void updateProductAttributesUpdateTypeForProducts(List<Integer> productIds, Boolean updateType) {
		if (!CollectionUtils.isEmpty(productIds)) {
			productAttributeMapper.updateProductAttributesUpdateTypeForProducts(productIds, updateType);
		}
	}

	public List<ProductAttributeModel> readAllAttributesByProductIds(List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.readAllAttributesByProductIds(productIds);
	}

	public List<ProductManager> getAllByProductIds(List<Integer> listProductId) {
		if (CollectionUtils.isEmpty(listProductId)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.getAllByProductIds(listProductId);
	}

	public List<ProductAttributeModel> isPetAllowed(String productId) {
		return productAttributeMapper.isPetAllowed(productId);
	}

	public KeyCollection getKeyCollection(Integer productId) {
		return productAttributeMapper.getKeyCollection(productId);
	}

	public void createKeyCollection(KeyCollection keyCollection) {
		if (keyCollection != null) {
			productAttributeMapper.createKeyCollection(keyCollection);
		}
	}

	public void updateKeyCollection(KeyCollection keyCollection) {
		if (keyCollection != null) {
			productAttributeMapper.updateKeyCollection(keyCollection);
		}
	}

	public void deleteKeyCollection(Integer id) {
		productAttributeMapper.deleteKeyCollection(id);
	}

	public Integer getAttributesCountByProductIdAndAttributeId(ProductAttributeModel productAttribute) {
		if (productAttribute == null) {
			return null;
		}
		return productAttributeMapper.getAttributesCountByProductIdAndAttributeId(productAttribute);
	}

	/**
	 * @deprecated
	 * Use {@link ProductAttributeDao#getByProductIdAndAttributeId(ProductAttributeModel)} instead
	 */
	@Deprecated
	public ProductAttributeModel readByProductIdAndAttribute(Integer productId, String attribute) {
		if (productId == null || StringUtils.isBlank(attribute)) {
			return null;
		}
		return productAttributeMapper.readByProductIdAndAttribute(productId, attribute);
	}

	public void deleteRoomCodesByProductId(Integer productId) {
		productAttributeMapper.deleteRoomCodesByProductId(productId);
	}

	public void deleteHotelCodesByProductId(Integer productId) {
		productAttributeMapper.deleteHotelCodesByProductId(productId);
	}

	public List<ProductAttributeWithName> getAllWithNamesByProductId(String productId) {
		return productAttributeMapper.getAllWithNamesByProductId(productId);
	}

	public List<IdVersion> getLastUpdateByProductIds(List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return productAttributeMapper.getLastUpdateByProductIds(productIds);
	}

}

