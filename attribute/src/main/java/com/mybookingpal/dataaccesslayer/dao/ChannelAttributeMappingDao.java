package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelAttributeMappingModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelAttributeMappingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
public class ChannelAttributeMappingDao {
	@Autowired
	ChannelAttributeMappingMapper channelAttributeMappingMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelAttributeMappingModel> readbyCodes(List<String> attributeCodes, Integer channelId) {
		if (CollectionUtils.isEmpty(attributeCodes)) {
			return Collections.emptyList();
		}
		return channelAttributeMappingMapper.readbyCodes(attributeCodes, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelAttributeMappingModel channelAttributeMappingModel) {
		if (channelAttributeMappingModel != null) {
			channelAttributeMappingMapper.create(channelAttributeMappingModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelAttributeMappingModel> readByExample(ChannelAttributeMappingModel example) {
		if (example == null) {
			return Collections.emptyList();
		}
		return channelAttributeMappingMapper.readByExample(example);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelAttributeMappingModel> readExistingListByCodesAndChannelId(List<String> attributeCodes, Integer channelId) {
		return channelAttributeMappingMapper.readExistingListByCodesAndChannelId(attributeCodes, channelId);
	}

}
