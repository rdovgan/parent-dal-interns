package com.mybookingpal.dataaccesslayer.dto;

public interface IsBinaryRuleKey {
	Long getId();

	String getName();

	Boolean getChecked();
}
