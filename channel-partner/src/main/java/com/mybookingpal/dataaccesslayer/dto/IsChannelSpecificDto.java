package com.mybookingpal.dataaccesslayer.dto;

public interface IsChannelSpecificDto {

	String getName();

	Integer getId();

	String getAbbreviation();
}
