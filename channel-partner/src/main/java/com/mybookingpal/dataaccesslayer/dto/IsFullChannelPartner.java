package com.mybookingpal.dataaccesslayer.dto;

import java.util.List;
import java.util.Map;

public interface IsFullChannelPartner {

	Integer getId();

	Integer getChannelPartnerId();

	String getName();

	String getChannelName();

	String getChannelAbbreviation();

	String getExtraName();

	String getEmailAddress();

	String getPhone();

	String getWebAddress();

	String getState();

	Map<String, String> getPostalAddress();

	String getAddressInString();

	String getPostalCode();

	String getCountry();

	Integer getParentId();

	String getPassword();

	String getType();

	Double getCommission();

	String getGuestProtections();

	String getHostProtections();

	String getCustomerServiceOverview();

	Boolean getGenerateXml();

	String getSignUpProcess();

	String getListingFees();

	String getCoverage();

	String getDescription();

	String getPaymentProcess();

	String getPayouts();

	String getResponsibleParty();

	String getContactType();

	String getChannelType();

	String getTraffic();

	String getFtpPassword();

	Double getBpCommission();

	String getTermsConditions();

	Boolean getCreateAccount();

	String getPrivacyPolicy();

	Boolean getFundsHolder();

	String getCustomerServiceContact();

	String getAccountingContact();

	String getLogoUrl();

	String getCallbackUrl();

	String getAddingSupplyIn();

	String getCancellationText();

	Boolean getAllowPublishLogo();

	String getAllowPropertiesWith();

	String getPropertiesWillNotPublish();

	String getIncludedInTotal();

	Boolean getSendEmail();

	Integer getMinimumOfRequiredProperties();

	Boolean getUseChannelApi();

	List<IsBinaryRuleKey> getSynchronization();

	Long getMask();

	Boolean getMasterChannel();

	String getPaymentModel();

	String getPartyLogoUrl();

	String getEmailsLogoURL();
}
