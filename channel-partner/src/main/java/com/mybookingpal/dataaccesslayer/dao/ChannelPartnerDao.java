package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelPartnerModel;
import com.mybookingpal.dataaccesslayer.constants.CommissionCalculationMethodType;
import com.mybookingpal.dataaccesslayer.dto.IsBinaryRuleKey;
import com.mybookingpal.dataaccesslayer.dto.IsChannelSpecificDto;
import com.mybookingpal.dataaccesslayer.dto.IsFullChannelPartner;
import com.mybookingpal.dataaccesslayer.constants.PaymentModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelPartnerMapper;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ChannelPartnerDao {
	private final ChannelPartnerMapper mapper;

	public ChannelPartnerDao(ChannelPartnerMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel read(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readChannelPartner(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return mapper.readChannelPartner(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> list() {
		return mapper.list();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (Objects.isNull(id)) {
			return;
		}
		mapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelPartnerModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		if (!EnumUtils.isValidEnum(PaymentModel.class, action.getPaymentModel())
				|| CommissionCalculationMethodType.getByNumber(action.getCommissionCalculationMethodType()) == null) {
			return;
		}
		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createChannelPartnerPart(IsFullChannelPartner action) {
		if (Objects.isNull(action)) {
			return;
		}
		if (!EnumUtils.isValidEnum(PaymentModel.class, action.getPaymentModel())) {
			return;
		}
		mapper.createChannelPartnerPart(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelPartnerModel partner) {
		if (Objects.isNull(partner)) {
			return;
		}
		if (!EnumUtils.isValidEnum(PaymentModel.class, partner.getPaymentModel())
				|| CommissionCalculationMethodType.getByNumber(partner.getCommissionCalculationMethodType()) == null) {
			return;
		}
		mapper.update(partner);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readBySupplierId(int supplierId) {
		return mapper.readBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateFullChannelPartner(IsFullChannelPartner channelPartner) {
		if (Objects.isNull(channelPartner)) {
			return;
		}
		if (!EnumUtils.isValidEnum(PaymentModel.class, channelPartner.getPaymentModel())) {
			return;
		}

		mapper.updateFullChannelPartner(channelPartner);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readByPartyId(int partyId) {
		return mapper.readByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readByAbbreviation(String abbreviation) {
		if (StringUtils.isBlank(abbreviation)) {
			return null;
		}
		return mapper.readByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> readListByAbbreviation(String abbreviation) {
		if (StringUtils.isBlank(abbreviation)) {
			return Collections.emptyList();
		}
		return mapper.readListByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readByAbbreviationChannelIDs(String abbreviation) {
		if (StringUtils.isBlank(abbreviation)) {
			return Collections.emptyList();
		}
		return mapper.readByAbbreviationChannelIDs(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readChannelIdsByAbbreviationMask(String abbreviation) {
		if (StringUtils.isBlank(abbreviation)) {
			return Collections.emptyList();
		}
		return mapper.readChannelIdsByAbbreviationMask(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readRelatedManagersByPartyId(int partyId) {
		return mapper.readRelatedManagersByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> ManagerByChannelList(int channelID) {
		return mapper.ManagerByChannelList(channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> getListChannelPartnerIdsForXMLs() {
		return mapper.getListChannelPartnerIdsForXMLs();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public double getMaxChannelCommission() {
		return mapper.getMaxChannelCommission();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getListOfChannelAbbreviationsByChannelIds(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return Collections.emptyList();
		}
		return mapper.getListOfChannelAbbreviationsByChannelIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> getListOfChannelPartnerByIds(List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return Collections.emptyList();
		}
		return mapper.getListOfChannelPartnerByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> getAllChannelByAbbreviation(List<String> abbreviations) {
		if (CollectionUtils.isEmpty(abbreviations)) {
			return Collections.emptyList();
		}
		return mapper.getAllChannelByAbbreviation(abbreviations);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> readChannels() {
		return mapper.readChannels();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> readChannelsWithAbbreviation() {
		return mapper.readChannelsWithAbbreviation();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> readChannelsWithActiveChannelStateInChannelProductMap() {
		return mapper.readChannelsWithActiveChannelStateInChannelProductMap();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readAbbreviationByChannelSpecificTaxGroupId(String groupId) {
		return mapper.readAbbreviationByChannelSpecificTaxGroupId(groupId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> readChannelPartnerByChannelSpecificTaxGroupId(String groupId) {
		if (StringUtils.isBlank(groupId)) {
			return Collections.emptyList();
		}
		return mapper.readChannelPartnerByChannelSpecificTaxGroupId(groupId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createChannelSpecificTaxGroupRelation(String groupId, Integer channelPartnerId) {
		if (StringUtils.isBlank(groupId) || Objects.isNull(channelPartnerId)) {
			return;
		}
		mapper.createChannelSpecificTaxGroupRelation(groupId, channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteChannelSpecificTaxGroupRelation(String groupId, Integer channelPartnerId) {
		if (StringUtils.isBlank(groupId) || Objects.isNull(channelPartnerId)) {
			return;
		}
		mapper.deleteChannelSpecificTaxGroupRelation(groupId, channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public IsFullChannelPartner getFullChannelPartnerByChannelId(Integer channelPartnerId) {
		if (Objects.isNull(channelPartnerId)) {
			return null;
		}
		return mapper.getFullChannelPartnerByChannelId(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsChannelSpecificDto> getChannelPartnerParentList() {
		return mapper.getChannelPartnerParentList();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsFullChannelPartner> getFullChannelPartners() {
		return mapper.getFullChannelPartners();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsBinaryRuleKey> readSynchronization() {
		return mapper.readSynchronization();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readByWebsiteUrl(String websiteUrl) {
		if (StringUtils.isBlank(websiteUrl)) {
			return null;
		}
		return mapper.readByWebsiteUrl(websiteUrl);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartnerModel> checkAbbreviation(String abbreviation) {
		if (StringUtils.isBlank(abbreviation)) {
			return null;
		}
		return mapper.checkAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartnerModel readByProductIdForExpedia(Integer productId) {
		if (Objects.isNull(productId)) {
			return null;
		}
		return mapper.readByProductIdForExpedia(productId);
	}

}
