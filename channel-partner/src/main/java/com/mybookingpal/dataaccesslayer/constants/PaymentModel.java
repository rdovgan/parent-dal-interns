package com.mybookingpal.dataaccesslayer.constants;

public enum PaymentModel {
	Centralized, Direct
}
