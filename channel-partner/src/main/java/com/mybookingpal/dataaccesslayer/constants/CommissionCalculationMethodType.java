package com.mybookingpal.dataaccesslayer.constants;

public enum CommissionCalculationMethodType {
	AIRBNB(1),
	BOOKING_COM(2),
	EXPEDIA(3),
	HOMEAWAY(4),
	GOOGLE(5);

	private final int number;

	CommissionCalculationMethodType(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public static CommissionCalculationMethodType getByNumber(int number) {
		for (CommissionCalculationMethodType commissionCalculationMethodType : values()) {
			if (commissionCalculationMethodType.number == number) {
				return commissionCalculationMethodType;
			}
		}
		return null;
	}
}
