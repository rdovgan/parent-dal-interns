package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.ChannelPartnerModel;
import com.mybookingpal.dataaccesslayer.constants.CommissionCalculationMethodType;
import org.springframework.stereotype.Component;

@Component
public class ChannelPartnerUtils {

	public boolean isCommissionCalculationMethodTypeEqualsTo(ChannelPartnerModel channelPartnerModel, CommissionCalculationMethodType commissionCalculationMethodType) {
		return commissionCalculationMethodType.getNumber() == channelPartnerModel.getCommissionCalculationMethodType();
	}
}
