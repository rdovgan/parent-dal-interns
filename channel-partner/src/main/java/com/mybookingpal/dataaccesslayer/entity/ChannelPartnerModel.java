package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * TODO: add annotations @XmlRootElement, @XmlAccessorType, @JsonSerialize
 * @XmlRootElement(name = "channel_partner")
 * @XmlAccessorType(XmlAccessType.FIELD)
 * @JsonSerialize(include = JsonSerialize.Inclusion.ALWAYS)
 */

@Component
@Scope(value = "prototype")
public class ChannelPartnerModel implements IsChannelPartner{

	public ChannelPartnerModel() {
		super();
		// default values regarding Net rates story
		this.bpCommission = 2.0;
	}

	public ChannelPartnerModel(Integer partyId, String channelName, String logoURL, String wizardLogoURL, String websiteURL, String channelType,
			String coverage, String contactType, String paymentProcess, String payouts, String damageCoverage, String traffic, Double commission,
			String listingFees, String privacyPolicy, String termsConditions, Boolean selected, String phone, String email, String officeAddress,
			String description, Double bpCommission, Boolean fundsHolder, Boolean createAccount, Boolean useChannelApi, Boolean allowUserToCancelReservation,
			String emailsLogoURL) {

		super();
		this.partyId = partyId;
		this.channelName = channelName;
		this.logoURL = logoURL;
		this.wizardLogoURL = wizardLogoURL;
		this.websiteURL = websiteURL;
		this.channelType = channelType;
		this.coverage = coverage;
		this.contactType = contactType;
		this.paymentProcess = paymentProcess;
		this.payouts = payouts;
		this.damageCoverage = damageCoverage;
		this.traffic = traffic;
		this.commission = commission;
		this.listingFees = listingFees;
		this.privacyPolicy = privacyPolicy;
		this.termsConditions = termsConditions;
		this.selected = selected;
		this.phone = phone;
		this.email = email;
		this.officeAddress = officeAddress;
		this.description = description;
		this.bpCommission = bpCommission;
		this.fundsHolder = fundsHolder;
		this.createAccount = createAccount;
		this.useChannelApi = useChannelApi;
		this.allowUserToCancelReservation = allowUserToCancelReservation;
		this.emailsLogoURL = emailsLogoURL;
	}

	public ChannelPartnerModel(Integer partyId, String channelName, String logoURL, String websiteURL, String channelType, String coverage, String contractType,
			String paymentProcess, String payouts, String damageCoverage, String traffic, Double commission, String listingFees, String privacyPolicy,
			String termsConditions, Boolean selected, String phone, String email, String officeAddress, String description, Double bpCommission,
			Boolean fundsHolder, String callbackURL, Boolean useChannelApi) {

		super();
		this.partyId = partyId;
		this.channelName = channelName;
		this.logoURL = logoURL;
		this.websiteURL = websiteURL;
		this.channelType = channelType;
		this.coverage = coverage;
		this.contractType = contractType;
		this.paymentProcess = paymentProcess;
		this.payouts = payouts;
		this.damageCoverage = damageCoverage;
		this.traffic = traffic;
		this.commission = commission;
		this.listingFees = listingFees;
		this.privacyPolicy = privacyPolicy;
		this.termsConditions = termsConditions;
		this.selected = selected;
		this.phone = phone;
		this.email = email;
		this.officeAddress = officeAddress;
		this.description = description;
		this.bpCommission = bpCommission;
		this.fundsHolder = fundsHolder;
		this.callbackURL = callbackURL;
		this.useChannelApi = useChannelApi;
	}

	//TODO: add annotations @XmlTransient, @SerializedName, @XmlElement

	private Integer id;

	//	@XmlTransient
	private Integer partyId;

	//	@SerializedName("channel_name")
	//	@XmlElement(name = "channel_name")
	private String channelName;

	//	@SerializedName("logo_url")
	//	@XmlElement(name = "logo_url")
	private String logoURL;

	//	@SerializedName("wizard_logo_url")
	//	@XmlElement(name = "wizard_logo_url")
	private String wizardLogoURL;

	//	@SerializedName("website_url")
	//	@XmlElement(name = "website_url")
	private String websiteURL;

	//	@SerializedName("channel_type")
	//	@XmlElement(name = "channel_type")
	private String channelType;

	private String coverage;

	//	@SerializedName("contact_type")
	//	@XmlElement(name = "contact_type")
	private String contactType;

	//	@SerializedName("payment_process")
	//	@XmlElement(name = "payment_process")
	private String paymentProcess;

	private String payouts;

	//	@SerializedName("damage_coverage")
	//	@XmlElement(name = "damage_coverage")
	private String damageCoverage;

	private String traffic;

	private Double commission;

	//	@SerializedName("listing_fees")
	//	@XmlElement(name = "listing_fees")
	private String listingFees;

	//	@SerializedName("privacy_policy")
	//	@XmlElement(name = "privacy_policy")
	private String privacyPolicy;

	//	@SerializedName("terms_conditions")
	//	@XmlElement(name = "terms_conditions")
	private String termsConditions;

	private Boolean selected;

	//	@SerializedName("phone")
	//	@XmlElement(name = "phone")
	private String phone;

	//	@SerializedName("email")
	//	@XmlElement(name = "email")
	private String email;

	//	@SerializedName("office_address")
	//	@XmlElement(name = "office_address")
	private String officeAddress;

	//	@SerializedName("description")
	//	@XmlElement(name = "description")
	private String description;

	//	@SerializedName("bp_commission")
	//	@XmlElement(name = "bp_commission")
	private Double bpCommission;

	//	@SerializedName("use_channel_api")
	//	@XmlElement(name = "use_channel_api")
	private Boolean useChannelApi;

	private Boolean sendConfirmationEmail;
	private Boolean sendFailureEmail;

	//	@SerializedName("CreateAccount")
	//	@XmlElement(name = "CreateAccount")
	private Boolean createAccount;

	//	@SerializedName("funds_holder")
	//	@XmlElement(name = "funds_holder")
	private Boolean fundsHolder;

	//	@SerializedName("failed_reservation")
	//	@XmlElement(name = "failed_reservation")
	private Boolean failedReservation;

	//	@SerializedName("virtual_credit_card")
	//	@XmlElement(name = "virtual_credit_card")
	private Integer virtualCreditCard;

	private Integer specificMorGateway;

	//	@SerializedName("funds_available_days")
	//	@XmlElement(name = "funds_available_days")
	private Integer fundsAvailableDays;

	//	@SerializedName("card_ expiration")
	//	@XmlElement(name = "card_ expiration")
	private Integer cardExpiration;

	//	@SerializedName("support_cancellation")
	//	@XmlElement(name = "support_cancellation")
	private Boolean supportCancellation;

	//	@SerializedName("allow_user_to_cancel_reservation")
	//	@XmlElement(name = "allow_user_to_cancel_reservation")
	private Boolean allowUserToCancelReservation;

	private Integer commissionCalculationMethodType;

	private String abbreviation;

	private String paymentModel;

	private Boolean supressInvoiceEmail;

	//	@SerializedName("emails_logo_url")
	//	@XmlElement(name = "emails_logo_url")
	private String emailsLogoURL;

	//@SerializedName("contract_type")
	//@XmlElement(name = "contract_type")
	private String contractType;

	private String callbackURL;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getLogoURL() {
		return logoURL;
	}

	public void setLogoURL(String logoURL) {
		this.logoURL = logoURL;
	}

	public String getWizardLogoURL() {
		return wizardLogoURL;
	}

	public void setWizardLogoURL(String wizardLogoURL) {
		this.wizardLogoURL = wizardLogoURL;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getPaymentProcess() {
		return paymentProcess;
	}

	public void setPaymentProcess(String paymentProcess) {
		this.paymentProcess = paymentProcess;
	}

	public String getPayouts() {
		return payouts;
	}

	public void setPayouts(String payouts) {
		this.payouts = payouts;
	}

	public String getDamageCoverage() {
		return damageCoverage;
	}

	public void setDamageCoverage(String damageCoverage) {
		this.damageCoverage = damageCoverage;
	}

	public String getTraffic() {
		return traffic;
	}

	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public String getListingFees() {
		return listingFees;
	}

	public void setListingFees(String listingFees) {
		this.listingFees = listingFees;
	}

	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public String getTermsConditions() {
		return termsConditions;
	}

	public void setTermsConditions(String termsConditions) {
		this.termsConditions = termsConditions;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getBpCommission() {
		return bpCommission;
	}

	public void setBpCommission(Double bpCommission) {
		this.bpCommission = bpCommission;
	}

	public Boolean getUseChannelApi() {
		return useChannelApi;
	}

	public void setUseChannelApi(Boolean useChannelApi) {
		this.useChannelApi = useChannelApi;
	}

	public Boolean isSendConfirmationEmail() {
		return sendConfirmationEmail;
	}

	public void setSendConfirmationEmail(Boolean sendConfirmationEmail) {
		this.sendConfirmationEmail = sendConfirmationEmail;
	}

	public Boolean isSendFailureEmail() {
		return sendFailureEmail;
	}

	public void setSendFailureEmail(Boolean sendFailureEmail) {
		this.sendFailureEmail = sendFailureEmail;
	}

	public Boolean getCreateAccount() {
		return createAccount;
	}

	public void setCreateAccount(Boolean createAccount) {
		this.createAccount = createAccount;
	}

	public Boolean isFundsHolder() {
		return fundsHolder != null && fundsHolder;
	}

	public void setFundsHolder(Boolean fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public Boolean isFailedReservation() {
		return failedReservation;
	}

	public void setFailedReservation(Boolean failedReservation) {
		this.failedReservation = failedReservation;
	}

	public Integer getVirtualCreditCard() {
		return virtualCreditCard;
	}

	public void setVirtualCreditCard(Integer virtualCreditCard) {
		this.virtualCreditCard = virtualCreditCard;
	}

	public Integer getSpecificMorGateway() {
		return specificMorGateway;
	}

	public void setSpecificMorGateway(Integer specificMorGateway) {
		this.specificMorGateway = specificMorGateway;
	}

	public Integer getFundsAvailableDays() {
		return fundsAvailableDays;
	}

	public void setFundsAvailableDays(Integer fundsAvailableDays) {
		this.fundsAvailableDays = fundsAvailableDays;
	}

	public Integer getCardExpiration() {
		return cardExpiration;
	}

	public void setCardExpiration(Integer cardExpiration) {
		this.cardExpiration = cardExpiration;
	}

	public Boolean getSupportCancellation() {
		return supportCancellation;
	}

	public void setSupportCancellation(Boolean supportCancellation) {
		this.supportCancellation = supportCancellation;
	}

	public Boolean getAllowUserToCancelReservation() {
		return allowUserToCancelReservation;
	}

	public void setAllowUserToCancelReservation(Boolean allowUserToCancelReservation) {
		this.allowUserToCancelReservation = allowUserToCancelReservation;
	}

	public Integer getCommissionCalculationMethodType() {
		return commissionCalculationMethodType;
	}

	public void setCommissionCalculationMethodType(Integer commissionCalculationMethodType) {
		this.commissionCalculationMethodType = commissionCalculationMethodType;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getPaymentModel() {
		return paymentModel;
	}

	public void setPaymentModel(String paymentModel) {
		this.paymentModel = paymentModel;
	}

	public Boolean getSupressInvoiceEmail() {
		return supressInvoiceEmail;
	}

	public void setSupressInvoiceEmail(Boolean supressInvoiceEmail) {
		this.supressInvoiceEmail = supressInvoiceEmail;
	}

	public String getEmailsLogoURL() {
		return emailsLogoURL;
	}

	public void setEmailsLogoURL(String emailsLogoURL) {
		this.emailsLogoURL = emailsLogoURL;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getCallbackURL() {
		return callbackURL;
	}

	public void setCallbackURL(String callbackURL) {
		this.callbackURL = callbackURL;
	}
}
