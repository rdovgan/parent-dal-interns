package com.mybookingpal.dataaccesslayer.entity;

public interface IsChannelPartner {

	Integer getId();

	Integer getPartyId();

	String getAbbreviation();
}
