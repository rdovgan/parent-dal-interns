package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelPartnerModel;
import com.mybookingpal.dataaccesslayer.dto.IsBinaryRuleKey;
import com.mybookingpal.dataaccesslayer.dto.IsChannelSpecificDto;
import com.mybookingpal.dataaccesslayer.dto.IsFullChannelPartner;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelPartnerMapper {

	ChannelPartnerModel read(Integer id);

	ChannelPartnerModel readChannelPartner(Integer id);

	List<ChannelPartnerModel> list();

	void delete(Integer id);

	void create(ChannelPartnerModel action);

	void createChannelPartnerPart(IsFullChannelPartner action);

	void update(ChannelPartnerModel partner);

	ChannelPartnerModel readBySupplierId(Integer supplierId);

	void updateFullChannelPartner(IsFullChannelPartner channelPartner);

	ChannelPartnerModel readByPartyId(Integer partyId);

	ChannelPartnerModel readByAbbreviation(String abbreviation);

	List<ChannelPartnerModel> readListByAbbreviation(@Param("abbreviation") String abbreviation);

	List<Integer> readByAbbreviationChannelIDs(@Param("abbreviation") String abbreviation);

	List<Integer> readChannelIdsByAbbreviationMask(@Param("abbreviation") String abbreviation);

	List<String> readRelatedManagersByPartyId(Integer partyId);

	List<Integer> ManagerByChannelList(Integer channelID);

	List<ChannelPartnerModel> getListChannelPartnerIdsForXMLs();

	Double getMaxChannelCommission();

	List<String> getListOfChannelAbbreviationsByChannelIds(@Param("ids") List<Integer> ids);

	List<ChannelPartnerModel> getListOfChannelPartnerByIds(@Param("ids") List<Integer> ids);

	List<ChannelPartnerModel> getAllChannelByAbbreviation(@Param("abbreviations") List<String> abbreviations);

	List<ChannelPartnerModel> readChannels();

	List<ChannelPartnerModel> readChannelsWithAbbreviation();

	List<ChannelPartnerModel> readChannelsWithActiveChannelStateInChannelProductMap();

	List<String> readAbbreviationByChannelSpecificTaxGroupId(@Param("groupId") String groupId);

	List<ChannelPartnerModel> readChannelPartnerByChannelSpecificTaxGroupId(@Param("groupId") String groupId);

	void createChannelSpecificTaxGroupRelation(@Param("groupId") String groupId, @Param("channelPartnerId") Integer channelPartnerId);

	void deleteChannelSpecificTaxGroupRelation(@Param("groupId") String groupId, @Param("channelPartnerId") Integer channelPartnerId);

	IsFullChannelPartner getFullChannelPartnerByChannelId(@Param("channelPartnerId") Integer channelPartnerId);

	List<IsChannelSpecificDto> getChannelPartnerParentList();

	List<IsFullChannelPartner> getFullChannelPartners();

	List<IsBinaryRuleKey> readSynchronization();

	ChannelPartnerModel readByWebsiteUrl(@Param("websiteUrl") String websiteUrl);

	List<ChannelPartnerModel> checkAbbreviation(@Param("abbreviation") String abbreviation);

	ChannelPartnerModel readByProductIdForExpedia(@Param("productId") Integer productId);
}
