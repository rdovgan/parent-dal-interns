## Tables used:
- **Main table**
    - channel_partner (main table)  
- **Additional tables** 
    - enums (used in these methods: _readSynchronization_)
    - party(used in these methods: _getFullChannelPartnerByChannelId, getFullChannelPartners, readChannels_)
    - manager_to_channel (used in these methods: _readRelatedManagersByPartyId, ManagerByChannelList_)
    - channel_specific_tax_group_relation (used in these methods: _deleteChannelSpecificTaxGroupRelation_)
    - channel_product_map (used in these methods: _readChannelsWithActiveChannelStateInChannelProductMap, readByProductIdForExpedia_)
    
    