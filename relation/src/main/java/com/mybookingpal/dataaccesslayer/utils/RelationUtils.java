package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.RelationModel;
import org.springframework.stereotype.Component;

@Component
public class RelationUtils {

	public boolean noLink(RelationModel relationModel) {
		return relationModel.getLink() == null || relationModel.getLink().isEmpty();
	}

	public boolean noHeadid(RelationModel relationModel) {
		return relationModel.getHeadid() == null || relationModel.getHeadid().isEmpty();
	}

	public boolean noLineid(RelationModel relationModel) {
		return relationModel.getLineid() == null || relationModel.getLineid().isEmpty();
	}

	public boolean noHeadidOrLineid(RelationModel relationModel) {
		return this.noHeadid(relationModel) && this.noLineid(relationModel);
	}

	public boolean isEmpty(RelationModel relationModel) {
		return relationModel.getLink() == null || relationModel.getLink().isEmpty() ||
				relationModel.getHeadid() == null || relationModel.getHeadid().isEmpty() ||
				relationModel.getLineid() == null || relationModel.getLineid().isEmpty();
	}
}
