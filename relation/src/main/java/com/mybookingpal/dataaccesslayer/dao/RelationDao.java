package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.RelationModel;
import com.mybookingpal.dataaccesslayer.mappers.RelationMapper;
import com.mybookingpal.dataaccesslayer.utils.RelationUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class RelationDao {

	private final RelationUtils relationUtils;
	private final RelationMapper relationMapper;

	public RelationDao(RelationUtils relationUtils, RelationMapper relationMapper) {
		this.relationUtils = relationUtils;
		this.relationMapper = relationMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(RelationModel relation) {
		if (Objects.isNull(relation) || relationUtils.isEmpty(relation)) {
			return;
		}
		relationMapper.create(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RelationModel exists(RelationModel relation) {
		if (Objects.isNull(relation) || relationUtils.isEmpty(relation)) {
			return null;
		}
		return relationMapper.exists(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public RelationModel match(RelationModel relation) {
		if (Objects.isNull(relation) || relationUtils.isEmpty(relation)) {
			return null;
		}
		return relationMapper.match(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(RelationModel relation) {
		if (Objects.isNull(relation)) {
			return;
		}
		relationMapper.delete(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deletekey(RelationModel relation) {
		if (Objects.isNull(relation) || relationUtils.isEmpty(relation)) {
			return;
		}
		relationMapper.deletekey(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RelationModel> list(RelationModel relation) {
		if (Objects.isNull(relation)) {
			return Collections.emptyList();
		}
		return relationMapper.list(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> headids(RelationModel relation) {
		if (Objects.isNull(relation)) {
			return Collections.emptyList();
		}
		return relationMapper.headids(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> lineids(RelationModel relation) {
		if (Objects.isNull(relation)) {
			return Collections.emptyList();
		}
		return relationMapper.lineids(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> attributes(RelationModel relation) {
		if (Objects.isNull(relation) || relationUtils.isEmpty(relation)) {
			return Collections.emptyList();
		}
		return relationMapper.attributes(relation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return;
		}
		relationMapper.deleteByProductId(productId);
	}
}
