package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.RelationModel;

import java.util.List;

public interface RelationMapper {

	void create(RelationModel relation);

	RelationModel exists(RelationModel relation);

	RelationModel match(RelationModel relation);

	void delete(RelationModel relation);

	void deletekey(RelationModel relation);

	List<RelationModel> list(RelationModel relation);

	List<String> headids(RelationModel relation);

	List<String> lineids(RelationModel relation);

	List<String> attributes(RelationModel relation);

	void deleteByProductId(String productId);
}
