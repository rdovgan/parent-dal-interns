package com.mybookingpal.dataaccesslayer.dto;

public interface IsDownloaded {
	String name();
}
