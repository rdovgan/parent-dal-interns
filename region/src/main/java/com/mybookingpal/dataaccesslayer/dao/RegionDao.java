package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.Region;
import com.mybookingpal.dataaccesslayer.mappers.RegionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegionDao {

	@Autowired
	private RegionMapper regionMapper;

	public Region readByLocation(String id, String country) {
		return regionMapper.readbylocation(id,country);
	}

	public Region readByName(Region action) {
		return regionMapper.readbyname(action);
	}
}