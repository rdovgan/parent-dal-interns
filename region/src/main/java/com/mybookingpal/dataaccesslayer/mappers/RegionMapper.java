package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.Region;
import org.apache.ibatis.annotations.Param;

public interface RegionMapper {

	Region readbylocation(@Param("id") String id, @Param("country") String country);

	Region readbyname(Region action);
}