package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.utils.entity.NameId;

public class Region extends NameId {

	public static final String ALL = "ALL";
	public static final String ZAWC = "ZAWC"; //South Africa Western Cape


	private String country;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Region [country=");
		builder.append(country);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}
