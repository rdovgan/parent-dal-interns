package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.AddressComponents;
import com.mybookingpal.dataaccesslayer.entity.ProductAddressComponents;
import com.mybookingpal.dataaccesslayer.entity.ProductIdWithAddressComponentsDto;
import com.mybookingpal.dataaccesslayer.mappers.AddressComponentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressComponentsDao {

	@Autowired
	private AddressComponentsMapper addressComponentsMapper;

	public void create(AddressComponents addressComponents) {
		addressComponentsMapper.create(addressComponents);
	}

	public AddressComponents read(int id) {
		return addressComponentsMapper.read(id);
	}

	public void update(AddressComponents addressComponents) {
		addressComponentsMapper.update(addressComponents);
	}

	public void delete(AddressComponents addressComponents) {
		addressComponentsMapper.delete(addressComponents);
	}

	public List<AddressComponents> list(List<Integer> addressComponentsIdList) {
		return addressComponentsMapper.list(addressComponentsIdList);
	}

	public List<AddressComponents> readRangeOfAddressComponentsWithoutZipCode9(Integer limit, Integer numberDays) {
		return addressComponentsMapper.readRangeOfAddressComponentsWithoutZipCode9(limit, numberDays);
	}

	public List<ProductAddressComponents> readMissingZipCode9AddressComponentsWithLatLongAfterCreatedDate(List<String> countries){
		return addressComponentsMapper.readMissingZipCode9AddressComponentsWithLatLongAfterCreatedDate(countries);
	}

	public void updateZipCode9(AddressComponents addressComponents) {
		addressComponentsMapper.updateZipCode9(addressComponents);
	}

	public void updateAddress(AddressComponents addressComponents) {
		addressComponentsMapper.updateAddress(addressComponents);
	}

	public List<ProductIdWithAddressComponentsDto> getProductIdWithAddressComponentsList(Integer limit) {
		return addressComponentsMapper.getProductIdWithAddressComponentsList(limit);
	}

}
