package com.mybookingpal.dataaccesslayer.entity;

public class ProductAddressComponents {
	private int productId, addressComponentId;
	private String postalCode, country, region, city, street,state;
	private double longitude, latitude;

	public ProductAddressComponents() {
	}

	public ProductAddressComponents(int productId, int addressComponentId,String state, String postalCode, String country, String region, String city, String street, double longitude, double latitude) {
		this.productId = productId;
		this.addressComponentId = addressComponentId;
		this.postalCode = postalCode;
		this.country = country;
		this.region = region;
		this.city = city;
		this.street = street;
		this.longitude = longitude;
		this.latitude = latitude;
		this.state = state;
	}



	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getAddressComponentId() {
		return addressComponentId;
	}

	public void setAddressComponentId(int addressComponentId) {
		this.addressComponentId = addressComponentId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
}
