package com.mybookingpal.dataaccesslayer.entity;


public class AddressComponents  {
	private Integer id;
	private String postalCode;
	private String country;
	private String region;
	private String city;
	private String street;
	private String apartmentSuiteBuilding;
	private String checkinAddress;
	private String checkinCity;
	private String checkinZip;
	private String zipCode9;

	public AddressComponents() {
	}

	public AddressComponents(AddressComponents addressComponents) {
		this.postalCode = addressComponents.getPostalCode();
		this.country = addressComponents.getCountry();
		this.region = addressComponents.getRegion();
		this.city = addressComponents.getCity();
		this.street = addressComponents.getStreet();
	}

	public AddressComponents(String street, String city,String region,String country,String postalCode) {
		this.postalCode = postalCode;
		this.country = country;
		this.region = region;
		this.city = city;
		this.street = street;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getApartmentSuiteBuilding() {
		return apartmentSuiteBuilding;
	}

	public void setApartmentSuiteBuilding(String apartmentSuiteBuilding) {
		this.apartmentSuiteBuilding = apartmentSuiteBuilding;
	}

	public String getCheckinAddress() {
		return checkinAddress;
	}

	public void setCheckinAddress(String checkinAddress) {
		this.checkinAddress = checkinAddress;
	}

	public String getCheckinCity() {
		return checkinCity;
	}

	public void setCheckinCity(String checkinCity) {
		this.checkinCity = checkinCity;
	}

	public String getCheckinZip() {
		return checkinZip;
	}

	public void setCheckinZip(String checkinZip) {
		this.checkinZip = checkinZip;
	}

	public String getZipCode9() {
		return zipCode9;
	}

	public void setZipCode9(String zipCode9) {
		this.zipCode9 = zipCode9;
	}

	@Override
	public String toString() {
		return "AddressComponents{" + "id: " + id + ", postalCode: '" + postalCode + '\'' + ", country: '" + country + '\'' + ", region: '" + region + '\''
				+ ", city: '" + city + '\'' + ", street: '" + street + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AddressComponents))
			return false;

		AddressComponents that = (AddressComponents) o;

		if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null)
			return false;
		if (getPostalCode() != null ? !getPostalCode().equals(that.getPostalCode()) : that.getPostalCode() != null)
			return false;
		if (getCountry() != null ? !getCountry().equals(that.getCountry()) : that.getCountry() != null)
			return false;
		if (getRegion() != null ? !getRegion().equals(that.getRegion()) : that.getRegion() != null)
			return false;
		if (getCity() != null ? !getCity().equals(that.getCity()) : that.getCity() != null)
			return false;
		return getStreet() != null ? getStreet().equals(that.getStreet()) : that.getStreet() == null;

	}

	@Override
	public int hashCode() {
		int result = getId() != null ? getId().hashCode() : 0;
		result = 31 * result + (getPostalCode() != null ? getPostalCode().hashCode() : 0);
		result = 31 * result + (getCountry() != null ? getCountry().hashCode() : 0);
		result = 31 * result + (getRegion() != null ? getRegion().hashCode() : 0);
		result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
		result = 31 * result + (getStreet() != null ? getStreet().hashCode() : 0);
		return result;
	}
}
