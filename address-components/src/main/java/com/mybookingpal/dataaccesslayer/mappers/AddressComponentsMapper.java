package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.AddressComponents;
import com.mybookingpal.dataaccesslayer.entity.ProductAddressComponents;
import com.mybookingpal.dataaccesslayer.entity.ProductIdWithAddressComponentsDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressComponentsMapper {

	void create(AddressComponents addressComponents);

	AddressComponents read(int id);

	void update(AddressComponents addressComponents);

	void delete(AddressComponents addressComponents);

	List<AddressComponents> list(List<Integer> idList);

	List<AddressComponents> readRangeOfAddressComponentsWithoutZipCode9(@Param("limit") Integer limit, @Param("numberDays") Integer numberDays);

	List<ProductAddressComponents> readMissingZipCode9AddressComponentsWithLatLongAfterCreatedDate(@Param("countries") List<String> countries);

	void updateZipCode9(AddressComponents addressComponents);

	void updateAddress(AddressComponents addressComponents);

	List<ProductIdWithAddressComponentsDto> getProductIdWithAddressComponentsList(@Param("limit") Integer limit);
}
