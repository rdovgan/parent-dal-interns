package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PartnerModel;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface PartnerMapper {

	void create(PartnerModel action);

	PartnerModel read(String id);

	void update(PartnerModel action);

	PartnerModel exists(String partyId);

	String partyIDFromEmail(String emailAddress);

	String partyIDFromAbbreviation(String abbreviation);

	Boolean isSupportCreditCard(String partyId);

	List<String> getPartnerPartyIDByAbbreviation(String[] abbreviations);

	ArrayList<PartnerModel> listPropertyManagementSoftware();

	List<String> getAllChildPartnerPartyIDByAbbreviation(String abbreviation);

	List<PartnerModel> getAllChildPartnerByAbbreviation(String abbreviation);

	List<PartnerModel> getAllActivePmsForSupplierApi();

	List<PartnerModel> getAllActivePmsWhichNotInBlacklist();

	PartnerModel getbyPMSId(@Param("pmsId") Integer pmsId);

	PartnerModel getByPMorPMSId(@Param("pmOrPmsId") Integer pmOrPmsId);

	PartnerModel readByPartyId(Integer partyId);

	List<PartnerModel> readListByPartyIds(@Param("list") List<Integer> partyIds);

	PartnerModel getParentByPartyId(Integer partyId);

	PartnerModel getByAbbreviationAndApiKey(@Param("apiKey") String apiKey, @Param("abbreviation") String abbreviation);

	List<String> listAbbreviations();

	List<String> getAllPartyIdFromPartnerId(String partnerId);

	List<PartnerModel> getAllPartnerByAbbreviation(Integer organizationId);

	List<PartnerModel> getAllPartnerByOrganizationID(String[] organizationId);
}
