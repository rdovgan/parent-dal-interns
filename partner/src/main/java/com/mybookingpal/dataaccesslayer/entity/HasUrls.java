package com.mybookingpal.dataaccesslayer.entity;

public interface HasUrls {

	/**
	 * Razor Local Development
	 boolean LIVE = true;
	 String ROOT_DIRECTORY = "C:/htdocs/";
	 String TOMCAT = "C:/tomcat";
	 */

	/**
	 * Razor Cloud Live
	 *
	 * ADD TO JournalMapper.xml license query
	 */
	boolean LIVE = true;

	String ROOT_DIRECTORY = "H:/htdocs/";
	String ROOT_LINUX_DIRECTORY = "/data1/htdocs/";

	String TOMCAT = System.getProperty("os.name").contains("Win") ? "C:/tomcat" : "/opt/tomcatRazor";
	String SERVICE = "razor";

	String ABBREVIATION_NO_PMS = "NOPMS";
	//start using BrandAbbrevation.
	String ABBREVATION_INTERHOME_AG = "INT";
	String ABBREVATION_NEXTPAX_MAIN_ACCOUNT = "NEX";
	String ABBERVATION_DANCENTER_NEXTPAX = "DAN";
	String ABBERVATION_HOGENBOOM_NEXTPAX = "HOG";
	String ABBERVATION_HAPPYHOME_NEXTPAX = "HAP";
	String ABBERVATION_GRANALACANT_NEXTPAX = "GRA";
	String ABBREVATION_HOLIDAYHOME_NEXTPAX = "HOL";
	String ABBREVATION_INTERCHALET_NEXTPAX = "INC";
	String ABBREVATION_INTERHOME_NEXTPAX = "INH";
	String ABBREVATION_NOVASOL_NEXTPAX = "NOV";
	String ABBREVATION_ROOMPOT_NEXTPAX = "ROO";
	String ABBREVATION_TOPICTRAVEL_NEXTPAX = "TOP";
	String ABBREVATION_TUIFERIENHAUS_NEXTPAX = "TUI";
	String ABBREVATION_UPHILLTRAVEL_NEXTPAX = "UPH";
	String ABBREVATION_VACASOL_NEXTPAX = "VAC";
	String ABBREVATION_BELVILLA_NEXTPAX = "BEL";
	String ABBREVATION_BUNGALOWNET_NEXTPAX = "BUN";
	String ABBREVATION_B2F = "B2F";
	String ABBREVATION_LAKE_TAHOE_ACCOMODATIONS = "LAK";
	String ABBREVATION_LEADERSTAY = "LST";
	String ABBREVATION_WAYTOSTAY = "WTS";
	String ABBREVATION_MAXXTON = "MEX";
	String ABBREVATION_CIIRUS = "CII";
	String ABBREVATION_AAXSYS = "AAX";
	String ABBREVATION_RENTALS_UNITED = "REN";
	String ABBREVATION_APARTSMENT_APART = "AAP";
	String ABBREVATION_FRIENDLYRENTALS = "FRR";
	String ABBREVATION_MPCONCEPT = "MPC";
	String ABBREVATION_BOOKT_INSTAMANAGER = "BKT";
	String ABBREVATION_RESORT_MANAGEMENT_SYSTEMS = "RMS";
	String ABBREVATION_PATHWAY_GDS_MAIN = "PAG";
	String ABBREVATION_INTERHOME_PATHWAY = "INP";
	String ABBREVATION_WEBCHALET = "WBC";
	String ABBREVATION_STREAMLINE = "STR";
	String ABBREVATION_ORLANDO = "ORD";
	String ABBREVATION_BACHCARE = "BCC";
	String ABBREVATION_LODGIX = "LDX";
	String ABBREVATION_LEISURELINK = "LEL";
	String ABBREVATION_BAREFOOT = "BAR";
	String ABBREVATION_ADRIATIC = "ADR";
	String ABBREVATION_HOMEAWAY_ISI = "ISI";
	String ABBREVATION_ISI_V12 = "V12";
	String ABBREVATION_ISI_PROPERTY_PLUS = "PPL";
	String ABBREVATION_ISI_ENTECH = "ENT";
	String ABBREVATION_ISI_FIRST_RESORT = "FRS";
	String ABBREVATION_HOMEAWAY_ESCAPIA = "HES";
	String ABBREVATION_ODALYS = "ODL";
	String ABBREVATION_BELVILLA = "BLV";
	String ABBREVATION_TRIPVILLAS = "TRP";
	String ABBREVATION_VACATIONBRIDGE = "TVB";
	String ABBREVATION_VACATIONBRIDGE_VRM = "VRM";
	String ABBREVATION_VACATIONBRIDGE_RNS = "RNS";
	String ABBREVATION_ITRIP ="ITR";
	String ABBREVATION_RAU ="RAU";
	String ABBREVATION_AGNI_TRAVEL ="AGI";
	String ABBREVATION_NATURAL_RETREATS = "NAR";
	String ABBREVATION_HOMERESA="HRS";
	String ABBREVATION_WIMCO ="WMC";
	String ABBREVATION_SUMMER_IN_ITALY ="SII";
	String ABBREVATION_RHOF ="ROF";
	String ABBREVATION_B2B_ISLANDS_TRAVEL ="BIS";
	String ABBREVATION_SKYRUN ="SKY";
	String ABBREVATION_VACASA ="VCS";
	String ABBREVATION_IQWARE ="IQW";
	String ABBREVATION_VRP ="VRP";
	String ABBREVATION_MARQUIS_RENTALS ="MQR";
	String ABBREVATION_TUSCANY ="TUS";
	String ABBREVATION_HOMEREZ="HRZ";
	String ABBREVATION_ESTARGROUP="ESG";
	String ABBREVATION_VRBOOKING="VRB";
	String ABBREVATION_LIVE_REZ ="LRZ";
	String ABBREVATION_INNTOPIA ="INN";
	String ABBREVATION_MAESTRO ="MST";
	String ABBREVATION_LODGIFY ="LGY";
	String ABBREVATION_TEST_PMS ="TST";
	String ABBREVATION_BOOKERVILLE ="BKV";
	String ABBREVATION_RAZOR ="RAZ";
	String ABBREVATION_VREASY ="VRY";
	String ABBREVATION_BOOKINGSYNC ="BSY";
	String ABBREVATION_SYNXIS ="SYN";
	String ABBREVATION_FRONTDESKANYWHERE="FDA";
	String ABBREVATION_REALTIMERENTAL ="RTR";
	String ABBREVATION_MOUNTAINSKI ="MSK";
	String ABBREVATION_LOCALLER = "LCL";
	String ABBREVATION_MUCHOSOL = "MCS";
	String ABBREVATION_ORBIRENTAL = "ORB";
	String ABBREVATION_AMAZING_ACCOMODATION = "AAC";
	String ABBREVATION_AMADEUS= "AMD";
	String ABBREVATION_MAGARENTAL = "MGR";
	String ABBREVATION_VACASOL = "VSL";
	String ABBREVATION_ARKIANE_NEW = "ARK";
	String ABBREVATION_ARKIANE_OLD = "ARN";
	String ABBREVATION_ARKIANE_NEW_LV = "ARN";
	String ABBREVATION_ARKIANE_OLD_LS = "ARO";
	String ABBREVATION_BOOKINGLETTINGS = "BKL";
	String ABBREVATION_LESPRI = "LSP";
	String ABBREVATION_THILAND_HOLIDAY_HOMES = "THH";
	String ABBREVATION_CONDO_WORLD = "COW";
	String ABBREVATION_LACURE = "LAC";
	String ABBREVATION_TRACK = "TRK";
	String ABBREVATION_BRIGHT_SIDE = "BGS";
	String ABBREVATION_DESTINATIONHOTELS = "DNH";
	String ABBREVATION_WINTERGREEN = "WIG";
	String ABBREVATION_GUESTY = "GST";
	String ABBREVATION_PRIVATE_VILLA_RENTAL = "PVR";
	String ABBREVATION_VEEVE = "VEE";
	String ABBREVATION_UNDER_THE_DOORMAT = "UTD";
	String ABBREVATION_AUTO_TEST_PMS = "ATP";
	String ABBREVATION_OLIVER_TRAVELS = "OLT";
	String ABBREVATION_OASIS = "OSC";
	String BESTSTAYZ_PMS = "BST";


	String PARTY_MAXXTON_EMAIL = "api@maxxton.com";
	String PARTY_NEXTPAX_EMAIL = "api@nextpax.com";
	String PARTY_LAKETAHOEACCOMODATIONS_EMAIL = "api@laketahoeaccommodations.com";
	String PARTY_AAXSYS_EMAIL = "api@aaxsys.com";
	String PARTY_STREAMLINE_EMAIL = "api@streamline.com";
	String PARTY_STREAMLINE_RESERVATION_EMAIL = "bpalsuccess@streamlinevrs.com";
	String PARTY_STREAMLINE_FAILED_RESERVATION_EMAIL = "bpalfail@streamlinevrs.com";
	String PARTY_RMS_EMAIL = "api@rms.com";
	String PARTY_PATHWAY_EMAIL = "api@pathway.com";
	String PARTY_BOOKT = "api@instamanager.com";
	String PARTY_WEBCHALET_EMAIL = "api@webchalet.com";
	String PARTY_LODGIX_EMAIL = "api@lodgix.com";
	String PARTY_LEISURELINK_EMAIL = "api@leisurelink.com";
	String PARTY_BAREFOOT_EMAIL = "api@barefoot.com";
	String PARTY_BOOKERVILLE_EMAIL = "api@bookerville.com";

	String FLIPKEY_CLIENTS = "5"; //make CSV when needed

	String BERMI_EXCHANGE_RATE_SERVICE = "http://webservices.bermilabs.com/exchange/";
	String OPEN_EXCHANGE_RATE_SERVICE = "https://openexchangerates.org/api/latest.json?app_id=e4abf9924e014610906a73cb9957bb46";
	String OPEN_EXCHANGE_RATE_CURRENCY_LIST_SERVICE = "https://openexchangerates.org/api/currencies.json?app_id=e4abf9924e014610906a73cb9957bb46";
	String GOOGLE_DIRECTIONS_SERVICE = "http://maps.googleapis.com/maps/api/directions/json?";

	String DB_CONFIG = "com/mybookingpal/server/config/Configuration.xml";
	String ADMIN_EMAIL = "pavel.boiko@gmail.com";
	//String CHIRAYU_SHAH_EMAIL = "chirayu@mybookingpal.com";
	String RAY_KARIMI_EMAIL = "ray@mybookingpal.com";
	String STEPHEN_ANANIAS_EMAIL = "stephen@mybookingpal.com";
	String EDDY_EMAIL = "eddy@mybookingpal.com";
	String MYBOOKINGPAL_PAYPAL_EMAIL = "platfo_1255077030_biz@gmail.com";
	String SUPPORT_EMAIL = "support@mybookingpal.com";

	String MAPS_KEY_OLD = "ABQIAAAA30LJM-VtBK-8CmbP2eic4RTf7B8Hz92uOSGpB3j3J64rWpwdFRRtnzeji201m8XeOjxG-9pkasreJg"; //http://razorpms.com

	String GOOGLE_CLIENT_KEY = "AIzaSyDkdwL3blntxliu3Dpx1FGhXzKVwuMKkyA"; //uat.mybookingpal.com maps
	String GOOGLE_SERVER_KEY = "AIzaSyDFLGBq-ms2EIHKPLbVwbJVxFzGCnkBy-c";
	String GOOGLE_API_KEY = "AIzaSyDEfTrFgxRr8N0XY51KDlBB62RJBON3-Qg";//uat.mybookingpal.com translation //bookingpaltranslate@gmail.com | ps: bookingpal
	String FLIPKEY_KEY = "28816157edbf16d560e14a816c9d107c";

	String PUBLIC_DIRECTORY = "";
	String AUDIO_DIRECTORY = "audio/";
	String VIDEO_DIRECTORY = "video/";
	String PICTURES_DIRECTORY = "pictures/";
	/** The Constant REPORT_URL is where the BIRT reports are located. */
	/** The Constant REPORT_DESIGNS_DIRECTORY is where the report design scripts are located. */
	String REPORT_DESIGNS_DIRECTORY = TOMCAT + "/webapps/" + SERVICE + "/WEB-INF/classes/com/mybookingpal/designs/";
	/** The Constant REPORTS_DIRECTORY is where the generated reports are located. */
	String REPORTS_DIRECTORY = TOMCAT + "/webapps/" + SERVICE + "/reports/";
	String APPLICATION_DIRECTORY = TOMCAT + "/webapps/" + SERVICE + "/WEB-INF/classes/";
}