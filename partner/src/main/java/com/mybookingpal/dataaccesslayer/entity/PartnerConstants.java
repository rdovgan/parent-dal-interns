package com.mybookingpal.dataaccesslayer.entity;

public class PartnerConstants {
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String SUSPENDED = "Suspended";
}
