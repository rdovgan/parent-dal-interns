package com.mybookingpal.dataaccesslayer.entity;

public class PartnerModel {

	private Integer id;
	private String organizationId;
	private String actorId;
	private String altPartyId;
	private String altId;
	private String state;
	private String options;
	private String partyId;
	private String partyName;
	private String bookEmailAddress;
	private String bookWebAddress;
	private String apiKey;
	private String currency;
	private String dateFormat;
	private String webAddress;
	private String alertCron;
	private String priceCron;
	private String productCron;
	private String scheduleCron;
	private String specialCron;
	private Integer alertWait;
	private Integer priceWait;
	private Integer productWait;
	private Integer scheduleWait;
	private Integer specialWait;
	private Double commission;
	private Double discount;
	private Double subscription;
	private Double transaction;
	private Boolean supportsCreditCard;
	private Boolean sendConfirmationEmails;
	private Boolean registerEmptyProperties;
	private Boolean separatePmAccounts;
	private Boolean bookOffline;
	private String abbreviation;
	private Double pmsMarkup;
	private Boolean processFailedApiPayment;
	private Integer exceptionDelay;
	private Boolean useSupplierApi;
	private Boolean supportsModifyByCancel;
	private Boolean pmsMarkupType;
	private String notes;
	private String requestLinks;
	private Boolean supportsModify;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getActorId() {
		return actorId;
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}

	public String getAltPartyId() {
		return altPartyId;
	}

	public void setAltPartyId(String altPartyId) {
		this.altPartyId = altPartyId;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getBookEmailAddress() {
		return bookEmailAddress;
	}

	public void setBookEmailAddress(String bookEmailAddress) {
		this.bookEmailAddress = bookEmailAddress;
	}

	public String getBookWebAddress() {
		return bookWebAddress;
	}

	public void setBookWebAddress(String bookWebAddress) {
		this.bookWebAddress = bookWebAddress;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getAlertCron() {
		return alertCron;
	}

	public void setAlertCron(String alertCron) {
		this.alertCron = alertCron;
	}

	public String getPriceCron() {
		return priceCron;
	}

	public void setPriceCron(String priceCron) {
		this.priceCron = priceCron;
	}

	public String getProductCron() {
		return productCron;
	}

	public void setProductCron(String productCron) {
		this.productCron = productCron;
	}

	public String getScheduleCron() {
		return scheduleCron;
	}

	public void setScheduleCron(String scheduleCron) {
		this.scheduleCron = scheduleCron;
	}

	public String getSpecialCron() {
		return specialCron;
	}

	public void setSpecialCron(String specialCron) {
		this.specialCron = specialCron;
	}

	public Integer getAlertWait() {
		return alertWait;
	}

	public void setAlertWait(Integer alertWait) {
		this.alertWait = alertWait;
	}

	public Integer getPriceWait() {
		return priceWait;
	}

	public void setPriceWait(Integer priceWait) {
		this.priceWait = priceWait;
	}

	public Integer getProductWait() {
		return productWait;
	}

	public void setProductWait(Integer productWait) {
		this.productWait = productWait;
	}

	public Integer getScheduleWait() {
		return scheduleWait;
	}

	public void setScheduleWait(Integer scheduleWait) {
		this.scheduleWait = scheduleWait;
	}

	public Integer getSpecialWait() {
		return specialWait;
	}

	public void setSpecialWait(Integer specialWait) {
		this.specialWait = specialWait;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getSubscription() {
		return subscription;
	}

	public void setSubscription(Double subscription) {
		this.subscription = subscription;
	}

	public Double getTransaction() {
		return transaction;
	}

	public void setTransaction(Double transaction) {
		this.transaction = transaction;
	}

	public Boolean getSupportsCreditCard() {
		return supportsCreditCard;
	}

	public void setSupportsCreditCard(Boolean supportsCreditCard) {
		this.supportsCreditCard = supportsCreditCard;
	}

	public Boolean getSendConfirmationEmails() {
		return sendConfirmationEmails;
	}

	public void setSendConfirmationEmails(Boolean sendConfirmationEmails) {
		this.sendConfirmationEmails = sendConfirmationEmails;
	}

	public Boolean getRegisterEmptyProperties() {
		return registerEmptyProperties;
	}

	public void setRegisterEmptyProperties(Boolean registerEmptyProperties) {
		this.registerEmptyProperties = registerEmptyProperties;
	}

	public Boolean getSeparatePmAccounts() {
		return separatePmAccounts;
	}

	public void setSeparatePmAccounts(Boolean separatePmAccounts) {
		this.separatePmAccounts = separatePmAccounts;
	}

	public Boolean getBookOffline() {
		return bookOffline;
	}

	public void setBookOffline(Boolean bookOffline) {
		this.bookOffline = bookOffline;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Double getPmsMarkup() {
		return pmsMarkup;
	}

	public void setPmsMarkup(Double pmsMarkup) {
		this.pmsMarkup = pmsMarkup;
	}

	public Boolean getProcessFailedApiPayment() {
		return processFailedApiPayment;
	}

	public void setProcessFailedApiPayment(Boolean processFailedApiPayment) {
		this.processFailedApiPayment = processFailedApiPayment;
	}

	public Integer getExceptionDelay() {
		return exceptionDelay;
	}

	public void setExceptionDelay(Integer exceptionDelay) {
		this.exceptionDelay = exceptionDelay;
	}

	public Boolean getUseSupplierApi() {
		return useSupplierApi;
	}

	public void setUseSupplierApi(Boolean useSupplierApi) {
		this.useSupplierApi = useSupplierApi;
	}

	public Boolean getSupportsModifyByCancel() {
		return supportsModifyByCancel;
	}

	public void setSupportsModifyByCancel(Boolean supportsModifyByCancel) {
		this.supportsModifyByCancel = supportsModifyByCancel;
	}

	public Boolean getPmsMarkupType() {
		return pmsMarkupType;
	}

	public void setPmsMarkupType(Boolean pmsMarkupType) {
		this.pmsMarkupType = pmsMarkupType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getRequestLinks() {
		return requestLinks;
	}

	public void setRequestLinks(String requestLinks) {
		this.requestLinks = requestLinks;
	}

	public Boolean getSupportsModify() {
		return supportsModify;
	}

	public void setSupportsModify(Boolean supportsModify) {
		this.supportsModify = supportsModify;
	}
}
