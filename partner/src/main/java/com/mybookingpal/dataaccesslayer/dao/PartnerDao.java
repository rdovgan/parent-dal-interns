package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PartnerModel;
import com.mybookingpal.dataaccesslayer.mappers.PartnerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PartnerDao {

	@Autowired
	private PartnerMapper partnerMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PartnerModel action) {
		partnerMapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel read(String id) {
		return partnerMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PartnerModel action) {
		partnerMapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel exists(String partyId) {
		return partnerMapper.exists(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String partyIDFromEmail(String emailAddress) {
		return partnerMapper.partyIDFromEmail(emailAddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String partyIDFromAbbrevation(String abbreviation) {
		return partnerMapper.partyIDFromAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean isSupportCreditCard(String partyId) {
		return partnerMapper.isSupportCreditCard(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getPartnerPartyIDByAbbreviation(String[] abbreviations) {
		return partnerMapper.getPartnerPartyIDByAbbreviation(abbreviations);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<PartnerModel> listPropertyManagementSoftware() {
		return partnerMapper.listPropertyManagementSoftware();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllChildPartnerPartyIDByAbbreviation(String abbreviation) {
		return partnerMapper.getAllChildPartnerPartyIDByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> getAllChildPartnerByAbbreviation(String abbreviation) {
		return partnerMapper.getAllChildPartnerByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> getAllActivePmsForSupplierApi() {
		return partnerMapper.getAllActivePmsForSupplierApi();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> getAllActivePmsWhichNotInBlacklist() {
		return partnerMapper.getAllActivePmsWhichNotInBlacklist();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel getbyPMSId(Integer pmsId) {
		return partnerMapper.getbyPMSId(pmsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel readByPartyId(Integer partyId) {
		return partnerMapper.readByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> readListByPartyIds(List<Integer> partyIds) {
		return partnerMapper.readListByPartyIds(partyIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel getParentByPartyId(Integer partyId) {
		return partnerMapper.getParentByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartnerModel getByAbbreviationAndApiKey(String apiKey, String abbreviation) {
		return partnerMapper.getByAbbreviationAndApiKey(apiKey, abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> listAbbreviations() {
		return partnerMapper.listAbbreviations();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllPartyIdFromPartnerId(String partnerId) {
		return partnerMapper.getAllPartyIdFromPartnerId(partnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> getAllPartnerByAbbrevation(Integer organizationId) {
		return partnerMapper.getAllPartnerByAbbreviation(organizationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartnerModel> getAllPartnerByOrganizationID(String[] organizationId) {
		return partnerMapper.getAllPartnerByOrganizationID(organizationId);
	}

}
