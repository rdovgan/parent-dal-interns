package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.CountryCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CountryCodeMapper {

	List<CountryCode> getCountryCodes();

	List<String> getAllCountries();

	String getCountryName(String iso_code2);

	String getPhoneCode(String countryName);

	String getISO2Code(String country);

	String getCountryCodeByISO3(@Param("countryCode") String countryCode);

	List<CountryCode> getISO2ToCountry();
}