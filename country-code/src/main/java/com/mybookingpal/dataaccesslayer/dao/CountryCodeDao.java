package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.CountryCode;
import com.mybookingpal.dataaccesslayer.mappers.CountryCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryCodeDao {

	@Autowired
	CountryCodeMapper countryCodeMapper;

	public List<CountryCode> getCountryCodes() {
		return countryCodeMapper.getCountryCodes();
	}

	public List<String> getAllCountries() {
		return countryCodeMapper.getAllCountries();
	}

	public List<CountryCode> getISO2ToCountry(){
		return countryCodeMapper.getISO2ToCountry();
	}

	public String getCountryName(String iso_code2) {
		return countryCodeMapper.getCountryName(iso_code2);
	}

	public String getPhoneCode(String countryName) {
		return countryCodeMapper.getPhoneCode(countryName);
	}

	public String getISO2Code(String country) {
		return countryCodeMapper.getISO2Code(country);
	}

	public String getCountryCodeByISO3(String countryCode) {
		return countryCodeMapper.getCountryCodeByISO3(countryCode);
	}
}
