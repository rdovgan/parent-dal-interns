package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.RegionCode;
import com.mybookingpal.dataaccesslayer.mappers.RegionCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class RegionCodeDao {

	@Autowired
	RegionCodeMapper regionCodeMapper;

	public List<RegionCode> getRegionCodes() {
		return regionCodeMapper.getRegionCodes();
	}

	public List<String> getAllRegionNames() {
		return regionCodeMapper.getAllRegionNames();
	}

	public List<RegionCode> getStatesByCountryCode(String countryCode) {
		return regionCodeMapper.getStatesByCountryCode(countryCode);
	}

	public String getByName(String name) {
		return regionCodeMapper.getByName(name);
	}

	public String getNameById(String id) {
		return regionCodeMapper.getNameById(id);
	}
}

