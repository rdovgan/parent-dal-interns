package com.mybookingpal.dataaccesslayer.mappers;

import java.util.List;

import com.mybookingpal.dataaccesslayer.entity.RegionCode;
import org.apache.ibatis.annotations.Param;

public interface RegionCodeMapper {

	List<RegionCode> getRegionCodes();

	List<String> getAllRegionNames();

	String getByName(@Param("name") String name);

	List<RegionCode> getStatesByCountryCode(@Param("countryCode") String countryCode);

	String getNameById(@Param("id") String id);
}
