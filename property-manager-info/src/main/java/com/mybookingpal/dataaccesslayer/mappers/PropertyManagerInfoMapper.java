package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerInfoModel;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface PropertyManagerInfoMapper {

	void create(PropertyManagerInfoModel property);

	PropertyManagerInfoModel readByPmId(Integer pmID);

	PropertyManagerInfoModel readByVirtualPmId(Integer pmID);

	List<PropertyManagerInfoModel> list();

	void update(PropertyManagerInfoModel item);

	void updateByPmId(PropertyManagerInfoModel item);

	ArrayList<PropertyManagerInfoModel> listByIds(List<String> partyIds);

	List<Long> getPmIDList( @Param("pmsId") Integer pmsId);

	double getAVGPMSCommission();

	void updateFeeNetRate(@Param("propertyManagerId") Integer propertyManagerId, @Param("isNetRate") Boolean isNetRate);
}
