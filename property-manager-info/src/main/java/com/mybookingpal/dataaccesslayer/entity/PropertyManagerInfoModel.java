package com.mybookingpal.dataaccesslayer.entity;

import java.sql.Time;
import java.util.Date;

public class PropertyManagerInfoModel {

	private Integer id;
	private Integer propertyManagerId;
	private Integer pmsId;
	private Integer fundsHolder;
	private Integer paymentProcessingType;
	private Integer registrationStepId;
	private boolean inquireOnly;
	private Integer cancellationType;
	private Integer damageCoverageType;
	private String damageInsurance;
	private Time checkInTime;
	private Time checkOutTime;
	private String termsLink;
	private Integer numberOfPayments;
	private Double paymentAmount;
	private Integer paymentType;
	private Integer remainderPaymentDate;
	private Integer canProcessPayment;
	private Boolean newRegistration;
	private Date createdDate;
	private Double additionalCommission;
	private Double commission;
	private boolean netRate;
	private boolean feeNetRate;
	private Double pmsMarkup;
	private Boolean pmsMarkupType;
	private Integer configurationId;
	private Boolean pmTime;
	private String currency;
	private Boolean sendGuestManageBooking;
	private Boolean collectAllPayments;
	private Integer creditCardService;
	private boolean linkToAllAgents;
	private Boolean worksWithFailedPaymentServices;
	private Integer accountManagerId;
	private Integer onboardingPersonId;
	private Integer salesRepId;
	private Integer regionalManagerId;
	private Boolean supportCancellationRefund;
	private Boolean dailyExceptionReservationEmails;
	private Boolean isCompany;
	private Date version;

	public Integer getCanProcessPayment() {
		return canProcessPayment;
	}

	public void setCanProcessPayment(Integer canProcessPayment) {
		this.canProcessPayment = canProcessPayment;
	}

	public Integer getCancellationType() {
		return cancellationType;
	}

	public void setCancellationType(Integer cancellationType) {
		this.cancellationType = cancellationType;
	}

	public Boolean getCollectAllPayments() {
		return collectAllPayments;
	}

	public void setCollectAllPayments(Boolean collectAllPayments) {
		this.collectAllPayments = collectAllPayments;
	}

	public Integer getPmsId() {
		return pmsId;
	}

	public void setPmsId(Integer pmsId) {
		this.pmsId = pmsId;
	}

	public Boolean getSendGuestManageBooking() {
		return sendGuestManageBooking;
	}

	public void setSendGuestManageBooking(Boolean sendGuestManageBooking) {
		this.sendGuestManageBooking = sendGuestManageBooking;
	}

	public Boolean getNewRegistration() {
		return newRegistration;
	}

	public void setNewRegistration(Boolean newRegistration) {
		this.newRegistration = newRegistration;
	}

	public Integer getNumberOfPayments() {
		return numberOfPayments;
	}

	public void setNumberOfPayments(Integer numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getRemainderPaymentDate() {
		return remainderPaymentDate;
	}

	public void setRemainderPaymentDate(Integer remainderPaymentDate) {
		this.remainderPaymentDate = remainderPaymentDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public Integer getRegistrationStepId() {
		return registrationStepId;
	}

	public void setRegistrationStepId(Integer registrationStepId) {
		this.registrationStepId = registrationStepId;
	}

	public Integer getDamageCoverageType() {
		return damageCoverageType;
	}

	public void setDamageCoverageType(Integer damageCoverageType) {
		this.damageCoverageType = damageCoverageType;
	}

	public String getDamageInsurance() {
		return damageInsurance;
	}

	public void setDamageInsurance(String damageInsurance) {
		this.damageInsurance = damageInsurance;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getPaymentProcessingType() {
		return paymentProcessingType;
	}

	public void setPaymentProcessingType(Integer paymentProcessingType) {
		this.paymentProcessingType = paymentProcessingType;
	}

	public java.sql.Time getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(java.sql.Time checkInTime) {
		this.checkInTime = checkInTime;
	}

	public java.sql.Time getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(java.sql.Time checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public String getTermsLink() {
		return termsLink;
	}

	public void setTermsLink(String termsLink) {
		this.termsLink = termsLink;
	}

	public boolean isInquireOnly() {
		return inquireOnly;
	}

	public void setInquireOnly(boolean inquireOnly) {
		this.inquireOnly = inquireOnly;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public boolean isNetRate() {
		return netRate;
	}

	public void setNetRate(boolean netRate) {
		this.netRate = netRate;
	}

	public boolean isFeeNetRate() {
		return feeNetRate;
	}

	public void setFeeNetRate(boolean feeNetRate) {
		this.feeNetRate = feeNetRate;
	}

	public Double getPmsMarkup() {
		return pmsMarkup;
	}

	public void setPmsMarkup(Double pmsMarkup) {
		this.pmsMarkup = pmsMarkup;
	}

	public Double getAdditionalCommission() {
		return additionalCommission;
	}

	public void setAdditionalCommission(Double additionalCommission) {
		this.additionalCommission = additionalCommission;
	}

	public Integer getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(Integer configurationId) {
		this.configurationId = configurationId;
	}

	public Boolean getPmTime() {
		return pmTime;
	}

	public void setPmTime(Boolean pmTime) {
		this.pmTime = pmTime;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getPmsMarkupType() {
		return pmsMarkupType;
	}

	public void setPmsMarkupType(Boolean pmsMarkupType) {
		this.pmsMarkupType = pmsMarkupType;
	}

	public boolean isLinkToAllAgents() {
		return linkToAllAgents;
	}

	public void setLinkToAllAgents(boolean linkToAllAgents) {
		this.linkToAllAgents = linkToAllAgents;
	}

	public Boolean getWorksWithFailedPaymentServices() {
		return worksWithFailedPaymentServices;
	}

	public void setWorksWithFailedPaymentServices(Boolean worksWithFailedPaymentServices) {
		this.worksWithFailedPaymentServices = worksWithFailedPaymentServices;
	}

	public Integer getCreditCardService() {
		return creditCardService;
	}

	public void setCreditCardService(Integer creditCardService) {
		this.creditCardService = creditCardService;
	}

	public Integer getAccountManagerId() {
		return accountManagerId;
	}

	public void setAccountManagerId(Integer accountManagerId) {
		this.accountManagerId = accountManagerId;
	}

	public Integer getOnboardingPersonId() {
		return onboardingPersonId;
	}

	public void setOnboardingPersonId(Integer onboardingPersonId) {
		this.onboardingPersonId = onboardingPersonId;
	}

	public Integer getSalesRepId() {
		return salesRepId;
	}

	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}

	public Integer getRegionalManagerId() {
		return regionalManagerId;
	}

	public void setRegionalManagerId(Integer regionalManagerId) {
		this.regionalManagerId = regionalManagerId;
	}

	public Boolean getSupportCancellationRefund() {
		return supportCancellationRefund;
	}

	public void setSupportCancellationRefund(Boolean supportCancellationRefund) {
		this.supportCancellationRefund = supportCancellationRefund;
	}

	public Boolean getDailyExceptionReservationEmails() {
		return dailyExceptionReservationEmails;
	}

	public void setDailyExceptionReservationEmails(Boolean dailyExceptionReservationEmails) {
		this.dailyExceptionReservationEmails = dailyExceptionReservationEmails;
	}

	public Boolean isCompany() {
		return isCompany;
	}

	public void setIsCompany(Boolean company) {
		isCompany = company;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

}
