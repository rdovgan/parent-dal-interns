package com.mybookingpal.dataaccesslayer.entity;

import java.util.Arrays;

public class PropertyManagerInfoConstants {

	public static Integer DEFAULT_PMS_ID = 55;

	public enum PaymentType {
		PERCENT(1), FLAT_FEE(2), NIGHTS(3);

		private final int number;

		PaymentType(int number) {

			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		public static PaymentType getByNumber(int number) {
			return Arrays.stream(values()).filter(p -> p.getNumber() == number).findFirst().orElse(PERCENT);
		}

	}

	public enum CancellationType {
		CUSTOM_CANCELLATION(1),  FULLY_REFUNDABLE(2), NON_REFUNDABLE(3);

		private final int number;

		CancellationType(int number) {
			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		public static CancellationType getByNumber(int number) {
			return Arrays.stream(values()).filter(p -> p.getNumber() == number).findFirst().orElse(NON_REFUNDABLE);
		}
	}
}
