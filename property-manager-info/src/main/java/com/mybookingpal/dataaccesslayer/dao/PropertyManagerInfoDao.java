package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerInfoModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PropertyManagerInfoDao {

	@Autowired
	private PropertyManagerInfoMapper propertyManagerInfoMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PropertyManagerInfoModel property) {
		propertyManagerInfoMapper.create(property);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerInfoModel readByPmId(Integer pmID) {
		return propertyManagerInfoMapper.readByPmId(pmID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerInfoModel readByVirtualPmId(Integer pmID) {
		return propertyManagerInfoMapper.readByVirtualPmId(pmID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerInfoModel> list() {
		return propertyManagerInfoMapper.list();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PropertyManagerInfoModel item) {
		propertyManagerInfoMapper.update(item);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateByPmId(PropertyManagerInfoModel item) {
		propertyManagerInfoMapper.updateByPmId(item);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<PropertyManagerInfoModel> listByIds(List<String> partyIds) {
		return propertyManagerInfoMapper.listByIds(partyIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Long> getPmIDList(Integer pmsId) {
		return propertyManagerInfoMapper.getPmIDList(pmsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public double getAVGPMSCommission() {
		return propertyManagerInfoMapper.getAVGPMSCommission();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateFeeNetRate(Integer propertyManagerId, Boolean isNetRate) {
		propertyManagerInfoMapper.updateFeeNetRate(propertyManagerId, isNetRate);
	}

}
