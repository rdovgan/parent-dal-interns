package com.mybookingpal.dataaccesslayer.constants;

public enum ProductPolicyValue {
	PetsAllowedRequest ("Allowed on request"),
	PetsAllowed ("Allowed"),
	NoPetsAllowed ("Not allowed"),
	ParkingAvailability ("Yes"),
	ParkingNearby ("Nearby"),
	ParkingOnSite ("On site"),
	PrivateParking ("Private"),
	PublicParking ("Public"),
	ParkingNotAvailability ("No"),
	InternetAvailability ("Yes"),
	InternetNotAvailability ("No"),
	WirelessInternet ("Wifi"),
	WiredInternet ("Wired"),
	ChargedMayBeApplicable ("Charged may be applicable"),
	WiredAndWirelessInternet ("All");

	private final String value;

	ProductPolicyValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
