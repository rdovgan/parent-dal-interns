package com.mybookingpal.dataaccesslayer.constants;

public enum ProductPolicyID {
	InternetAvailable(1), InternetCharge(2), ParkingCharge(3), ParkingTimeFrame(4), ParkingReservationNecessary(5), PetFee(6);

	private final Integer id;

	ProductPolicyID(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
}
