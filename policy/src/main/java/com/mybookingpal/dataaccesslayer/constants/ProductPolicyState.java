package com.mybookingpal.dataaccesslayer.constants;

public enum ProductPolicyState {
	Created,
	Final;
}
