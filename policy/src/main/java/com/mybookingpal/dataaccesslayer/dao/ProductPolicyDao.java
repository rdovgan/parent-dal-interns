package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ProductPolicyModel;
import com.mybookingpal.dataaccesslayer.dto.ProductPolicyWithNameDto;
import com.mybookingpal.dataaccesslayer.mappers.ProductPolicyMapper;
import com.mybookingpal.utils.service.MbpCollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class ProductPolicyDao {

	private final ProductPolicyMapper mapper;

	public ProductPolicyDao(ProductPolicyMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyModel> getByProductId(int productId) {
		return mapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyModel> getActiveByProductId(int productId) {
		return mapper.getActiveByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyModel> getByProductIdAndPolicyId(String productId, String policyId) {
		return mapper.getByProductIdAndPolicyId(productId, policyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyWithNameDto> getByProductIdWithName(String productId) {
		return mapper.getByProductIdWithName(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyModel> getProductPoliciesFromLastTimeStamp(String productId, Date version) {
		return mapper.getProductPoliciesFromLastTimeStamp(productId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelList(List<ProductPolicyModel> propertyMinStays, Date version) {
		if (Objects.isNull(propertyMinStays)) {
			return;
		}

		mapper.cancelList(propertyMinStays, version);
	}
	// TODO: Mistake in method naming
	public List<ProductPolicyModel> getAcitvePetPolicyByProductId(Integer productId) {
		return mapper.getAcitvePetPolicyByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<ProductPolicyModel> productPolicies) {
		if (MbpCollectionUtils.isEmpty(productPolicies)) {
			return;
		}
		List<ProductPolicyModel> notNullPolicyList = productPolicies.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.insertList(notNullPolicyList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductPolicyWithNameDto> getAllPoliciesByProductId(Long productId) {
		return mapper.getAllPoliciesByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductIdAndPolicies(Long productId, List<Integer> policies) {
		mapper.deleteByProductIdAndPolicies(productId, policies);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAllProductPolicies(Integer id) {
		mapper.deleteAllProductPolicies(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteInternetPoliciesForRooms(Long id) {
		mapper.deleteInternetPoliciesForRooms(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyProductPolicies(Integer fromProductId, Integer toProductId) {
		mapper.copyProductPolicies(fromProductId, toProductId);
	}
}
