package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PolicyModel;
import com.mybookingpal.dataaccesslayer.mappers.PolicyMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PolicyDao {

	private final PolicyMapper mapper;

	public PolicyDao(PolicyMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PolicyModel> listAll() {
		return mapper.listAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String readId(String name) {
		return mapper.readId(name);
	}
}
