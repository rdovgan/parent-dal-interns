package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.ProductPolicyState;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("ProductPolicy")
@Scope(value = "prototype")
public class ProductPolicyModel {
	protected Integer id;
	protected Integer policyId;
	protected Integer productId;
	protected ProductPolicyState state;
	protected String value;
	protected Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ProductPolicyState getProductPolicyState() {
		return state;
	}

	public void setProductPolicyState(ProductPolicyState productPolicyState) {
		this.state = productPolicyState;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + policyId;
		result = prime * result + productId;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductPolicyModel other = (ProductPolicyModel) obj;
		if (!policyId.equals(other.policyId))
			return false;
		if (!productId.equals(other.productId))
			return false;
		if (state != other.state)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductPolicy{" + "id=" + id + ", policyId=" + policyId + ", productId=" + productId + ", value='" + value + '\'' + ", version=" + version
				+ ", state=" + state + '}';
	}

}

