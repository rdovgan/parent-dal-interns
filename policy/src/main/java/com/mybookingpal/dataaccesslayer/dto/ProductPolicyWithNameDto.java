package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ProductPolicyModel;

public class ProductPolicyWithNameDto extends ProductPolicyModel {
	private String name;
	private String policyGroup;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPolicyGroup() {
		return policyGroup;
	}

	public void setPolicyGroup(String policyGroup) {
		this.policyGroup = policyGroup;
	}
}