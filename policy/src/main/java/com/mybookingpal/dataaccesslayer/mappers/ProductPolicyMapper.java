package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ProductPolicyModel;
import com.mybookingpal.dataaccesslayer.dto.ProductPolicyWithNameDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProductPolicyMapper {

	List<ProductPolicyModel> getByProductId(Integer productId);

	List<ProductPolicyModel> getActiveByProductId(Integer productId);

	List<ProductPolicyModel> getByProductIdAndPolicyId(@Param("productId") String productId,@Param("policyId") String policyId);

	List<ProductPolicyWithNameDto> getByProductIdWithName(String productId);

	List<ProductPolicyModel> getProductPoliciesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);

	void cancelList(@Param("list") List<ProductPolicyModel> propertyMinStays, @Param("version") Date version);

	List<ProductPolicyModel> getAcitvePetPolicyByProductId(Integer productId); /*TODO: Mistake in method naming*/

	//TODO: Investigate productId type | List<ProductPolicy> getByProductId(String productId);

	//TODO: Investigate productId type | List<ProductPolicy> getActiveByProductId(String productId);

	void insertList(@Param("list") List<ProductPolicyModel> productPolicies);

	List<ProductPolicyWithNameDto> getAllPoliciesByProductId(Long productId);

	void deleteByProductIdAndPolicies(@Param("productId") Long productId, @Param("list") List<Integer> policies);

	void deleteAllProductPolicies(@Param("id") Integer id);

	void deleteInternetPoliciesForRooms(@Param("id") Long id);

	void copyProductPolicies(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

}
