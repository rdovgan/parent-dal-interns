package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PolicyModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolicyMapper {

	List<PolicyModel> listAll();

	String readId(String name);
}
