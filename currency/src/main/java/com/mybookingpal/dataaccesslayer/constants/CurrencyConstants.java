package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;
import java.util.List;

//using temporary class (NameIdUtils)
public class CurrencyConstants {
	public static final String[] EXCHANGE_CURRENCIES = { CurrencyCode.AUD.name(), CurrencyCode.BGN.name(), CurrencyCode.BRL.name(), CurrencyCode.CAD.name(),
			CurrencyCode.CHF.name(), CurrencyCode.CNY.name(), CurrencyCode.CZK.name(), CurrencyCode.DKK.name(), CurrencyCode.EUR.name(),
			CurrencyCode.GBP.name(), CurrencyCode.HKD.name(), CurrencyCode.HRK.name(), CurrencyCode.HUF.name(), CurrencyCode.IDR.name(),
			CurrencyCode.INR.name(), CurrencyCode.ISK.name(), CurrencyCode.JPY.name(), CurrencyCode.KRW.name(), CurrencyCode.LTL.name(),
			CurrencyCode.LVL.name(), CurrencyCode.MXN.name(), CurrencyCode.MYR.name(), CurrencyCode.NOK.name(), CurrencyCode.NZD.name(),
			CurrencyCode.PHP.name(), CurrencyCode.PLN.name(), CurrencyCode.RON.name(), CurrencyCode.RUB.name(), CurrencyCode.SEK.name(),
			CurrencyCode.SGD.name(), CurrencyCode.SKK.name(), CurrencyCode.THB.name(), CurrencyCode.TRY.name(), CurrencyCode.USD.name(),
			CurrencyCode.ZAR.name(), CurrencyCode.ILS.name() };

	private static final String[] EXCHANGE_CURRENCY_NAMES = { "Australian Dollar", "Bulgarian Lev", "Brasilian Real", "Canadian Dollar", "Swiss Franc",
			"Chinese Yuan Renminbi", "Czech Koruna", "Danish Krone", "Euro", "Pound Sterling", "Hong Kong Dollar", "Croatian Kuna", "Hungarian Forint",
			"Indonesian Rupiah", "Indian Rupee", "Icelandic Krona", "Japanese Yen", "South Korean Won", "Lithuanian Litas", "Latvian Lats", "Mexican Peso",
			"Malaysian Ringgit", "Norwegian Krone", "New Zealand Dollar", "Philippine Peso", "Polish Zloty", "New Romanian Leu", "Russian Rouble",
			"Swedish Krona", "Singapore Dollar", "Slovak Koruna", "Thai Baht", "Turkish Lira", "US Dollar", "Rand", "Israel Shekel" };

	private static final List<String> convertibleCurrencies = Arrays.asList(EXCHANGE_CURRENCIES);

	@Deprecated
	public static final boolean isConvertible(String currency) {
		return convertibleCurrencies.contains(currency);
	}

	public static final String[] PRICE_CURRENCIES = { CurrencyCode.EUR.name(), CurrencyCode.GBP.name(), CurrencyCode.USD.name(), CurrencyCode.JPY.name(),
			CurrencyCode.ZAR.name() };

	public static final String CODE = "code";
	public static final String NAME = "name";


}
