package com.mybookingpal.dataaccesslayer.constants;

/**
 * USD US dollar http://webservices.bermilabs.com/exchange/eur/usd
 * JPY Japanese yen http://webservices.bermilabs.com/exchange/eur/jpy
 * BGN Bulgarian lev http://webservices.bermilabs.com/exchange/eur/bgn
 * CZK Czech koruna http://webservices.bermilabs.com/exchange/eur/czk
 * DKK Danish krone http://webservices.bermilabs.com/exchange/eur/dkk
 * EEK Estonian kroon http://webservices.bermilabs.com/exchange/eur/eek
 * GBP Pound sterling http://webservices.bermilabs.com/exchange/eur/gbp
 * HUF Hungarian forint http://webservices.bermilabs.com/exchange/eur/huf
 * LTL Lithuanian litas http://webservices.bermilabs.com/exchange/eur/ltl
 * LVL Latvian lats http://webservices.bermilabs.com/exchange/eur/lvl
 * PLN Polish zloty http://webservices.bermilabs.com/exchange/eur/pln
 * RON New Romanian leu 1 http://webservices.bermilabs.com/exchange/eur/ron
 * SEK Swedish krona http://webservices.bermilabs.com/exchange/eur/sek
 * SKK Slovak koruna http://webservices.bermilabs.com/exchange/eur/skk
 * CHF Swiss franc http://webservices.bermilabs.com/exchange/eur/chf
 * ISK Icelandic krona http://webservices.bermilabs.com/exchange/eur/isk
 * NOK Norwegian krone http://webservices.bermilabs.com/exchange/eur/nok
 * HRK Croatian kuna http://webservices.bermilabs.com/exchange/eur/hrk
 * RUB Russian rouble http://webservices.bermilabs.com/exchange/eur/rub
 * TRY New Turkish lira 2 http://webservices.bermilabs.com/exchange/eur/try
 * AUD Australian dollar http://webservices.bermilabs.com/exchange/eur/aud
 * BRL Brasilian real http://webservices.bermilabs.com/exchange/eur/brl
 * CAD Canadian dollar http://webservices.bermilabs.com/exchange/eur/cad
 * CNY Chinese yuan renminbi http://webservices.bermilabs.com/exchange/eur/cny
 * HKD Hong Kong dollar http://webservices.bermilabs.com/exchange/eur/hkd
 * IDR Indonesian rupiah http://webservices.bermilabs.com/exchange/eur/idr
 * KRW South Korean won http://webservices.bermilabs.com/exchange/eur/krw
 * MXN Mexican peso http://webservices.bermilabs.com/exchange/eur/mxn
 * MYR Malaysian ringgit http://webservices.bermilabs.com/exchange/eur/myr
 * NZD New Zealand dollar http://webservices.bermilabs.com/exchange/eur/nzd
 * PHP Philippine peso http://webservices.bermilabs.com/exchange/eur/php
 * SGD Singapore dollar http://webservices.bermilabs.com/exchange/eur/sgd
 * THB Thai baht http://webservices.bermilabs.com/exchange/eur/thb
 * ZAR South African rand
 * ILS Israel Shekel
 */

public enum CurrencyCode {
	AUD,BGN,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HRK,HUF,IDR,INR,ISK,JPY,
	KRW,LTL,LVL,MXN,MYR,NOK,NZD,PHP,PLN,RON,RUB,SEK,SGD,SKK,THB,TRY,USD,ZAR,ILS
}
