package com.mybookingpal.dataaccesslayer.constants;

public class CurrencyrateConstants {
	public static final String TOCODE = "tocode";
	public static final String DATE = "date";
	public static final String RATE = "rate";
}
