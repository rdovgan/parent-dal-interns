package com.mybookingpal.dataaccesslayer.entity;

/**
 * temporary class
 * TODO: must be replaced on com.mybookingpal.shared.Model
 */
public abstract class Model {
	protected String id;
	protected String state;
	protected String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
