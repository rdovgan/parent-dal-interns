package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class CurrencyrateModel extends CurrencyModel {
	private String toid;
	private Date date;
	private Double rate;

	public CurrencyrateModel() {}

	public CurrencyrateModel(String id, String toid, Date date) {
		super();
		this.id = id;
		this.toid = toid;
		this.date = date;
	}

	public CurrencyrateModel(String id, String toid, Date date, Double rate) {
		super();
		this.id = id;
		this.toid = toid;
		this.date = date;
		this.rate = rate;
	}

	public String getToid() {
		return toid;
	}

	public void setToid(String toid) {
		this.toid = toid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean noDate() {
		return date == null;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" Id ").append(getId());
		sb.append(" ToId ").append(getToid());
		sb.append(" Date ").append(getDate());
		sb.append(" Rate ").append(getRate());
		return sb.toString();
	}
}
