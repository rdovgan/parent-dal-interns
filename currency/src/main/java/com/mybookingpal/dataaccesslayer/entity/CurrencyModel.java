package com.mybookingpal.dataaccesslayer.entity;

//using temporary class (Model)
public class CurrencyModel extends Model {

	private String number;
	private String symbol;
	private Short decimals;
	private Boolean convertible;
	private Boolean paypal;
	private Boolean jetpay;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Short getDecimals() {
		return decimals;
	}

	public void setDecimals(Short decimals) {
		this.decimals = decimals;
	}

	public Boolean getConvertible() {
		return convertible;
	}

	public void setConvertible(Boolean convertible) {
		this.convertible = convertible;
	}

	public Boolean getPaypal() {
		return paypal;
	}

	public void setPaypal(Boolean paypal) {
		this.paypal = paypal;
	}

	public Boolean getJetpay() {
		return jetpay;
	}

	public void setJetpay(Boolean jetpay) {
		this.jetpay = jetpay;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Currency [number=");
		builder.append(number);
		builder.append(", decimals=");
		builder.append(decimals);
		builder.append(", convertible=");
		builder.append(convertible);
		builder.append(", paypal=");
		builder.append(paypal);
		builder.append(", jetpay=");
		builder.append(jetpay);
		builder.append(", state=");
		builder.append(state);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}