package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.CurrencyModel;

import com.mybookingpal.utils.entity.NameId;

import java.util.List;

public interface CurrencyMapper {
	CurrencyModel read(String id);

	List<CurrencyMapper> readAllWithStateCreated();

	void create(CurrencyModel currency);

	NameId nameidwidget(String id, String organizationid);
}
