package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.CurrencyrateModel;

import java.util.List;

public interface CurrencyrateMapper {

	void create(CurrencyrateModel action);

	void update(CurrencyrateModel action);

	CurrencyrateModel readbyexample(CurrencyrateModel currencyRateModel);

	CurrencyrateModel readNearestByExample(CurrencyrateModel currencyRateModel);

	void createList(List<CurrencyrateModel> currencyRateModelList);
}
