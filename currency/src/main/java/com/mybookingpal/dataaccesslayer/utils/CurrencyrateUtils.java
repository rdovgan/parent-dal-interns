package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.CurrencyrateModel;
import com.mybookingpal.utils.entity.Time;
import org.springframework.stereotype.Component;

@Component
//using temporary interface (IsTime)
public class CurrencyrateUtils {

	public boolean noToid(CurrencyrateModel currencyRate) {
		return currencyRate.getToid()== null || currencyRate.getToid().isEmpty();
	}

	public boolean hasToid(CurrencyrateModel currencyRate) {
		return !noToid(currencyRate);
	}

	public boolean hasToid(String toid, CurrencyrateModel currencyRate) {
		return currencyRate.getToid() != null && currencyRate.getToid().equalsIgnoreCase(toid);
	}

	public boolean before(CurrencyrateModel currencyRate) {
		return String.valueOf(currencyRate.getId()).compareToIgnoreCase(currencyRate.getToid()) <= 0;
	}

	public boolean is(String id, String toid, CurrencyrateModel currencyRate) {
		return currencyRate.getId() == id && currencyRate.getToid() == toid;
	}

	@Deprecated
	public String key(CurrencyrateModel currencyRate) {
		return currencyRate.getId() + currencyRate.getToid() + Time.getDay(currencyRate.getDate());
	}
}
