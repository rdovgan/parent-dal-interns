package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.CurrencyModel;

import com.mybookingpal.dataaccesslayer.mappers.CurrencyMapper;
import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CurrencyDao {
	private final CurrencyMapper mapper;

	public CurrencyDao(CurrencyMapper currencyMapper) {
		this.mapper = currencyMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CurrencyModel read(String id) {
		if (StringUtils.isBlank(id)) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CurrencyMapper> readAllWithStateCreated() {
		return readAllWithStateCreated();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CurrencyModel currencyModel) {
		if (Objects.isNull(currencyModel)) {
			return;
		}
		mapper.create(currencyModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public NameId nameidwidget(String id, String organizationid) {
		if (Objects.isNull(id) || Objects.isNull(organizationid)) {
			return null;
		}
		return mapper.nameidwidget(id, organizationid);
	}

}
