package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.CurrencyrateModel;
import com.mybookingpal.dataaccesslayer.mappers.CurrencyrateMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CurrencyrateDao {
	private final CurrencyrateMapper currencyrateMapper;

	public CurrencyrateDao(CurrencyrateMapper currencyrateMapper) {
		this.currencyrateMapper = currencyrateMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CurrencyrateModel currencyRateModel) {
		if (Objects.isNull(currencyRateModel)) {
			return;
		}
		currencyrateMapper.create(currencyRateModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CurrencyrateModel currencyRateModel) {
		if (Objects.isNull(currencyRateModel)) {
			return;
		}
		currencyrateMapper.update(currencyRateModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CurrencyrateModel readByExample(CurrencyrateModel currencyRateModel) {
		if (Objects.isNull(currencyRateModel)) {
			return null;
		}
		return currencyrateMapper.readbyexample(currencyRateModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CurrencyrateModel readNearestByExample(CurrencyrateModel currencyRateModel) {
		if (Objects.isNull(currencyRateModel)) {
			return null;
		}
		return currencyrateMapper.readNearestByExample(currencyRateModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createList(List<CurrencyrateModel> currencyRateModelList) {
		if (CollectionUtils.isEmpty(currencyRateModelList)) {
			return;
		}
		currencyrateMapper.createList(currencyRateModelList.stream().filter(Objects::nonNull).collect(Collectors.toList()));
	}
}
