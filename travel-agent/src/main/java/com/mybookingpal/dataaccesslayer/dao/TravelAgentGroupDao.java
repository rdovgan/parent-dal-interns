package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.TravelAgentNameStateId;
import com.mybookingpal.dataaccesslayer.entity.ManagerToTravelAgentGroupModel;
import com.mybookingpal.dataaccesslayer.entity.TravelAgentGroupAddressComponents;
import com.mybookingpal.dataaccesslayer.mappers.TravelAgentGroupMapper;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameStateId;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class TravelAgentGroupDao {
	private final TravelAgentGroupMapper travelAgentGroupMapper;

	@Autowired
	public TravelAgentGroupDao(TravelAgentGroupMapper travelAgentGroupMapper) {
		this.travelAgentGroupMapper = travelAgentGroupMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer createTravelAgentGroup(TravelAgentGroupAddressComponents travelAgent) {
		if (travelAgent == null) {
			return null;
		}
		return travelAgentGroupMapper.createTravelAgentGroup(travelAgent);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TravelAgentGroupAddressComponents> getTravelAgentGroups(Integer id, String name, Integer companyId, String type, Integer pagination, Integer paginationPage,
			String sortingType, String sortingBy) {
		if (type == null) {
			return Collections.emptyList();
		}
		return travelAgentGroupMapper.getTravelAgentGroups(id, name, companyId, type, pagination, paginationPage, sortingType, sortingBy);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateTravelAgentGroup(TravelAgentGroupAddressComponents travelAgent) {
		if (travelAgent == null) {
			return;
		}
		travelAgentGroupMapper.updateTravelAgentGroup(travelAgent);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TravelAgentGroupAddressComponents getTravelAgentGroupById(Integer id, String type) {
		if (id == null) {
			return null;
		}
		return travelAgentGroupMapper.getTravelAgentGroupById(id, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TravelAgentGroupAddressComponents> getTravelAgentGroupsByIdsAndType(List<Integer> ids, String type) {
		if (CollectionUtils.isEmpty(ids)) {
			return Collections.emptyList();
		}
		return travelAgentGroupMapper.getTravelAgentGroupsByIdsAndType(ids, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assignManagerGroupToEntity(List<ManagerToTravelAgentGroupModel> groups) {
		if (CollectionUtils.isEmpty(groups)) {
			return;
		}
		travelAgentGroupMapper.assignManagerGroupToEntity(groups);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAssignEntityToGroup(List<ManagerToTravelAgentGroupModel> groups, String type) {
		if (CollectionUtils.isEmpty(groups)) {
			return;
		}
		travelAgentGroupMapper.deleteAssignEntityToGroup(groups, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameStateId> getEntityByGroupIdAndType(Integer id, String type) {
		return travelAgentGroupMapper.getEntityByGroupIdAndType(id, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getEntityIdsByGroupIdAndType(Integer id, String type) {
		return travelAgentGroupMapper.getEntityIdsByGroupIdAndType(id, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public NameId getNameIdTravelAgentGroupById(@Param("id") Integer id) {
		return travelAgentGroupMapper.getNameIdTravelAgentGroupById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> getCompaniesListByAdminsIds(List<Integer> adminIds) {
		if (CollectionUtils.isEmpty(adminIds)) {
			return Collections.emptyList();
		}
		return travelAgentGroupMapper.getCompaniesListByAdminsIds(adminIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateTravelAdminAssignToTravelCompany(Integer companyId, Integer adminId) {
		travelAgentGroupMapper.updateTravelAdminAssignToTravelCompany(companyId, adminId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateGroupById(NameStateId nameStateId) {
		travelAgentGroupMapper.updateStateGroupById(nameStateId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToTravelAgentGroupModel> getManagerToTravelAgentGroup(String type) {
		return travelAgentGroupMapper.getManagerToTravelAgentGroup(type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToTravelAgentGroupModel> getSingleManagerToTravelAgentGroup(String type, Integer entityId, Integer groupId) {
		return travelAgentGroupMapper.getSingleManagerToTravelAgentGroup(type, entityId, groupId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double getAVGAgencyCommission() {
		return travelAgentGroupMapper.getAVGAgencyCommission();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double getAVGCompanyCommission() {
		return travelAgentGroupMapper.getAVGCompanyCommission();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double getAgencyCommissionByEntity(Integer entityId) {
		return travelAgentGroupMapper.getAgencyCommissionByEntity(entityId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Double getCompanyCommissionByEntity(Integer entityId) {
		return travelAgentGroupMapper.getCompanyCommissionByEntity(entityId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToTravelAgentGroupModel> getManagerToTravelAgentByTypeEntityId(String type, Integer entityId, Integer groupId) {
		return travelAgentGroupMapper.getManagerToTravelAgentByTypeEntityId(type, entityId, groupId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TravelAgentNameStateId> getAllTravelAgentAgenciesByListAgents(List<Integer> agentIds) {
		if (CollectionUtils.isEmpty(agentIds)) {
			return Collections.emptyList();
		}
		return travelAgentGroupMapper.getAllTravelAgentAgenciesByListAgents(agentIds);
	}
}