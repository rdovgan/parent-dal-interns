package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.TravelAgentNameStateId;
import com.mybookingpal.dataaccesslayer.entity.ManagerToTravelAgentGroupModel;
import com.mybookingpal.dataaccesslayer.entity.TravelAgentGroupAddressComponents;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameStateId;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TravelAgentGroupMapper {

	Integer createTravelAgentGroup(TravelAgentGroupAddressComponents travelAgent);

	List<TravelAgentGroupAddressComponents> getTravelAgentGroups(@Param("id") Integer id, @Param("name") String name, @Param("company_id") Integer companyId,
			@Param("type") String type, @Param("paginationLimit") Integer pagination, @Param("paginationPage") Integer paginationPage,
			@Param("sortingType") String sortingType, @Param("sortingBy") String sortingBy);

	List<TravelAgentGroupAddressComponents> getTravelAgeniesByCompanyAdmin(@Param("id") Integer id, @Param("name") String name, @Param("adminId") String adminId,
			@Param("paginationLimit") Integer pagination, @Param("paginationPage") Integer paginationPage, @Param("sortingType") String sortingType,
			@Param("sortingBy") String sortingBy);

	void updateTravelAgentGroup(TravelAgentGroupAddressComponents travelAgent);

	TravelAgentGroupAddressComponents getTravelAgentGroupById(@Param("id") Integer id, @Param("type") String type);

	List<TravelAgentGroupAddressComponents> getTravelAgentGroupsByIdsAndType(@Param("ids") List<Integer> ids, @Param("type") String type);

	void assignManagerGroupToEntity(@Param("groups") List<ManagerToTravelAgentGroupModel> groups);

	void deleteAssignEntityToGroup(@Param("groups") List<ManagerToTravelAgentGroupModel> groups, @Param("type") String type);

	List<NameStateId> getEntityByGroupIdAndType(@Param("groupId") Integer id, @Param("entityType") String type);

	List<Integer> getEntityIdsByGroupIdAndType(@Param("groupId") Integer id, @Param("entityType") String type);

	List<Integer> getGroupIdsByEntityIdAndType(@Param("entityId") Integer id, @Param("entityType") String type);

	NameId getNameIdTravelAgentGroupById(@Param("id") Integer id);

	List<NameStateId> getGroupsByEntityId(@Param("entityId") Integer id, @Param("groupType") String type, @Param("companyId") Integer companyId);

	NameId getNameIdEntityById(@Param("id") Integer id);

	void assignManagerGroupIdToEntityId(@Param("groupId") Integer groupId, @Param("entityId") String entityId, @Param("type") String type);

	NameId getCompanyByAdminId(@Param("adminId") Integer id);

	List<NameId> getCompaniesListByAdminsIds(@Param("adminIds") List<Integer> adminIds);

	void updateTravelAdminAssignToTravelCompany(@Param("companyId") Integer companyId, @Param("adminId") Integer adminId);

	void updateStateGroupById(NameStateId nameStateId);

	List<ManagerToTravelAgentGroupModel> getManagerToTravelAgentGroup(@Param("type") String type);

	List<ManagerToTravelAgentGroupModel> getSingleManagerToTravelAgentGroup(@Param("type") String type, @Param("entityId") Integer entityId,
			@Param("groupId") Integer groupId);

	Double getAVGAgencyCommission();

	Double getAVGCompanyCommission();

	Double getAgencyCommissionByEntity(@Param("entityId") Integer entityId);

	Double getCompanyCommissionByEntity(@Param("entityId") Integer entityId);

	List<ManagerToTravelAgentGroupModel> getManagerToTravelAgentByTypeEntityId(@Param("type") String type, @Param("entityId") Integer entityId,
			@Param("groupId") Integer groupId);

	List<TravelAgentNameStateId> getAllTravelAgentAgenciesByListAgents(@Param("agentIds") List<Integer> agentIds);
}
