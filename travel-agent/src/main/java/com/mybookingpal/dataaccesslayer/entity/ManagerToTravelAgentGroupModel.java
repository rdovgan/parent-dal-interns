package com.mybookingpal.dataaccesslayer.entity;

public class ManagerToTravelAgentGroupModel {

	private Integer id;

	private Integer groupId;

	private Integer entityId;

	private String type;

	public ManagerToTravelAgentGroupModel() {
	}

	public ManagerToTravelAgentGroupModel(Integer groupId, Integer entityId, String type) {
		this.groupId = groupId;
		this.entityId = entityId;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// TODO: View usage and readjust builder
	public static class Builder {
		private Integer id;
		private Integer groupId;
		private Integer entityId;
		private String type;

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder groupId(Integer groupId) {
			this.groupId = groupId;
			return this;
		}

		public Builder entityId(Integer entityId) {
			this.entityId = entityId;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public ManagerToTravelAgentGroupModel build() {
			return new ManagerToTravelAgentGroupModel(this);
		}
	}

	private ManagerToTravelAgentGroupModel(Builder builder) {
		this.id = builder.id;
		this.groupId = builder.groupId;
		this.entityId = builder.entityId;
		this.type = builder.type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManagerToTravelAgentGroupModel other = (ManagerToTravelAgentGroupModel) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}
