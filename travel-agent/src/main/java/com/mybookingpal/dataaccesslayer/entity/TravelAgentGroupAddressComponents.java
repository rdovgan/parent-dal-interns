package com.mybookingpal.dataaccesslayer.entity;

public class TravelAgentGroupAddressComponents extends TravelAgentGroupModel {
	private String zipCode;
	private String country;
	private String city;
	private String stateProvince;

	public TravelAgentGroupAddressComponents() {
	}

	public TravelAgentGroupAddressComponents(String zipCode, String country, String city, String stateProvince) {
		this.zipCode = zipCode;
		this.country = country;
		this.city = city;
		this.stateProvince = stateProvince;
	}

	// TODO: View usage and readjust builder
	public static class Builder {
		private Integer id;
		private Integer companyId;
		private String companyName;
		private String contactName;
		private String emailAddress;
		private String abbreviation;
		private String state;
		private String website;
		private String address;
		private String city;
		private String stateProvince;
		private String country;
		private String zipCode;
		private String phone;
		private Double companyMarkup;
		private String type;
		private Integer locationId;

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder companyId(Integer companyId) {
			this.companyId = companyId;
			return this;
		}

		public Builder companyName(String companyName) {
			this.companyName = companyName;
			return this;
		}

		public Builder contactName(String contactName) {
			this.contactName = contactName;
			return this;
		}

		public Builder emailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
			return this;
		}

		public Builder abbreviation(String abbreviation) {
			this.abbreviation = abbreviation;
			return this;
		}

		public Builder state(String state) {
			this.state = state;
			return this;
		}

		public Builder website(String website) {
			this.website = website;
			return this;
		}

		public Builder address(String address) {
			this.address = address;
			return this;
		}

		public Builder city(String city) {
			this.city = city;
			return this;
		}

		public Builder stateProvince(String stateProvince) {
			this.stateProvince = stateProvince;
			return this;
		}

		public Builder country(String country) {
			this.country = country;
			return this;
		}

		public Builder zipCode(String zipCode) {
			this.zipCode = zipCode;
			return this;
		}

		public Builder phone(String phone) {
			this.phone = phone;
			return this;
		}

		public Builder companyMarkup(Double companyMarkup) {
			this.companyMarkup = companyMarkup;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder locationId(Integer locationId) {
			this.locationId = locationId;
			return this;
		}

		public TravelAgentGroupAddressComponents build() {
			return new TravelAgentGroupAddressComponents(this);
		}
	}

	private TravelAgentGroupAddressComponents(Builder builder) {
		setId(builder.id);
		setCompanyId(builder.companyId);
		setCompanyName(builder.companyName);
		setCompanyName(builder.contactName);
		setEmailAddress(builder.emailAddress);
		setAbbreviation(builder.abbreviation);
		setState(builder.state);
		setWebsite(builder.website);
		setAddress(builder.address);
		this.city = builder.city;
		this.stateProvince = builder.stateProvince;
		this.country = builder.country;
		this.zipCode = builder.zipCode;
		setPhone(builder.phone);
		setCompanyMarkup(builder.companyMarkup);
		setType(builder.type);
		setLocationId(builder.locationId);
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}