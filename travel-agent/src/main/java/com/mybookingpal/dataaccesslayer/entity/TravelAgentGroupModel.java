package com.mybookingpal.dataaccesslayer.entity;

//import com.mybookingpal.shared.validator.descriptor.Email;

public class TravelAgentGroupModel {

	private Integer id;

	private Integer companyId;

	private String companyName;

	private String contactName;

	//TODO :  add Email annotation from com.mybookingpal.shared.validator.descriptor.Email
	//@Email
	private String emailAddress;

	private String abbreviation;

	private String state;

	private String website;

	private String address;

	private String phone;

	private Double companyMarkup;

	private String type;

	private Integer locationId;

	private String travelCompanyName;

	public TravelAgentGroupModel() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTravelCompanyName() {
		return travelCompanyName;
	}

	public void setTravelCompanyName(String travelCompanyName) {
		this.travelCompanyName = travelCompanyName;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Double getCompanyMarkup() {
		return companyMarkup;
	}

	public void setCompanyMarkup(Double companyMarkup) {
		this.companyMarkup = companyMarkup;
	}
}
