package com.mybookingpal.dataaccesslayer.dto;

public class TravelAgentNameStateId {

	private Integer id;

	private String name;

	private String state;

	private Integer travelAgentGroupId;

	private String travelAgentGroupName;

	public TravelAgentNameStateId() {
	}

	public TravelAgentNameStateId(Integer id, String name, String state, Integer travelAgentGroupId, String travelAgentGroupName) {
		this.id = id;
		this.name = name;
		this.state = state;
		this.travelAgentGroupId = travelAgentGroupId;
		this.travelAgentGroupName = travelAgentGroupName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getTravelAgentGroupId() {
		return travelAgentGroupId;
	}

	public void setTravelAgentGroupId(Integer travelAgentGroupId) {
		this.travelAgentGroupId = travelAgentGroupId;
	}

	public String getTravelAgentGroupName() {
		return travelAgentGroupName;
	}

	public void setTravelAgentGroupName(String travelAgentGroupName) {
		this.travelAgentGroupName = travelAgentGroupName;
	}

	// TODO: View usage and readjust builder
	public static class Builder {
		private Integer id;
		private String name;
		private String state;
		private Integer travelAgentGroupId;
		private String travelAgentGroupName;

		public Builder id(Integer id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder state(String state) {
			this.state = state;
			return this;
		}

		public Builder travelAgentGroupId(Integer travelAgentGroupId) {
			this.travelAgentGroupId = travelAgentGroupId;
			return this;
		}

		public Builder travelAgentGroupName(String travelAgentGroupName) {
			this.travelAgentGroupName = travelAgentGroupName;
			return this;
		}

		public TravelAgentNameStateId build() {
			return new TravelAgentNameStateId(this);
		}
	}

	private TravelAgentNameStateId(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.state = builder.state;
		this.travelAgentGroupId = builder.travelAgentGroupId;
		this.travelAgentGroupName = builder.travelAgentGroupName;
	}
}
