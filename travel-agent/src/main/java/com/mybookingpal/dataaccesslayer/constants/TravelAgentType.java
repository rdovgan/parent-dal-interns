package com.mybookingpal.dataaccesslayer.constants;

public enum TravelAgentType {
	COMPANY("Company"), AGENCY("Agency");

	private final String value;

	TravelAgentType(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
