package com.mybookingpal.dataaccesslayer.constants;

public enum TravelAgentEntityType {
	PM("PM"), AGENT("Agent"), ADMIN("Admin");

	private final String value;

	TravelAgentEntityType(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}