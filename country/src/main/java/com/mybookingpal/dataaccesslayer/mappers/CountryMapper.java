package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.Country;
import com.mybookingpal.dataaccesslayer.entity.CountryInfo;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface CountryMapper {

	void create(Country action);

	Country read(String id);

	void update(Country action);

	Country readbyname(String name);

	String idbyname(String name);

	List<NameId> nameidwidget(@Param("id") String id, @Param("organizationid") String organizationid);

	List<Country> getAllCountries();

	List<CountryInfo> getAllCountriesInfo();

	List<CountryInfo> getCountryPhoneInfo();
}