package com.mybookingpal.dataaccesslayer.entity;

public class CountryInfo {

	private String countryName;
	private String countryCodeTwoSymbols;
	private String countryCodeThreeSymbols;
	private String language;
	private String currencyName;
	private String currencyCode;
	private String currencySymbol;
	private String phoneCode;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCodeTwoSymbols() {
		return countryCodeTwoSymbols;
	}

	public void setCountryCodeTwoSymbols(String countryCodeTwoSymbols) {
		this.countryCodeTwoSymbols = countryCodeTwoSymbols;
	}

	public String getCountryCodeThreeSymbols() {
		return countryCodeThreeSymbols;
	}

	public void setCountryCodeThreeSymbols(String countryCodeThreeSymbols) {
		this.countryCodeThreeSymbols = countryCodeThreeSymbols;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
