package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.utils.entity.NameId;

public class Country extends NameId {
	public enum Code {
		AT,
		AU,
		BE,
		CA,
		CH,
		CZ,
		DE,
		DK,
		ES,
		FI,
		FR,
		GB,
		HR,
		IT,
		NL,
		NO,
		PL,
		SE,
		US
	};
	public static final String US = "US";
	public static final String US_NAME = "United States";

	private String currency;
	private String language;
	private String phone;

	public Country() {
	}

	public Country(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Country [currency=");
		builder.append(currency);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", language=");
		builder.append(language);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}