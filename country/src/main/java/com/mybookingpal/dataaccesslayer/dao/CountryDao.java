package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.Country;
import com.mybookingpal.dataaccesslayer.entity.CountryInfo;
import com.mybookingpal.dataaccesslayer.mappers.CountryMapper;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryDao {

	@Autowired
	private CountryMapper countryMapper;

	public Country readById(String name) {
		return countryMapper.read(name);
	}

	public Country readByName(String name) {
		return countryMapper.readbyname(name);
	}

	public List<Country> getAllCountries() {
		return countryMapper.getAllCountries();
	}

	public List<CountryInfo> getAllCountriesInfo() {
		return countryMapper.getAllCountriesInfo();
	}

	public List<CountryInfo> getCountryPhoneInfo() {
		return countryMapper.getCountryPhoneInfo();
	}

	public List<NameId> nameidwidget(String id, String organizationId) {
		return countryMapper.nameidwidget(id, organizationId);
	}

	public String idbyname(String name){
		return countryMapper.idbyname(name);
	}

	public void update(Country action){
	countryMapper.update(action);
	}

	public void create(Country action){
		countryMapper.create(action);
	}
}