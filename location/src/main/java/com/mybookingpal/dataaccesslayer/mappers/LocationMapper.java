package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.FullLocationResult;
import com.mybookingpal.dataaccesslayer.entity.LocationModel;
import com.mybookingpal.dataaccesslayer.entity.LocationXsl;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LocationMapper {

	void create(LocationModel action);

	LocationModel read(Integer id);

	void update(LocationModel action);

	LocationModel readWithFlushCache(String id);

	LocationModel rentalsUnitedSearch(LocationModel action);

	LocationModel name(LocationModel action);

	List<NameId> countryListByNameId(NameIdAction action);

	List<NameId> locationsByCountryRegion(NameIdAction action);

	List<NameId> locationListByCountry(NameIdAction action);

	List<NameId> nameIdNearBy(@Param("neLatitude") Double neLatitude, @Param("swLatitude") Double swLatitude, @Param("neLongitude") Double neLongitude, @Param("swLongitude") Double swLongitude);

	List<LocationModel> activeLocations();

	LocationXsl restread(String locationid);

	List<LocationModel> getSearchLocations(NameId nameid);

	List<LocationModel> getSearchSublocations(NameId nameid);

	List<LocationModel> getUnUpdatedEntries();

	List<LocationModel> getLocations(Integer channelId);

	LocationModel read(LocationModel location);

	List<LocationModel> getLocationsWithoutZip(@Param("country") String country, @Param("region") String region, @Param("startIndex") Integer startIndex,
			@Param("limit") Integer limit);

	FullLocationResult getLocationInformationById(Integer productId);

	List<LocationModel> readCreatedLocationsWithoutTimeZoneId(@Param("limit") Integer limit);

	void updateTimeZoneIdForLocations(@Param("timeZoneId") Integer timeZoneId, @Param("list") List<Integer> locationIds);

	List<NameId> regionNameId(NameIdAction action);

	List<LocationModel> googleExists(LocationModel action);

	List<LocationModel> readByGName();

	List<LocationModel> readByExample(LocationModel location);

	LocationModel exists(LocationModel action);

	LocationModel readByNameLike(LocationModel action);

	List<LocationModel> readByRegionName(@Param("region") String region);

}
