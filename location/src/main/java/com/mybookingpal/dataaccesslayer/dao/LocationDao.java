package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.FullLocationResult;
import com.mybookingpal.dataaccesslayer.entity.LocationModel;
import com.mybookingpal.dataaccesslayer.entity.LocationXsl;
import com.mybookingpal.dataaccesslayer.mappers.LocationMapper;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import com.mybookingpal.utils.service.MbpCollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class LocationDao {

	@Autowired
	private LocationMapper locationMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel findLocationById(Integer id) {
		return locationMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel exists(LocationModel location) {
		return locationMapper.exists(location);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(LocationModel location) {
		locationMapper.update(location);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> readByExample(LocationModel example) {
		return locationMapper.readByExample(example);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(LocationModel location) {
		locationMapper.create(location);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel read(String id) {
		return locationMapper.read(NumberUtils.createInteger(id));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel readWithFlushCache(String id) {
		return locationMapper.readWithFlushCache(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FullLocationResult getLocationInformationById(Integer id) {
		return locationMapper.getLocationInformationById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> readCreatedLocationsWithoutTimeZoneId(Integer limit) {
		return locationMapper.readCreatedLocationsWithoutTimeZoneId(limit);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateTimeZoneIdForLocations(Integer timeZoneId, List<Integer> locationIds) {
		if (MbpCollectionUtils.isEmpty(locationIds)) {
			return;
		}
		locationMapper.updateTimeZoneIdForLocations(timeZoneId, locationIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> readByRegionName(String region) {
		return locationMapper.readByRegionName(region);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> googleExists(LocationModel action) {
		return locationMapper.googleExists(action);
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> getLocations(Integer channelId) {
		return locationMapper.getLocations(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> countryListByNameId(NameIdAction action) {
		return locationMapper.countryListByNameId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> locationListByCountry(NameIdAction country) {
		return locationMapper.locationListByCountry(country);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> regionNameId(NameIdAction action) {
		return locationMapper.regionNameId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> locationsByCountryRegion(NameIdAction action) {
		return locationMapper.locationsByCountryRegion(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameIdNearBy(Double neLatitude, Double swLatitude, Double neLongitude, Double swLongitude) {
		return locationMapper.nameIdNearBy(neLatitude, swLatitude, neLongitude, swLongitude);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel name(LocationModel action) {
		return locationMapper.name(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationXsl restread(String locationId) {
		return locationMapper.restread(locationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> getUnUpdatedEntries() {
		return locationMapper.getUnUpdatedEntries();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> getSearchLocations(NameId action) {
		return locationMapper.getSearchLocations(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> getSearchSublocations(NameId action) {
		return locationMapper.getSearchSublocations(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> getLocationsWithoutZip(@Param("country") String country, @Param("region") String region, @Param("startIndex") Integer startIndex,
			@Param("limit") Integer limit) {
		return locationMapper.getLocationsWithoutZip(country, region, startIndex, limit);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel rentalsUnitedSearch(LocationModel action) {
		return locationMapper.rentalsUnitedSearch(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocationModel readByNameLike(LocationModel action) {
		return locationMapper.readByNameLike(action);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocationModel> activeLocations() {
		return locationMapper.activeLocations();
	}

}
