package com.mybookingpal.dataaccesslayer.entity;

public class LocationModel {

	private Integer id;
	private String name;
	private String state;
	private String gName;
	private String code;
	private String country;
	private String region;
	private String adminAreaLvl1;
	private String adminAreaLvl2;
	private String area;
	private String locationType;
	private String iata;
	private String notes;
	private String codeInterHome;
	private String codeRentalsUnited;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	private Integer parentId;
	private String zipCode;
	private Integer timeZoneId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getGName() {
		return gName;
	}

	public void setGName(String gName) {
		this.gName = gName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAdminAreaLvl1() {
		return adminAreaLvl1;
	}

	public void setAdminAreaLvl1(String adminAreaLvl1) {
		this.adminAreaLvl1 = adminAreaLvl1;
	}

	public String getAdminAreaLvl2() {
		return adminAreaLvl2;
	}

	public void setAdminAreaLvl2(String adminAreaLvl2) {
		this.adminAreaLvl2 = adminAreaLvl2;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCodeInterHome() {
		return codeInterHome;
	}

	public void setCodeInterHome(String codeInterHome) {
		this.codeInterHome = codeInterHome;
	}

	public String getCodeRentalsUnited() {
		return codeRentalsUnited;
	}

	public void setCodeRentalsUnited(String codeRentalsUnited) {
		this.codeRentalsUnited = codeRentalsUnited;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}
}
