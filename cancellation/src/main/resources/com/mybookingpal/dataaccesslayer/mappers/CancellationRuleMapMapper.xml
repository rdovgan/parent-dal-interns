<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.mybookingpal.dataaccesslayer.mappers.CancellationRuleMapMapper">

	<resultMap id="ResultMap" type="com.mybookingpal.dataaccesslayer.entity.ChannelSpecificCancellationRuleModel">
		<id column="id" jdbcType="BIGINT" property="id"/>
		<result column="name" jdbcType="VARCHAR" property="name"/>
		<result column="channel_abbreviation" jdbcType="VARCHAR" property="channel"/>
		<result column="cancellation_group" jdbcType="VARCHAR" property="channelGroup"/>
		<result column="cancellation_refund_before" jdbcType="DOUBLE" property="refundBefore"/>
		<result column="cancellation_refund_after" jdbcType="DOUBLE" property="refundAfter"/>
		<result column="cancellation_date" jdbcType="INTEGER" property="days"/>
		<result column="optional" jdbcType="INTEGER" property="optional"/>
		<result column="hours" jdbcType="INTEGER" property="hours"/>
	</resultMap>

	<resultMap id="PaymentPolicyMap" type="com.mybookingpal.dataaccesslayer.dto.ChannelPaymentPolicyMap">
		<id column="id" jdbcType="BIGINT" property="id"/>
		<result column="cancellation_rule_map_id" jdbcType="BIGINT" property="cancellationRuleMapId"/>
		<result column="payment_date" jdbcType="INTEGER" property="paymentDate"/>
		<result column="charge_percent_before" jdbcType="DOUBLE" property="chargePercentBefore"/>
		<result column="charge_percent_after" jdbcType="DOUBLE" property="chargePercentAfter"/>
		<result column="policy_description" jdbcType="VARCHAR" property="policyDescription"/>
	</resultMap>

	<sql id="columns">
		id, name, channel_abbreviation, cancellation_group,cancellation_refund_before, cancellation_refund_after, cancellation_date, optional, hours
	</sql>

	<sql id="payment_policy_columns">
		id, cancellation_rule_map_id, payment_date, charge_percent_before, charge_percent_after, policy_description
	</sql>


	<insert id="create" parameterType="com.mybookingpal.dataaccesslayer.entity.ChannelSpecificCancellationRuleModel" useGeneratedKeys="true" keyProperty="id">
		insert into cancellation_rule_map (
			name,
			channel_abbreviation,
			cancellation_group,
			cancellation_refund_before,
			cancellation_refund_after,
			cancellation_date,
			optional,
			hours
		)
		values (
				   #{name,jdbcType=VARCHAR},
				   #{channel,jdbcType=VARCHAR},
				   #{channelGroup,jdbcType=VARCHAR},
				   #{refundBefore,jdbcType=DOUBLE},
				   #{refundAfter,jdbcType=DOUBLE},
				   #{days,jdbcType=INTEGER},
				   #{optional,jdbcType=INTEGER},
				   #{hours,jdbcType=INTEGER}
			   )
	</insert>

	<select id="read" parameterType="int" resultMap="ResultMap">
		select
		<include refid="columns"/>
		from cancellation_rule_map
		where id = #{id,jdbcType=INTEGER}
	</select>

	<select id="readAll" parameterType="java.util.List" resultMap="ResultMap">
		select
		<include refid="columns"/>
		from cancellation_rule_map
		where state = 1
	</select>

	<select id="readChannelAbbreviation" parameterType="int" resultType="java.lang.String">
		select channel_abbreviation
		from cancellation_rule_map
		where id = #{id,jdbcType=INTEGER}
	</select>

	<update id="update" parameterType="com.mybookingpal.dataaccesslayer.entity.ChannelSpecificCancellationRuleModel">
		update  cancellation_rule_map
		<set>
			name = #{name},
			channel_abbreviation = #{channel},
			cancellation_group = #{channelGroup},
			cancellation_refund_before = #{refundBefore},
			cancellation_refund_after = #{refundAfter},
			cancellation_date = #{days},
			optional = #{optional},
			hours = #{hours}
		</set>
		where ID = #{id,jdbcType=INTEGER}
	</update>

	<select id="readByPmName" parameterType="string" resultMap="ResultMap">
		select
		<include refid="columns"/>
		from cancellation_rule_map
		where cancellation_group = #{channel,jdbcType=INTEGER}
	</select>

	<select id="getAllActiveCancellationRules" resultMap="ResultMap">
		select
		<include refid="columns"/>
		from cancellation_rule_map
		where hours is null
		and state = 1
		order by id
	</select>

	<select id="getAllCancellationRules" resultMap="ResultMap">
		select
		<include refid="columns"/>
		from cancellation_rule_map
		where hours is null
		order by id
	</select>

	<select id="readChannelSpecificCancellationRulesByIds" parameterType="java.util.List" resultMap="ResultMap">
		SELECT
		<include refid="columns"/>
		from cancellation_rule_map
		WHERE ID in
		<foreach item="item" index="index" collection="list" open="(" separator="," close=")">
			#{item}
		</foreach>
		and state = 1
	</select>

	<select id="getPaymentPolicyByChannelAndPm" parameterType="integer" resultMap="PaymentPolicyMap">
		SELECT <include refid="payment_policy_columns"/>
		FROM channel_payment_policy_map
		WHERE cancellation_rule_map_id = #{cancellationRuleMapId,jdbcType=BIGINT}
	</select>

	<select id="readCancellationsByPmIdOrProductIdAndChannelId"
			parameterType="com.mybookingpal.dataaccesslayer.entity.ChannelCancellationRuleModel" resultMap="ResultMap">
		SELECT
		<include refid="columns"/>
		FROM cancellation_rule_map ccrm
		LEFT JOIN channel_cancellation_rule ccr ON (ccrm.id =
		ccr.channel_rule_map_id)
		WHERE ccr.channel_id = #{channelId, jdbcType=INTEGER}
		AND ccr.active = 1
		<if test="productId != null">
			AND ccr.product_id = #{productId, jdbcType=INTEGER}
		</if>
		<if test="propertyManagerId != null">
			AND ccr.property_manager_id = #{propertyManagerId, jdbcType=INTEGER}
			AND ccr.product_id IS NULL
		</if>
		<!-- Only check records which are not NULL in case of B.com, for other channels is not mandatory -->
		<if test="channelId == 276">
			AND ccrm.channel_cancellation_map_id IS NOT NULL
		</if>
	</select>

	<select id="getPaymentPolicyByCancellationIds" parameterType="java.util.List" resultMap="PaymentPolicyMap">
		SELECT
		<include refid="payment_policy_columns"/>
		from channel_payment_policy_map
		WHERE cancellation_rule_map_id in
		<foreach item="item" index="index" collection="list" open="(" separator="," close=")">
			#{item}
		</foreach>
	</select>

	<select id="readByAbbreviationAndCancellationDate" resultMap="ResultMap">
		SELECT <include refid="columns"/>
		FROM cancellation_rule_map
		WHERE cancellation_date = #{cancellationDate, jdbcType=INTEGER}
		AND channel_abbreviation = #{abbreviation, jdbcType=VARCHAR}
	</select>

	<select id="readByChannelCancellationMapId" parameterType="String"
			resultMap="ResultMap">
		select
		<include refid="columns" />
		from cancellation_rule_map
		where channel_cancellation_map_id = #{channelCancellationMapId,jdbcType=VARCHAR}
		LIMIT 1
	</select>

	<select id="readByChannelCancellationMapPerChannelAbbreviation" parameterType="String"
			resultMap="ResultMap">
		select
		<include refid="columns" />
		from cancellation_rule_map
		where channel_abbreviation = #{channelAbbreviation,jdbcType=VARCHAR}
		and state = 1
	</select>

</mapper>