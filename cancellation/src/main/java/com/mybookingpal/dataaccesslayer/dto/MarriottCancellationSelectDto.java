package com.mybookingpal.dataaccesslayer.dto;

public class MarriottCancellationSelectDto {

	private Integer id;
	private Boolean value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}
}
