package com.mybookingpal.dataaccesslayer.dto;

public class ChannelPaymentPolicyMap {

	private Integer id;
	private Long cancellationRuleMapId;
	private Integer paymentDate;
	private Integer paymentHour;
	private Double chargePercentBefore;
	private Double chargePercentAfter;
	private String policyDescription;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCancellationRuleMapId() {
		return cancellationRuleMapId;
	}

	public void setCancellationRuleMapId(Long cancellationRuleMapId) {
		this.cancellationRuleMapId = cancellationRuleMapId;
	}

	public Integer getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Integer paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getPaymentHour() {
		return paymentHour;
	}

	public void setPaymentHour(Integer paymentHour) {
		this.paymentHour = paymentHour;
	}

	public Double getChargePercentBefore() {
		return chargePercentBefore;
	}

	public void setChargePercentBefore(Double chargePercentBefore) {
		this.chargePercentBefore = chargePercentBefore;
	}

	public Double getChargePercentAfter() {
		return chargePercentAfter;
	}

	public void setChargePercentAfter(Double chargePercentAfter) {
		this.chargePercentAfter = chargePercentAfter;
	}

	public String getPolicyDescription() {
		return policyDescription;
	}

	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}
}
