package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.MarriottCancellationSelectDto;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationDateRuleModel;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationNightsRuleModel;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationRuleModel;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PropertyManagerCancellationRuleMapper {
	void create(PropertyManagerCancellationRuleModel cancellationRule);

	PropertyManagerCancellationRuleModel read(Integer id);

	List<PropertyManagerCancellationRuleModel> readbypmid(Integer propertyManagerId);

	List<PropertyManagerCancellationRuleModel> readByProductId(Integer productId);

	List<PropertyManagerCancellationRuleModel> readChannelRules(NameIdAction action);

	List<PropertyManagerCancellationDateRuleModel> readdatebypmid(Integer propertyManagerId);

	void update(PropertyManagerCancellationRuleModel cancellationRule);

	void insertOrUpdateList(@Param("cancellationRules") List<PropertyManagerCancellationRuleModel> cancellationRules);

	void delete(Integer id);

	void deletebypmid(Integer propertyManagerId);

	void deletebypmidNotInList(@Param("propertyManagerId") Integer propertyManagerId,
			@Param("propertyManagerCancellationRules") List<Integer> propertyManagerCancellationRules);

	List<PropertyManagerCancellationRuleModel> readCancellationsByRatePlanId(Long ratePlanId);

	void deleteAllByIds(List<Integer> listIds);

	List<PropertyManagerCancellationRuleModel> readSpecificCancellationsByProductId(@Param("productId") Integer productId,
			@Param("channelId") Integer channelId);

	List<PropertyManagerCancellationRuleModel> readSpecificCancellationsByProductIds(@Param("productIds") List<Integer> productId,
			@Param("channelIds") List<Integer> channelId);

	Integer readCancellationDaysForProductByProductId(Integer productId);

	List<PropertyManagerCancellationRuleModel> readAllByProductId(@Param("productId") Long productId);

	List<PropertyManagerCancellationRuleModel> readAllByPropertyManager(@Param("pmId") Integer pmId);

	List<PropertyManagerCancellationRuleModel> readAllByPropertyManagerWithChannelRuleConnection(@Param("pmId") Integer pmId);

	List<PropertyManagerCancellationRuleModel> readAllByPropertyManagerWithChannelRuleConnectionWithoutProductIds(@Param("pmId") Integer pmId);

	List<MarriottCancellationSelectDto> getProductsSpecificCancellationRules(@Param("ids") List<Integer> productIds, @Param("channelId") Integer channelId);

	List<MarriottCancellationSelectDto> getProductsChannelPartnerCancellationRules(@Param("ids") List<Integer> pmIds, @Param("channelId") Integer channelId);

	List<MarriottCancellationSelectDto> getProductsCancellationRules(@Param("ids") List<Integer> productIds);

	List<MarriottCancellationSelectDto> getPropertyManagersCancellationRules(@Param("ids") List<Integer> pmIds);

	List<PropertyManagerCancellationNightsRuleModel> readByChannelIdSupplierId(@Param("supplierId") Integer supplierId,
			@Param("list") List<Integer> channelIds);

	List<PropertyManagerCancellationNightsRuleModel> readByChannelIdProductId(@Param("productId") Integer productId, @Param("list") List<Integer> channelIds);

	List<PropertyManagerCancellationRuleModel> readByPmId(String pmId);
}
