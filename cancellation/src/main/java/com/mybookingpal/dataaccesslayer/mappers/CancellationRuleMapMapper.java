package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelCancellationRuleModel;
import com.mybookingpal.dataaccesslayer.dto.ChannelPaymentPolicyMap;
import com.mybookingpal.dataaccesslayer.entity.ChannelSpecificCancellationRuleModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CancellationRuleMapMapper {

	void create(ChannelSpecificCancellationRuleModel rule);

	ChannelSpecificCancellationRuleModel read(Integer id);

	List<ChannelSpecificCancellationRuleModel> readAll();

	String readChannelAbbreviation(Integer id);

	void update(ChannelSpecificCancellationRuleModel rule);

	List<ChannelSpecificCancellationRuleModel> readByPmName(String name);

	List<ChannelSpecificCancellationRuleModel> getAllActiveCancellationRules();

	List<ChannelSpecificCancellationRuleModel> getAllCancellationRules();

	List<ChannelSpecificCancellationRuleModel> readChannelSpecificCancellationRulesByIds(List<Integer> cancellationIds);

	ChannelPaymentPolicyMap getPaymentPolicyByChannelAndPm(Integer cancellationRuleMapId);

	List<ChannelSpecificCancellationRuleModel> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRuleModel cancellationRule);

	List<ChannelPaymentPolicyMap> getPaymentPolicyByCancellationIds(List<Integer> cancellationRuleMapIds);

	ChannelSpecificCancellationRuleModel readByAbbreviationAndCancellationDate(@Param("abbreviation") String abbreviation,
			@Param("cancellationDate") Integer cancellationDate);

	ChannelSpecificCancellationRuleModel readByChannelCancellationMapId(@Param("channelCancellationMapId") String channelCancellationMapId);

	List<ChannelSpecificCancellationRuleModel> readByChannelCancellationMapPerChannelAbbreviation(@Param("channelAbbreviation") String channelAbbreviation);

}
