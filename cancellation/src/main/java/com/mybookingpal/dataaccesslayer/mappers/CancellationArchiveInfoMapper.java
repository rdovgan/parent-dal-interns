package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.CancellationArchiveInfoModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CancellationArchiveInfoMapper {
	CancellationArchiveInfoModel getByReservationId(Integer reservationId);

	void save(@Param("list") List<CancellationArchiveInfoModel> list);

	List<Integer> getActiveReservationIdsWithoutArchivedCancellations(@Param("states") List<String> states, Integer limit);
}