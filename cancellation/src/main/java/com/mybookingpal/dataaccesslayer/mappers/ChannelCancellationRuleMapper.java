package com.mybookingpal.dataaccesslayer.mappers;

import java.util.List;

import com.mybookingpal.dataaccesslayer.entity.ChannelCancellationRuleModel;
import org.apache.ibatis.annotations.Param;

public interface ChannelCancellationRuleMapper {
	void create(ChannelCancellationRuleModel cancellationRule);

	ChannelCancellationRuleModel read(Integer id);

	void update(ChannelCancellationRuleModel cancellationRule);

	void insertOrUpdateList(@Param("cancellationRules") List<ChannelCancellationRuleModel> cancellationRules);

	void delete(Integer id);

	List<ChannelCancellationRuleModel> readByChannelId(Integer channelId);

	List<ChannelCancellationRuleModel> readByPmId(Integer pmId);

	List<ChannelCancellationRuleModel> readByPmIdWithPolicies(Integer pmId);

	void deleteByChannelId(Integer channelId);

	List<ChannelCancellationRuleModel> readCancellationsByPmIdAndChannelPartner(@Param("pmId") Integer pmId,
			@Param("channelPartnerIds") List<Integer> channelPartnerIds);

	void deleteAllByIds(List<Integer> listIds);

	List<ChannelCancellationRuleModel> readCancellationsByProductIdAndChannelPartner(@Param("productId") Integer productId,
			@Param("channelPartnerIds") List<Integer> channelPartnerIds);

	List<ChannelCancellationRuleModel> readByProductId(Integer productId);

	List<ChannelCancellationRuleModel> readByProductIds(@Param("productIds") List<Integer> productIds);

	List<ChannelCancellationRuleModel> readByPMIds(@Param("pmId") Integer pmId);

	List<ChannelCancellationRuleModel> readByProductIdsChannelIds(@Param("productIds") List<Integer> productIds, @Param("channelIds") List<Integer> channelIds);

	List<ChannelCancellationRuleModel> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRuleModel cancellationRule);

	List<String> getPropertyIdsByPmIdAndChannelId(@Param("pmId") Integer pmId, @Param("list") List<Integer> channelIds);
}
