package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PropertyManagerCancellationNightsRuleModel extends PropertyManagerCancellationRuleModel {

	public PropertyManagerCancellationNightsRuleModel() {
		super();
	}

	public PropertyManagerCancellationNightsRuleModel(Integer propertyManagerId, Integer cancellationDate, Integer cancellationRefund,
			Double cancellationTransactionFee, Integer cancellationNights) {
		super();
		setPropertyManagerId(propertyManagerId);
		setCancellationDate(cancellationDate);
		setCancellationRefund(cancellationRefund);
		setCancellationTransactionFee(cancellationTransactionFee);
		setCancellationNights(cancellationNights);
	}

	private Integer cancellationNights; // if cancellationNights set, we need to charge customer for the amount equal to this nights

	public Integer getCancellationNights() {
		return cancellationNights;
	}

	public void setCancellationNights(Integer cancellationNights) {
		this.cancellationNights = cancellationNights;
	}

	public Integer getCancellationRuleType() {
		return 2;
	}
}

