package com.mybookingpal.dataaccesslayer.entity;

import java.util.Objects;

//TODO need to investigate
//@XStreamAlias("propertyManagerCancellationRule")
public abstract class PropertyManagerCancellationRuleModel implements Cloneable {

	public PropertyManagerCancellationRuleModel() {
		super();
	}

	//TODO need to investigate
	//@XStreamOmitField
	protected Integer id;

	//TODO need to investigate
	//@XStreamOmitField
	protected Integer propertyManagerId; // ID of PM in Party table

	//TODO need to investigate
	//@XStreamOmitField
	protected Integer productId;

	protected Integer cancellationRefund; // Refund value (%) that traveler receive in case cancellation.
	protected Double cancellationTransactionFee; // Fee value that traveler pay for the transaction cancellation.
	protected Integer cancellationRuleType;
	protected Integer cancellationDate; // Period (days count) at what traveler can cancel reservation if cancellationType=2;
	private Long ratePlanId;
	private Integer cancellationNights;
	private Integer optional;
	private String description;
	private String cancellationRuleName;
	private String channelAbbreviation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getCancellationRefund() {
		return cancellationRefund;
	}

	public void setCancellationRefund(Integer cancellationRefund) {
		this.cancellationRefund = cancellationRefund;
	}

	public Double getCancellationTransactionFee() {
		return cancellationTransactionFee;
	}

	public void setCancellationTransactionFee(Double cancellationTransactionFee) {
		this.cancellationTransactionFee = cancellationTransactionFee;
	}

	public Integer getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Integer cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public abstract Integer getCancellationRuleType();

	public void setCancellationRuleType(Integer cancellationRuleType) {
		this.cancellationRuleType = cancellationRuleType;
	}

	public Long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Integer getCancellationNights() {
		return cancellationNights;
	}

	public void setCancellationNights(Integer cancellationNights) {
		this.cancellationNights = cancellationNights;
	}

	public Integer getOptional() {
		return optional;
	}

	public void setOptional(Integer optional) {
		this.optional = optional;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCancellationRuleName() {
		return cancellationRuleName;
	}

	public void setCancellationRuleName(String cancellationRuleName) {
		this.cancellationRuleName = cancellationRuleName;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PropertyManagerCancellationRuleModel{");
		sb.append("id=").append(id);
		sb.append(", propertyManagerId=").append(propertyManagerId);
		sb.append(", productId=").append(productId);
		sb.append(", cancellationRefund=").append(cancellationRefund);
		sb.append(", cancellationTransactionFee=").append(cancellationTransactionFee);
		sb.append(", cancellationRuleType=").append(cancellationRuleType);
		sb.append(", cancellationDate=").append(cancellationDate);
		sb.append(", ratePlanId=").append(ratePlanId);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public PropertyManagerCancellationRuleModel clone() throws CloneNotSupportedException {
		return (PropertyManagerCancellationRuleModel) super.clone();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PropertyManagerCancellationRuleModel that = (PropertyManagerCancellationRuleModel) o;
		return Objects.equals(id, that.id) && Objects.equals(propertyManagerId, that.propertyManagerId) && Objects.equals(productId, that.productId) && Objects
				.equals(cancellationRefund, that.cancellationRefund) && Objects.equals(cancellationTransactionFee, that.cancellationTransactionFee) && Objects
				.equals(cancellationRuleType, that.cancellationRuleType) && Objects.equals(cancellationDate, that.cancellationDate) && Objects
				.equals(ratePlanId, that.ratePlanId) && Objects.equals(cancellationNights, that.cancellationNights) && Objects.equals(optional, that.optional)
				&& Objects.equals(description, that.description) && Objects.equals(cancellationRuleName, that.cancellationRuleName) && Objects
				.equals(channelAbbreviation, that.channelAbbreviation);
	}

	@Override
	public int hashCode() {
		return Objects
				.hash(id, propertyManagerId, productId, cancellationRefund, cancellationTransactionFee, cancellationRuleType, cancellationDate, ratePlanId,
						cancellationNights, optional, description, cancellationRuleName, channelAbbreviation);
	}
}
