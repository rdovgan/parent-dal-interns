package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Scope("prototype")
public class ChannelSpecificCancellationRuleModel {

	private BigDecimal id;
	private String name;
	private String channel;
	private String channelGroup;
	private Double refundBefore;
	private Double refundAfter;
	private Integer days;
	private Integer optional;
	private Integer hours;
	private String channelCancellationMapId;

	public String getChannelCancellationMapId() {
		return channelCancellationMapId;
	}

	public void setChannelCancellationMapId(String channelCancellationMapId) {
		this.channelCancellationMapId = channelCancellationMapId;
	}

	public ChannelSpecificCancellationRuleModel() {
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelGroup() {
		return channelGroup;
	}

	public void setChannelGroup(String channelGroup) {
		this.channelGroup = channelGroup;
	}

	public Double getRefundBefore() {
		return refundBefore;
	}

	public void setRefundBefore(Double refundBefore) {
		this.refundBefore = refundBefore;
	}

	public Double getRefundAfter() {
		return refundAfter;
	}

	public void setRefundAfter(Double refundAfter) {
		this.refundAfter = refundAfter;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Integer getOptional() {
		return optional;
	}

	public void setOptional(Integer optional) {
		this.optional = optional;
	}
}

