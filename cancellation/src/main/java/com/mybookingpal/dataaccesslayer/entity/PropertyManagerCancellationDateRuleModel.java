package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PropertyManagerCancellationDateRuleModel extends PropertyManagerCancellationRuleModel {

	public PropertyManagerCancellationDateRuleModel() {
		super();
	}

	public PropertyManagerCancellationDateRuleModel(Integer propertyManagerId, Integer cancellationDate, Integer cancellationRefund,
			Double cancellationTransactionFee) {
		super();
		setPropertyManagerId(propertyManagerId);
		setCancellationDate(cancellationDate);
		setCancellationRefund(cancellationRefund);
		setCancellationTransactionFee(cancellationTransactionFee);
	}

	public Integer getCancellationRuleType() {
		return 1;
	}
}
