package com.mybookingpal.dataaccesslayer.entity;

import java.math.BigInteger;

public class CancellationArchiveInfoModel {

	private Integer id;
	private Integer channelCancellationRuleId;
	private Integer propertyManagerId;
	private Integer productId;
	private Integer cancellationRefund;
	private Double cancellationTransactionFee;
	private Integer cancellationRuleType;
	private Integer cancellationDate;
	private BigInteger ratePlanId;
	private Integer cancellationNights;
	private Integer reservationId;
	private Integer channelCancellationData;
	private String cancellationPolicyDescription;
	private String cancellationRuleName;
	private String channelAbbreviation;

	public CancellationArchiveInfoModel() {
	}

	public String getCancellationPolicyDescription() {
		return cancellationPolicyDescription;
	}

	public void setCancellationPolicyDescription(String cancellationPolicyDescription) {
		this.cancellationPolicyDescription = cancellationPolicyDescription;
	}

	public String getCancellationRuleName() {
		return cancellationRuleName;
	}

	public void setCancellationRuleName(String cancellationRuleName) {
		this.cancellationRuleName = cancellationRuleName;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChannelCancellationRuleId() {
		return channelCancellationRuleId;
	}

	public void setChannelCancellationRuleId(Integer channelCancellationRuleId) {
		this.channelCancellationRuleId = channelCancellationRuleId;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getCancellationRefund() {
		return cancellationRefund;
	}

	public void setCancellationRefund(Integer cancellationRefund) {
		this.cancellationRefund = cancellationRefund;
	}

	public Double getCancellationTransactionFee() {
		return cancellationTransactionFee;
	}

	public void setCancellationTransactionFee(Double cancellationTransactionFee) {
		this.cancellationTransactionFee = cancellationTransactionFee;
	}

	public Integer getCancellationRuleType() {
		return cancellationRuleType;
	}

	public void setCancellationRuleType(Integer cancellationRuleType) {
		this.cancellationRuleType = cancellationRuleType;
	}

	public Integer getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Integer cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public BigInteger getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(BigInteger ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Integer getCancellationNights() {
		return cancellationNights;
	}

	public void setCancellationNights(Integer cancellationNights) {
		this.cancellationNights = cancellationNights;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getChannelCancellationData() {
		return channelCancellationData;
	}

	public void setChannelCancellationData(Integer channelCancellationData) {
		this.channelCancellationData = channelCancellationData;
	}
}
