package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope("prototype")
public class ChannelCancellationRuleModel {

	private Integer id;
	private Integer channelId;
	private Integer propertyManagerId;
	private Integer cancellationRuleId;
	private Boolean active;
	private Date datetime;
	private Integer channelCancellationRuleId;
	private Integer productId;

	public ChannelCancellationRuleModel() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getCancellationRuleId() {
		return cancellationRuleId;
	}

	public void setCancellationRuleId(Integer cancellationRuleId) {
		this.cancellationRuleId = cancellationRuleId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public Integer getChannelCancellationRuleId() {
		return channelCancellationRuleId;
	}

	public void setChannelCancellationRuleId(Integer channelCancellationRuleId) {
		this.channelCancellationRuleId = channelCancellationRuleId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
}
