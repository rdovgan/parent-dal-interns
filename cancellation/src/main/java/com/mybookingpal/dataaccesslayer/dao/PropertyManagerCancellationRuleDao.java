package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.MarriottCancellationSelectDto;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationDateRuleModel;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationNightsRuleModel;
import com.mybookingpal.dataaccesslayer.entity.PropertyManagerCancellationRuleModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerCancellationRuleMapper;
import com.mybookingpal.utils.entity.NameIdAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PropertyManagerCancellationRuleDao {

	@Autowired
	PropertyManagerCancellationRuleMapper managerCancellationRuleMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PropertyManagerCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule)) {
			return;
		}
		managerCancellationRuleMapper.create(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerCancellationRuleModel read(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return managerCancellationRuleMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readbypmid(Integer propertyManagerId) {
		if (Objects.isNull(propertyManagerId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readbypmid(propertyManagerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readChannelRules(NameIdAction action) {
		if (Objects.isNull(action) || Objects.isNull(action.getId()) || Objects.isNull(action.getOrganizationId())) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readChannelRules(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationDateRuleModel> readdatebypmid(Integer propertyManagerId) {
		if (Objects.isNull(propertyManagerId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readdatebypmid(propertyManagerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PropertyManagerCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule) || Objects.isNull(cancellationRule.getId())) {
			return;
		}
		managerCancellationRuleMapper.update(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertOrUpdateList(List<PropertyManagerCancellationRuleModel> cancellationRules) {
		if (Objects.isNull(cancellationRules)) {
			return;
		}
		managerCancellationRuleMapper.insertOrUpdateList(cancellationRules);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (Objects.isNull(id)) {
			return;
		}
		managerCancellationRuleMapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deletebypmid(Integer propertyManagerId) {
		if (Objects.isNull(propertyManagerId)) {
			return;
		}
		managerCancellationRuleMapper.deletebypmid(propertyManagerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deletebypmidNotInList(Integer propertyManagerId, List<Integer> propertyManagerCancellationRules) {
		if (Objects.isNull(propertyManagerId) || Objects.isNull(propertyManagerCancellationRules)) {
			return;
		}
		managerCancellationRuleMapper.deletebypmidNotInList(propertyManagerId, propertyManagerCancellationRules);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readCancellationsByRatePlanId(Long ratePlanId) {
		if (Objects.isNull(ratePlanId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readCancellationsByRatePlanId(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAllByIds(List<Integer> listIds) {
		if (Objects.isNull(listIds)) {
			return;
		}
		managerCancellationRuleMapper.deleteAllByIds(listIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readSpecificCancellationsByProductId(Integer productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return null;
		}
		return managerCancellationRuleMapper.readSpecificCancellationsByProductId(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readSpecificCancellationsByProductIds(List<Integer> productId, List<Integer> channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readSpecificCancellationsByProductIds(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer readCancellationDaysForProductByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return null;
		}
		return managerCancellationRuleMapper.readCancellationDaysForProductByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readAllByProductId(Long productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readAllByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readAllByPropertyManager(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readAllByPropertyManager(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readAllByPropertyManagerWithChannelRuleConnection(Integer pmId) {
		if (pmId == null) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readAllByPropertyManagerWithChannelRuleConnection(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readAllByPropertyManagerWithChannelRuleConnectionWithoutProductIds(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readAllByPropertyManagerWithChannelRuleConnectionWithoutProductIds(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MarriottCancellationSelectDto> getProductsSpecificCancellationRules(List<Integer> productIds, Integer channelId) {
		if (Objects.isNull(productIds) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.getProductsSpecificCancellationRules(productIds, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MarriottCancellationSelectDto> getProductsChannelPartnerCancellationRules(List<Integer> pmIds, Integer channelId) {
		if (Objects.isNull(pmIds) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.getProductsChannelPartnerCancellationRules(pmIds, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MarriottCancellationSelectDto> getProductsCancellationRules(List<Integer> productIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.getProductsCancellationRules(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MarriottCancellationSelectDto> getPropertyManagersCancellationRules(List<Integer> pmIds) {
		if (Objects.isNull(pmIds)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.getPropertyManagersCancellationRules(pmIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationNightsRuleModel> readByChannelIdSupplierId(Integer supplierId, List<Integer> channelIds) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readByChannelIdSupplierId(supplierId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationNightsRuleModel> readByChannelIdProductId(Integer productId, List<Integer> channelIds) {
		if (Objects.isNull(productId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readByChannelIdProductId(productId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerCancellationRuleModel> readByPmId(String pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return managerCancellationRuleMapper.readByPmId(pmId);
	}
}
