package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.CancellationArchiveInfoModel;
import com.mybookingpal.dataaccesslayer.mappers.CancellationArchiveInfoMapper;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Repository
public class CancellationArchiveInfoDao {

	@Autowired
	private CancellationArchiveInfoMapper archiveInfoMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CancellationArchiveInfoModel getByReservationId(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return archiveInfoMapper.getByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void save(List<CancellationArchiveInfoModel> list) {
		if (Objects.isNull(list)) {
			return;
		}
		archiveInfoMapper.save(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getActiveReservationIdsWithoutArchivedCancellations(List<String> states, Integer limit) {
		return archiveInfoMapper.getActiveReservationIdsWithoutArchivedCancellations(states, limit);
	}
}
