package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelCancellationRuleModel;
import com.mybookingpal.dataaccesslayer.dto.ChannelPaymentPolicyMap;
import com.mybookingpal.dataaccesslayer.entity.ChannelSpecificCancellationRuleModel;
import com.mybookingpal.dataaccesslayer.mappers.CancellationRuleMapMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class CancellationRuleMapDao {

	@Autowired
	private CancellationRuleMapMapper cancellationRuleMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelSpecificCancellationRuleModel rule) {
		cancellationRuleMapper.create(rule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelSpecificCancellationRuleModel read(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return cancellationRuleMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> readAll() {
		return cancellationRuleMapper.readAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String readChannelAbbreviation(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return cancellationRuleMapper.readChannelAbbreviation(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelSpecificCancellationRuleModel rule) {
		if (rule == null || rule.getId() == null) {
			return;
		}
		cancellationRuleMapper.update(rule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> readByPmName(String name) {
		if (name == null) {
			return null;
		}
		return cancellationRuleMapper.readByPmName(name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> getAllActiveCancellationRules() {
		return cancellationRuleMapper.getAllActiveCancellationRules();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> getAllCancellationRules() {
		return cancellationRuleMapper.getAllCancellationRules();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> readChannelSpecificCancellationRulesByIds(List<Integer> cancellationIds) {
		return cancellationRuleMapper.readChannelSpecificCancellationRulesByIds(cancellationIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPaymentPolicyMap getPaymentPolicyByChannelAndPm(Integer cancellationRuleMapId) {
		if (Objects.isNull(cancellationRuleMapId)) {
			return null;
		}
		return cancellationRuleMapper.getPaymentPolicyByChannelAndPm(cancellationRuleMapId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule) || Objects.isNull(cancellationRule.getChannelId())) {
			return Collections.emptyList();
		}
		return cancellationRuleMapper.readCancellationsByPmIdOrProductIdAndChannelId(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPaymentPolicyMap> getPaymentPolicyByCancellationIds(List<Integer> cancellationRuleMapIds) {
		return cancellationRuleMapper.getPaymentPolicyByCancellationIds(cancellationRuleMapIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelSpecificCancellationRuleModel readByAbbreviationAndCancellationDate(String abbreviation, Integer cancellationDate) {
		if (Objects.isNull(abbreviation) || Objects.isNull(cancellationDate)) {
			return null;
		}
		return cancellationRuleMapper.readByAbbreviationAndCancellationDate(abbreviation, cancellationDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelSpecificCancellationRuleModel readByChannelCancellationMapId(String channelCancellationMapId) {
		if (Objects.isNull(channelCancellationMapId)) {
			return null;
		}
		return cancellationRuleMapper.readByChannelCancellationMapId(channelCancellationMapId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelSpecificCancellationRuleModel> readByChannelCancellationMapPerChannelAbbreviation(String channelAbbreviation) {
		if (Objects.isNull(channelAbbreviation)) {
			return Collections.emptyList();
		}
		return cancellationRuleMapper.readByChannelCancellationMapPerChannelAbbreviation(channelAbbreviation);
	}
}
