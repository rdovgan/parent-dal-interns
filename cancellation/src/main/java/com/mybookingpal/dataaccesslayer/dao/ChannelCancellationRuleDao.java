package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelCancellationRuleModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelCancellationRuleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelCancellationRuleDao {

	@Autowired
	private ChannelCancellationRuleMapper channelCancellationRuleMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule)) {
			return;
		}
		channelCancellationRuleMapper.create(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelCancellationRuleModel read(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return channelCancellationRuleMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule)) {
			return;
		}
		channelCancellationRuleMapper.update(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertOrUpdateList(List<ChannelCancellationRuleModel> cancellationRules) {
		if (Objects.isNull(cancellationRules)) {
			return;
		}
		channelCancellationRuleMapper.insertOrUpdateList(cancellationRules);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (Objects.isNull(id)) {
			return;
		}
		channelCancellationRuleMapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByChannelId(Integer channelId) {
		return channelCancellationRuleMapper.readByChannelId(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByPmId(Integer pmId) {
		return channelCancellationRuleMapper.readByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByPmIdWithPolicies(Integer pmId) {
		return channelCancellationRuleMapper.readByPmIdWithPolicies(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByChannelId(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return;
		}
		channelCancellationRuleMapper.deleteByChannelId(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readCancellationsByPmIdAndChannelPartner(Integer pmId, List<Integer> channelPartnerIds) {
		if (Objects.isNull(pmId) || Objects.isNull(channelPartnerIds)) {
			return Collections.emptyList();
		}
		return channelCancellationRuleMapper.readCancellationsByPmIdAndChannelPartner(pmId, channelPartnerIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAllByIds(List<Integer> listIds) {
		if (Objects.isNull(listIds)) {
			return;
		}
		channelCancellationRuleMapper.deleteAllByIds(listIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readCancellationsByProductIdAndChannelPartner(Integer productId, List<Integer> channelPartnerIds) {
		if (Objects.isNull(channelPartnerIds)) {
			return null;
		}
		return channelCancellationRuleMapper.readCancellationsByProductIdAndChannelPartner(productId, channelPartnerIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByProductId(Integer productId) {
		return channelCancellationRuleMapper.readByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByProductIds(List<Integer> productIds) {
		if (Objects.isNull(productIds)) {
			return null;
		}
		return channelCancellationRuleMapper.readByProductIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByPMIds(Integer pmId) {
		return channelCancellationRuleMapper.readByPMIds(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readByProductIdsChannelIds(List<Integer> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return null;
		}
		return channelCancellationRuleMapper.readByProductIdsChannelIds(productIds, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelCancellationRuleModel> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRuleModel cancellationRule) {
		if (Objects.isNull(cancellationRule)) {
			return null;
		}
		return channelCancellationRuleMapper.readCancellationsByPmIdOrProductIdAndChannelId(cancellationRule);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getPropertyIdsByPmIdAndChannelId(Integer pmId, List<Integer> channelIds) {
		if (Objects.isNull(channelIds)) {
			return null;
		}
		return channelCancellationRuleMapper.getPropertyIdsByPmIdAndChannelId(pmId, channelIds);
	}
}
