package com.mybookingpal.dataaccesslayer.constants;

public enum CancellationRuleTypeModel {

	Percentage(1), Flat(2);

	Integer value;

	CancellationRuleTypeModel(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}
