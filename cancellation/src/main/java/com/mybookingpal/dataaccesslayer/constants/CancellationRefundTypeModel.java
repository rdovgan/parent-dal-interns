package com.mybookingpal.dataaccesslayer.constants;

public enum CancellationRefundTypeModel {

	FullRefundable(100), NonRefundable(0);

	Integer value;

	CancellationRefundTypeModel(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}
