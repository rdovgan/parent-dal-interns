package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.RateModel;
import com.mybookingpal.dataaccesslayer.utils.HasResponse;

import java.util.Date;

public class RateNoteDto extends RateModel implements HasResponse {

	private Date date;
	private Double rating;
	private String notes;
	private int status;

	public RateNoteDto() {
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public int getStatus() {
		return status;
	}

	@Override
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RateNote [date=");
		builder.append(date);
		builder.append(", rating=");
		builder.append(rating);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", status=");
		builder.append(status);
		builder.append(", id=");
		builder.append(id);
		builder.append(", eventId=");
		builder.append(eventId);
		builder.append(", customerId=");
		builder.append(customerId);
		builder.append(", productId=");
		builder.append(productId);
		builder.append(", type=");
		builder.append(type);
		builder.append(", name=");
		builder.append(name);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append("]");
		return builder.toString();
	}

}
