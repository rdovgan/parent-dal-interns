package com.mybookingpal.dataaccesslayer.dto;

public class RateWidgetItemDto {
	private String id;
	private String eventId;
	private String customerId;
	private String productId;
	private String type;
	private String name;
	private Integer quantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEventId() {
		return eventId;
	}

	/**
	 * @deprecated use {@link #getEventId()} instead
	 */
	@Deprecated
	public String getEventid() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	/**
	 * @deprecated use {@link #setEventId(String)} instead
	 */
	@Deprecated
	public void setEventid(String eventId) {
		this.eventId = eventId;
	}

	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @deprecated use {@link #getCustomerId()} instead
	 */
	@Deprecated
	public String getCustomerid() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @deprecated use {@link #setCustomerId(String)} instead
	 */
	@Deprecated
	public void setCustomerid(String customerId) {
		this.customerId = customerId;
	}

	public String getProductId() {
		return productId;
	}

	/**
	 * @deprecated use {@link #getProductId()} instead
	 */
	@Deprecated
	public String getProductid() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @deprecated use {@link #setProductId(String)} instead
	 */
	@Deprecated
	public void setProductid(String productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RateWidgetItem [id=");
		builder.append(id);
		builder.append(", eventid=");
		builder.append(eventId);
		builder.append(", customerId=");
		builder.append(customerId);
		builder.append(", productId=");
		builder.append(productId);
		builder.append(", type=");
		builder.append(type);
		builder.append(", name=");
		builder.append(name);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append("]");
		return builder.toString();
	}

}
