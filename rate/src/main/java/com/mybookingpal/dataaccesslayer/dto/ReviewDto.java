package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.utils.HasXsl;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@XmlRootElement(name = "review")
// TODO: investigate @XStreamAlias("review")
public class ReviewDto implements HasXsl {
	private Date date;
	private String id;
	private String notes;
	private String productId;
	private String state;
	private Collection<RatingDto> ratings = new ArrayList<>();
	private String xsl; //NB HAS GET&SET = private

	public ReviewDto() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @deprecated use {@link #getProductId()} instead
	 */
	@Deprecated
	public String getProductid() {
		return productId;
	}

	/**
	 * @deprecated use {@link #setProductId(String)} instead
	 */
	@Deprecated
	public void setProductid(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Collection<RatingDto> getRatings() {
		return ratings;
	}

	public void setRatings(Collection<RatingDto> ratings) {
		this.ratings = ratings;
	}

	@Override
	public String getXsl() {
		return xsl;
	}

	@Override
	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Review [productId=");
		builder.append(productId);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", state=");
		builder.append(state);
		builder.append(", date=");
		builder.append(date);
		builder.append(", xsl=");
		builder.append(xsl);
		builder.append("]");
		return builder.toString();
	}
}