package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelRateMapModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ChannelRateMapMapper {

	/**
	 * @deprecated use {@link #readChannelRateMap(String priceId, String yieldId)} instead
	 */
	@Deprecated
	List<ChannelRateMapModel> readChannelRateMap(ChannelRateMapModel tempChannelRateMap);

	List<ChannelRateMapModel> readChannelRateMap(@Param("priceId") String priceId, @Param("yieldId") String yieldId);

	/**
	 * @deprecated use {@link #readChannelRateMapByYieldId(String yieldId)} instead
	 */
	@Deprecated
	List<ChannelRateMapModel> readChannelRateMapByYieldId(ChannelRateMapModel tempChannelRateMap);

	List<ChannelRateMapModel> readChannelRateMapByYieldId(@Param("yieldId") String yieldId);

	ChannelRateMapModel exists(ChannelRateMapModel channelRateMap);

	void create(ChannelRateMapModel action);

	void update(ChannelRateMapModel action);

}
