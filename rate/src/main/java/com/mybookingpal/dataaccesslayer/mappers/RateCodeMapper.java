package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.RateBookingCodeModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RateCodeMapper {
	void create(RateBookingCodeModel item);

	void update(RateBookingCodeModel item);

	List<RateBookingCodeModel> getRAByProductId(Integer productId);

	/**
	 * @deprecated use {@link #allByProductIdAndDate(Integer productId, Date fromDate, Date toDate)} instead
	 */
	@Deprecated
	List<RateBookingCodeModel> allByProductIdAndDate(RateBookingCodeModel item);

	List<RateBookingCodeModel> allByProductIdAndDate(@Param("productId") Integer productId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	void insertFromList(List<RateBookingCodeModel> list);
}
