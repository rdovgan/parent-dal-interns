package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.RateNoteDto;
import com.mybookingpal.dataaccesslayer.dto.RateWidgetItemDto;
import com.mybookingpal.dataaccesslayer.dto.RatingDto;
import com.mybookingpal.dataaccesslayer.dto.ReviewDto;
import com.mybookingpal.dataaccesslayer.dto.ReviewWidgetItemDto;
import com.mybookingpal.dataaccesslayer.entity.RateModel;
import com.mybookingpal.dataaccesslayer.utils.IsRateNoteTable;
import com.mybookingpal.utils.entity.Parameter;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RateMapper {

	void create(RateModel item);

	/**
	 * @deprecated use {@link #review(String productId, String orderBy, Integer rowToStart, Integer numberOfRows)} instead
	 */
	@Deprecated
	List<RateNoteDto> review(IsRateNoteTable action);

	List<RateNoteDto> review(@Param("productid") String productId, @Param("orderby") String orderBy, @Param("rowToStart") Integer rowToStart,
			@Param("numrows") Integer numberOfRows);

	//TODO: Fix naming
	List<RatingDto> restrating(String productId);

	//TODO: Fix naming
	List<ReviewDto> restreview(String productId);

	/**
	 * @deprecated use {@link #ratewidget(String reservationName, String type)} instead
	 */
	@Deprecated
	//TODO: Fix naming
	List<RateWidgetItemDto> ratewidget(Parameter action);

	//TODO: Fix naming
	List<RateWidgetItemDto> ratewidget(@Param("id") String reservationName, @Param("type") String type);

	//TODO: Fix naming
	List<RateWidgetItemDto> ratewidget(@Param("id") String reservationName);

	/**
	 * @deprecated use {@link #widgetreview(String productId, String orderBy, Integer rowToStart, Integer numberOfRows)} instead
	 */
	@Deprecated
	//TODO: Fix naming
	List<ReviewWidgetItemDto> widgetreview(IsRateNoteTable action);

	//TODO: Fix naming
	List<ReviewWidgetItemDto> widgetreview(@Param("productid") String productId, @Param("orderby") String orderBy, @Param("rowToStart") Integer rowToStart,
			@Param("numrows") Integer numberOfRows);

	void deleteByProductId(String productId);

	void copyRates(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);
}
