package com.mybookingpal.dataaccesslayer.utils;

public interface HasResponse {
	int getStatus();

	void setStatus(int status);
}
