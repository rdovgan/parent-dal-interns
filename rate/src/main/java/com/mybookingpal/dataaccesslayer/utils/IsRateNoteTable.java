package com.mybookingpal.dataaccesslayer.utils;

public interface IsRateNoteTable {
	String getProductid();

	String getOrderby();

	int getStartrow();

	int getNumrows();
}
