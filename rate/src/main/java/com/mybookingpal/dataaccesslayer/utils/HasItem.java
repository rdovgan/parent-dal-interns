package com.mybookingpal.dataaccesslayer.utils;

public interface HasItem {
	String getId();

	void setId(String id);

	boolean noId();
}
