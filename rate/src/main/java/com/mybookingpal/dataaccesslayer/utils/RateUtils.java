package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.RateModel;

public class RateUtils {

	public static boolean hasId(RateModel rate) {
		return !(rate.getId() == null || rate.getId().isEmpty() || rate.getId().equals("0" /*TODO: change to Model.ZERO*/));
	}

	public static boolean hasCustomerId(RateModel rate) {
		return !(rate.getCustomerId() == null || rate.getCustomerId().isEmpty() || rate.getCustomerId().equals("0" /*TODO: change to Model.ZERO*/));
	}

	public static boolean hasProductId(RateModel rate) {
		return !(rate.getProductId() == null || rate.getProductId().isEmpty() || rate.getProductId().equals("0" /*TODO: change to Model.ZERO*/));
	}

	public static boolean hasType(RateModel rate, String type) {
		return rate.getType() != null && rate.getType().equals(type);
	}

	public static boolean hasQuantity(RateModel rate) {
		return !(rate.getQuantity() == null || rate.getQuantity() < 0 || rate.getQuantity() >= 100);
	}

	public static boolean hasQuantity(RateModel rate, int targetQuantity) {
		return rate.getQuantity() == targetQuantity;
	}

	public static boolean noId(RateModel rate) {
		return !RateUtils.hasId(rate);
	}

	public static boolean noCustomerId(RateModel rate) {
		return !RateUtils.hasCustomerId(rate);
	}

	public static boolean noProductId(RateModel rate) {
		return !RateUtils.hasProductId(rate);
	}

	public static boolean noQuantity(RateModel rate) {
		return !RateUtils.hasQuantity(rate);
	}

}
