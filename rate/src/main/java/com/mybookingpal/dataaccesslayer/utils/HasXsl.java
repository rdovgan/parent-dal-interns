package com.mybookingpal.dataaccesslayer.utils;

public interface HasXsl {
	String getXsl();

	void setXsl(String xsl);
}
