package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.RateBookingCodeModel;
import com.mybookingpal.dataaccesslayer.mappers.RateCodeMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class RateCodeDao {

	private final RateCodeMapper mapper;

	public RateCodeDao(RateCodeMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(RateBookingCodeModel item) {
		if (Objects.isNull(item)) {
			return;
		}

		mapper.create(item);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(RateBookingCodeModel item) {
		if (Objects.isNull(item) || Objects.isNull(item.getId())) {
			return;
		}

		mapper.update(item);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateBookingCodeModel> getRAByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getRAByProductId(productId);
	}

	/**
	 * @deprecated use {@link #allByProductIdAndDate(Integer productId, Date fromDate, Date toDate)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateBookingCodeModel> allByProductIdAndDate(RateBookingCodeModel item) {
		if (Objects.isNull(item)) {
			return Collections.emptyList();
		}

		return allByProductIdAndDate(item.getProductId(), item.getFromDate(), item.getToDate());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateBookingCodeModel> allByProductIdAndDate(Integer productId, Date fromDate, Date toDate) {
		if (Objects.isNull(productId) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}

		return mapper.allByProductIdAndDate(productId, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertFromList(List<RateBookingCodeModel> list) {
		if (Objects.isNull(list)) {
			return;
		}

		List<RateBookingCodeModel> cleanList = list.stream().filter(Objects::nonNull).collect(Collectors.toList());
		if (cleanList.isEmpty()) {
			return;
		}

		mapper.insertFromList(cleanList);
	}
}
