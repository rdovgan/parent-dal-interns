package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.RateNoteDto;
import com.mybookingpal.dataaccesslayer.dto.RateWidgetItemDto;
import com.mybookingpal.dataaccesslayer.dto.RatingDto;
import com.mybookingpal.dataaccesslayer.dto.ReviewDto;
import com.mybookingpal.dataaccesslayer.dto.ReviewWidgetItemDto;
import com.mybookingpal.dataaccesslayer.entity.RateModel;
import com.mybookingpal.dataaccesslayer.mappers.RateMapper;
import com.mybookingpal.dataaccesslayer.utils.IsRateNoteTable;
import com.mybookingpal.utils.entity.Parameter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class RateDao {

	private final RateMapper mapper;

	public RateDao(RateMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(RateModel item) {
		if (Objects.isNull(item)) {
			return;
		}

		mapper.create(item);
	}

	/**
	 * @deprecated use {@link #review(String productId, String orderBy, Integer rowToStart, Integer numberOfRows)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateNoteDto> review(IsRateNoteTable action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.review(action.getProductid(), action.getOrderby(), action.getStartrow(), action.getNumrows());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateNoteDto> review(String productId, String orderBy, Integer rowToStart, Integer numberOfRows) {
		if (Objects.isNull(productId) || Objects.isNull(orderBy) || Objects.isNull(rowToStart) || Objects.isNull(numberOfRows)) {
			return Collections.emptyList();
		}

		return mapper.review(productId, orderBy, rowToStart, numberOfRows);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<RatingDto> restrating(String id) {
		if (Objects.isNull(id)) {
			return Collections.emptyList();
		}

		return mapper.restrating(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<ReviewDto> restreview(String id) {
		if (Objects.isNull(id)) {
			return Collections.emptyList();
		}

		return mapper.restreview(id);
	}

	/**
	 * @deprecated use {@link #rateWidget(String reservationName, String type)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<RateWidgetItemDto> ratewidget(Parameter action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.ratewidget(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateWidgetItemDto> rateWidget(String reservationName, String type) {
		if (Objects.isNull(reservationName)) {
			return Collections.emptyList();
		}

		return mapper.ratewidget(reservationName, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<RateWidgetItemDto> rateWidget(String reservationName) {
		if (Objects.isNull(reservationName)) {
			return Collections.emptyList();
		}

		return mapper.ratewidget(reservationName);
	}

	/**
	 * @deprecated use {@link #widgetReview(String productId, String orderBy, Integer rowToStart, Integer numberOfRows)} instead
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<ReviewWidgetItemDto> widgetreview(IsRateNoteTable action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.widgetreview(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<ReviewWidgetItemDto> widgetReview(String productId, String orderBy, Integer rowToStart, Integer numberOfRows) {
		if (Objects.isNull(productId) || Objects.isNull(orderBy) || Objects.isNull(rowToStart) || Objects.isNull(numberOfRows)) {
			return Collections.emptyList();
		}

		return mapper.widgetreview(productId, orderBy, rowToStart, numberOfRows);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return;
		}

		mapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyRates(Integer fromProductId, Integer toProductId) {
		if (Objects.isNull(fromProductId) || Objects.isNull(toProductId)) {
			return;
		}

		mapper.copyRates(fromProductId, toProductId);
	}
}
