package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelRateMapModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelRateMapMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelRateMapDao {

	private final ChannelRateMapMapper mapper;

	public ChannelRateMapDao(ChannelRateMapMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * @deprecated use {@link #readChannelRateMap(String priceId, String yieldId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRateMapModel> readChannelRateMap(ChannelRateMapModel tempChannelRateMap) {
		if (Objects.isNull(tempChannelRateMap)) {
			return Collections.emptyList();
		}

		return readChannelRateMap(tempChannelRateMap.getPriceId(), tempChannelRateMap.getYieldId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRateMapModel> readChannelRateMap(String priceId, String yieldId) {
		return mapper.readChannelRateMap(priceId, yieldId);
	}

	/**
	 * @deprecated use {@link #readChannelRateMapByYieldId(String yieldId)} instead
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRateMapModel> readChannelRateMapByYieldId(ChannelRateMapModel tempChannelRateMap) {
		if (Objects.isNull(tempChannelRateMap)) {
			return Collections.emptyList();
		}

		return readChannelRateMapByYieldId(tempChannelRateMap.getYieldId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelRateMapModel> readChannelRateMapByYieldId(String yieldId) {
		return mapper.readChannelRateMapByYieldId(yieldId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelRateMapModel exists(ChannelRateMapModel channelRateMap) {
		if (Objects.isNull(channelRateMap)) {
			return null;
		}

		return mapper.exists(channelRateMap);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelRateMapModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelRateMapModel action) {
		if (Objects.isNull(action) || Objects.isNull(action.getId())) {
			return;
		}

		mapper.update(action);
	}

}
