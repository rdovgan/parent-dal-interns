package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.utils.HasItem;
import com.mybookingpal.dataaccesslayer.utils.RateUtils;

public class RateModel implements HasItem {

	protected String id;
	protected String eventId;
	protected String customerId;
	protected String productId;
	protected String type;
	protected String name;
	protected Integer quantity;

	public RateModel() {
	}

	public RateModel(String name, String type, Integer quantity) {
		super();
		this.name = name;
		this.type = type;
		this.quantity = quantity;
	}

	public RateModel(String name, String type, boolean value) {
		super();
		this.name = name;
		this.type = type;
		this.quantity = value ? 1 : 0;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean noId() {
		return !RateUtils.hasId(this);
	}

	/**
	 * @deprecated use {@link #getEventId()} instead
	 */
	@Deprecated
	public String getEventid() {
		return eventId;
	}

	/**
	 * @deprecated use {@link #setEventId(String)} instead
	 */
	@Deprecated
	public void setEventid(String eventid) {
		this.eventId = eventid;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	/**
	 * @deprecated use {@link #getCustomerId()} instead
	 */
	@Deprecated
	public String getCustomerid() {
		return customerId;
	}

	/**
	 * @deprecated use {@link #setCustomerId(String)} instead
	 */
	@Deprecated
	public void setCustomerid(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @deprecated use {@link #getProductId()} instead
	 */
	@Deprecated
	public String getProductid() {
		return productId;
	}

	/**
	 * @deprecated use {@link #setProductId(String)} instead
	 */
	@Deprecated
	public void setProductid(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rate [id=");
		builder.append(id);
		builder.append(", eventId=");
		builder.append(eventId);
		builder.append(", type=");
		builder.append(type);
		builder.append(", name=");
		builder.append(name);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append("]");
		return builder.toString();
	}
}
