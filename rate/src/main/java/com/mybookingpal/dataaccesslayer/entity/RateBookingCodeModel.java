package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class RateBookingCodeModel {
	private Integer id;
	private Integer productId;
	private Date fromDate;
	private Date toDate;
	private String value;
	private Date createdDate;
	private Date version;

	public RateBookingCodeModel(Integer productId, Date fromDate, Date toDate, String value) {
		this.productId = productId;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.value = value;
		this.createdDate = new Date();
		this.version = new Date();
	}

	public RateBookingCodeModel() {
	}

	public RateBookingCodeModel(Integer id, Integer productId, Date fromDate, Date toDate, String value, Date createdDate, Date version) {
		this.id = id;
		this.productId = productId;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.value = value;
		this.createdDate = createdDate;
		this.version = version;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @deprecated use {@link #setProductId(Integer)} instead
	 */
	@Deprecated
	public void setProductid(Integer productId) {
		this.productId = productId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}
