package com.mybookingpal.dataaccesslayer.entity.Enums;

public enum Type {

	PrimaryPointOfContact("Primary point of contact"),
	ReservationsContact("Reservations contact"),
	CentralReservationsContact("Central reservations contact"),
	InvoicesContact("Invoices contact"),
	SpecialRequestsContact("Special requests contact"),
	AvailabilityContact("Availability contact"),
	PhotosAndDescrriptionsContact("Photos & Descriptions contact"),
	PricingContact("Pricing contact"),
	ContractContact("Contract contact"),
	ReservationManager("Reservation manager"),
	PropertyExtranetUser("Property extranet user");

	private String type;

	Type(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public static Type getByValue(String str) {
		for (Type type : values()) {
			if (type.getType().equals(str)) {
				return type;
			}
		}
		return null;
	}
}
