package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "support_cc")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "supportAE", "supportDINERSCLUB", "supportDISCOVER", "supportJCB", "supportMC", "supportVISA",
		"none" })
@XmlTransient
@Component
@Scope(value = "prototype")
public class PropertyManagerSupportCCModel {

	private Integer id;
	private Integer partyId;
	private Boolean supportMC;
	private Boolean supportVISA;
	private Boolean supportAE;
	private Boolean supportDISCOVER;
	private Boolean supportDINERSCLUB;
	private Boolean supportJCB;
	private Boolean none;

	@XmlTransient
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlTransient
	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public Boolean getSupportMC() {
		return supportMC;
	}

	public void setSupportMC(Boolean supportMC) {
		this.supportMC = supportMC;
	}

	public Boolean getSupportVISA() {
		return supportVISA;
	}

	public void setSupportVISA(Boolean supportVISA) {
		this.supportVISA = supportVISA;
	}

	public Boolean getSupportAE() {
		return supportAE;
	}

	public void setSupportAE(Boolean supportAE) {
		this.supportAE = supportAE;
	}

	public Boolean getSupportDISCOVER() {
		return supportDISCOVER;
	}

	public void setSupportDISCOVER(Boolean supportDISCOVER) {
		this.supportDISCOVER = supportDISCOVER;
	}

	public Boolean getSupportDINERSCLUB() {
		return supportDINERSCLUB;
	}

	public void setSupportDINERSCLUB(Boolean supportDINERSCLUB) {
		this.supportDINERSCLUB = supportDINERSCLUB;
	}

	public Boolean getSupportJCB() {
		return supportJCB;
	}

	public void setSupportJCB(Boolean supportJCB) {
		this.supportJCB = supportJCB;
	}

	public Boolean getNone() {
		return none;
	}

	public void setNone(Boolean none) {
		this.none = none;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PropertyManagerSupportCC [id=");
		builder.append(id);
		builder.append(", \npartyId=");
		builder.append(partyId);
		builder.append(", \nsupportMC=");
		builder.append(supportMC);
		builder.append(", \n=supportVISA=");
		builder.append(supportVISA);
		builder.append(", \nsupportAE=");
		builder.append(supportAE);
		builder.append(", \nsupportDISCOVER=");
		builder.append(supportDISCOVER);
		builder.append(", \n=supportDINERSCLUB");
		builder.append(supportDINERSCLUB);
		builder.append(", \nsupportJCB=");
		builder.append(supportJCB);
		builder.append(", \nnone=");
		builder.append(none);
		builder.append("]");
		return builder.toString();
	}
}
