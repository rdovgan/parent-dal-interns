package com.mybookingpal.dataaccesslayer.entity;

public class PropertyManagerConfigurationModel {
	private Integer id;
	private	Integer validation;
	private	Integer initialization;
	private	Integer price;
	private	Integer availability;
	private	Integer payment;
	private	Integer partnerReservation;
	private	Integer verifyAndCancel;
	private	Integer paymentTransaction;
	private	Integer format;
	private	Integer sendAdminEmail;
	private	Integer reservationWorkflow;

	public PropertyManagerConfigurationModel() {
	}

	public PropertyManagerConfigurationModel(Integer id, Integer validation, Integer initialization, Integer price,
			Integer availability, Integer payment, Integer partnerReservation, Integer verifyAndCancel,
			Integer paymentTransaction, Integer format, Integer sendAdminEmail, Integer reservationWorkflow) {
		this.id = id;
		this.validation = validation;
		this.initialization = initialization;
		this.price = price;
		this.availability = availability;
		this.payment = payment;
		this.partnerReservation = partnerReservation;
		this.verifyAndCancel = verifyAndCancel;
		this.paymentTransaction = paymentTransaction;
		this.format = format;
		this.sendAdminEmail = sendAdminEmail;
		this.reservationWorkflow = reservationWorkflow;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getValidation() {
		return validation;
	}

	public void setValidation(Integer validation) {
		this.validation = validation;
	}

	public Integer getInitialization() {
		return initialization;
	}

	public void setInitialization(Integer initialization) {
		this.initialization = initialization;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getAvailability() {
		return availability;
	}

	public void setAvailability(Integer availability) {
		this.availability = availability;
	}

	public Integer getPayment() {
		return payment;
	}

	public void setPayment(Integer payment) {
		this.payment = payment;
	}

	public Integer getPartnerReservation() {
		return partnerReservation;
	}

	public void setPartnerReservation(Integer partnerReservation) {
		this.partnerReservation = partnerReservation;
	}

	public Integer getVerifyAndCancel() {
		return verifyAndCancel;
	}

	public void setVerifyAndCancel(Integer verifyAndCancel) {
		this.verifyAndCancel = verifyAndCancel;
	}

	public Integer getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(Integer paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	public Integer getFormat() {
		return format;
	}

	public void setFormat(Integer format) {
		this.format = format;
	}

	public Integer getSendAdminEmail() {
		return sendAdminEmail;
	}

	public void setSendAdminEmail(Integer sendAdminEmail) {
		this.sendAdminEmail = sendAdminEmail;
	}

	public Integer getReservationWorkflow() {
		return reservationWorkflow;
	}

	public void setReservationWorkflow(Integer reservationWorkflow) {
		this.reservationWorkflow = reservationWorkflow;
	}

	@Override
	public String toString() {
		return "PropertyManagerConfigurationModel{" + "id=" + id + ", validation=" + validation + ", initialization="
				+ initialization + ", price=" + price + ", availability=" + availability + ", payment=" + payment
				+ ", partnerReservation=" + partnerReservation + ", verifyAndCancel=" + verifyAndCancel
				+ ", paymentTransaction=" + paymentTransaction + ", format=" + format + ", sendAdminEmail="
				+ sendAdminEmail + ", reservationWorkflow=" + reservationWorkflow + '}';
	}
}
