package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.entity.Enums.Type;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
public class PropertyManagerAdditionalContactsModel {
	private Integer id;
	private Integer pmId; //AltId
	private Integer productId;
	private String emailAddress;
	private String Name;
	private String lastName;
	private String phone;
	private String fax;
	private String language;
	private Date version;
	private Type type;

	public PropertyManagerAdditionalContactsModel() {

	}

	public PropertyManagerAdditionalContactsModel(int pmId, String emailAddress, String name, String phone) {
		this.pmId = pmId;
		this.emailAddress = emailAddress;
		Name = name;
		this.phone = phone;
	}

	public PropertyManagerAdditionalContactsModel(int pmId, String emailAddress, String name, String phone,
			String type) {
		this.pmId = pmId;
		this.emailAddress = emailAddress;
		Name = name;
		this.phone = phone;
		this.type = Type.valueOf(type);
	}

	public Integer getId() {
		return id;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		if (type != null) {
			return type.getType();
		}
		return null;
	}

	public void setType(String type) {
		this.type = Type.getByValue(type.trim());
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + productId;
		result = prime * result + pmId;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyManagerAdditionalContactsModel other = (PropertyManagerAdditionalContactsModel) obj;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (pmId != other.pmId)
			return false;
		if (productId != other.productId)
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
