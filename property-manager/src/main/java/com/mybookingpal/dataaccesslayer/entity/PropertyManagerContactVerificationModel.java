package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class PropertyManagerContactVerificationModel {
	private Integer id;
	private Integer partyId;
	private Integer additionalContactsId;
	private String phone;
	private Boolean phoneVerified;
	private Date createdDate;
	private Date version;

	public PropertyManagerContactVerificationModel() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public Integer getAdditionalContactsId() {
		return additionalContactsId;
	}

	public void setAdditionalContactsId(Integer additionalContactsId) {
		this.additionalContactsId = additionalContactsId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getPhoneVerified() {
		return phoneVerified;
	}

	public void setPhoneVerified(Boolean phoneVerified) {
		this.phoneVerified = phoneVerified;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

}
