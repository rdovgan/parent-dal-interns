package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerInvoiceDetailsModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerInvoiceDetailsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.mybookingpal.dataaccesslayer.utils.PropertyManagerInvoiceDetailsUtil;

import java.util.Objects;

@Repository
public class PropertyManagerInvoiceDetailsDao {

	@Autowired
	private PropertyManagerInvoiceDetailsMapper propertyManagerInvoiceDetailsMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerInvoiceDetailsModel getByPmId(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return null;
		}
		return propertyManagerInvoiceDetailsMapper.getByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PropertyManagerInvoiceDetailsModel model) {
		if (PropertyManagerInvoiceDetailsUtil.isValidModel(model)) {
			propertyManagerInvoiceDetailsMapper.create(model);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PropertyManagerInvoiceDetailsModel model) {
			propertyManagerInvoiceDetailsMapper.update(model);
	}
}
