package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerContactVerificationModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerContactVerificationMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PropertyManagerContactVerificationDao {

	@Autowired
	private PropertyManagerContactVerificationMapper propertyManagerContactVerificationMapper;

	public PropertyManagerContactVerificationDao(
			PropertyManagerContactVerificationMapper propertyManagerContactVerificationMapper) {
		this.propertyManagerContactVerificationMapper = propertyManagerContactVerificationMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	PropertyManagerContactVerificationModel isContactVerified(Integer partyId, String phone) {
		if (Objects.isNull(partyId) || StringUtils.isBlank(phone)) {
			return null;
		}
		return propertyManagerContactVerificationMapper.isContactVerified(partyId, phone);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<PropertyManagerContactVerificationModel> getVerifiedContacts(Integer partyId) {
		if (Objects.isNull(partyId)) {
			return Collections.emptyList();
		}
		return propertyManagerContactVerificationMapper.getVerifiedContacts(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<PropertyManagerContactVerificationModel> read(Integer partyId, Integer additionalContactsId) {
		if (Objects.isNull(partyId)) {
			return Collections.emptyList();
		}
		return propertyManagerContactVerificationMapper.read(partyId, additionalContactsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void insert(Integer partyId, Integer additionalContactsId, String phone, Boolean phoneVerified) {
		if (Objects.isNull(partyId)) {
			return;
		}
		propertyManagerContactVerificationMapper.insert(partyId, additionalContactsId, phone, phoneVerified);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void updateVerificationStatus(Integer id, String phone, Boolean phoneVerified) {
		propertyManagerContactVerificationMapper.updateVerificationStatus(id, phone, phoneVerified);
	}
}
