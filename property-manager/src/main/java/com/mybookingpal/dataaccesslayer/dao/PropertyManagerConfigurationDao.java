package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerConfigurationModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerConfigurationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Repository
public class PropertyManagerConfigurationDao {

	@Autowired
	private PropertyManagerConfigurationMapper propertyManagerConfigurationMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PropertyManagerConfigurationModel model) {
		if (Objects.isNull(model)) {
			return;
		}
		propertyManagerConfigurationMapper.create(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerConfigurationModel read(Integer id) {
		return propertyManagerConfigurationMapper.read(id);
	}
}
