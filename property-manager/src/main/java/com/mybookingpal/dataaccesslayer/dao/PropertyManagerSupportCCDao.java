package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerSupportCCModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerSupportCCMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PropertyManagerSupportCCDao {

	private final PropertyManagerSupportCCMapper propertyManagerSupportCCMapper;

	@Autowired
	public PropertyManagerSupportCCDao(PropertyManagerSupportCCMapper propertyManagerSupportCCMapper) {
		this.propertyManagerSupportCCMapper = propertyManagerSupportCCMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PropertyManagerSupportCCModel model) {
		if (Objects.isNull(model) || Objects.isNull(model.getPartyId())) {
			return;
		}
		propertyManagerSupportCCMapper.create(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerSupportCCModel readByPartyId(Integer partyId) {
		if (Objects.isNull(partyId)) {
			return null;
		}
		return propertyManagerSupportCCMapper.readByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerSupportCCModel> readByPartyIds(List<Integer> partyIds) {
		if (CollectionUtils.isEmpty(partyIds)) {
			return Collections.emptyList();
		}
		return propertyManagerSupportCCMapper.readByPartyIds(partyIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PropertyManagerSupportCCModel item) {
		if (Objects.isNull(item) || Objects.isNull(item.getPartyId())) {
			return;
		}
		propertyManagerSupportCCMapper.update(item);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByPartyId(Integer partyId) {
		if (Objects.isNull(partyId)) {
			return;
		}
		propertyManagerSupportCCMapper.deleteByPartyId(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerSupportCCModel readByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return null;
		}
		return propertyManagerSupportCCMapper.readByProductId(productId);
	}
}
