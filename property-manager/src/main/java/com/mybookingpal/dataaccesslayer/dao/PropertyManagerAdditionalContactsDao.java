package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerAdditionalContactsModel;
import com.mybookingpal.dataaccesslayer.mappers.PropertyManagerAdditionalContactsMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PropertyManagerAdditionalContactsDao {

	@Autowired
	private PropertyManagerAdditionalContactsMapper propertyManagerAdditionalContactsMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByIdAndType(PropertyManagerAdditionalContactsModel model) {
		if (Objects.isNull(model)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByIdAndType(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByPropertyId(PropertyManagerAdditionalContactsModel model) {
		if (Objects.isNull(model)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByPropertyId(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByPropertyIdAndType(
			PropertyManagerAdditionalContactsModel model) {
		if (Objects.isNull(model)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByPropertyIdAndType(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByPmIdOrPropertyId(
			PropertyManagerAdditionalContactsModel model) {
		if (Objects.isNull(model)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByPmIdOrPropertyId(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByPmIdOrPropertyIds(Integer pmId,
			List<Integer> propertyIds) {
		if (pmId == null || CollectionUtils.isEmpty(propertyIds)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByPmIdOrPropertyIds(pmId, propertyIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByProductIdAndType(String productId, String type) {
		if (StringUtils.isBlank(productId) || StringUtils.isBlank(type)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByProductIdAndType(productId, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PropertyManagerAdditionalContactsModel getByIdAndPmId(Long id, Long pmId) {
		if (Objects.isNull(id) || Objects.isNull(pmId)) {
			return null;
		}
		return propertyManagerAdditionalContactsMapper.getByIdAndPmId(id, pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByProductId(String productId) {
		if (StringUtils.isBlank(productId)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PropertyManagerAdditionalContactsModel> getByPmId(String pmId) {
		if (StringUtils.isBlank(pmId)) {
			return Collections.emptyList();
		}
		return propertyManagerAdditionalContactsMapper.getByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<PropertyManagerAdditionalContactsModel> additionalContactsList) {
		if (CollectionUtils.isEmpty(additionalContactsList)) {
			return;
		}
		propertyManagerAdditionalContactsMapper.insertList(additionalContactsList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteList(List<PropertyManagerAdditionalContactsModel> additionalContactsList) {
		if (CollectionUtils.isEmpty(additionalContactsList)) {
			return;
		}
		propertyManagerAdditionalContactsMapper.deleteList(additionalContactsList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteListByPmId(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return;
		}
		propertyManagerAdditionalContactsMapper.deleteListByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PropertyManagerAdditionalContactsModel additionalContacs) {
		if (Objects.isNull(additionalContacs)) {
			return;
		}
		propertyManagerAdditionalContactsMapper.update(additionalContacs);
	}
}
