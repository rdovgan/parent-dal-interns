package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerContactVerificationModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PropertyManagerContactVerificationMapper {

	PropertyManagerContactVerificationModel isContactVerified(@Param("partyId") Integer partyId,
			@Param("phone") String phone);

	List<PropertyManagerContactVerificationModel> getVerifiedContacts(Integer partyId);

	List<PropertyManagerContactVerificationModel> read(@Param("partyId") Integer partyId, @Param("additionalContactsId") Integer additionalContactsId);

	void insert(@Param("partyId") Integer partyId, @Param("additionalContactsId") Integer additionalContactsId, @Param("phone") String phone,
			@Param("phoneVerified") Boolean phoneVerified);

	void updateVerificationStatus(@Param("id") Integer id, @Param("phone") String phone, @Param("phoneVerified") Boolean phoneVerified);
}
