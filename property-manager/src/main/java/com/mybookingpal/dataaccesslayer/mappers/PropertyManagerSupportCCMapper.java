package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerSupportCCModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PropertyManagerSupportCCMapper {

	void create(PropertyManagerSupportCCModel item);

	PropertyManagerSupportCCModel readByPartyId(Integer partyId);

	List<PropertyManagerSupportCCModel> readByPartyIds(List<Integer> partyIds);

	void update(PropertyManagerSupportCCModel item);

	void deleteByPartyId(Integer partyId);

	PropertyManagerSupportCCModel readByProductId(@Param("productId") Integer productId);
}
