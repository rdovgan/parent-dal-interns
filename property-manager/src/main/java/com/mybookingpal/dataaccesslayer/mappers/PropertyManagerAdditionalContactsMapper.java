package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerAdditionalContactsModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PropertyManagerAdditionalContactsMapper {

	List<PropertyManagerAdditionalContactsModel> getByIdAndType(
			PropertyManagerAdditionalContactsModel propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContactsModel> getByPropertyId(
			PropertyManagerAdditionalContactsModel propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContactsModel> getByPropertyIdAndType(
			PropertyManagerAdditionalContactsModel propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContactsModel> getByPmIdOrPropertyId(
			PropertyManagerAdditionalContactsModel propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContactsModel> getByPmIdOrPropertyIds(@Param("pmId") Integer pmId,
			@Param("productIds") List<Integer> productIds);

	List<PropertyManagerAdditionalContactsModel> getByProductIdAndType(@Param("productId") String productId,
			@Param("type") String type);

	PropertyManagerAdditionalContactsModel getByIdAndPmId(@Param("id") Long id, @Param("pmId") Long pmId);

	List<PropertyManagerAdditionalContactsModel> getByProductId(String productId);

	List<PropertyManagerAdditionalContactsModel> getByPmId(@Param("pmId") String pmId);

	void insertList(@Param("list") List<PropertyManagerAdditionalContactsModel> additionalContactsList);

	void deleteList(@Param("list") List<PropertyManagerAdditionalContactsModel> additionalContactsList);

	void deleteListByPmId(Integer pmId);

	void update(PropertyManagerAdditionalContactsModel additionalContacs);
}
