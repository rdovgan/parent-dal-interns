package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerInvoiceDetailsModel;

public interface PropertyManagerInvoiceDetailsMapper {

	PropertyManagerInvoiceDetailsModel getByPmId(Integer pmId);

	void create(PropertyManagerInvoiceDetailsModel propertyManagerInvoiceDetails);

	void update(PropertyManagerInvoiceDetailsModel propertyManagerInvoiceDetails);
}
