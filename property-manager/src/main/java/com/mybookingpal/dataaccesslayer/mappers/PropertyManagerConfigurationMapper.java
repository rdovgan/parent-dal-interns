package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerConfigurationModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PropertyManagerConfigurationMapper {

	void create(PropertyManagerConfigurationModel propertyManagerConfigurationModel);

	PropertyManagerConfigurationModel read(Integer id);
}
