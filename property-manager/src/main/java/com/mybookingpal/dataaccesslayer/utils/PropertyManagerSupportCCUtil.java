package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerSupportCCModel;
import org.springframework.stereotype.Component;

@Component
public class PropertyManagerSupportCCUtil {

	public Boolean isSupportNone(PropertyManagerSupportCCModel model) {
		if (model.getSupportMC() == null && model.getSupportVISA() == null && model.getSupportAE() == null
				&& model.getSupportDISCOVER() == null && model.getSupportDINERSCLUB() == null
				&& model.getSupportJCB() == null) {
			return true;
		}
		return false;
	}

	public Boolean hasSupportOfMC(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportMC());
	}

	public Boolean hasSupportOfAE(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportAE());
	}

	public Boolean hasSupportOfDinnersClub(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportDINERSCLUB());
	}

	public Boolean hasSupportOfDiscover(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportDISCOVER());
	}

	public Boolean hasSupportOfJCB(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportJCB());
	}

	public Boolean hasSupportOfVISA(PropertyManagerSupportCCModel model) {
		return defineNotNullBoolean(model.getSupportVISA());
	}

	private Boolean defineNotNullBoolean(Boolean value) {
		if (value == null) {
			return Boolean.valueOf(false);
		} else {
			return value;
		}
	}
}
