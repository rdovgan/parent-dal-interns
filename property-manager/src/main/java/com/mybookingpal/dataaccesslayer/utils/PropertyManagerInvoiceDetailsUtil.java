package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.PropertyManagerInvoiceDetailsModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class PropertyManagerInvoiceDetailsUtil {

	public static boolean isValidModel(PropertyManagerInvoiceDetailsModel model) {
		return (!Objects.isNull(model.getPmId()) && !StringUtils.isBlank(model.getAddress()) && !StringUtils
				.isBlank(model.getCountry()) && !StringUtils.isBlank(model.getCompanyName()) && !StringUtils
				.isBlank(model.getCity()) && !StringUtils.isBlank(model.getZip()));
	}
}
