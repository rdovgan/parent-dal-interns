package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.IsManagerToChannelDto;
import com.mybookingpal.dataaccesslayer.dto.IsProductAction;
import com.mybookingpal.dataaccesslayer.entity.ManagerToChannelModel;
import com.mybookingpal.dataaccesslayer.mappers.ManagerToChannelMapper;
import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ManagerToChannelDao {
	private final ManagerToChannelMapper managerToChannelMapper;

	@Autowired
	public ManagerToChannelDao(ManagerToChannelMapper managerToChannelMapper) {
		this.managerToChannelMapper = managerToChannelMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> listPropertyManagersDistributedChannel(String channelPartnerId) {
		if (StringUtils.isBlank(channelPartnerId)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listPropertyManagersDistributedChannel(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> getListByChannelIdAndPmList(List<String> pmIds, String channelId) {
		if (StringUtils.isBlank(channelId)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.getListByChannelIdAndPmList(pmIds, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> justPropertyManagersDistributedChannel(String channelPartnerId) {
		if (StringUtils.isBlank(channelPartnerId)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.justPropertyManagersDistributedChannel(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> justPMCreatedOrNotChangedInAWhile(IsProductAction channelId) {
		if (channelId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.justPMCreatedOrNotChangedInAWhile(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> listPMNotDistributedToChannelString(List<String> supplierIds) {
		if (CollectionUtils.isEmpty(supplierIds)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listPMNotDistributedToChannelString(supplierIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToChannelModel getByPmIDAndChannelID(NameId nameId) {
		if (nameId == null) {
			return null;
		}
		return managerToChannelMapper.getByPmIDAndChannelID(nameId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToChannelModel getByPmIDAndChannel(NameId nameId) {
		if (nameId == null) {
			return null;
		}
		return managerToChannelMapper.getByPmIDAndChannel(nameId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> readAll() {
		return managerToChannelMapper.readAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> listDistributedPMIdsByAbbreviation(String abbreviation) {
		if (StringUtils.isEmpty(abbreviation)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listDistributedPMIdsByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> listOfChannelAbbreviationsRelatedToPm(Integer pmId) {
		if (pmId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listOfChannelAbbreviationsRelatedToPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> listOfChannelIdsRelatedToPm(Integer pmId) {
		if (pmId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listOfChannelIdsRelatedToPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> listOfExpediaChannelIdsRelatedToPm(Integer pmId) {
		if (pmId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listOfExpediaChannelIdsRelatedToPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(Integer commission, Integer pmId, Integer cpId) {
		if (ObjectUtils.allNotNull(commission, pmId, cpId)) {
			managerToChannelMapper.update(commission, pmId, cpId);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToChannelModel readByPmIdAndChannelId(Integer pmId, Integer channelId) {
		if (pmId == null || channelId == null) {
			return null;
		}
		return managerToChannelMapper.readByPmIdAndChannelId(pmId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createConnection(ManagerToChannelModel managerToChannel) {
		if (managerToChannel == null) {
			return;
		}
		managerToChannelMapper.createConnection(managerToChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteConnection(ManagerToChannelModel managerToChannel) {
		if (managerToChannel == null) {
			return;
		}
		managerToChannelMapper.deleteConnection(managerToChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> readPropertyManagerToChannelsConnection(Integer pmId, List<Integer> channelIds) {
		if (pmId == null || CollectionUtils.isEmpty(channelIds)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.readPropertyManagerToChannelsConnection(pmId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void fullUpdate(ManagerToChannelModel managerToChannel) {
		if (managerToChannel == null) {
			return;
		}
		managerToChannelMapper.fullUpdate(managerToChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateFundsHolder(ManagerToChannelModel managerToChannel) {
		if (managerToChannel == null) {
			return;
		}
		managerToChannelMapper.updateFundsHolder(managerToChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ManagerToChannelModel managerToChannel) {
		if (managerToChannel == null) {
			return;
		}
		managerToChannelMapper.create(managerToChannel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToChannelModel selectByPMIdAndChannelAbbreviation(String pmId, String abbreviation) {
		if (pmId == null || StringUtils.isEmpty(abbreviation)) {
			return null;
		}
		return managerToChannelMapper.selectByPMIdAndChannelAbbreviation(pmId, abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateFeeNetRateByPropertyManagerAndChannelPartner(Integer propertyManagerId, Integer channelPartnerId, Boolean isNetRate) {
		if (ObjectUtils.allNotNull(propertyManagerId, channelPartnerId, isNetRate)) {
			managerToChannelMapper.updateFeeNetRateByPropertyManagerAndChannelPartner(propertyManagerId, channelPartnerId, isNetRate);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsManagerToChannelDto> selectFeeCommissionByPmId(@Param("pmId") Integer pmId) {
		if (pmId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.selectFeeCommissionByPmId(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> getListByChannelIdsAndPmId(Integer pmId, List<Integer> channelIds) {
		if (pmId == null || CollectionUtils.isEmpty(channelIds)) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.getListByChannelIdsAndPmId(pmId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> listAllChannelPropertyManagers() {
		return managerToChannelMapper.listAllChannelPropertyManagers();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> listPropertyManagersNotDistributedChannel(String channelPartnerId) {
		if (channelPartnerId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.listPropertyManagersNotDistributedChannel(channelPartnerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> listFeed() {
		return managerToChannelMapper.listFeed();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getChannelPartnerIdsByPmId(String id) {
		if (id == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.getChannelPartnerIdsByPmId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelModel> findSupplierForExpedia(Integer supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return managerToChannelMapper.findSupplierForExpedia(supplierId);
	}
}