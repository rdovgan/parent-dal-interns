package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.IsProductAction;
import com.mybookingpal.dataaccesslayer.entity.ManagerToChannelAccountModel;
import com.mybookingpal.dataaccesslayer.mappers.ManagerToChannelAccountMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

// TODO : ADD Logger from org.apache.log4j.Logger;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ManagerToChannelAccountDao {

	private final ManagerToChannelAccountMapper managerToChannelAccountMapper;

	@Autowired
	public ManagerToChannelAccountDao(ManagerToChannelAccountMapper managerToChannelAccountMapper) {
		this.managerToChannelAccountMapper = managerToChannelAccountMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> readByPropertyManagerAndChannelPartner(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.readByPropertyManagerAndChannelPartner(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> readByPropertyManagerAndChannelPartnerWithoutState(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.readByPropertyManagerAndChannelPartnerWithoutState(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> findByPropertyManagerAndChannelPartnerOrderByVersion(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.findByPropertyManagerAndChannelPartnerOrderByVersion(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> findActiveAccountsByChannelPartner(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.findActiveAccountsByChannelPartner(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> findActiveAccountsWithMltRepProperties(List<Integer> listOfAllSupplierIds, Integer channelId) {
		if (CollectionUtils.isEmpty(listOfAllSupplierIds) || channelId == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.findActiveAccountsWithMltRepProperties(listOfAllSupplierIds, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return;
		}
		managerToChannelAccountMapper.update(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateState(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return;
		}
		managerToChannelAccountMapper.updateState(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return;
		}
		managerToChannelAccountMapper.create(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createOrUpdate(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return;
		}
		managerToChannelAccountMapper.createOrUpdate(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToChannelAccountModel findByPropertyManagerAndChannel(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return null;
		}
		return managerToChannelAccountMapper.findByPropertyManagerAndChannel(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> fetchPropertyManagersDistributedChannel(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.fetchPropertyManagersDistributedChannel(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllActiveAPMIdsForChannel(Integer channelID) {
		if (channelID == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.getAllActiveAPMIdsForChannel(channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> getlAllActiveVirtualAccountsForSupplier(Integer supplierId) {
		if (supplierId == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.getlAllActiveVirtualAccountsForSupplier(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getPaymentCredentialsBySupplierIdAndChannelId(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return null;
		}
		return managerToChannelAccountMapper.getPaymentCredentialsBySupplierIdAndChannelId(managerToChannelAccountModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> findByActivePropertyManager(Integer supplierId) {
		if (supplierId == null) {
			return null;
		}
		return managerToChannelAccountMapper.findByActivePropertyManager(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> getlAllHomeownerAccounts() {
		return managerToChannelAccountMapper.getlAllHomeownerAccounts();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> findSupplierForExpedia(String supplierId) {
		if (StringUtils.isBlank(supplierId)) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.findSupplierForExpedia(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToChannelAccountModel> justPMCreatedOrSuspendedNotChangedInAWhile(IsProductAction productAction) {
		if (productAction == null) {
			return Collections.emptyList();
		}
		return managerToChannelAccountMapper.justPMCreatedOrSuspendedNotChangedInAWhile(productAction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateManagerToChannelAccount(ManagerToChannelAccountModel managerToChannelAccountModel) {
		if (managerToChannelAccountModel == null) {
			return;
		}
		managerToChannelAccountMapper.updateManagerToChannelAccount(managerToChannelAccountModel);
	}
}