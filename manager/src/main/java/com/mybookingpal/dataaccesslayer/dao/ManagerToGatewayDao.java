package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ManagerToGatewayModel;
import com.mybookingpal.dataaccesslayer.mappers.ManagerToGatewayMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ManagerToGatewayDao {

	private final ManagerToGatewayMapper managerToGatewayMapper;

	@Autowired
	public ManagerToGatewayDao(ManagerToGatewayMapper managerToGatewayMapper) {
		this.managerToGatewayMapper = managerToGatewayMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ManagerToGatewayModel managerToGatewayModel) {
		if (managerToGatewayModel != null) {
			managerToGatewayMapper.create(managerToGatewayModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToGatewayModel read(Integer id) {
		if (id == null) {
			return null;
		}
		return managerToGatewayMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ManagerToGatewayModel readBySupplierId(Integer supplierId) {
		if (supplierId == null) {
			return null;
		}
		return managerToGatewayMapper.readBySupplierId(supplierId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ManagerToGatewayModel> readBySupplierIds(List<Integer> supplierIds) {
		if (CollectionUtils.isEmpty(supplierIds)) {
			return Collections.emptyList();
		}
		return managerToGatewayMapper.readBySupplierIds(supplierIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateNotNullValues(ManagerToGatewayModel managerToGatewayModel) {
		if (managerToGatewayModel == null) {
			return;
		}
		managerToGatewayMapper.updateNotNullValues(managerToGatewayModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ManagerToGatewayModel managerToGatewayModel) {
		if (managerToGatewayModel == null) {
			return;
		}
		managerToGatewayMapper.update(managerToGatewayModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (id == null) {
			return;
		}
		managerToGatewayMapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteBySupplierId(Integer supplierId) {
		if (supplierId == null) {
			return;
		}
		managerToGatewayMapper.deleteBySupplierId(supplierId);
	}
}