package com.mybookingpal.dataaccesslayer.dto;

public interface IsManagerToChannelDto {

	Integer getChannelPartnerId();

	String getPaymentModel();

	String getMainstayType();

	Boolean getFeeNetRate();

	Integer getFundsHolder();

	String getName();

	Integer getPartyId();

	String getCpAbbreviation();
}
