package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

public interface IsProductAction {

	Integer getChannelId();

	Date getVersion();
}
