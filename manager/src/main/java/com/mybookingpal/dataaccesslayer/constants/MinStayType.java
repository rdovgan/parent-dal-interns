package com.mybookingpal.dataaccesslayer.constants;

public enum MinStayType {
	arrival, through
}
