package com.mybookingpal.dataaccesslayer.constants;

public enum PartnerPaymentModel {
	Centralized, Direct
}
