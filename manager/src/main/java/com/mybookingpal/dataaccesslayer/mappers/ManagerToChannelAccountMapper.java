package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.IsProductAction;
import com.mybookingpal.dataaccesslayer.entity.ManagerToChannelAccountModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManagerToChannelAccountMapper {

	List<ManagerToChannelAccountModel> readByPropertyManagerAndChannelPartner(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<ManagerToChannelAccountModel> readByPropertyManagerAndChannelPartnerWithoutState(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<ManagerToChannelAccountModel> findByPropertyManagerAndChannelPartnerOrderByVersion(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<ManagerToChannelAccountModel> findActiveAccountsByChannelPartner(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<ManagerToChannelAccountModel> findActiveAccountsWithMltRepProperties(@Param("list") List<Integer> listOfAllSupplierIds,
			@Param("channelId") Integer channelId);

	void update(ManagerToChannelAccountModel managerToChannelAccountModel);

	void updateState(ManagerToChannelAccountModel managerToChannelAccountModel);

	void create(ManagerToChannelAccountModel managerToChannelAccountModel);

	void createOrUpdate(ManagerToChannelAccountModel managerToChannelAccountModel);

	ManagerToChannelAccountModel findByPropertyManagerAndChannel(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<String> fetchPropertyManagersDistributedChannel(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<String> getAllActiveAPMIdsForChannel(@Param("channelID") Integer channelID);

	List<ManagerToChannelAccountModel> getlAllActiveVirtualAccountsForSupplier(@Param("supplierId") Integer supplierId);

	String getPaymentCredentialsBySupplierIdAndChannelId(ManagerToChannelAccountModel managerToChannelAccountModel);

	List<ManagerToChannelAccountModel> findByActivePropertyManager(@Param("supplierId") Integer supplierId);

	List<ManagerToChannelAccountModel> justPMCreatedOrSuspendedNotChangedInAWhile(IsProductAction productAction);

	List<ManagerToChannelAccountModel> getlAllHomeownerAccounts();

	List<ManagerToChannelAccountModel> findSupplierForExpedia(String supplierId);

	void updateManagerToChannelAccount(ManagerToChannelAccountModel managerToChannelAccountModel);
}
