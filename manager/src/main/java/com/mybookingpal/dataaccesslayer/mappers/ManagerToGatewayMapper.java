package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ManagerToGatewayModel;

import java.util.List;

public interface ManagerToGatewayMapper {

	void create(ManagerToGatewayModel managerToGatewayModel);

	ManagerToGatewayModel read(Integer id);

	ManagerToGatewayModel readBySupplierId(Integer supplierId);

	List<ManagerToGatewayModel> readBySupplierIds(List<Integer> supplierIds);

	void updateNotNullValues(ManagerToGatewayModel managerToGatewayModel);

	void update(ManagerToGatewayModel managerToGatewayModel);

	void delete(Integer id);

	void deleteBySupplierId(Integer supplierId);
}
