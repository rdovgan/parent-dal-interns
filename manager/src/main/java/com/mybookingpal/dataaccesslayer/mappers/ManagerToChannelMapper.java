package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.IsManagerToChannelDto;
import com.mybookingpal.dataaccesslayer.dto.IsProductAction;
import com.mybookingpal.dataaccesslayer.entity.ManagerToChannelModel;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManagerToChannelMapper {

	List<ManagerToChannelModel> listPropertyManagersDistributedChannel(String channelPartnerId);

	List<ManagerToChannelModel> getListByChannelIdAndPmList(@Param("list") List<String> pmIds, @Param("channelId") String channelId);

	List<String> justPropertyManagersDistributedChannel(String channelPartnerId);

	List<String> justPMCreatedOrNotChangedInAWhile(IsProductAction channelId);

	List<String> listPMNotDistributedToChannelString(List<String> supplierIds);

	ManagerToChannelModel getByPmIDAndChannelID(NameId nameId);

	ManagerToChannelModel getByPmIDAndChannel(NameId nameId);

	List<ManagerToChannelModel> readAll();

	List<Integer> listDistributedPMIdsByAbbreviation(String abbreviation);

	List<String> listOfChannelAbbreviationsRelatedToPm(Integer pmId);

	List<Integer> listOfChannelIdsRelatedToPm(Integer pmId);

	List<Integer> listOfExpediaChannelIdsRelatedToPm(Integer pmId);

	void update(@Param("commission") Integer commission, @Param("pmId") Integer pmId, @Param("cpId") Integer cpId);

	ManagerToChannelModel readByPmIdAndChannelId(@Param("pmId") Integer pmId, @Param("channelId") Integer channelId);

	void createConnection(ManagerToChannelModel managerToChannel);

	void deleteConnection(ManagerToChannelModel managerToChannel);

	List<ManagerToChannelModel> readPropertyManagerToChannelsConnection(@Param("pmId") Integer pmId, @Param("ids") List<Integer> channelIds);

	void fullUpdate(ManagerToChannelModel managerToChannel);

	void updateFundsHolder(ManagerToChannelModel managerToChannel);

	void create(ManagerToChannelModel managerToChannel);

	ManagerToChannelModel selectByPMIdAndChannelAbbreviation(@Param("pmId") String pmId, @Param("abbreviation") String abbreviation);

	void updateFeeNetRateByPropertyManagerAndChannelPartner(@Param("propertyManagerId") Integer propertyManagerId,
			@Param("channelPartnerId") Integer channelPartnerId, @Param("isNetRate") Boolean isNetRate);

	List<IsManagerToChannelDto> selectFeeCommissionByPmId(@Param("pmId") Integer pmId);

	List<ManagerToChannelModel> getListByChannelIdsAndPmId(@Param("pmId") Integer pmId, @Param("channelIds") List<Integer> channelIds);

	List<ManagerToChannelModel> listAllChannelPropertyManagers();

	List<String> listPropertyManagersNotDistributedChannel(String channelPartnerId);

	List<ManagerToChannelModel> listFeed();

	List<Integer> getChannelPartnerIdsByPmId(String id);

	List<ManagerToChannelModel> findSupplierForExpedia(Integer supplierId);
}
