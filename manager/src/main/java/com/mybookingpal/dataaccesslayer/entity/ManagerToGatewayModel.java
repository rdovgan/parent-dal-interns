package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class ManagerToGatewayModel implements Cloneable {
	private Integer id;
	private Date createDate;
	private Integer paymentGatewayId;
	private Integer fundsHolder;
	private Integer supplierId;
	private String gatewayCode;
	private String gatewayAccountId;
	private String additionalInfo;
	private Boolean supressInvoiceEmail;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public int getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(int fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public String getGatewayCode() {
		return gatewayCode;
	}

	public void setGatewayCode(String gatewayCode) {
		this.gatewayCode = gatewayCode;
	}

	public String getGatewayAccountId() {
		return gatewayAccountId;
	}

	public void setGatewayAccountId(String gatewayAccountId) {
		this.gatewayAccountId = gatewayAccountId;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Boolean getSupressInvoiceEmail() {
		return supressInvoiceEmail;
	}

	public void setSupressInvoiceEmail(Boolean supressInvoiceEmail) {
		this.supressInvoiceEmail = supressInvoiceEmail;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ManagerToGateway [id=");
		builder.append(getId());
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", paymentGatewayId=");
		builder.append(paymentGatewayId);
		builder.append(", fundsHolder=");
		builder.append(fundsHolder);
		builder.append(", supplierId=");
		builder.append(supplierId);
		builder.append(", gatewayCode=");
		builder.append(gatewayCode);
		builder.append(", gatewayAccountId=");
		builder.append(gatewayAccountId);
		builder.append(", additionalInfo=");
		builder.append(additionalInfo);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public ManagerToGatewayModel clone() throws CloneNotSupportedException {
		return (ManagerToGatewayModel) super.clone();
	}
}