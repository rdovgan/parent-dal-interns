package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Component
@Scope(value = "prototype")
public class ManagerToChannelAccountModel implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer supplierID;
	private Integer channelID;
	private Integer channelSupplierID;
	private String authToken;
	private String credentials;
	private Boolean state = false;
	private Date createdDate = new Date();
	private Date version = new Date();

	public ManagerToChannelAccountModel() {
	}

	public ManagerToChannelAccountModel(Integer channelID) {
		super();
		this.channelID = channelID;
	}

	public ManagerToChannelAccountModel(Integer supplierID, Integer channelID, String authToken) {
		super();
		this.supplierID = supplierID;
		this.channelID = channelID;
		this.authToken = authToken;
	}

	public ManagerToChannelAccountModel(Integer supplierID, Integer channelID, String authToken, Integer channelSupplierId) {
		super();
		this.supplierID = supplierID;
		this.channelID = channelID;
		this.authToken = authToken;
		this.channelSupplierID = channelSupplierId;
	}

	public ManagerToChannelAccountModel(Integer supplierID, Integer channelID) {
		super();
		this.supplierID = supplierID;
		this.channelID = channelID;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSupplierID() {
		return supplierID;
	}

	public void setSupplierID(Integer supplierID) {
		this.supplierID = supplierID;
	}

	public Integer getChannelID() {
		return channelID;
	}

	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	public Integer getChannelSupplierID() {
		return channelSupplierID;
	}

	public void setChannelSupplierID(Integer channelSupplierID) {
		this.channelSupplierID = channelSupplierID;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public boolean equals(Object account) {
		boolean equal = false;
		if (account instanceof ManagerToChannelAccountModel) {
			equal = this.getChannelID()
					.intValue() == ((ManagerToChannelAccountModel) account).getChannelID()
					.intValue() && this.getSupplierID()
					.intValue() == ((ManagerToChannelAccountModel) account).getSupplierID()
					.intValue();
		}
		return equal;
	}

	@Override
	public int hashCode() {
		return Objects.hash(channelID, supplierID);
	}
}
