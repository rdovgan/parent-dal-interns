package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.utils.entity.BigDecimalExt;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ManagerToChannelModel {
	private Integer id;
	private Integer propertyManagerId;
	private Integer channelPartnerId;
	private Integer propertyManagerAltid;
	private BigDecimalExt channelPartnerCommission = BigDecimalExt.ZERO;
	private Integer agreement;
	private Integer fundsHolder;
	private Boolean netRate;
	private Boolean feeNetRate;
	private String partnerPaymentModel;
	private String minStayType;

	public ManagerToChannelModel() {

	}

	public ManagerToChannelModel(Integer propertyManagerId, Integer channelPartnerId, String partnerPaymentModel, String minStayType, Integer agreement,
			Integer fundsHolder, Boolean netRate, BigDecimalExt channelPartnerCommission) {
		this.propertyManagerId = propertyManagerId;
		this.channelPartnerId = channelPartnerId;
		this.partnerPaymentModel = partnerPaymentModel;
		this.minStayType = minStayType;
		this.agreement = agreement;
		this.fundsHolder = fundsHolder;
		this.netRate = netRate;
		this.channelPartnerCommission = channelPartnerCommission;
	}

	public ManagerToChannelModel(Integer propertyManagerId, Integer channelPartnerId) {
		this.propertyManagerId = propertyManagerId;
		this.channelPartnerId = channelPartnerId;
	}

	public ManagerToChannelModel(Integer id, Integer property_manager_id, Integer channel_partner_id) {
		this.id = id;
		this.propertyManagerId = property_manager_id;
		this.channelPartnerId = channel_partner_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getNetRate() {
		return netRate;
	}

	public void setNetRate(Boolean netRate) {
		this.netRate = netRate;
	}

	public Boolean getFeeNetRate() {
		return feeNetRate;
	}

	public void setFeeNetRate(Boolean feeNetRate) {
		this.feeNetRate = feeNetRate;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public Integer getPropertyManagerAltid() {
		return propertyManagerAltid;
	}

	public void setPropertyManagerAltid(Integer propertyManagerAltid) {
		this.propertyManagerAltid = propertyManagerAltid;
	}

	public Integer getAgreement() {
		return agreement;
	}

	public void setAgreement(Integer agreement) {
		this.agreement = agreement;
	}

	public Integer getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public BigDecimalExt getChannelPartnerCommission() {
		return channelPartnerCommission;
	}

	public void setChannelPartnerCommission(BigDecimalExt channelPartnerCommission) {
		this.channelPartnerCommission = channelPartnerCommission;
	}

	public void setChannelPartnerCommission(Double channelPartnerCommission) {
		this.channelPartnerCommission = new BigDecimalExt(channelPartnerCommission);
	}

	public String getPartnerPaymentModel() {
		return partnerPaymentModel;
	}

	public void setPartnerPaymentModel(String partnerPaymentModel) {
		this.partnerPaymentModel = partnerPaymentModel;
	}

	public String getMinStayType() {
		return minStayType;
	}

	public void setMinStayType(String minStayType) {
		this.minStayType = minStayType;
	}

	@Override
	public String toString() {
		return "ManagerToChannel [id=" + id + ", propertyManagerId=" + propertyManagerId + ", channelPartnerId=" + channelPartnerId + ", propertyManagerAltid="
				+ propertyManagerAltid + ", channelPartnerCommission=" + channelPartnerCommission + ", agreement=" + agreement + "]" + ", fundsHolder="
				+ fundsHolder + "]" + ", partnerPaymentModel=" + partnerPaymentModel + "]" + ", minStayType=" + minStayType + "]";
	}
}