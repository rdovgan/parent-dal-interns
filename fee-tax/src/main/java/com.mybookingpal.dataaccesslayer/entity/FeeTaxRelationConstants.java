package com.mybookingpal.dataaccesslayer.entity;

import java.util.Arrays;

public class FeeTaxRelationConstants {
	public enum StateFeeTaxEnum {
		CREATED(2),
		FINAL(3);

		private Integer value;

		StateFeeTaxEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}

		public static StateFeeTaxEnum getByInt(int value) {
			return Arrays.stream(StateFeeTaxEnum.values()).filter(v -> v.value == value).findFirst().orElse(CREATED);
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}
	}
}
