package com.mybookingpal.dataaccesslayer.entity;

public class TaxFeeWithPartyNameModel {

	private String productId, partyId, partnerName, partyName, feeTaxName, channelIDs, channelNames;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getFeeTaxName() {
		return feeTaxName;
	}

	public void setFeeTaxName(String feeTaxName) {
		this.feeTaxName = feeTaxName;
	}

	public String getChannelIDs() {
		return channelIDs;
	}

	public void setChannelIDs(String channelIDs) {
		this.channelIDs = channelIDs;
	}

	public String getChannelNames() {
		return channelNames;
	}

	public void setChannelNames(String channelNames) {
		this.channelNames = channelNames;
	}
}
