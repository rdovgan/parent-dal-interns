package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class TaxModel {

	private String id;
	private String name;
	private String accountId;
	private String partyId;
	private String productId;
	private String partyName;
	private String entityType;
	private String entityId;
	private String state;
	private String type;
	private String currency;
	private String notes;
	private Date date;
	private Integer threshold;
	private Double amount;
	private String mandatoryType = "MandatoryTax";
	private String altId;
	private String optionValue;
	private int status = 0;
	private Date version;
	private Integer updateType;

	public TaxModel() {}

	public TaxModel(TaxModel other) {
		id = other.id;
		name = other.name;
		accountId = other.accountId;
		partyId = other.partyId;
		productId = other.productId;
		partyName = other.partyName;
		entityType = other.entityType;
		entityId = other.entityId;
		state = other.state;
		type = other.type;
		currency = other.currency;
		notes = other.notes;
		date = other.date;
		threshold = other.threshold;
		amount = other.amount;
		mandatoryType = other.mandatoryType;
		altId = other.altId;
		optionValue = other.optionValue;
		state = other.state;
		version = other.version;
		updateType = other.updateType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getMandatoryType() {
		return mandatoryType;
	}

	public void setMandatoryType(String mandatoryType) {
		this.mandatoryType = mandatoryType;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Integer getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Integer updateType) {
		this.updateType = updateType;
	}
}
