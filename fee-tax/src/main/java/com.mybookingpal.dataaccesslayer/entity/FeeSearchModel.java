package com.mybookingpal.dataaccesslayer.entity;

import java.util.List;

public class FeeSearchModel extends FeeModel {

	private List<Integer> entityTypes;
	private List<String> productIds;

	public List<Integer> getEntityTypes() {
		return entityTypes;
	}

	public void setEntityTypes(List<Integer> entityTypes) {
		this.entityTypes = entityTypes;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

}
