package com.mybookingpal.dataaccesslayer.entity;

import java.util.List;

public class TaxSearchModel extends TaxModel {

	private List<String> productIds;

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}
}
