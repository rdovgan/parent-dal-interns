package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

import com.mybookingpal.dataaccesslayer.entity.FeeTaxRelationConstants.StateFeeTaxEnum;

public class FeeTaxRelationModel {

	private Integer id;
	private Integer feeId;
	private Integer taxId;
	private StateFeeTaxEnum state;
	private Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFeeId() {
		return feeId;
	}

	public void setFeeId(Integer feeId) {
		this.feeId = feeId;
	}

	public Integer getTaxId() {
		return taxId;
	}

	public void setTaxId(Integer taxId) {
		this.taxId = taxId;
	}

	public StateFeeTaxEnum getState() {
		return state;
	}

	public void setState(StateFeeTaxEnum state) {
		this.state = state;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}
