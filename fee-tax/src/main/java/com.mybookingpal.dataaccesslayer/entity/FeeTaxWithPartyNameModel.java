package com.mybookingpal.dataaccesslayer.entity;

public class FeeTaxWithPartyNameModel {

	private String productId, partyId, partnerName, partyName, feeTaxName, channelIDs, channelNames;

	public String getChannelIDs() {
		return channelIDs;
	}

	public void setChannelIDs(String channelIDs) {
		this.channelIDs = channelIDs;
	}

	public String getChannelNames() {
		return channelNames;
	}

	public void setChannelNames(String channelNames) {
		this.channelNames = channelNames;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = String.valueOf(productId);
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(int partyId) {
		this.partyId = String.valueOf(partyId);
	}

	public String getFeeTaxName() {
		return feeTaxName;
	}

	public void setFeeTaxName(String feeTaxName) {
		this.feeTaxName = feeTaxName;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

}
