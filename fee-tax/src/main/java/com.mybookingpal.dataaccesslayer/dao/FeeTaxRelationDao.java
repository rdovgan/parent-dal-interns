package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.FeeTaxRelationModel;
import com.mybookingpal.dataaccesslayer.mappers.FeeTaxRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class FeeTaxRelationDao {

	@Autowired
	private FeeTaxRelationMapper feeTaxRelationMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeTaxRelationModel> getAllByProductIdAndPartyId(String productId, String partyId) {
		return feeTaxRelationMapper.getAllByProductIdAndPartyId(productId, partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<FeeTaxRelationModel> feeTaxRelationList) {
		if (CollectionUtils.isEmpty(feeTaxRelationList)) {
			return;
		}
		feeTaxRelationMapper.insertList(feeTaxRelationList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelFeeTaxRelationList(List<Integer> feeTaxRelationIds) {
		if (CollectionUtils.isEmpty(feeTaxRelationIds)) {
			return;
		}
		feeTaxRelationMapper.cancelFeeTaxRelationList(feeTaxRelationIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeTaxRelationModel> getByProductId(String productId) {
		return feeTaxRelationMapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyFeesNotInRelation(Integer fromProductId, Integer toProductId) {
		feeTaxRelationMapper.copyFeesNotInRelation(fromProductId, toProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyTaxesNotInRelation(Integer fromProductId, Integer toProductId) {
		feeTaxRelationMapper.copyTaxesNotInRelation(fromProductId, toProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		feeTaxRelationMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(FeeTaxRelationModel feeTaxRelation) {
		feeTaxRelationMapper.create(feeTaxRelation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void finalizeByFeeId(Long feeId) {
		feeTaxRelationMapper.finalizeByFeeId(feeId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void finalizeByTaxId(Long taxId) {
		feeTaxRelationMapper.finalizeByTaxId(taxId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createList(List<FeeTaxRelationModel> feeTaxRelations) {
		if (CollectionUtils.isEmpty(feeTaxRelations)) {
			return;
		}
		feeTaxRelationMapper.createList(feeTaxRelations);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeTaxRelationModel> readByProductIds(List<String> productIds, Integer state) {
		return feeTaxRelationMapper.readByProductIds(productIds, state);
	}

}
