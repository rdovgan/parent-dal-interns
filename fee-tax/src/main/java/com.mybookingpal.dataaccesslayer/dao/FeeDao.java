package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.FeeModel;
import com.mybookingpal.dataaccesslayer.entity.FeeSearchModel;
import com.mybookingpal.dataaccesslayer.entity.FeeTaxWithPartyNameModel;
import com.mybookingpal.dataaccesslayer.entity.PriceDiscrepancyFeeModel;
import com.mybookingpal.dataaccesslayer.mappers.FeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class FeeDao {

	@Autowired
	private FeeMapper feeMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(FeeModel action) {
		feeMapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(FeeModel action) {
		feeMapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FeeModel exists(FeeModel action) {
		return feeMapper.exists(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FeeModel equalExists(FeeModel action) {
		return feeMapper.equalExists(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FeeModel read(Integer id) {
		return feeMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelVersion(FeeModel action) {
		feeMapper.cancelVersion(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelFeeList(List<FeeModel> list, Date version) {
		feeMapper.cancelFeeList(list, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<FeeModel> list) {
		feeMapper.insertList(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertFeeWithUpdate(FeeModel action) {
		feeMapper.insertFeeWithUpdate(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readByProductAndState(FeeModel action) {
		return feeMapper.readByProductAndState(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readByExactProductAndState(FeeModel action) {
		return feeMapper.readByExactProductAndState(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readUniqueByProductAndState(FeeModel action) {
		return feeMapper.readUniqueByProductAndState(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readMandatoryFeesForProduct(FeeModel action) {
		return feeMapper.readMandatoryFeesForProduct(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readUniqueFeesWithoutExtraPersonFee(FeeModel action) {
		return feeMapper.readUniqueFeesWithoutExtraPersonFee(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> readAllMandatoryFeesWithoutExtraPersonFee(FeeModel action) {
		return feeMapper.readAllMandatoryFeesWithoutExtraPersonFee(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readUniqueExtraPersonFee(FeeModel action) {
		return feeMapper.readUniqueExtraPersonFee(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readUniqueByProductAndStateAndMaxValue(FeeModel action) {
		return feeMapper.readUniqueByProductAndStateAndMaxValue(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readUniqueByProductAndStateAndMinPerson(FeeModel action) {
		return feeMapper.readUniqueByProductAndStateAndMinPerson(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> readByDateAndProduct(FeeModel action) {
		return feeMapper.readByDateAndProduct(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getAllFeesByDateAndProduct(FeeModel action) {
		return feeMapper.getAllFeesByDateAndProduct(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> listByProductIds(FeeSearchModel action) {
		return feeMapper.listByProductIds(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ArrayList<FeeModel> readByProductAndUnit(FeeModel action) {
		return feeMapper.readByProductAndUnit(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		feeMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> readAllByProductIdList(List<Integer> productIdList) {
		return feeMapper.readAllByProductIdList(productIdList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FeeModel readFeeByRatePlanId(Long ratePlanId) {
		return feeMapper.readFeeByRatePlanId(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void makeFeeFinalByRatePlanId(Long ratePlanId) {
		feeMapper.makeFeeFinalByRatePlanId(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerPm(String partyId) {
		feeMapper.updateStateOnFinalPerPm(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateToFinalPerId(String id) {
		feeMapper.updateStateToFinalPerId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateToCrated(String id) {
		feeMapper.updateStateToCrated(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getAllFeesByProductAndVersion(FeeModel feeModel) {
		return feeMapper.getAllFeesByProductAndVersion(feeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> listFeeByEntityAndLastVersion(String productId) {
		return feeMapper.listFeeByEntityAndLastVersion(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> listCreatedFeeByProductId(Long productId) {
		return feeMapper.listCreatedFeeByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> listCreatedAndInitialFeeByProductId(Long productId) {
		return feeMapper.listCreatedAndInitialFeeByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getExtraPersonFeeByProductAndVersion(FeeModel feeModel) {
		return feeMapper.getExtraPersonFeeByProductAndVersion(feeModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PriceDiscrepancyFeeModel> getPriceDiscrepancyFeesByIds(List<Long> ids) {
		return feeMapper.getPriceDiscrepancyFeesByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> readProductFees(Long productId, Long feeId) {
		return feeMapper.readProductFees(productId, feeId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteListFees(List<Integer> ids) {
		feeMapper.deleteListFees(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getFeesByIds(List<Integer> ids) {
		return feeMapper.getFeesByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getListFeesByProductId(Integer productId) {
		return feeMapper.getListFeesByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> getListFeesFromQuoteByProductId(Integer productId) {
		return feeMapper.getListFeesFromQuoteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeTaxWithPartyNameModel> getFeeDuplicatesWithPartyNames() {
		return feeMapper.getFeeDuplicatesWithPartyNames();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeModel> readPersonOrExtraPersonFeeByProductId(Integer productId) {
		return feeMapper.readPersonOrExtraPersonFeeByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateCurrency(FeeModel action) {
		feeMapper.updateCurrency(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerProduct(Integer productId) {
		feeMapper.updateStateOnFinalPerProduct(productId);
	}
}
