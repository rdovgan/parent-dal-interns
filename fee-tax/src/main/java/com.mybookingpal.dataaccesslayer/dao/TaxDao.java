package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.TaxFeeWithPartyNameModel;
import com.mybookingpal.dataaccesslayer.entity.TaxModel;
import com.mybookingpal.dataaccesslayer.entity.TaxSearchModel;
import com.mybookingpal.dataaccesslayer.mappers.TaxMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class TaxDao {

	@Autowired
	private TaxMapper taxMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(TaxModel action) {
		taxMapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TaxModel read(String id) {
		return taxMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(TaxModel action) {
		taxMapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TaxModel exists(TaxModel action) {
		return taxMapper.exists(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelVersion(TaxModel action) {
		taxMapper.cancelVersion(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelTaxList(List<String> taxIdList, Date version) {
		taxMapper.cancelTaxList(taxIdList, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> taxDetail(TaxModel action) {
		return taxMapper.taxDetail(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> taxByRelation(TaxModel action) {
		return taxMapper.taxByRelation(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> readByProductId(TaxModel action) {
		return taxMapper.readByProductId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> readByProductIdAllStateExceptFinal(TaxModel action) {
		return taxMapper.readByProductIdAllStateExceptFinal(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> readByExactProductAndState(TaxModel action) {
		return taxMapper.readByExactProductAndState(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> listByProductIds(TaxSearchModel action) {
		return taxMapper.listByProductIds(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> listByRelationsProductIds(TaxSearchModel action) {
		return taxMapper.listByRelationsProductIds(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		taxMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<TaxModel> taxesToCreate) {
		taxMapper.insertList(taxesToCreate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertTaxWithUpdate(TaxModel action) {
		taxMapper.insertTaxWithUpdate(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerPm(String partyId) {
		taxMapper.updateStateOnFinalPerPm(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> getAllTaxesByProductAndVersion(TaxModel taxModel) {
		return taxMapper.getAllTaxesByProductAndVersion(taxModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> getListTaxesFromQuoteByProductId(Integer productId) {
		return taxMapper.getListTaxesFromQuoteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> listInitialAndCreatedTaxByProductId(Long productId, Long taxId) {
		return taxMapper.listInitialAndCreatedTaxByProductId(productId, taxId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> readCreatedTaxesByProductId(Long productId) {
		return taxMapper.readCreatedTaxesByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteList(List<Long> ids) {
		taxMapper.deleteList(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> getTaxByIds(List<Long> ids) {
		return taxMapper.getTaxByIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxModel> getListTaxByProductId(Integer productId) {
		return taxMapper.getListTaxByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TaxFeeWithPartyNameModel> getTaxDuplicatesWithPartyName() {
		return taxMapper.getTaxDuplicatesWithPartyName();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateCurrency(TaxModel taxModel) {
		taxMapper.updateCurrency(taxModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateToCrated(String id) {
		taxMapper.updateStateToCrated(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerProduct(Integer productId) {
		taxMapper.updateStateOnFinalPerProduct(productId);
	}
}
