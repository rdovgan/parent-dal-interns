package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.FeeModel;
import com.mybookingpal.dataaccesslayer.entity.FeeSearchModel;
import com.mybookingpal.dataaccesslayer.entity.FeeTaxWithPartyNameModel;
import com.mybookingpal.dataaccesslayer.entity.PriceDiscrepancyFeeModel;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface FeeMapper {
	void create(FeeModel action);

	void update(FeeModel action);

	FeeModel exists(FeeModel action);

	FeeModel equalExists(FeeModel action);

	FeeModel read(Integer id);

	void cancelVersion(FeeModel action);

	void cancelFeeList(@Param("list") List<FeeModel> list, @Param("version") Date version);

	void insertList(@Param("list") List<FeeModel> list);

	void insertFeeWithUpdate(FeeModel action);

	ArrayList<FeeModel> readByProductAndState(FeeModel action);

	ArrayList<FeeModel> readByExactProductAndState(FeeModel action);

	ArrayList<FeeModel> readUniqueByProductAndState(FeeModel action);

	ArrayList<FeeModel> readMandatoryFeesForProduct(FeeModel action);

	ArrayList<FeeModel> readUniqueFeesWithoutExtraPersonFee(FeeModel action);

	List<FeeModel> readAllMandatoryFeesWithoutExtraPersonFee(FeeModel action);

	ArrayList<FeeModel> readUniqueExtraPersonFee(FeeModel action);

	ArrayList<FeeModel> readUniqueByProductAndStateAndMaxValue(FeeModel action);

	ArrayList<FeeModel> readUniqueByProductAndStateAndMinPerson(FeeModel action);

	List<FeeModel> readByDateAndProduct(FeeModel action);

	List<FeeModel> getAllFeesByDateAndProduct(FeeModel action);

	List<FeeModel> listByProductIds(FeeSearchModel action);

	ArrayList<FeeModel> readByProductAndUnit(FeeModel action);

	void deleteByProductId(String productId);

	List<FeeModel> readAllByProductIdList(List<Integer> productIdList);

	FeeModel readFeeByRatePlanId(Long ratePlanId);

	void makeFeeFinalByRatePlanId(Long ratePlanId);

	void updateStateOnFinalPerPm(String partyId);

	void updateStateToFinalPerId(String id);

	void updateStateToCrated(String id);

	List<FeeModel> getAllFeesByProductAndVersion(FeeModel feeModel);

	List<FeeModel> listFeeByEntityAndLastVersion(String productId);

	List<FeeModel> listCreatedFeeByProductId(Long productId);

	List<FeeModel> listCreatedAndInitialFeeByProductId(Long productId);

	List<FeeModel> getExtraPersonFeeByProductAndVersion(FeeModel feeModel);

	List<PriceDiscrepancyFeeModel> getPriceDiscrepancyFeesByIds(@Param("ids") List<Long> ids);

	List<FeeModel> readProductFees(@Param("productId") Long productId, @Param("feeId") Long feeId);

	void deleteListFees(@Param("ids") List<Integer> ids);

	List<FeeModel> getFeesByIds(@Param("ids") List<Integer> ids);

	List<FeeModel> getListFeesByProductId(@Param("productId") Integer productId);

	List<FeeModel> getListFeesFromQuoteByProductId(@Param("productId") Integer productId);

	List<FeeTaxWithPartyNameModel> getFeeDuplicatesWithPartyNames();

	List<FeeModel> readPersonOrExtraPersonFeeByProductId(@Param("productId") Integer productId);

	void updateCurrency(FeeModel action);

	void updateStateOnFinalPerProduct(@Param("productId") Integer productId);
}