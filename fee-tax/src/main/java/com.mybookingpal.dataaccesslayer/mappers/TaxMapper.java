package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.TaxFeeWithPartyNameModel;
import com.mybookingpal.dataaccesslayer.entity.TaxModel;
import com.mybookingpal.dataaccesslayer.entity.TaxSearchModel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TaxMapper {

	void create(TaxModel action);

	TaxModel read(String id);

	void update(TaxModel action);

	TaxModel exists(TaxModel action);

	void cancelVersion(TaxModel action);

	void cancelTaxList(@Param("list") List<String> taxIdList, @Param("version") Date version);

	List<TaxModel> taxDetail(TaxModel action);

	List<TaxModel> taxByRelation(TaxModel action);

	List<TaxModel> readByProductId(TaxModel action);

	List<TaxModel> readByProductIdAllStateExceptFinal(TaxModel action);

	List<TaxModel> readByExactProductAndState(TaxModel action);

	List<TaxModel> listByProductIds(TaxSearchModel action);

	List<TaxModel> listByRelationsProductIds(TaxSearchModel action);

	void deleteByProductId(String productId);

	void insertList(List<TaxModel> taxesToCreate);

	void insertTaxWithUpdate(TaxModel action);

	void updateStateOnFinalPerPm(String partyId);

	List<TaxModel> getAllTaxesByProductAndVersion(TaxModel taxModel);

	List<TaxModel> getListTaxesFromQuoteByProductId(@Param("productId") Integer productId);

	List<TaxModel> listInitialAndCreatedTaxByProductId(@Param("productId") Long productId, @Param("taxId") Long taxId);

	List<TaxModel> readCreatedTaxesByProductId(Long productId);

	void deleteList(@Param("ids") List<Long> ids);

	List<TaxModel> getTaxByIds(@Param("ids") List<Long> ids);

	List<TaxModel> getListTaxByProductId(@Param("productId") Integer productId);

	List<TaxFeeWithPartyNameModel> getTaxDuplicatesWithPartyName();

	void updateCurrency(TaxModel taxModel);

	void updateStateToCrated(String id);

	void updateStateOnFinalPerProduct(@Param("productId") Integer productId);
}
