package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.FeeTaxRelationModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FeeTaxRelationMapper {
	List<FeeTaxRelationModel> getAllByProductIdAndPartyId(@Param("productId") String productId, @Param("partyId") String partyId);

	void insertList(@Param("list") List<FeeTaxRelationModel> feeTaxRelationList);

	void cancelFeeTaxRelationList(List<Integer> feeTaxRelationIds);

	List<FeeTaxRelationModel> getByProductId(String productId);

	void copyFeesNotInRelation(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	void copyTaxesNotInRelation(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	void deleteByProductId(String productId);

	void create(FeeTaxRelationModel feeTaxRelation);

	void finalizeByFeeId(Long feeId);

	void finalizeByTaxId(Long taxId);

	void createList(@Param("feeTaxRelations") List<FeeTaxRelationModel> feeTaxRelations);

	List<FeeTaxRelationModel> readByProductIds(@Param("productIds") List<String> productIds, @Param("state") Integer state);
}