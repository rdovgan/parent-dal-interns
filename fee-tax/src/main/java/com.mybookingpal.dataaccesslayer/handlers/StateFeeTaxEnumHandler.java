package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.dataaccesslayer.entity.FeeTaxRelationConstants.StateFeeTaxEnum;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StateFeeTaxEnumHandler extends EnumTypeHandler<StateFeeTaxEnum> {

	public StateFeeTaxEnumHandler() {
		super(StateFeeTaxEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, StateFeeTaxEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getValue());
	}

	public StateFeeTaxEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : StateFeeTaxEnum.getByInt(value);
	}

	public StateFeeTaxEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : StateFeeTaxEnum.getByInt(value);
	}
}
