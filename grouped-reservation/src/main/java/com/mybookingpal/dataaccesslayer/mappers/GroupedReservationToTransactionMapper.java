package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.GroupedReservationToTransactionModel;
import com.mybookingpal.dataaccesslayer.entity.enums.TransactionType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupedReservationToTransactionMapper {

	void create(GroupedReservationToTransactionModel groupedReservation);

	GroupedReservationToTransactionModel read(Long id);

	String readStateOfLastPaymentTransactionByGroupReservationId(Long groupedReservationId);

	GroupedReservationToTransactionModel readGroupedReservationWithPendingTransactionByGroupedReservationId(Long groupedReservationId);

	GroupedReservationToTransactionModel readByTransactionIdAndType(@Param("transactionId") Long transactionId,
																	@Param("transactionType") TransactionType transactionType);

	List<Long> readTransactionIdsByGroupedReservationIdAndType(@Param("groupedReservationId") Long groupedReservationId,
															   @Param("type") TransactionType type);

	GroupedReservationToTransactionModel readLastGroupedReservationTransactionByGroupIdAndType(@Param("reservationId") String reservationId,
																							   @Param("type") TransactionType type);

	GroupedReservationToTransactionModel readGroupedReservationByTransactionTypeAndTransactionId(@Param("transactionId") Long transactionId,
																								 @Param("type") TransactionType transactionType);
}
