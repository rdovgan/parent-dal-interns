package com.mybookingpal.dataaccesslayer.mappers;


import com.mybookingpal.dataaccesslayer.entity.GroupedReservationModel;
import org.apache.ibatis.annotations.Param;

public interface GroupedReservationMapper {

	void create(GroupedReservationModel action);

	void update(GroupedReservationModel action);

	GroupedReservationModel read(Long id);

	String readGroupReservationPmId(@Param("id") Integer id);

}
