package com.mybookingpal.dataaccesslayer.entity.enums;

public enum TransactionType {
	PAYMENT_TRANSACTION,
	PENDING_TRANSACTION
}
