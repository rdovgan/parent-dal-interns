package com.mybookingpal.dataaccesslayer.entity;


import com.mybookingpal.dataaccesslayer.entity.enums.State;

import java.math.BigDecimal;

public class GroupedReservationModel {

	private Long id;
	private BigDecimal amount;
	private State state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
