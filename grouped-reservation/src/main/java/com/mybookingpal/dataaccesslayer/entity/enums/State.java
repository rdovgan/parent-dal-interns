package com.mybookingpal.dataaccesslayer.entity.enums;

public enum State {
		Cancelled,
		Confirmed,
		Departed,
		Exception,
		Failed,
		FullyPaid,
		Initial,
		Provisional
}
