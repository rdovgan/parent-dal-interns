package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.GroupedReservationToTransactionModel;
import com.mybookingpal.dataaccesslayer.entity.enums.TransactionType;
import com.mybookingpal.dataaccesslayer.mappers.GroupedReservationToTransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class GroupedReservationToTransactionDao {

	@Autowired
	private GroupedReservationToTransactionMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(GroupedReservationToTransactionModel action) {
		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public GroupedReservationToTransactionModel read(Long id) {
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String readStateOfLastPaymentTransactionByGroupReservationId(Long groupedReservationId) {
		return mapper.readStateOfLastPaymentTransactionByGroupReservationId(groupedReservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public GroupedReservationToTransactionModel readGroupedReservationWithPendingTransactionByGroupedReservationId(
			Long groupedReservationId) {
		return mapper.readGroupedReservationWithPendingTransactionByGroupedReservationId(groupedReservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public GroupedReservationToTransactionModel readByTransactionIdAndType(Long transactionId,
																		   TransactionType transactionType) {
		return mapper.readByTransactionIdAndType(transactionId, transactionType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Long> readTransactionIdsByGroupedReservationIdAndType(Long groupedReservationId,
																	  TransactionType type) {
		return mapper.readTransactionIdsByGroupedReservationIdAndType(groupedReservationId, type);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public GroupedReservationToTransactionModel readLastGroupedReservationTransactionByGroupIdAndType(String reservationId,
																									  TransactionType type) {
		return mapper.readLastGroupedReservationTransactionByGroupIdAndType(reservationId, type);
	}

	public GroupedReservationToTransactionModel readGroupedReservationByTransactionTypeAndTransactionId(Long transactionId,
																										TransactionType transactionType) {
		return mapper.readGroupedReservationByTransactionTypeAndTransactionId(transactionId, transactionType);
	}


}
