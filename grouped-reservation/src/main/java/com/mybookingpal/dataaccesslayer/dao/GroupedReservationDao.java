package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.GroupedReservationModel;
import com.mybookingpal.dataaccesslayer.mappers.GroupedReservationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class GroupedReservationDao {

	@Autowired
	private GroupedReservationMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(GroupedReservationModel action) {
		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(GroupedReservationModel action) {
		mapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public GroupedReservationModel read(Long id) {
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String readGroupReservationPmId(Integer id) {
		return mapper.readGroupReservationPmId(id);
	}
}
