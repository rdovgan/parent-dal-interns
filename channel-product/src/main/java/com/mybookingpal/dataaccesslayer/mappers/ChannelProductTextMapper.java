package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductTextModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductTextMapper {
	List<ChannelProductTextModel> getByProductId(Integer productId);

	List<ChannelProductTextModel> getByProdcutIdAndTypeTextAndChanelIds(@Param("productId") Integer productId, @Param("channelIds") List<Integer> channelIds,
			@Param("typeText") Integer typeText);

	List<ChannelProductTextModel> getByProdcutIdAndTypeTextAndChanelIdsAndLanguages(@Param("productId") Integer productId,
			@Param("channelIds") List<Integer> channelIds, @Param("typeText") Integer typeText, @Param("languages") List<String> languages);

	void insertList(@Param("list") List<ChannelProductTextModel> channelProductTextModelList);

	void update(ChannelProductTextModel channelProductTextModelList);

	void deleteList(List<ChannelProductTextModel> idList);

	String getHeadlineTextByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	List<ChannelProductTextModel> getByProductIdAndChannelId(@Param("productId") Integer productId, @Param("channelId") Integer channelId);

	List<ChannelProductTextModel> getListByProductChannelLanguage(@Param("productId") Integer productId, @Param("channelId") Integer channelId,
			@Param("language") String language);

	void insertAllList(@Param("list") List<ChannelProductTextModel> channelProductTextModelList);

	void updateToFinale(ChannelProductTextModel channelProductTextModel);

	void updateListToFinal(@Param("ids") List<Integer> ids);

	List<ChannelProductTextModel> getListByProductsChannelLanguage(@Param("products") List<Integer> products, @Param("channelId") Integer channelId,
			@Param("language") String language);

	ChannelProductTextModel getProductTextByTypeForMarriott(@Param("productId") Integer productId, @Param("textType") Integer textType);

	List<Integer> readIdsByChannelProductTextList(@Param("list") List<ChannelProductTextModel> channelProductTextModelList);

	ChannelProductTextModel getProductTextByType(@Param("productId") Integer productId, @Param("textType") Integer textType,
			@Param("channelId") Integer channelId);

	List<ChannelProductTextModel> getByProductIdAndChannelId(ChannelProductTextModel channelProductTextModel);

	List<ChannelProductTextModel> getHeadlineTextsByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	String getShortDescriptionProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	String getDescriptionByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	String getDisplayNameByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	List<ChannelProductTextModel> getShortDescriptionsByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel);

	List<ChannelProductTextModel> getNameByTextType(ChannelProductTextModel channelProductTextModel);
}
