package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductMapModel;
import com.mybookingpal.dataaccesslayer.dto.IsExpediaInactiveReport;
import com.mybookingpal.utils.entity.IdVersion;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductMapMapper {

	void update(ChannelProductMapModel tempChannelProductMapModel);

	void updateOrCreateList(@Param("channelProductMapModels") List<ChannelProductMapModel> channelProductMapModels);

	ChannelProductMapModel readBPProduct(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel readBPProductWithoutState(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> readAllBPProduct(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> readAllBPProductNoLimit(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> readAllBPProductNoLimitNoState(ChannelProductMapModel tempChannelProductMapModel);

	Integer readChannelProductID(ChannelProductMapModel tempChannelProductMapModel);

	List<Integer> readProductIDs(@Param("channelId") Integer channelId);

	List<Integer> readChannelIDs(Long productId);

	void createProductMap(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByBPProductIDForTriggerEvent(String productID);

	ChannelProductMapModel findByBPProductAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel checkIdPerChannel(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findDuplicateByBPProductAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByBPProductAndChannelIdForNonAirbnb(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByListingAndChannelIdForNonAirbnb(ChannelProductMapModel tempChannelProductMapModel);

	// CS: lookup properties by their ids
	List<ChannelProductMapModel> findByBPProductsAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByBPProductAndChannelIdWithoutStates(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findInActiveListingsByProductsAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByBPProductsAndChannelIdAndState(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndRoomID(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findChannelProductsMapWithoutConsideringChannelState(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByBPProductChannelIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByBPProductChannelIdAndChannelIdAndChannelRoomId(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findUnavailableProductsByChannelId(Integer channelId);

	Integer findCountOfActivePropertiesForAChannel(ChannelProductMapModel tempChannelProductMapModel);

	void suspendProducts(ChannelProductMapModel tempChannelProductMapModel);

	void blockProducts(List<String> productIds);

	void blockProductByChannelId(ChannelProductMapModel tempChannelProductMapModel);

	void activateProducts(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByChannelProductIDAndChannelID(ChannelProductMapModel tempChannelProductMapModel);

	void suspendProduct(ChannelProductMapModel tempChannelProductMapModel);

	void deleteByProductId(String productId);

	List<ChannelProductMapModel> findByProductId(String productID);

	List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndChannelState(ChannelProductMapModel tempChannelProductMapModel);

	void archiveChannelProductMapForPM(ChannelProductMapModel channelProductMapModel);

	void updateChannelRateId(ChannelProductMapModel channelProductMapModel);

	ChannelProductMapModel findByBPProductAndChannelForExpedia(ChannelProductMapModel channelProductMapModel);

	ChannelProductMapModel findByChannelProductIdAndRateId(ChannelProductMapModel channelProductMapModel);

	ChannelProductMapModel findByChannelProductIdChannelIdAndRateId(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> readAllBPProductNoLimitNoStateForSupplier(ChannelProductMapModel tempChannelProductMapModel);

	List<String> getProductsOnChannelForZipCodes(@Param("channelID") Integer channelID, @Param("zipCodes") List<String> zipCodes);

	List<String> findDistributedProductsOnChannel(@Param("supplierId") String supplierID, @Param("channelID") String channelID);

	List<Integer> findCreatedSingleProductsDistributedOnChannel(@Param("supplierId") Integer supplierId, @Param("channelId") Integer channelId);

	List<String> findDistributedCreateAndSuspendedProductsOnChannel(@Param("supplierId") String supplierID, @Param("channelID") String channelID);

	List<Integer> readProductExpediaChannelIDs(Integer productId);

	List<ChannelProductMapModel> findAllKeyChannelProductsByChannelIdAndSupplierId(@Param("supplierId") String supplierID,
			@Param("channelID") String channelID);

	List<ChannelProductMapModel> findForAllProductChildrenById(String productId);

	List<ChannelProductMapModel> findForAllLinkedProducts();

	List<Integer> findAllDistributedProducts(@Param("ids") List<Long> productIds);

	ChannelProductMapModel findByProductIdAndChannelIdWhereStateActive(@Param("productId") String productId, @Param("channelId") String channelID);

	List<ChannelProductMapModel> findByProductIdAndChannelIds(@Param("productId") String productId, @Param("channelIds") List<Integer> channelIds);

	List<ChannelProductMapModel> findByProductIdsAndChannelIds(@Param("productIds") List<Integer> productIds, @Param("channelIds") List<Integer> channelIds);

	Boolean isLosPriceEnable(@Param("productId") String productId, @Param("channelId") String channelID);

	void setLosPriceEnable(@Param("productId") String productId, @Param("channelId") String channelID, @Param("losPriceEnable") Boolean losPriceEnable);

	ChannelProductMapModel readByProductIdAndChannelId(@Param("productId") Integer productId, @Param("channelId") Integer channelId);

	ChannelProductMapModel readById(@Param("id") Long id);

	ChannelProductMapModel readByChannelProductIdAndChannelId(@Param("channelProductId") String channelProductId, @Param("channelId") Integer channelId,
			@Param("state") Integer state);

	void delete(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> getByProductIds(@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId);

	ChannelProductMapModel getMarriottProduct(@Param("productId") Integer productId);

	List<Integer> getMarriottProductsIdsWithJustAddedFlagAfterDays(@Param("days") Integer days);

	List<Integer> checkIfProductConnectToChannel(@Param("productId") Integer productId, @Param("abbreviations") List<String> abbreviations);

	List<ChannelProductMapModel> readListByChannelId(@Param("channelId") Integer channelId);

	List<ChannelProductMapModel> readAllBPProductWithOutState(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findDuplicatesByChannelId(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> findCompleteDuplicateListByProductAndChannelId(ChannelProductMapModel channelProductMapModel);

	Integer findCountByProductIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findActiveRoomsOfHotel(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findAllMLTbyParentAndChannelId(@Param("parentId") Integer parentId, @Param("channelId") String channelId);

	// CS: lookup properties by their ids
	List<ChannelProductMapModel> findByProductsForExpedia(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByProductIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> findByChannelProductIDAndChannelIDWithRoomIdNotNull(ChannelProductMapModel tempChannelProductMapModel);

	List<Integer> findChannelIdsBySupplierId(String supplierId);

	List<ChannelProductMapModel> findActiveKeyChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findInActiveKeyChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findSuspendedKeyChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findActiveMLTChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findInActiveMLTChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findAllMLTChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<ChannelProductMapModel> findSuspendedMLTChannelProductsByChannelIdAndSupplierId(NameId nameId);

	List<String> productIdsBySupplier(@Param("supplierId") String supplierId, @Param("channelId") String channelId);

	List<ChannelProductMapModel> getBySupplierIdAndChannelId(@Param("supplierId") String supplierId, @Param("channelId") String channelId);

	List<ChannelProductMapModel> findByReport(ChannelProductMapModel tempChannelProductMapModel);

	List<String> getAllChannelProductIdsForChannel(@Param("list") List<String> channelProductIds, @Param("channelId") Integer channelId);

	List<ChannelProductMapModel> getAllProductByChannel(@Param("channelId") Integer channelId);

	List<ChannelProductMapModel> getAllProductBy2Channel(@Param("channelId1") Integer channelId1, @Param("channelId2") Integer channelId2);

	List<ChannelProductMapModel> getAllProductPerPmListByChannel(@Param("list") List<String> pmList, @Param("channelId") Integer channelId);

	ChannelProductMapModel readByProductIdChannelProductIdChannelRoomId(ChannelProductMapModel tempChannelProductMapModel);

	ChannelProductMapModel findByProductIdChannelIdStateAndChannelState(ChannelProductMapModel tempChannelProductMapModel);

	void blockChannelProducts(List<String> productIds);

	ChannelProductMapModel findByBPProductIdAndChannelIdAndState(ChannelProductMapModel tempChannelProductMapModel);

	List<ChannelProductMapModel> getAllProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> getAllActiveProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);

	ChannelProductMapModel getActiveChannelProductMapByProductByChannel(@Param("productId") String productId, @Param("channelId") Integer channelId);

	List<Integer> getChannelIdsByProductId(@Param("productId") String productId);

	List<Integer> getChannelIdsBySupplierId(@Param("supplierId") String supplierId);

	List<String> getAllChannelProductIdsForChannelId(@Param("channelID") Integer channelId);

	List<ChannelProductMapModel> findByListingAndChannelIDdisregardingState(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> findActiveListingByChannelId(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(@Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> findAllChannelProductsByChannelIdAndSupplierId(NameId action);

	List<ChannelProductMapModel> getAllActiveProductsByChannel(@Param("channelId") Integer channelId);

	List<String> getActiveSupplierIdsByChannel(@Param("channelId") Integer channelId);

	List<ChannelProductMapModel> findByChannelIdsProductIds(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds);

	List<String> findAllMLTChannelProductsByChannelIdAndParentList(@Param("ownList") List<String> ownList, @Param("channelId") String channelId);

	String findReviewStatusByProductID(@Param("productId") Integer productId, @Param("channelId") Integer channelId);

	List<ChannelProductMapModel> findAllSwithoverPropertiesBySupplierID(@Param("supplierId") Integer supplierId, @Param("channelId") Integer channelId);

	ChannelProductMapModel findByProductIdAndChannelAbbreviation(@Param("productId") Integer productId, @Param("abbreviation") String abbreviation);

	Integer findChannelProductIdByProductOwnerIdAndChannelId(ChannelProductMapModel channelProductMapModel);

	List<ChannelProductMapModel> findByReviewStatusAndChannelIDForSupplierApi(@Param("reviewStatus") String reviewStatus,
			@Param("channelId") Integer channelId);

	List<IsExpediaInactiveReport> allPropertiesOnExpedia();

	List<IdVersion> getMaxVersions(@Param("productIds") List<Integer> productIds);

	void updateWithoutNullCheck(ChannelProductMapModel channelProductMapModel);

	void create(ChannelProductMapModel channelProductMapModel);
}
