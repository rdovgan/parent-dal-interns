package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductSettingModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductSettingsMapper {

	ChannelProductSettingModel readByProductIdAndChannelIdAndType(ChannelProductSettingModel channelProductSettingModel);

	List<ChannelProductSettingModel> read(ChannelProductSettingModel example);

	List<ChannelProductSettingModel> readListOfRBBPassThroughSettings(@Param("productIds") List<Integer> productIds);

	void delete(ChannelProductSettingModel channelProductSettingModel);

	void update(ChannelProductSettingModel channelProductSettingModel);

	void create(ChannelProductSettingModel action);

	void insertList(@Param("list") List<ChannelProductSettingModel> channelProductSettingModels);
}
