package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductTextSettingsModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductTextSettingsMapper {

	void create(ChannelProductTextSettingsModel channelProductTextSettings);

	ChannelProductTextSettingsModel read(ChannelProductTextSettingsModel channelProductTextSettings);

	List<ChannelProductTextSettingsModel> readByProductId(@Param("productId") Integer productId);

	void deleteByProductChannelLocalization(@Param("productId") Integer productId, @Param("channelId") Integer channelId,
			@Param("localization") String localization);

	void deleteByProduct(@Param("productId") Integer productId);

	void update(ChannelProductTextSettingsModel channelProductTextSettings);

	void insertList(@Param("list") List<ChannelProductTextSettingsModel> channelProductTextSettings);
}
