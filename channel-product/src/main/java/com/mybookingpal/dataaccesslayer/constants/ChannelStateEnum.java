package com.mybookingpal.dataaccesslayer.constants;

public enum ChannelStateEnum {
	CHANNEL_STATE_INACTIVE(0),
	CHANNEL_STATE_ACTIVE(1),
	CHANNEL_STATE_SUSPENDED(2),
	CHANNEL_STATE_REMOVED(3),
	CHANNEL_STATE_REJECTED(4),
	CHANNEL_STATE_SWITCHOVER(5),
	CHANNEL_STATE_REMOVED_HA(6),
	CHANNEL_STATE_AUTHENTICATION_FAILED(7);

	Integer channelState;

	ChannelStateEnum(Integer channelStateValue) {
		channelState = channelStateValue;
	}

	public Integer getIntegerValue() {
		return channelState;
	}

	public static ChannelStateEnum getByInt(Integer value) {
		for (ChannelStateEnum v : values()) {
			if (v.channelState.equals(value)) {
				return v;
			}
		}
		return null;
	}
}
