package com.mybookingpal.dataaccesslayer.constants;

public enum BuildTypeEnum {
	A("A"), // API
	S("S"), // Switchover
	B("B"); // Bulk

	private String value;

	BuildTypeEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static BuildTypeEnum getByStr(String value) {
		for (BuildTypeEnum v : values()) {
			if (v.value.equals(value)) {
				return v;
			}
		}
		return null;
	}

	public static String getNameByStr(String value) {
		return getByStr(value).name();
	}
}
