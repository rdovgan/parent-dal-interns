package com.mybookingpal.dataaccesslayer.constants;

public enum PortalState {
	New(0),
	Pending(1),
	Approved(2),
	Listed(3),
	Delisted(4),
	TemporarilyRejected(5),
	PermanentlyRejected(6),
	NotDestributed(7);

	private Integer id;

	PortalState(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
}
