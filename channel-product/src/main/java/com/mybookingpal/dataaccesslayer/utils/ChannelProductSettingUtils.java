package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductSettingModel;
import org.springframework.stereotype.Component;

@Component
public class ChannelProductSettingUtils {

	public static ChannelProductSettingModel getChannelProductSettingModel(Integer productId, Integer channelId, Integer type) {
		ChannelProductSettingModel channelProductSettingModel = new ChannelProductSettingModel();
		channelProductSettingModel.setProductId(productId);
		channelProductSettingModel.setChannelId(channelId);
		channelProductSettingModel.setType(type);
		return channelProductSettingModel;
	}
}
