package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductTextSettingsModel;
import org.springframework.stereotype.Component;

@Component
public class ChannelProductTextSettingsUtils {

	public static ChannelProductTextSettingsModel getChannelProductTextSettingsModel(Integer productId, Integer channelId, String localization) {
		ChannelProductTextSettingsModel channelProductTextSettingsModel = new ChannelProductTextSettingsModel();
		channelProductTextSettingsModel.setProductId(productId);
		channelProductTextSettingsModel.setChannelId(channelId);
		channelProductTextSettingsModel.setLocalization(localization);
		return channelProductTextSettingsModel;
	}

	public static ChannelProductTextSettingsModel getChannelProductTextSettingsModel(Integer productId, Integer channelId) {
		ChannelProductTextSettingsModel channelProductTextSettingsModel = new ChannelProductTextSettingsModel();
		channelProductTextSettingsModel.setProductId(productId);
		channelProductTextSettingsModel.setChannelId(channelId);
		return channelProductTextSettingsModel;
	}
}
