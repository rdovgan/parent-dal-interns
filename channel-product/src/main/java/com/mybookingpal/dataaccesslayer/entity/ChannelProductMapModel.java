package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.BuildTypeEnum;
import com.mybookingpal.dataaccesslayer.constants.ChannelStateEnum;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Component
@Scope(value = "prototype")
public class ChannelProductMapModel {

	protected Long id;
	protected String productId;
	protected String channelProductId;
	protected String channelRoomId;
	protected Integer channelId;
	protected String channelRateId;
	protected BuildTypeEnum buildType;
	protected String reviewStatus;
	protected String reviewResult;
	protected String ratePlanType;
	protected Boolean losPriceEnable;
	protected Integer portalState;
	protected Integer portalPriorState;
	protected LocalDateTime portalApprovedDate;
	protected LocalDateTime portalRejectedDate;
	protected String portalRejectedReason;
	protected String channelPropertyType;
	protected Integer syncType;
	protected Date created;
	protected Date version;
	// CS: list of product ids
	// used in the in clause of the query
	protected List<String> list;
	protected Boolean state = true;
	protected Integer staging;

	protected ChannelStateEnum channelState = ChannelStateEnum.CHANNEL_STATE_INACTIVE;// 0-Inactive 1-Active 2-Suspended(availability is blocked but still listed)

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String bpProductId) {
		this.productId = bpProductId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChannelRoomId() {
		return channelRoomId;
	}

	public void setChannelRoomId(String channelRoomId) {
		this.channelRoomId = channelRoomId;
	}

	public String getChannelRateId() {
		return channelRateId;
	}

	public void setChannelRateId(String channelRateId) {
		this.channelRateId = channelRateId;
	}

	public Boolean isState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public ChannelStateEnum getChannelState() {
		return channelState;
	}

	public void setChannelState(ChannelStateEnum channelState) {
		this.channelState = channelState;
	}

	public BuildTypeEnum getBuildType() {
		return buildType;
	}

	public void setBuildType(BuildTypeEnum a) {
		this.buildType = a;
	}

	public String getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getReviewResult() {
		return reviewResult;
	}

	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult;
	}

	public String getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(String ratePlanType) {
		this.ratePlanType = ratePlanType;
	}

	public String getChannelPropertyType() {
		return channelPropertyType;
	}

	public void setChannelPropertyType(String channelPropertyType) {
		this.channelPropertyType = channelPropertyType;
	}

	public Integer getSyncType() {
		return syncType;
	}

	public void setSyncType(Integer syncType) {
		this.syncType = syncType;
	}

	public boolean equals(Object obj) {
		return (obj instanceof ChannelProductMapModel && this.getProductId().equals(((ChannelProductMapModel) obj).getProductId()) && this.getChannelId()
				.equals(((ChannelProductMapModel) obj).getChannelId()));
	}

	public int hashCode() {
		return (this.getProductId().hashCode() + this.getChannelId().hashCode());
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Boolean isLosPriceEnable() {
		return losPriceEnable;
	}

	public void setLosPriceEnable(Boolean losPriceEnable) {
		this.losPriceEnable = losPriceEnable;
	}

	public Integer getPortalState() {
		return portalState;
	}

	public void setPortalState(Integer portalState) {
		this.portalState = portalState;
	}

	public Integer getPortalPriorState() {
		return portalPriorState;
	}

	public void setPortalPriorState(Integer portalPriorState) {
		this.portalPriorState = portalPriorState;
	}

	public LocalDateTime getPortalApprovedDate() {
		return portalApprovedDate;
	}

	public void setPortalApprovedDate(LocalDateTime portalApprovedDate) {
		this.portalApprovedDate = portalApprovedDate;
	}

	public LocalDateTime getPortalRejectedDate() {
		return portalRejectedDate;
	}

	public void setPortalRejectedDate(LocalDateTime portalRejectedDate) {
		this.portalRejectedDate = portalRejectedDate;
	}

	public String getPortalRejectedReason() {
		return portalRejectedReason;
	}

	public void setPortalRejectedReason(String portalRejectedReason) {
		this.portalRejectedReason = portalRejectedReason;
	}

	public Integer getStaging() {
		return staging;
	}

	public void setStaging(Integer staging) {
		this.staging = staging;
	}

	public String toString() {
		return ("Product Id: " + this.getProductId() + " Channel Id " + this.getChannelId() + " Channel Product Id " + this.getChannelProductId()
				+ " Channel Room Id " + this.getChannelRoomId() + " Channel Rate Id " + this.getChannelRateId() + " Channel State " + this.getChannelState()
				+ " State " + this.isState() + " Los price enable " + this.isLosPriceEnable());
	}

}
