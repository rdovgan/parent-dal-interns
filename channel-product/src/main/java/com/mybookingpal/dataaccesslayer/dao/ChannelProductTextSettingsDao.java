package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductTextSettingsModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelProductTextSettingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelProductTextSettingsDao {

	@Autowired
	private ChannelProductTextSettingsMapper channelProductTextSettingsMapper;

	public void create(ChannelProductTextSettingsModel channelProductTextSettings) {
		if (Objects.isNull(channelProductTextSettings)) {
			return;
		}
		channelProductTextSettingsMapper.create(channelProductTextSettings);
	}

	public ChannelProductTextSettingsModel read(ChannelProductTextSettingsModel channelProductTextSettings) {
		if (Objects.isNull(channelProductTextSettings)) {
			return null;
		}
		return channelProductTextSettingsMapper.read(channelProductTextSettings);
	}

	public List<ChannelProductTextSettingsModel> readByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductTextSettingsMapper.readByProductId(productId);
	}

	public void deleteByProductChannelLocalization(Integer productId, Integer channelId, String localization) {
		if (Objects.isNull(productId) || Objects.isNull(channelId) || Objects.isNull(localization)) {
			return;
		}
		channelProductTextSettingsMapper.deleteByProductChannelLocalization(productId, channelId, localization);
	}

	public void deleteByProduct(Integer productId) {
		if (Objects.isNull(productId)) {
			return;
		}
		channelProductTextSettingsMapper.deleteByProduct(productId);
	}

	public void update(ChannelProductTextSettingsModel channelProductTextSettings) {
		if (Objects.isNull(channelProductTextSettings)) {
			return;
		}
		channelProductTextSettingsMapper.update(channelProductTextSettings);
	}

	public void insertList(List<ChannelProductTextSettingsModel> channelProductTextSettings) {
		if (Objects.isNull(channelProductTextSettings)) {
			return;
		}
		channelProductTextSettingsMapper.insertList(channelProductTextSettings);
	}
}
