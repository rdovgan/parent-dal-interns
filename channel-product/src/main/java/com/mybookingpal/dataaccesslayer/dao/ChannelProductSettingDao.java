package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductSettingModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelProductSettingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelProductSettingDao {

	@Autowired
	private ChannelProductSettingsMapper channelProductSettingsMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductSettingModel readByProductIdAndChannelIdAndType(ChannelProductSettingModel channelProductSettingModel) {
		if (Objects.isNull(channelProductSettingModel)) {
			return null;
		}
		return channelProductSettingsMapper.readByProductIdAndChannelIdAndType(channelProductSettingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductSettingModel> read(ChannelProductSettingModel example) {
		if (Objects.isNull(example)) {
			return Collections.emptyList();
		}
		return channelProductSettingsMapper.read(example);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductSettingModel> readListOfRBBPassThroughSettings(List<Integer> productIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}
		return channelProductSettingsMapper.readListOfRBBPassThroughSettings(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(ChannelProductSettingModel channelProductSettingModel) {
		if (Objects.isNull(channelProductSettingModel)) {
			return;
		}
		channelProductSettingsMapper.delete(channelProductSettingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelProductSettingModel channelProductSettingModel) {
		if (Objects.isNull(channelProductSettingModel)) {
			return;
		}
		channelProductSettingsMapper.update(channelProductSettingModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelProductSettingModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		channelProductSettingsMapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<ChannelProductSettingModel> channelProductSettingModels) {
		if (Objects.isNull(channelProductSettingModels)) {
			return;
		}
		channelProductSettingsMapper.insertList(channelProductSettingModels);
	}
}
