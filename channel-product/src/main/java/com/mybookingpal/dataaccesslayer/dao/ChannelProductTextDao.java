package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductTextModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelProductTextMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelProductTextDao {

	@Autowired
	private ChannelProductTextMapper channelProductTextMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getByProductId(Integer productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getByProdcutIdAndTypeTextAndChanelIds(Integer productId, List<Integer> channelIds, Integer typeText) {
		if (Objects.isNull(productId) || Objects.isNull(channelIds) || Objects.isNull(typeText)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getByProdcutIdAndTypeTextAndChanelIds(productId, channelIds, typeText);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getByProdcutIdAndTypeTextAndChanelIdsAndLanguages(Integer productId, List<Integer> channelIds, Integer typeText,
			List<String> languages) {
		if (Objects.isNull(productId) || Objects.isNull(channelIds) || Objects.isNull(typeText) || Objects.isNull(languages)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getByProdcutIdAndTypeTextAndChanelIdsAndLanguages(productId, channelIds, typeText, languages);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<ChannelProductTextModel> channelProductTextModelList) {
		if (Objects.isNull(channelProductTextModelList)) {
			return;
		}
		channelProductTextMapper.insertList(channelProductTextModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelProductTextModel channelProductTextModelList) {
		if (Objects.isNull(channelProductTextModelList)) {
			return;
		}
		channelProductTextMapper.update(channelProductTextModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteList(List<ChannelProductTextModel> idList) {
		if (Objects.isNull(idList)) {
			return;
		}
		channelProductTextMapper.deleteList(idList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getHeadlineTextByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return null;
		}
		return channelProductTextMapper.getHeadlineTextByProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getByProductIdAndChannelId(Integer productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getByProductIdAndChannelId(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getListByProductChannelLanguage(Integer productId, Integer channelId, String language) {
		if (Objects.isNull(productId) || Objects.isNull(channelId) || Objects.isNull(language)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getListByProductChannelLanguage(productId, channelId, language);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertAllList(List<ChannelProductTextModel> channelProductTextModelList) {
		if (Objects.isNull(channelProductTextModelList)) {
			return;
		}
		channelProductTextMapper.insertAllList(channelProductTextModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateToFinale(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return;
		}
		channelProductTextMapper.updateToFinale(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateListToFinal(List<Integer> ids) {
		if (Objects.isNull(ids)) {
			return;
		}
		channelProductTextMapper.updateListToFinal(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getListByProductsChannelLanguage(List<Integer> products, Integer channelId, String language) {
		if (Objects.isNull(products) || Objects.isNull(channelId) || Objects.isNull(language)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getListByProductsChannelLanguage(products, channelId, language);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductTextModel getProductTextByTypeForMarriott(Integer productId, Integer textType) {
		if (Objects.isNull(productId) || Objects.isNull(textType)) {
			return null;
		}
		return channelProductTextMapper.getProductTextByTypeForMarriott(productId, textType);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readIdsByChannelProductTextList(List<ChannelProductTextModel> channelProductTextModelList) {
		if (Objects.isNull(channelProductTextModelList)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.readIdsByChannelProductTextList(channelProductTextModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductTextModel getProductTextByType(Integer productId, Integer textType, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(textType) || Objects.isNull(channelId)) {
			return null;
		}
		return channelProductTextMapper.getProductTextByType(productId, textType, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getByProductIdAndChannelId(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getByProductIdAndChannelId(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getHeadlineTextsByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getHeadlineTextsByProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getShortDescriptionProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return null;
		}
		return channelProductTextMapper.getShortDescriptionProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getDescriptionByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return null;
		}
		return channelProductTextMapper.getShortDescriptionProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getDisplayNameByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return null;
		}
		return channelProductTextMapper.getDisplayNameByProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getShortDescriptionsByProductIdAndPartyForChannel(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getShortDescriptionsByProductIdAndPartyForChannel(channelProductTextModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductTextModel> getNameByTextType(ChannelProductTextModel channelProductTextModel) {
		if (Objects.isNull(channelProductTextModel)) {
			return Collections.emptyList();
		}
		return channelProductTextMapper.getNameByTextType(channelProductTextModel);
	}
}
