package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductMapModel;
import com.mybookingpal.dataaccesslayer.dto.IsExpediaInactiveReport;
import com.mybookingpal.utils.entity.IdVersion;
import com.mybookingpal.dataaccesslayer.dto.IsProductZipCode9DTO;
import com.mybookingpal.dataaccesslayer.mappers.ChannelProductMapMapper;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelProductMapDao {

	@Autowired
	ChannelProductMapMapper channelProductMapMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.update(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateOrCreateList(List<ChannelProductMapModel> channelProductMapModels) {
		if (Objects.isNull(channelProductMapModels)) {
			return;
		}
		channelProductMapMapper.updateOrCreateList(channelProductMapModels);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel readBPProduct(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.readBPProduct(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel readBPProductWithoutState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.readBPProductWithoutState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> readAllBPProduct(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readAllBPProduct(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> readAllBPProductNoLimit(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readAllBPProductNoLimit(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> readAllBPProductNoLimitNoState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readAllBPProductNoLimitNoState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer readChannelProductID(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.readChannelProductID(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readProductIDs(Integer channelId) {
		return channelProductMapMapper.readProductIDs(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readChannelIDs(Long productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readChannelIDs(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createProductMap(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.createProductMap(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductIDForTriggerEvent(String productID) {
		if (Objects.isNull(productID)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductIDForTriggerEvent(productID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByBPProductAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel checkIdPerChannel(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.checkIdPerChannel(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findDuplicateByBPProductAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findDuplicateByBPProductAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByBPProductAndChannelIdForNonAirbnb(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(channelProductMapMapper)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductAndChannelIdForNonAirbnb(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByListingAndChannelIdForNonAirbnb(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByListingAndChannelIdForNonAirbnb(tempChannelProductMapModel);
	}

	// CS: lookup properties by their ids
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductsAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductsAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductAndChannelIdWithoutStates(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductAndChannelIdWithoutStates(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findInActiveListingsByProductsAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findInActiveListingsByProductsAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductsAndChannelIdAndState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductsAndChannelIdAndState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndRoomID(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductsAndChannelIdAndStateAndRoomID(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findChannelProductsMapWithoutConsideringChannelState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findChannelProductsMapWithoutConsideringChannelState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByBPProductChannelIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductChannelIdAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByBPProductChannelIdAndChannelIdAndChannelRoomId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductChannelIdAndChannelIdAndChannelRoomId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findUnavailableProductsByChannelId(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findUnavailableProductsByChannelId(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer findCountOfActivePropertiesForAChannel(ChannelProductMapModel tempChannelProductMapModel) {
		return channelProductMapMapper.findCountOfActivePropertiesForAChannel(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void suspendProducts(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.suspendProducts(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void blockProducts(List<String> productIds) {
		if (Objects.isNull(productIds)) {
			return;
		}
		channelProductMapMapper.blockProducts(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void blockProductByChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.blockProductByChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void activateProducts(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.activateProducts(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByChannelProductIDAndChannelID(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByChannelProductIDAndChannelID(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void suspendProduct(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return;
		}
		channelProductMapMapper.suspendProduct(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return;
		}
		channelProductMapMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByProductId(String productID) {
		if (Objects.isNull(productID)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductId(productID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByBPProductsAndChannelIdAndStateAndChannelState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByBPProductsAndChannelIdAndStateAndChannelState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void archiveChannelProductMapForPM(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return;
		}
		channelProductMapMapper.archiveChannelProductMapForPM(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateChannelRateId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return;
		}
		channelProductMapMapper.updateChannelRateId(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByBPProductAndChannelForExpedia(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductAndChannelForExpedia(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByChannelProductIdAndRateId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByChannelProductIdAndRateId(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByChannelProductIdChannelIdAndRateId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByChannelProductIdChannelIdAndRateId(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> readAllBPProductNoLimitNoStateForSupplier(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(channelProductMapMapper)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readAllBPProductNoLimitNoStateForSupplier(tempChannelProductMapModel);
	}

	@Deprecated
	public List<String> getProductsOnChannelForZipCodes(IsProductZipCode9DTO productZipCode9Dto) {
		if (Objects.isNull(productZipCode9Dto)) {
			return Collections.emptyList();
		}
		return this.getProductsOnChannelForZipCodes(productZipCode9Dto.getChannelID(), productZipCode9Dto.getZipCodes());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductsOnChannelForZipCodes(Integer channelId, List<String> zipCodes) {
		if (Objects.isNull(channelId) || Objects.isNull(zipCodes)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getProductsOnChannelForZipCodes(channelId, zipCodes);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> findDistributedProductsOnChannel(String supplierID, String channelID) {
		if (Objects.isNull(supplierID) || Objects.isNull(channelID)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findDistributedProductsOnChannel(supplierID, channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> findCreatedSingleProductsDistributedOnChannel(Integer supplierId, Integer channelId) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findCreatedSingleProductsDistributedOnChannel(supplierId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> findDistributedCreateAndSuspendedProductsOnChannel(String supplierID, String channelID) {
		if (Objects.isNull(supplierID) || Objects.isNull(channelID)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findDistributedCreateAndSuspendedProductsOnChannel(supplierID, channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readProductExpediaChannelIDs(Integer productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readProductExpediaChannelIDs(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findAllKeyChannelProductsByChannelIdAndSupplierId(String supplierID, String channelID) {
		if (Objects.isNull(supplierID)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllKeyChannelProductsByChannelIdAndSupplierId(supplierID, channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findForAllProductChildrenById(String productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findForAllProductChildrenById(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findForAllLinkedProducts() {
		return channelProductMapMapper.findForAllLinkedProducts();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> findAllDistributedProducts(List<Long> productIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllDistributedProducts(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel findByProductIdAndChannelIdWhereStateActive(String productId, String channelID) {
		if (Objects.isNull(productId) || Objects.isNull(channelID)) {
			return null;
		}
		return channelProductMapMapper.findByProductIdAndChannelIdWhereStateActive(productId, channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByProductIdAndChannelIds(String productId, List<Integer> channelIds) {
		if (Objects.isNull(productId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdAndChannelIds(productId, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> findByProductIdsAndChannelIds(List<Integer> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdsAndChannelIds(productIds, channelIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean isLosPriceEnable(String productId, String channelID) {
		if (Objects.isNull(productId) || Objects.isNull(channelID)) {
			return null;
		}
		return channelProductMapMapper.isLosPriceEnable(productId, channelID);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setLosPriceEnable(String productId, String channelID, Boolean losPriceEnable) {
		if (Objects.isNull(productId)) {
			return;
		}
		channelProductMapMapper.setLosPriceEnable(productId, channelID, losPriceEnable);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel readByProductIdAndChannelId(Integer productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return null;
		}
		return channelProductMapMapper.readByProductIdAndChannelId(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel readById(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return channelProductMapMapper.readById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel readByChannelProductIdAndChannelId(String channelProductId, Integer channelId, Integer state) {
		if (Objects.isNull(channelProductId) || Objects.isNull(channelId)) {
			return null;
		}
		return channelProductMapMapper.readByChannelProductIdAndChannelId(channelProductId, channelId, state);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return;
		}
		channelProductMapMapper.delete(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> getByProductIds(List<Integer> productIds, Integer channelId) {
		if (Objects.isNull(productIds) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getByProductIds(productIds, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMapModel getMarriottProduct(Integer productId) {
		if (Objects.isNull(productId)) {
			return null;
		}
		return channelProductMapMapper.getMarriottProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> getMarriottProductsIdsWithJustAddedFlagAfterDays(Integer days) {
		if (Objects.isNull(days)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getMarriottProductsIdsWithJustAddedFlagAfterDays(days);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> checkIfProductConnectToChannel(Integer productId, List<String> abbreviations) {
		if (Objects.isNull(productId) || Objects.isNull(abbreviations)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.checkIfProductConnectToChannel(productId, abbreviations);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductMapModel> readListByChannelId(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readListByChannelId(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<ChannelProductMapModel> readAllBPProductWithOutState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.readAllBPProductWithOutState(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<ChannelProductMapModel> findDuplicatesByChannelId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findDuplicatesByChannelId(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<ChannelProductMapModel> findCompleteDuplicateListByProductAndChannelId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findCompleteDuplicateListByProductAndChannelId(channelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	Integer findCountByProductIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findCountByProductIdAndChannelId(tempChannelProductMapModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	List<ChannelProductMapModel> findActiveRoomsOfHotel(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findActiveRoomsOfHotel(tempChannelProductMapModel);
	}

	List<ChannelProductMapModel> findAllMLTbyParentAndChannelId(Integer parentId, String channelId) {
		if (Objects.isNull(parentId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllMLTbyParentAndChannelId(parentId, channelId);
	}

	// CS: lookup properties by their ids
	List<ChannelProductMapModel> findByProductsForExpedia(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductsForExpedia(tempChannelProductMapModel);
	}

	ChannelProductMapModel findByProductIdAndChannelId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByProductIdAndChannelId(tempChannelProductMapModel);
	}

	List<ChannelProductMapModel> findByChannelProductIDAndChannelIDWithRoomIdNotNull(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByChannelProductIDAndChannelIDWithRoomIdNotNull(tempChannelProductMapModel);
	}

	List<Integer> findChannelIdsBySupplierId(String supplierId) {
		if (Objects.isNull(supplierId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findChannelIdsBySupplierId(supplierId);
	}

	List<ChannelProductMapModel> findActiveKeyChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findActiveKeyChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findInActiveKeyChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findActiveKeyChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findSuspendedKeyChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findSuspendedKeyChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findActiveMLTChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findActiveMLTChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findInActiveMLTChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findInActiveMLTChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findAllMLTChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllMLTChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<ChannelProductMapModel> findSuspendedMLTChannelProductsByChannelIdAndSupplierId(NameId nameId) {
		if (Objects.isNull(nameId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findSuspendedMLTChannelProductsByChannelIdAndSupplierId(nameId);
	}

	List<String> productIdsBySupplier(String supplierId, String channelId) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.productIdsBySupplier(supplierId, channelId);
	}

	List<ChannelProductMapModel> getBySupplierIdAndChannelId(String supplierId, String channelId) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getBySupplierIdAndChannelId(supplierId, channelId);
	}

	List<ChannelProductMapModel> findByReport(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByReport(tempChannelProductMapModel);
	}

	List<String> getAllChannelProductIdsForChannel(List<String> channelProductIds, Integer channelId) {
		if (Objects.isNull(channelProductIds) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllChannelProductIdsForChannel(channelProductIds, channelId);
	}

	List<ChannelProductMapModel> getAllProductByChannel(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllProductByChannel(channelId);
	}

	List<ChannelProductMapModel> getAllProductBy2Channel(Integer channelId1, Integer channelId2) {
		if (Objects.isNull(channelId1) || Objects.isNull(channelId2)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllProductBy2Channel(channelId1, channelId2);
	}

	List<ChannelProductMapModel> getAllProductPerPmListByChannel(List<String> pmList, Integer channelId) {
		if (Objects.isNull(pmList) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllProductPerPmListByChannel(pmList, channelId);
	}

	ChannelProductMapModel readByProductIdChannelProductIdChannelRoomId(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.readByProductIdChannelProductIdChannelRoomId(tempChannelProductMapModel);
	}

	ChannelProductMapModel findByProductIdChannelIdStateAndChannelState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByProductIdChannelIdStateAndChannelState(tempChannelProductMapModel);
	}

	void blockChannelProducts(List<String> productIds) {
		if (Objects.isNull(productIds)) {
			return;
		}
		channelProductMapMapper.blockChannelProducts(productIds);
	}

	ChannelProductMapModel findByBPProductIdAndChannelIdAndState(ChannelProductMapModel tempChannelProductMapModel) {
		if (Objects.isNull(tempChannelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findByBPProductIdAndChannelIdAndState(tempChannelProductMapModel);
	}

	List<ChannelProductMapModel> getAllProductByPmAndByChannel(String supplierId, List<Integer> channelIds) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllProductByPmAndByChannel(supplierId, channelIds);
	}

	List<ChannelProductMapModel> getAllActiveProductByPmAndByChannel(String supplierId, List<Integer> channelIds) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllProductByPmAndByChannel(supplierId, channelIds);
	}

	ChannelProductMapModel getActiveChannelProductMapByProductByChannel(String productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return null;
		}
		return channelProductMapMapper.getActiveChannelProductMapByProductByChannel(productId, channelId);
	}

	List<Integer> getChannelIdsByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getChannelIdsByProductId(productId);
	}

	List<Integer> getChannelIdsBySupplierId(String supplierId) {
		if (Objects.isNull(supplierId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getChannelIdsBySupplierId(supplierId);
	}

	List<String> getAllChannelProductIdsForChannelId(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllChannelProductIdsForChannelId(channelId);
	}

	List<ChannelProductMapModel> findByListingAndChannelIDdisregardingState(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByListingAndChannelIDdisregardingState(channelProductMapModel);
	}

	List<ChannelProductMapModel> findActiveListingByChannelId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findActiveListingByChannelId(channelProductMapModel);
	}

	List<ChannelProductMapModel> findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(List<Integer> channelIds) {
		if (Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(channelIds);
	}

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(productIds, channelIds);
	}

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(productIds, channelIds);
	}

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(productIds, channelIds);
	}

	List<ChannelProductMapModel> findAllChannelProductsByChannelIdAndSupplierId(NameId action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllChannelProductsByChannelIdAndSupplierId(action);
	}

	List<ChannelProductMapModel> getAllActiveProductsByChannel(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getAllActiveProductsByChannel(channelId);
	}

	List<String> getActiveSupplierIdsByChannel(Integer channelId) {
		if (Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getActiveSupplierIdsByChannel(channelId);
	}

	List<ChannelProductMapModel> findByChannelIdsProductIds(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByChannelIdsProductIds(productIds, channelIds);
	}

	List<ChannelProductMapModel> findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(List<String> productIds, List<Integer> channelIds) {
		if (Objects.isNull(productIds) || Objects.isNull(channelIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(productIds, channelIds);
	}

	List<String> findAllMLTChannelProductsByChannelIdAndParentList(List<String> ownList, String channelId) {
		if (Objects.isNull(ownList) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllMLTChannelProductsByChannelIdAndParentList(ownList, channelId);
	}

	String findReviewStatusByProductID(Integer productId, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(channelId)) {
			return null;
		}
		return channelProductMapMapper.findReviewStatusByProductID(productId, channelId);
	}

	List<ChannelProductMapModel> findAllSwithoverPropertiesBySupplierID(Integer supplierId, Integer channelId) {
		if (Objects.isNull(supplierId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findAllSwithoverPropertiesBySupplierID(supplierId, channelId);
	}

	ChannelProductMapModel findByProductIdAndChannelAbbreviation(Integer productId, String abbreviation) {
		if (Objects.isNull(productId) || Objects.isNull(abbreviation)) {
			return null;
		}
		return channelProductMapMapper.findByProductIdAndChannelAbbreviation(productId, abbreviation);
	}

	Integer findChannelProductIdByProductOwnerIdAndChannelId(ChannelProductMapModel channelProductMapModel) {
		if (Objects.isNull(channelProductMapModel)) {
			return null;
		}
		return channelProductMapMapper.findChannelProductIdByProductOwnerIdAndChannelId(channelProductMapModel);
	}

	List<ChannelProductMapModel> findByReviewStatusAndChannelIDForSupplierApi(String reviewStatus, Integer channelId) {
		if (Objects.isNull(reviewStatus) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.findByReviewStatusAndChannelIDForSupplierApi(reviewStatus, channelId);
	}

	List<IsExpediaInactiveReport> allPropertiesOnExpedia() {
		return channelProductMapMapper.allPropertiesOnExpedia();
	}

	List<IdVersion> getMaxVersions(List<Integer> productIds) {
		if (Objects.isNull(productIds)) {
			return Collections.emptyList();
		}
		return channelProductMapMapper.getMaxVersions(productIds);
	}
}
