package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.ChannelStateEnum;

public interface IsExpediaInactiveReport {
	Integer getId();

	String getPropertyName();

	String getState();

	String getChannelProductId();

	Integer getSupplierId();

	String getPmName();

	ChannelStateEnum getStateCPM();

	ChannelStateEnum getChannelState();

	Integer setId();

	String setPropertyName();

	String setState();

	String setChannelProductId();

	Integer setSupplierId();

	String setPmName();

	ChannelStateEnum setStateCPM();

	ChannelStateEnum setChannelState();
}
