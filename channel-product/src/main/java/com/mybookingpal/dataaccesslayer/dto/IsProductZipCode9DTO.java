package com.mybookingpal.dataaccesslayer.dto;

import java.util.List;

public interface IsProductZipCode9DTO {
	Integer getChannelID();

	List<String> getZipCodes();
}
