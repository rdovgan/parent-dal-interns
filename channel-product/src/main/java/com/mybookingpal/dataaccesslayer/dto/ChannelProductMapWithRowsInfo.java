package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductMapModel;

public class ChannelProductMapWithRowsInfo extends ChannelProductMapModel {

	private Integer offsetrows;
	private Integer numrows;

	public Integer getOffsetrows() {
		return offsetrows;
	}

	public void setOffsetrows(Integer offsetrows) {
		this.offsetrows = offsetrows;
	}

	public Integer getNumrows() {
		return numrows;
	}

	public void setNumrows(Integer numrows) {
		this.numrows = numrows;
	}
}
