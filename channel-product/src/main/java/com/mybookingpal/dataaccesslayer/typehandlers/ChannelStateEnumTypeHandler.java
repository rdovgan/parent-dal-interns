package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.ChannelStateEnum;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChannelStateEnumTypeHandler implements TypeHandler<ChannelStateEnum> {
	@Override
	public void setParameter(PreparedStatement preparedStatement, int i, ChannelStateEnum channelStateEnum, JdbcType jdbcType) throws SQLException {
		preparedStatement.setString(i, String.valueOf(channelStateEnum.getIntegerValue()));
	}

	@Override
	public ChannelStateEnum getResult(ResultSet resultSet, String s) throws SQLException {
		int retrievedState = resultSet.getInt(s);
		for (ChannelStateEnum state : ChannelStateEnum.values()) {
			if (retrievedState == state.getIntegerValue()) {
				return state;
			}
		}
		return null;
	}

	@Override
	public ChannelStateEnum getResult(ResultSet resultSet, int i) throws SQLException {
		int retrievedState = resultSet.getInt(i);
		for (ChannelStateEnum state : ChannelStateEnum.values()) {
			if (retrievedState == state.getIntegerValue()) {
				return state;
			}
		}
		return null;
	}

	@Override
	public ChannelStateEnum getResult(CallableStatement callableStatement, int i) throws SQLException {
		int retrievedState = callableStatement.getInt(i);
		for (ChannelStateEnum state : ChannelStateEnum.values()) {
			if (retrievedState == state.getIntegerValue()) {
				return state;
			}
		}
		return null;
	}
}
