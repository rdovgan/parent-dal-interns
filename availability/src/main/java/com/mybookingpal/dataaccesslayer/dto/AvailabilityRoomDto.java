package com.mybookingpal.dataaccesslayer.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class AvailabilityRoomDto {

	private String communityName;
	private String unitName;
	private String customName;
	private Date date;
	private BigDecimal publishedRate;
	private String currency;
	private Integer availableCount;
	private Long productId;
	private List<AvailabilityRoomDto> calendarList;
	private String senderLogo;

	public String getSenderLogo() {
		return senderLogo;
	}

	public void setSenderLogo(String senderLogoLink) {
		this.senderLogo = senderLogoLink;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public List<com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto> getCalendarList() {
		return calendarList;
	}

	public void setCalendarList(List<com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto> calendarList) {
		this.calendarList = calendarList;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPublishedRate() {
		return publishedRate;
	}

	public void setPublishedRate(BigDecimal publishedRate) {
		this.publishedRate = publishedRate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(Integer availableCount) {
		this.availableCount = availableCount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto that = (com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto) o;
		return new EqualsBuilder().append(date, that.date).append(productId, that.productId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(date).append(productId).toHashCode();
	}
}
