package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityNotificationModel;
import com.mybookingpal.dataaccesslayer.mappers.AvailabilityNotificationMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class AvailabilityNotificationDao {

	private final AvailabilityNotificationMapper availabilityNotificationMapper;

	public AvailabilityNotificationDao(AvailabilityNotificationMapper availabilityNotificationMapper) {
		this.availabilityNotificationMapper = availabilityNotificationMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insert(List<AvailabilityNotificationModel> insertList) {
		if (!CollectionUtils.isEmpty(insertList)) {
			availabilityNotificationMapper.insert(insertList);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityNotificationModel> getAllNotificationForPropertyManager(String id) {
		if (StringUtils.isBlank(id)) {
			return Collections.emptyList();
		}
		return availabilityNotificationMapper.getAllNotificationForPropertyManager(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> getEmailsSentOnlyWhereAvailableCountEqualsZero(Long pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return availabilityNotificationMapper.getEmailsSentOnlyWhereAvailableCountEqualsZero(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> getAlreadySentWhereAvailableCountEqualsZeroForPm(Long pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return availabilityNotificationMapper.getAlreadySentWhereAvailableCountEqualsZeroForPm(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> getAllExistsNotificationForCalendar(Long pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return availabilityNotificationMapper.getAllExistsNotificationForCalendar(pmId);
	}
}
