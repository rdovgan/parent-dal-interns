package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityCalendarModel;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityCalendarSimpleModel;
import com.mybookingpal.dataaccesslayer.entity.IsLowInventoryRoom;
import com.mybookingpal.dataaccesslayer.mappers.AvailabilityCalendarMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class AvailabilityCalendarDao {

	private final AvailabilityCalendarMapper availabilityCalendarMapper;

	public AvailabilityCalendarDao(AvailabilityCalendarMapper availabilityCalendarMapper) {
		this.availabilityCalendarMapper = availabilityCalendarMapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel read(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return availabilityCalendarMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(AvailabilityCalendarModel availabilityCalendarModel) {
		if (!Objects.isNull(availabilityCalendarModel)) {
			availabilityCalendarMapper.create(availabilityCalendarModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(AvailabilityCalendarModel availabilityCalendarModel) {
		if (!Objects.isNull(availabilityCalendarModel)) {
			availabilityCalendarMapper.update(availabilityCalendarModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(List<Long> idList) {
		if (!CollectionUtils.isEmpty(idList)) {
			availabilityCalendarMapper.delete(idList);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodByProductIds(List<Integer> productIdList, Date fromDate,
			Date toDate) {
		if (CollectionUtils.isEmpty(productIdList) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.readForPeriodByProductIds(productIdList, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodByProductIds(List<Integer> productIdList, LocalDate fromDate,
			LocalDate toDate) {
		if (CollectionUtils.isEmpty(productIdList) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.readForPeriodByProductIds(productIdList, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIds(List<Integer> productIdList,
			Date fromDate, Date toDate) {
		if (CollectionUtils.isEmpty(productIdList) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.readForPeriodForAllStateByProductIds(productIdList, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodByProductIdsByVersion(List<Integer> productIdList,
			Date fromDate, Date toDate, Date version) {
		if (CollectionUtils.isEmpty(productIdList) || Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects
				.isNull(version)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.readForPeriodByProductIdsByVersion(productIdList, fromDate, toDate, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIdsByVersion(List<Integer> productIdList,
			Date fromDate, Date toDate, Date version) {
		if (CollectionUtils.isEmpty(productIdList) || Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects
				.isNull(version)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper
				.readForPeriodForAllStateByProductIdsByVersion(productIdList, fromDate, toDate, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarSimpleModel> readForPeriodForAllStateBySupplierIdByVersion(String supplierId,
			Date fromDate, Date toDate, Date version, Integer channelId) {
		if (StringUtils.isBlank(supplierId) || Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects
				.isNull(version) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper
				.readForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, version, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertUpdateFromList(List<AvailabilityCalendarModel> models) {
		if (!CollectionUtils.isEmpty(models)) {
			availabilityCalendarMapper.insertUpdateFromList(models);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelAvailabilityCalendarlist(List<AvailabilityCalendarModel> models) {
		if (!CollectionUtils.isEmpty(models)) {
			availabilityCalendarMapper.cancelAvailabilityCalendarlist(models);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAvailabilityCalendarlist(List<AvailabilityCalendarModel> models) {
		if (!CollectionUtils.isEmpty(models)) {
			availabilityCalendarMapper.deleteAvailabilityCalendarlist(models);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertUpdateFromTemplate(AvailabilityCalendarModel templateModel) {
		if (!Objects.isNull(templateModel)) {
			availabilityCalendarMapper.insertUpdateFromTemplate(templateModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> getCalendarByPropertyManagerForReport(Long pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.getCalendarByPropertyManagerForReport(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> readAvailabilityCalendarByRoomConfiguration(List<IsLowInventoryRoom> rooms,
			Long configurationId, Integer pmId, Integer noUnitCount) {
		if (CollectionUtils.isEmpty(rooms) || Objects.isNull(configurationId) || Objects.isNull(pmId) || Objects
				.isNull(noUnitCount)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper
				.readAvailabilityCalendarByRoomConfiguration(rooms, configurationId, pmId, noUnitCount);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertAvailabilityCalendarModels(List<AvailabilityCalendarModel> availabilityCalendarModelList) {
		if (!CollectionUtils.isEmpty(availabilityCalendarModelList)) {
			availabilityCalendarMapper.insertAvailabilityCalendarModels(availabilityCalendarModelList);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel readByProductIdAndDate(AvailabilityCalendarModel availabilityCalendarModel) {
		if (Objects.isNull(availabilityCalendarModel)) {
			return null;
		}
		return availabilityCalendarMapper.readByProductIdAndDate(availabilityCalendarModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateVersionInAvilabilityCalenderForProductAndDate(
			AvailabilityCalendarModel availabilityCalendarModel) {
		if (!Objects.isNull(availabilityCalendarModel)) {
			availabilityCalendarMapper.updateVersionInAvilabilityCalenderForProductAndDate(availabilityCalendarModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void saveAvailabilityCalendarHistory(List<AvailabilityCalendarModel> models) {
		if (!CollectionUtils.isEmpty(models)) {
			availabilityCalendarMapper.saveAvailabilityCalendarHistory(models);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getAvailableCountForProduct(String productId) {
		if (StringUtils.isBlank(productId)) {
			return null;
		}
		return availabilityCalendarMapper.getAvailableCountForProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readCreatedByProductIds(Integer pmId) {
		if (Objects.isNull(pmId)) {
			return Collections.emptyList();
		}
		return availabilityCalendarMapper.readCreatedByProductIds(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel getLowestActiveBasePriceForProduct(String productId, Date dateForCheck) {
		if (StringUtils.isBlank(productId) || Objects.isNull(dateForCheck)) {
			return null;
		}
		return availabilityCalendarMapper.getLowestActiveBasePriceForProduct(productId, dateForCheck);
	}
}
