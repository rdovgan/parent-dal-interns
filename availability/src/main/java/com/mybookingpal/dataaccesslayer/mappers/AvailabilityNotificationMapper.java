package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityNotificationModel;

import java.util.List;

public interface AvailabilityNotificationMapper {

	void insert(List<AvailabilityNotificationModel> notificationModel);

	List<AvailabilityNotificationModel> getAllNotificationForPropertyManager(String id);

	List<AvailabilityRoomDto> getEmailsSentOnlyWhereAvailableCountEqualsZero(Long pmId);

	List<AvailabilityRoomDto> getAlreadySentWhereAvailableCountEqualsZeroForPm(Long pmId);

	List<AvailabilityRoomDto> getAllExistsNotificationForCalendar(Long pmId);
}
