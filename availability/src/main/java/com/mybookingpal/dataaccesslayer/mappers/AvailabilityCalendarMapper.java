package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.AvailabilityRoomDto;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityCalendarModel;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityCalendarSimpleModel;
import com.mybookingpal.dataaccesslayer.entity.IsLowInventoryRoom;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface AvailabilityCalendarMapper {

	AvailabilityCalendarModel read(Long id);

	void create(AvailabilityCalendarModel availabilityCalendarModel);

	void update(AvailabilityCalendarModel availabilityCalendarModel);

	void delete(@Param("listId") List<Long> idList);

	List<AvailabilityCalendarModel> readForPeriodByProductIds(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	List<AvailabilityCalendarModel> readForPeriodByProductIds(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate);

	List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIds(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	List<AvailabilityCalendarModel> readForPeriodByProductIdsByVersion(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version);

	List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIdsByVersion(
			@Param("listId") List<Integer> productIdList, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("version") Date version);

	List<AvailabilityCalendarSimpleModel> readForPeriodForAllStateBySupplierIdByVersion(
			@Param("supplierId") String supplierId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
			@Param("version") Date version, @Param("channelId") Integer channelId);

	void insertUpdateFromList(@Param("list") List<AvailabilityCalendarModel> models);

	void cancelAvailabilityCalendarlist(@Param("list") List<AvailabilityCalendarModel> models);

	void deleteAvailabilityCalendarlist(@Param("list") List<AvailabilityCalendarModel> models);

	void insertUpdateFromTemplate(@Param("templateModel") AvailabilityCalendarModel templateModel);

	List<AvailabilityRoomDto> getCalendarByPropertyManagerForReport(Long pmId);

	List<AvailabilityRoomDto> readAvailabilityCalendarByRoomConfiguration(@Param("rooms") List<IsLowInventoryRoom> rooms,
			@Param("configurationId") Long configurationId, @Param("pmId") Integer pmId,
			@Param("noUnitCount") Integer noUnitCount);

	void insertAvailabilityCalendarModels(
			@Param("availabilityCalendarModelList") List<AvailabilityCalendarModel> availabilityCalendarModelList);

	AvailabilityCalendarModel readByProductIdAndDate(AvailabilityCalendarModel availabilityCalendarModel);

	void updateVersionInAvilabilityCalenderForProductAndDate(AvailabilityCalendarModel availabilityCalendarModel);

	void saveAvailabilityCalendarHistory(@Param("list") List<AvailabilityCalendarModel> models);

	Integer getAvailableCountForProduct(@Param("productId") String productId);

	List<AvailabilityCalendarModel> readCreatedByProductIds(Integer pmId);

	AvailabilityCalendarModel getLowestActiveBasePriceForProduct(@Param("productId") String productId,
			@Param("dateForCheck") Date dateForCheck);
	//BP-28512 Change storing process of availability_calendar
	/*List<AvailabilityCalendarModel> readByAvailabilityCalendarIds(@Param("list") List<Long> ids);*/
}

