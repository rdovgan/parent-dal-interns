package com.mybookingpal.dataaccesslayer.entity;

import java.time.LocalDate;

public interface IsCacheable {

	Long getCacheProductId();

	LocalDate getFromDate();

	LocalDate getToDate();
}
