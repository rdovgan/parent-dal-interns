package com.mybookingpal.dataaccesslayer.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

public class AvailabilityNotificationModel {

	private Long Id;
	private Long productId;
	private Long pmId;
	private LocalDate availabilityDate;
	private Boolean availableZeroSent;
	private Boolean availableTwoSent;
	private LocalDate createdDate;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getPmId() {
		return pmId;
	}

	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}

	public LocalDate getAvailabilityDate() {
		return availabilityDate;
	}

	public void setAvailabilityDate(LocalDate availabilityDate) {
		this.availabilityDate = availabilityDate;
	}

	public Boolean getAvailableZeroSent() {
		return availableZeroSent;
	}

	public void setAvailableZeroSent(Boolean availableZeroSent) {
		this.availableZeroSent = availableZeroSent;
	}

	public Boolean getAvailableTwoSent() {
		return availableTwoSent;
	}

	public void setAvailableTwoSent(Boolean availableTwoSent) {
		this.availableTwoSent = availableTwoSent;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("Id", Id).append("productId", productId).append("pmId", pmId)
				.append("availabilityDate", availabilityDate).append("availableZeroSent", availableZeroSent)
				.append("availableTwoSent", availableTwoSent).append("createdDate", createdDate).toString();
	}
}
