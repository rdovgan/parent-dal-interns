package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.entity.Enums.State;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Component
@Scope("prototype")
public class AvailabilityCalendarModel implements IsCacheable {
	private Long productId;
	private LocalDate availabilityDate;
	private Integer threshold = 0;
	private Boolean stopSell = false;
	private Integer minStay = 1;
	private Boolean closeToArrival = false;
	private Boolean closeToDeparture = false;
	private Integer availableCount = 0;
	private Integer unitsBooked = 0;
	private Integer maxStay = 0;
	private Integer bpAvailableCount = 0;
	private BigDecimal basePrice = BigDecimal.ZERO;
	private Date createdData;
	private Date version;
	private State state;

	public AvailabilityCalendarModel() {
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public LocalDate getAvailabilityDate() {
		return availabilityDate;
	}

	public void setAvailabilityDate(LocalDate availabilityDate) {
		this.availabilityDate = availabilityDate;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Boolean isStopSell() {
		return stopSell;
	}

	public void setStopSell(Boolean stopSell) {
		this.stopSell = stopSell;
	}

	public Integer getMinStay() {
		return minStay;
	}

	public void setMinStay(Integer minStay) {
		this.minStay = minStay;
	}

	public Boolean isCloseToArrival() {
		return closeToArrival;
	}

	public void setCloseToArrival(Boolean closeToArrival) {
		this.closeToArrival = closeToArrival;
	}

	public Boolean isCloseToDeparture() {
		return closeToDeparture;
	}

	public void setCloseToDeparture(Boolean closeToDeparture) {
		this.closeToDeparture = closeToDeparture;
	}

	public Integer getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(Integer availableCount) {
		this.availableCount = availableCount;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public Date getCreatedData() {
		return createdData;
	}

	public void setCreatedData(Date createdData) {
		this.createdData = createdData;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Integer getUnitsBooked() {
		return unitsBooked;
	}

	public void setUnitsBooked(Integer unitsBooked) {
		this.unitsBooked = unitsBooked;
	}

	public Integer getBpAvailableCount() {
		return bpAvailableCount;
	}

	public void setBpAvailableCount(Integer bpAvailableCount) {
		this.bpAvailableCount = bpAvailableCount;
	}

	public Integer getMaxStay() {
		return maxStay;
	}

	public void setMaxStay(Integer maxStay) {
		this.maxStay = maxStay;
	}

	@Override
	public Long getCacheProductId() {
		return productId;
	}

	@Override
	public LocalDate getFromDate() {
		return availabilityDate;
	}

	@Override
	public LocalDate getToDate() {
		return availabilityDate;
	}

	@Override
	public String toString() {
		return "AvailabilityCalendarModel{" + "productId=" + productId + ", availabilityDate=" + availabilityDate
				+ ", threshold=" + threshold + ", stopSell=" + stopSell + ", minStay=" + minStay + ", closeToArrival="
				+ closeToArrival + ", closeToDeparture=" + closeToDeparture + ", availableCount=" + availableCount
				+ ", unitsBooked=" + unitsBooked + ", maxStay=" + maxStay + ", bpAvailableCount=" + bpAvailableCount
				+ ", basePrice=" + basePrice + ", createdData=" + createdData + ", version=" + version + ", state="
				+ state + '}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((availabilityDate == null) ? 0 : availabilityDate.hashCode());
		result = prime * result + ((availableCount == null) ? 0 : availableCount.hashCode());
		result = prime * result + ((basePrice == null) ? 0 : basePrice.hashCode());
		result = prime * result + ((bpAvailableCount == null) ? 0 : bpAvailableCount.hashCode());
		result = prime * result + ((closeToArrival == null) ? 0 : closeToArrival.hashCode());
		result = prime * result + ((closeToDeparture == null) ? 0 : closeToDeparture.hashCode());
		result = prime * result + ((maxStay == null) ? 0 : maxStay.hashCode());
		result = prime * result + ((minStay == null) ? 0 : minStay.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((stopSell == null) ? 0 : stopSell.hashCode());
		result = prime * result + ((threshold == null) ? 0 : threshold.hashCode());
		result = prime * result + ((unitsBooked == null) ? 0 : unitsBooked.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvailabilityCalendarModel other = (AvailabilityCalendarModel) obj;
		if (availabilityDate == null) {
			if (other.availabilityDate != null)
				return false;
		} else if (!availabilityDate.equals(other.availabilityDate))
			return false;
		if (availableCount == null) {
			if (other.availableCount != null)
				return false;
		} else if (!availableCount.equals(other.availableCount))
			return false;
		if (basePrice == null) {
			if (other.basePrice != null)
				return false;
		} else if (!basePrice.equals(other.basePrice))
			return false;
		if (bpAvailableCount == null) {
			if (other.bpAvailableCount != null)
				return false;
		} else if (!bpAvailableCount.equals(other.bpAvailableCount))
			return false;
		if (closeToArrival == null) {
			if (other.closeToArrival != null)
				return false;
		} else if (!closeToArrival.equals(other.closeToArrival))
			return false;
		if (closeToDeparture == null) {
			if (other.closeToDeparture != null)
				return false;
		} else if (!closeToDeparture.equals(other.closeToDeparture))
			return false;
		if (maxStay == null) {
			if (other.maxStay != null)
				return false;
		} else if (!maxStay.equals(other.maxStay))
			return false;
		if (minStay == null) {
			if (other.minStay != null)
				return false;
		} else if (!minStay.equals(other.minStay))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state != other.state)
			return false;
		if (stopSell == null) {
			if (other.stopSell != null)
				return false;
		} else if (!stopSell.equals(other.stopSell))
			return false;
		if (threshold == null) {
			if (other.threshold != null)
				return false;
		} else if (!threshold.equals(other.threshold))
			return false;
		return true;
	}
}
