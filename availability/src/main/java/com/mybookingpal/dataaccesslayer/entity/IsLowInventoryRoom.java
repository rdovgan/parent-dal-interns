package com.mybookingpal.dataaccesslayer.entity;

public interface IsLowInventoryRoom {

	Long getId();

	Long getConfigurationId();

	Integer getResortId();

	Long getRoomId();

	Boolean getArticle();
}
