package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.dto.AvailabilityCalendarDTO;
import com.mybookingpal.dataaccesslayer.entity.AvailabilityCalendarModel;

public class AvailabilityCalendarUtil {

	public void recalculateBpAvailableCount(AvailabilityCalendarModel model) {
		final int availableCount = model.getAvailableCount() == null ? 0 : model.getAvailableCount();
		final int unitsBooked = model.getUnitsBooked() == null ? 0 : model.getUnitsBooked();
		final int threshold = model.getThreshold() == null ? 0 : model.getThreshold();
		int result = availableCount - unitsBooked - threshold;
		model.setAvailableCount(Math.max(result, 0));
	}

	public AvailabilityCalendarModel convertToAvailabilityCalendarModel(
			AvailabilityCalendarDTO availabilityCalendarDto) {

		AvailabilityCalendarModel model = new AvailabilityCalendarModel();
		model.setProductId(availabilityCalendarDto.getProductId());
		model.setAvailabilityDate(availabilityCalendarDto.getAvailabilityDate());
		if (availabilityCalendarDto.getThreshold() != null) {
			model.setThreshold(availabilityCalendarDto.getThreshold());
		}
		if (availabilityCalendarDto.getStopSell() != null) {
			model.setStopSell(availabilityCalendarDto.getStopSell());
		}
		if (availabilityCalendarDto.getMinStay() != null) {
			model.setMinStay(availabilityCalendarDto.getMinStay());
		}
		if (availabilityCalendarDto.getCloseToArrival() != null) {
			model.setCloseToArrival(availabilityCalendarDto.getCloseToArrival());
		}
		if (availabilityCalendarDto.getCloseToDeparture() != null) {
			model.setCloseToDeparture(availabilityCalendarDto.getCloseToDeparture());
		}
		if (availabilityCalendarDto.getAvailableCount() != null) {
			model.setAvailableCount(availabilityCalendarDto.getAvailableCount());
		}
		if (availabilityCalendarDto.getUnitsBooked() != null) {
			model.setUnitsBooked(availabilityCalendarDto.getUnitsBooked());
		}
		if (availabilityCalendarDto.getMaxStay() != null) {
			model.setMaxStay(availabilityCalendarDto.getMaxStay());
		}
		if (availabilityCalendarDto.getBasePrice() != null) {
			model.setBasePrice(availabilityCalendarDto.getBasePrice());
		}
		model.setState(availabilityCalendarDto.getState());
		recalculateBpAvailableCount(model);
		return model;
	}
}
