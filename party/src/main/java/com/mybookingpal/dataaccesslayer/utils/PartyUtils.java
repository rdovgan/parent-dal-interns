package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.entity.PartyModel;
import com.mybookingpal.dataaccesslayer.entity.PartyModelExt;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import static com.mybookingpal.dataaccesslayer.constants.PartyConstants.BLANK;

public class PartyUtils {

	// TODO : Add annotation @Description from org.apache.cxf.jaxrs.model.wadl.Description

	/**
	 * Gets the specified name in natural order (first + last).
	 *
	 * @param name the specified name.
	 * @return the natural name.
	 */
	public static String getNaturalName(String name) {
		if (name == null || name.isEmpty()) {
			return BLANK;
		}
		String[] args = name.split(",");
		if (args.length < 2) {
			return name;
		} else {
			return args[1] + " " + args[0];
		}
	}

	/**
	 * Checks if the specified value is a valid email address.
	 *
	 * @param value the specified value.
	 * @return true, if a valid email address, otherwise false.
	 * @see <pre>http://www.regular-expressions.info/email.html</pre>
	 * Pattern = <pre>"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	 * + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"</pre>
	 */
	public static boolean isEmailAddress(String value) {
		return EmailValidator.getInstance()
				.isValid(value);
	}

	public static boolean noEmployerid(PartyModel partyModel) {
		return partyModel.getEmployerid() == null || partyModel.getEmployerid()
				.isEmpty();
	}

	public static boolean noCreatorid(PartyModel partyModel, String creatorid) {
		return partyModel.getCreatorid() == null || creatorid == null || !partyModel.getCreatorid()
				.equalsIgnoreCase(creatorid);
	}

	public static boolean hasCreatorid(PartyModel partyModel, String creatorid) {
		return !noCreatorid(partyModel, creatorid);
	}

	public static boolean hasOldstate(PartyModelExt partyModel, String oldstate) {
		return partyModel.getOldstate() != null && partyModel.getOldstate()
				.equals(oldstate);
	}

	public static String defineName(String familyName, String firstName) {
		String fixedFamilyName = removeBadSymbolsFrom(familyName);
		String fixedFirstName = removeBadSymbolsFrom(firstName);
		if (StringUtils.isBlank(fixedFirstName)) {
			return fixedFamilyName;
		} else if (StringUtils.isBlank(fixedFamilyName)) {
			return fixedFirstName;
		} else {
			return fixedFamilyName.trim() + ", " + fixedFirstName.trim();
		}
	}

	private static String removeBadSymbolsFrom(String string) {
		if (string == null) {
			return null;
		}
		final String regexOfBadSymbols = string.replaceAll(",", "");
		return regexOfBadSymbols;
	}

	public String getExtraLastName(PartyModel partyModel) {
		return defineLastNameFromName(partyModel.getExtraname());
	}

	public String getExtraFirstName(PartyModel partyModel) {
		return defineFirstNameFromName(partyModel.getExtraname());
	}

	private static String defineLastNameFromName(String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		String[] args = name.split(",");
		return args[0].trim();
	}

	private static String defineFirstNameFromName(String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		String[] args = name.split(",");
		return args.length > 1 ? args[1].trim() : args[0].trim();
	}

	//@Description(value = "First name of the party if an individual", target = DocTarget.METHOD)
	public static String getFirstName(PartyModel partyModel) {
		return defineFirstNameFromName(partyModel.getName());
	}

	public static boolean noFirstName(PartyModel partyModel) {
		return getFirstName(partyModel) == null || getFirstName(partyModel).trim()
				.isEmpty();
	}

	public static boolean noEmailaddress(PartyModel partyModel) {
		return partyModel.getEmailaddresses() == null || partyModel.getEmailaddresses()
				.trim()
				.isEmpty() || !isEmailAddress(partyModel.getEmailaddresses());
	}

	/**
	 * Sets the specified string into US phone number format
	 *
	 * @param text the specified string
	 * @return the US phone number
	 */
	public static String setPhoneNumber(String text) {
		String newtext = text.replaceAll("\\D+", "");
		if (newtext.length() == 10) {
			return "(" + text.substring(0, 3) + ") " + text.substring(3, 6) + "-" + text.substring(6, 10);
		} else {
			return text;
		}
	}

	public boolean noTypes(PartyModelExt partyModel) {
		return partyModel.getTypes() == null || partyModel.getTypes()
				.isEmpty();
	}

	public boolean hasTypes(PartyModelExt partyModel) {
		return !noTypes(partyModel);
	}

	//@Description(value = "Name if an organization, or family or last name of the party if an individual", target = DocTarget.METHOD)
	public static String getFamilyName(PartyModel partyModel) {
		return defineLastNameFromName(partyModel.getName());
	}

	public static boolean noFamilyName(PartyModel partyModel) {
		return getFamilyName(partyModel) == null || getFamilyName(partyModel).trim()
				.isEmpty();
	}

	public static boolean noTaxnumber(PartyModel partyModel) {
		return partyModel.getTaxnumber() == null || partyModel.getTaxnumber()
				.trim()
				.isEmpty();
	}

	public static boolean hasTaxnumber(PartyModel partyModel) {
		return !noTaxnumber(partyModel);
	}

	public static boolean noPostaladdress(PartyModel partyModel) {
		return partyModel.getLocalAddress() == null;
	}

	public static boolean hasPostaladdress(PartyModel partyModel) {
		return !noPostaladdress(partyModel);
	}

	public static boolean noCountry(PartyModel partyModel) {
		return partyModel.getCountry() == null || partyModel.getCountry()
				.trim()
				.isEmpty();
	}

	public static boolean noLanguage(PartyModel partyModel) {
		return partyModel.getLanguage() == null || partyModel.getLanguage()
				.trim()
				.isEmpty();
	}

	public static boolean noMobilephone(PartyModel partyModel) {
		return partyModel.getMobilephone() == null || partyModel.getMobilephone()
				.trim()
				.isEmpty();
	}

	public static boolean noPassword(PartyModel partyModel) {
		return partyModel.getPassword() == null || partyModel.getPassword()
				.isEmpty();
	}

	public static boolean hasPassword(PartyModel partyModel) {
		return !noPassword(partyModel);
	}

	public static boolean noRoles(PartyModelExt partyModel) {
		return partyModel.getRoles() == null || partyModel.getRoles()
				.isEmpty();
	}

	public static boolean notType(PartyModelExt partyModel, String type) {
		return partyModel.getTypes() == null || partyModel.getTypes()
				.isEmpty() || type == null || !partyModel.getTypes()
				.contains(type);
	}

	public static boolean noFinanceid(PartyModel partyModel) {
		// TODO : Replace this variable to Model.ZERO from com.mybookingpal.shared.Model
		final String ModelZero = "0";
		return partyModel.getFinanceid() == null || partyModel.getFinanceid().isEmpty() || partyModel.getFinanceid().equals(ModelZero);
	}
}