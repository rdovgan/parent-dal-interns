package com.mybookingpal.dataaccesslayer.dto;

public interface HasXsl {
	String getXsl();

	void setXsl(String xsl);
}
