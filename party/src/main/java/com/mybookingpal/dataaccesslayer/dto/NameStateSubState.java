package com.mybookingpal.dataaccesslayer.dto;

//import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;

public class NameStateSubState {

	// TODO: Add @JsonProperty annotation from com.fasterxml.jackson.annotation.JsonProperty

	@XmlElement(name = "id")
	//@JsonProperty("id")
	private Long id;

	@XmlElement(name = "sub_state")
	//@JsonProperty("sub_state")
	private String name;

	@XmlElement(name = "state")
	//@JsonProperty("state")
	private String state;

	@XmlElement(name = "comment")
	//@JsonProperty("comment")
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public NameStateSubState() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public static class Builder {
		private Long id;
		private String name;
		private String state;
		private String comment;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder state(String state) {
			this.state = state;
			return this;
		}

		public Builder comment(String comment) {
			this.comment = comment;
			return this;
		}

		public NameStateSubState build() {
			return new NameStateSubState(this);
		}
	}

	private NameStateSubState(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.state = builder.state;
		this.comment = builder.comment;
	}
}


