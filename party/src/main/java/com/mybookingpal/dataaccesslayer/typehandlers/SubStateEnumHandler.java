package com.mybookingpal.dataaccesslayer.typehandlers;

import com.mybookingpal.dataaccesslayer.constants.PartyConstants;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class SubStateEnumHandler extends EnumTypeHandler<PartyConstants.SubStateEnum> {

	public SubStateEnumHandler() {
		super(PartyConstants.SubStateEnum.class);
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, PartyConstants.SubStateEnum parameter, JdbcType jdbcType) throws SQLException {
		if ((Objects.equals(parameter, PartyConstants.SubStateEnum.NULL))) {
			ps.setString(i, null);
		}
		ps.setString(i, ((PartyConstants.SubStateEnum) parameter).getState());
	}

	@Override
	public PartyConstants.SubStateEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? PartyConstants.SubStateEnum.NULL : PartyConstants.SubStateEnum.getSubStateById(value);
	}

	@Override
	public PartyConstants.SubStateEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? PartyConstants.SubStateEnum.NULL : PartyConstants.SubStateEnum.getSubStateById(value);
	}
}
