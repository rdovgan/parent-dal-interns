package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

public class PartyConstants {

	public static final String S_KEY = "party_id";
	public static final String S_CURRENCY = "party_currency";
	public static final String S_EMPLOYER = "party_employer";
	// Constants
	public static final String WIDGET = "0";
	public static final String ADMINISTRATOR = "1";
	public static final String NO_ACTOR = "2";
	public static final String CBT_LTD_PARTY = "4";
	public static final String ALL_ORGANIZATIONS = "-1";
	public static final String PASSWORD = "password";
	public static final String BLANK = "";

	//Fields
	public static final String DAYPHONE = "party.dayphone";
	public static final String EMAILADDRESS = "party.emailaddress";
	public static final String MOBILEPHONE = "party.mobilephone";
	public static final String RANK = "party.rank";

	// States
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String SUSPENDED = "Suspended";

	public static final String[] STATES = { INITIAL, CREATED, SUSPENDED, FINAL };

	public static final String PHONE = "(###)###-####";
	//Date Formats
	public static final String MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String YYYY_MM_DD = "yyyy/MM/dd";
	public static final String MMDDYYYY = "MM-dd-yyyy";
	public static final String DDMMYYYY = "dd-MM-yyyy";
	public static final String YYYYMMDD = "yyyy-MM-dd";
	public static final String HH_MM = " HH:mm";
	public static final String HH_MM_SS = " HH:mm:ss";

	public static final String[] DATE_FORMATS = { MM_DD_YYYY, DD_MM_YYYY, YYYY_MM_DD, MMDDYYYY, DDMMYYYY, YYYYMMDD };

	public static final String[] DEPOSITS = { "% Booking Value", "% Daily Rate", "Amount per Booking", "Amount per Day" };

	public static final String[] TIME_INTERVAL = { "0:DAY", "1:DAY", "3:DAY", "5:DAY", "7:DAY", "10:DAY", "2:WEE", "1:MON", "2:MON", "3:MON" };

	public enum Type {
		Actor, Affiliate, Agent, Customer, Employee, Employer, Jurisdiction, Organization, Owner, Supplier, SupportAgent, TravelAdmin, Administrator, OnboardingPerson, ChannelPartner, PropertyManager, AccountManager, SalesRep, RegionalManager, Beststayz, Homeowner, Renter, MRT_Admin, MRT_CEC_Agent, MRT_QA, PMS, PropertyManagerUser, RazorAuth, Admin, CEC_Agent, QA;

		public static Set<Type> getCredentialsPM() {
			return EnumSet.of(PropertyManager, AccountManager, RegionalManager, Beststayz);
		}

		public static Set<Type> getCredentialsForProduct() {
			return EnumSet.of(Administrator, OnboardingPerson, SupportAgent, SalesRep, AccountManager, RegionalManager, Beststayz);
		}

		public static Set<Type> getCredentialsWithBeststayz() {
			return EnumSet.of(Administrator, OnboardingPerson, SupportAgent, SalesRep, AccountManager, RegionalManager, Beststayz);
		}

		public static Set<Type> getResetPasswordsRoles() {
			return EnumSet.of(Administrator, PropertyManager);
		}

		public static Set<Type> getCredentialsForTravelPage() {
			return EnumSet.of(Administrator, OnboardingPerson, SupportAgent, SalesRep, AccountManager, TravelAdmin);
		}

		public static Set<Type> getCredentialsForCompanyList() {
			return EnumSet.of(Administrator, OnboardingPerson, SupportAgent, SalesRep, AccountManager, TravelAdmin, Agent);
		}

		public static Set<Type> getCredentialsForManualReservation() {
			return EnumSet.of(Administrator, OnboardingPerson, SupportAgent, SalesRep, AccountManager, PropertyManager);
		}

		public static Set<Type> getMarriottUserTypes() {
			return EnumSet.of(MRT_Admin, MRT_CEC_Agent, MRT_QA);
		}

		public static Set<Type> getChannelPortalUserTypes() {
			return EnumSet.of(Admin, CEC_Agent, QA);
		}

		public static Set<Type> getCredentialsForCustomerInfo() {
			return EnumSet.of(Administrator, PropertyManager, Agent, OnboardingPerson, SupportAgent, SalesRep, AccountManager, RegionalManager, TravelAdmin);
		}

		public static String stringToUserType(String userType) {
			return EnumSet.of(Actor, Affiliate, Agent, Customer, Employee, Employer, Jurisdiction, Organization, Owner, Supplier, SupportAgent, TravelAdmin,
					Administrator, OnboardingPerson, ChannelPartner, PropertyManager, AccountManager, SalesRep, RegionalManager, Beststayz, Homeowner, Renter)
					.stream()
					.filter(e -> e.name()
							.equalsIgnoreCase(userType))
					.findFirst()
					.map(Type::name)
					.orElse(null);
		}

		public static final EnumSet<Type> ADMINISTRATORS = EnumSet.of(Administrator, OnboardingPerson, SupportAgent, AccountManager, SalesRep);
	}

	// Preferences
	public enum Preference {LATITUDE, LONGITUDE, PRICEUNIT, ZOOM}

	public enum LicenseType {
		ABTA("ABTA/ITAA"), ARC("ARC"), ATOL("ATOL"), CICMA("CICMA/GC/CV"), CLIA("CLIA"), IATA("IATA"), PSUDEO("Psudeo"), TIDS("TIDS"), TRUE("TRUE"), OTHER(
				"Other");

		private String name;

		LicenseType(String name) {
			this.name = name;
		}

		public static LicenseType fromString(String text) {
			for (LicenseType license : LicenseType.values()) {
				if (license.name.equalsIgnoreCase(text)) {
					return license;
				}
			}
			return null;
		}
	}

	public enum Value {
		Countunit, Currency, Distance, DistanceUnit, Deposit, DepositType, CancelType, CancelTime, CancelRefund, CancelFee, DamageCoverageType, DamageInsurance, Interhome, Latitude, Longitude, LookBookCount, Payfull, Payunit, Paytype, Priceunit, ProgressActivity, ProgressActivityMax, ProgressAge, ProgressAgeMax, ProgressBrochure, ProgressBrochureMax, ProgressConfirm, ProgressCreator, ProgressCreatorMax, ProgressValue, ProgressValueMax, ZoomLevel
	}

	public enum SubStateEnum {
		NULL(null, null, null), Registration(0, "Registration", "Suspended"), ONB(1, "ONB", "Created"), Active(2, "Active", "Created"), Accounting(3,
				"Accounting", "Suspended"), PendingTerm(4, "Pending Term", "Suspended"), Temp(5, "Temp", "Suspended"), Hold(5, "Hold", "Suspended"), Terminated(
				7, "Terminated", "Suspended");

		private Integer id;
		private String name;
		private String state;

		SubStateEnum(Integer id, String name, String state) {
			this.id = id;
			this.name = name;
			this.state = state;
		}

		public Integer getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getState() {
			return state;
		}

		public static SubStateEnum getBySubState(String value) {
			return Arrays.stream(SubStateEnum.values())
					.filter(subStateEnum -> subStateEnum.getId() != null && subStateEnum.getName()
							.equals(value))
					.findFirst()
					.orElse(NULL);
		}

		public static SubStateEnum getSubStateById(Integer value) {
			return Arrays.stream(SubStateEnum.values())
					.filter(subStateEnum -> subStateEnum.getId() != null && subStateEnum.getId()
							.equals(value))
					.findFirst()
					.orElse(NULL);
		}
	}
}
