package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.NameStateSubState;
import com.mybookingpal.dataaccesslayer.dto.PartyWidgetItem;
import com.mybookingpal.dataaccesslayer.entity.FullChannelPartner;
import com.mybookingpal.dataaccesslayer.entity.PartyModel;
import com.mybookingpal.dataaccesslayer.mappers.PartyMapper;
import com.mybookingpal.utils.entity.IdName;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import com.mybookingpal.utils.entity.NameStateId;
import com.mybookingpal.utils.entity.Parameter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PartyDao {

	@Autowired
	private PartyMapper partyMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PartyModel action) {
		if (action == null) {
			return;
		}
		partyMapper.create(action);
	}

	public PartyModel read(String id) {
		return partyMapper.read(id);
	}

	public PartyModel readByEmailAddress(String emailAddress) {
		return partyMapper.readByEmailAddress(emailAddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updatePassword(Integer id, String password) {
		partyMapper.updatePassword(id, password);
	}

	public PartyModel readByAltIdAndPmsId(String altId, Integer pmsId) {
		return partyMapper.readByAltIdAndPmsId(altId, pmsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createChannelPartnerPartyPart(FullChannelPartner action) {
		partyMapper.createChannelPartnerPartyPart(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PartyModel action) {
		partyMapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readbyemailwithinfo(String emailaddress) {
		return partyMapper.readbyemailwithinfo(emailaddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertChannelPatner(Integer managerId, Integer channelId) {
		if (managerId == null && channelId == null) {
			return;
		}
		partyMapper.insertChannelPatner(managerId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> listChannelID(Integer pmId) {
		return partyMapper.listChannelID(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteChannelPartners(Integer pmId) {
		partyMapper.deleteChannelPartners(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readByEmailAddressWithoutRenter(String emailAddress) {
		return partyMapper.readByEmailAddressWithoutRenter(emailAddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> readPartyListByEmailAddress(String emailaddress) {
		return partyMapper.readPartyListByEmailAddress(emailaddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel organizationread(String id) {
		return partyMapper.organizationread(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertaccounts(String organizationid) {
		if (StringUtils.isBlank(organizationid)) {
			return;
		}
		partyMapper.insertaccounts(organizationid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertlogo(String organizationid) {
		partyMapper.insertlogo(organizationid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> credentials() {
		return partyMapper.credentials();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> listByPartName(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return partyMapper.listByPartName(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> propertymanagernameid() {
		return partyMapper.propertymanagernameid();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameStateId> getAllPropertyManagerByTravelCompanyAdmin(String companyAdminId, String name, Integer pagination, Integer paginationPage,
			String sortingType, String sortingBy) {
		return partyMapper.getAllPropertyManagerByTravelCompanyAdmin(companyAdminId, name, pagination, paginationPage, sortingType, sortingBy);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameStateId> getAllPropertyManagerByFilter(String name, Integer pagination, Integer paginationPage, String sortingType, String sortingBy) {
		return partyMapper.getAllPropertyManagerByFilter(name, pagination, paginationPage, sortingType, sortingBy);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameStateId> getAllTravelAgent(String name, Integer id) {
		return partyMapper.getAllTravelAgent(name, id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel getTravelEntityById(Integer id) {
		return partyMapper.getTravelEntityById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameStateId> getAllTravelAgentByCompanyAdmin(String name, Integer id, String companyAdminId) {
		return partyMapper.getAllTravelAgentByCompanyAdmin(name, id, companyAdminId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> type(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return partyMapper.type(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyWidgetItem partywidget(String partyid) {
		return partyMapper.partywidget(partyid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyWidgetItem partywidgetexists(String emailaddress) {
		return partyMapper.partywidgetexists(emailaddress);
	}

	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidwidget(Parameter action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return partyMapper.nameidwidget(action.getOrganizationid(), action.getId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidwidget(String organizationId, String id) {
		return partyMapper.nameidwidget(organizationId, id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel altread(NameId action) {
		if (action == null) {
			return null;
		}
		return partyMapper.altread(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidbyid(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return partyMapper.nameidbyid(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel getPartyForAltIdAndAbbervationCode(NameId action) {
		if (action == null) {
			return null;
		}
		return partyMapper.getPartyForAltIdAndAbbervationCode(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NameId> nameidbyname(NameIdAction action) {
		if (action == null) {
			return Collections.emptyList();
		}
		return partyMapper.nameidbyname(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> getAllPropertyManagerFromAvailabilityCalendar() {
		return partyMapper.getAllPropertyManagerFromAvailabilityCalendar();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getAllPartyIdsPerPMSAndChannel(String organizationId, Integer channelId) {
		return partyMapper.getAllPartyIdsPerPMSAndChannel(organizationId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> readAllCreatedSupportAgents() {
		return partyMapper.readAllCreatedSupportAgents();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readTravelAgentById(Integer travelAgentId) {
		return partyMapper.readTravelAgentById(travelAgentId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> getAllTravelAgentAdmin(String name, Integer id, Integer paginationLimit, Integer paginationPage) {
		return partyMapper.getAllTravelAgentAdmin(name, id, paginationLimit, paginationPage);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateAgentById(NameStateId nameStateId) {
		if (nameStateId == null) {
			return;
		}
		partyMapper.updateStateAgentById(nameStateId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateAndSubState(NameStateSubState nameStateSubState) {
		if (nameStateSubState == null) {
			return;
		}
		partyMapper.updateStateAndSubState(nameStateSubState);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> readPartiesByEmail(String email) {
		return partyMapper.readPartiesByEmail(email);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> readByProductIds(List<Integer> productIds) {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.emptyList();
		}
		return partyMapper.readByProductIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> readAllAgentsByRole(String role) {
		return partyMapper.readAllAgentsByRole(role);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setToFinalState(Long id) {
		partyMapper.setToFinalState(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readPartyByPartnerAbbreviation(String abbreviation) {
		return partyMapper.readPartyByPartnerAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readChannelPortalUser(Integer channelId, String emailAddress) {
		return partyMapper.readChannelPortalUser(channelId, emailAddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readPropertyManager(String emailAddress) {
		return partyMapper.readPropertyManager(emailAddress);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readChannelPortalUserById(Integer channelId, Integer id) {
		return partyMapper.readChannelPortalUserById(channelId, id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdName> readChannelPortalHmcNames(Integer channelId) {
		return partyMapper.readChannelPortalHmcNames(channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> getPmListByPms(Integer pmsId) {
		return partyMapper.getPmListByPms(pmsId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> suppliersIds(Integer action) {
		return partyMapper.suppliersIds(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PartyModel> partylistbyids(List<String> partyids) {
		if (CollectionUtils.isEmpty(partyids)) {
			return Collections.emptyList();
		}
		return partyMapper.partylistbyids(partyids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PartyModel readActiveParty(String id) {
		return partyMapper.readActiveParty(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getPartyIdByEmployerId(Integer channelId) {
		return partyMapper.getPartyIdByEmployerId(channelId);
	}
}
