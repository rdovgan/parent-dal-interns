package com.mybookingpal.dataaccesslayer.entity;

import org.apache.commons.lang3.StringUtils;

public class Address {

	public Address() {
		super();
	}

	public enum Type {
		address, city, zip, state, street, houseNumber, houseNumberAddition
	}

	;

	public Address(String postalAddress) {
		this.address = getAddressValue(postalAddress, Type.address);
		this.street = getAddressValue(postalAddress, Type.street);
		this.houseNumber = getAddressValue(postalAddress, Type.houseNumber);
		this.houseNumberAddition = getAddressValue(postalAddress, Type.houseNumberAddition);
		this.city = getAddressValue(postalAddress, Type.city);
		this.zip = getAddressValue(postalAddress, Type.zip);
		this.state = getAddressValue(postalAddress, Type.state);
		fillAddressFields();
		if (!isEmptyString(postalAddress) && !this.isFormattedAddress()) {
			this.postalAddress = postalAddress;
		}
	}

	private String address;
	private String street;
	private String houseNumber;
	private String houseNumberAddition;
	private String city;
	private String zip;
	private String state;
	private String postalAddress;

	private static final String ADDRESS_APPENDER = Type.address.name() + ":";
	private static final String STREET_APPENDER = Type.street.name() + ":";
	private static final String HOUSE_NUMBER_APPENDER = Type.houseNumber.name() + ":";
	private static final String HOUSE_NUMBER_ADDITION_APPENDER = Type.houseNumberAddition.name() + ":";
	private static final String CITY_APPENDER = Type.city.name() + ":";
	private static final String ZIP_APPENDER = Type.zip.name() + ":";
	private static final String STATE_APPENDER = Type.state.name() + ":";

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getHouseNumberAddition() {
		return houseNumberAddition;
	}

	public void setHouseNumberAddition(String houseNumberAddition) {
		this.houseNumberAddition = houseNumberAddition;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the specific value from party's address.
	 *
	 * @param party party that contains address
	 * @param value value to get. E.g. city, zip
	 * @return value
	 */
	private String getAddressValue(String address, Address.Type value) {
		if (address == null) {
			return StringUtils.EMPTY;
		}
		String[] tokens = address.split(";");
		if (tokens.length >= 1) {
			for (String string : tokens) {
				string = string.trim();
				String addressTypeValueWithColon = value.name() + ":";
				if (StringUtils.startsWith(string, addressTypeValueWithColon)) {
					String substringAfterColon = StringUtils.substringAfter(string, addressTypeValueWithColon);
					return StringUtils.isNotBlank(substringAfterColon) ? substringAfterColon : StringUtils.EMPTY;
				}
			}
		}
		return StringUtils.EMPTY;
	}

	// address:412 The Regent;city:Johannesburg;state:;zip:2196
	public String getPostalAddress() {
		StringBuilder postalAddress = new StringBuilder();
		if (!isEmptyString(address)) {
			postalAddress.append(ADDRESS_APPENDER + address + ";");
		}
		if (!isEmptyString(street)) {
			postalAddress.append(STREET_APPENDER + street + ";");
		}
		if (!isEmptyString(houseNumber)) {
			postalAddress.append(HOUSE_NUMBER_APPENDER + houseNumber + ";");
		}
		if (!isEmptyString(houseNumberAddition)) {
			postalAddress.append(HOUSE_NUMBER_ADDITION_APPENDER + houseNumberAddition + ";");
		}
		if (!isEmptyString(city)) {
			postalAddress.append(CITY_APPENDER + city + ";");
		}
		if (!isEmptyString(zip)) {
			postalAddress.append(ZIP_APPENDER + zip + ";");
		}
		if (!isEmptyString(state)) {
			postalAddress.append(STATE_APPENDER + state + ";");
		}
		if (!isFormattedAddress() && !isEmptyString(this.postalAddress)) {
			return this.postalAddress;
		}
		return postalAddress.toString();
	}

	private void fillAddressFields() {
		if (isEmptyString(address) && !isEmptyString(street) && !isEmptyString(houseNumber)) {
			houseNumberAddition = isEmptyString(houseNumberAddition) ? "" : houseNumberAddition;
			address = houseNumber + houseNumberAddition + " " + street;
		}
	}

	/**
	 * Check if postal address new formatted.
	 *
	 * @return value
	 */
	public boolean isFormattedAddress() {
		if (isEmptyString(this.address) && isEmptyString(this.city) && isEmptyString(this.zip) && isEmptyString(this.state)) {
			return false;
		} else {
			return true;
		}
	}

	private boolean isEmptyString(String value) {
		return value == null || value.equals("");
	}
}