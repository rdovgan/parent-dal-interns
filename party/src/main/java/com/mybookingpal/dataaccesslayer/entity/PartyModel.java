package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.PartyConstants;
import com.mybookingpal.dataaccesslayer.utils.PartyUtils;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
//import org.apache.cxf.jaxrs.model.wadl.Description;
//import org.apache.cxf.jaxrs.model.wadl.DocTarget;

// TODO : Add annotation @Description from org.apache.cxf.jaxrs.model.wadl.Description

@XmlRootElement(name = "party")
//Description(value = "Party data", target = DocTarget.RESPONSE)
public class PartyModel {

	// TODO : Fields from super-class Entity
	private Integer id;
	private String name;

	protected String employerid;

	@XmlTransient
	protected String creatorid;
	protected String financeid;
	protected String jurisdictionid;

	protected PartyConstants.SubStateEnum subState = PartyConstants.SubStateEnum.NULL;
	protected String extraname;
	protected String identitynumber;
	protected String taxnumber;
	protected String businessTaxId;
	protected String emailaddress;
	protected String webaddress;
	protected String dayphone;
	protected String nightphone;
	protected String faxphone;
	protected String mobilephone;
	protected String notes;
	protected String country;
	protected String paymentProfileId;
	private String altid;
	private Integer locationid;
	private Integer altpartyid;
	private String state;
	private String options;
	private String postaladdress;
	private String currency;
	private String unit;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	private Date version;

	// TODO : add annotation @SerializedName

	//@SerializedName("partylanguage")
	protected String language;
	protected Integer timeZoneId;

	@XmlTransient
	protected String formatdate;

	@XmlTransient
	protected String formatphone;
	protected Date birthdate;
	protected Address address = new Address("");

	@XmlTransient
	protected Integer rank;

	@XmlTransient
	protected String password;
	protected String postalcode;
	private String usertype;

	private boolean skipLicense;
	private Date createDate;

	public String getName() {
		return name;
	}

	public Integer getId() {
		return id;
	}

	public String getAltid() {
		return altid;
	}

	public Integer getLocationid() {
		return locationid;
	}

	public Integer getAltpartyid() {
		return altpartyid;
	}

	public String getState() {
		return state;
	}

	public String getOptions() {
		return options;
	}

	public String getCurrency() {
		return currency;
	}

	public String getUnit() {
		return unit;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public Date getVersion() {
		return version;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAltid(String altid) {
		this.altid = altid;
	}

	public void setLocationid(Integer locationid) {
		this.locationid = locationid;
	}

	public void setAltpartyid(Integer altpartyid) {
		this.altpartyid = altpartyid;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PartyConstants.SubStateEnum getSubState() {
		return subState;
	}

	public void setSubState(PartyConstants.SubStateEnum SubState) {
		this.subState = SubState;
	}

	public PartyModel() {
		//super(NameIdUtils.Type.Party.name());
	}

	//@Description(value = "Identity of the employer of the party", target = DocTarget.METHOD)
	public String getEmployerid() {
		return employerid;
	}

	public void setEmployerid(String parentid) {
		this.employerid = parentid;
	}

	public String getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}

	//@Description(value = "Identity of the primary bank account of the party", target = DocTarget.METHOD)
	public String getFinanceid() {
		return financeid;
	}

	public void setFinanceid(String financeid) {
		this.financeid = financeid;
	}

	//@Description(value = "Identity of the jurisdiction (for tax) of the party", target = DocTarget.METHOD)
	public String getJurisdictionid() {
		return jurisdictionid;
	}

	public void setJurisdictionid(String jurisdictionid) {
		this.jurisdictionid = jurisdictionid;
	}

	public void setName(String familyName, String firstName) {
		this.name = PartyUtils.defineName(familyName, firstName);
	}

	//@Description(value = "Name of additional contact of the party", target = DocTarget.METHOD)
	public String getExtraname() {
		return extraname;
	}

	public void setExtraname(String extraname) {
		this.extraname = extraname;
	}

	public void setExtraName(String lastName, String firstName) {
		this.extraname = PartyUtils.defineName(lastName, firstName);
	}

	//@Description(value = "Identification number of the party", target = DocTarget.METHOD)
	public String getIdentitynumber() {
		return identitynumber;
	}

	public void setIdentitynumber(String identitynumber) {
		this.identitynumber = identitynumber;
	}

	//@Description(value = "Tax number of the party in the jurisdiction", target = DocTarget.METHOD)
	public String getTaxnumber() {
		return taxnumber;
	}

	public void setTaxnumber(String taxnumber) {
		this.taxnumber = taxnumber;
	}

	//@Description(value = "Postal address of the party", target = DocTarget.METHOD)
	public String getPostaladdress() {
		return address.getPostalAddress();
	}

	public void setPostaladdress(String postaladdress) {
		address = new Address(postaladdress);
	}

	// TODO : Finish this method
	public void setAddress(String[] address) {
	}

	public String getLocalAddress() {
		return address.getAddress();
	}

	public void setLocalAddress(String localAddress) {
		address.setAddress(localAddress);
	}

	//@Description(value = "City or other location of the party", target = DocTarget.METHOD)
	public String getCity() {
		return address.getCity();
	}

	public void setCity(String city) {
		this.address.setCity(city);
	}

	//@Description(value = "State, province, county or other region code of the party", target = DocTarget.METHOD)
	public String getRegion() {
		return address.getState();
	}

	public void setRegion(String region) {
		address.setState(region);
	}

	//@Description(value = "Postal code or zip number of the party", target = DocTarget.METHOD)
	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	//@Description(value = "Three character ISO country code of the party", target = DocTarget.METHOD)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	//@Description(value = "Two character ISO language code of the party", target = DocTarget.METHOD)
	public String getLanguage() {
		return language;
	}

	//@Override
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFormatdate() {
		return formatdate;
	}

	public void setFormatdate(String formatdate) {
		this.formatdate = formatdate;
	}

	public Integer getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getFormatphone() {
		return formatphone;
	}

	public void setFormatphone(String formatphone) {
		this.formatphone = formatphone;
	}

	public String getEmailaddresses() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public String getEmailaddress() {
		return this.emailaddress;
	}

	//@Description(value = "Home web address of the party", target = DocTarget.METHOD)
	public String getWebaddress() {
		return webaddress;
	}

	public void setWebaddress(String webaddress) {
		this.webaddress = webaddress;
	}

	//@Description(value = "Primary phone number of the party during working hours", target = DocTarget.METHOD)
	public String getDayphone() {
		return dayphone;
	}

	public void setDayphone(String dayphone) {
		this.dayphone = dayphone;
	}

	//@Description(value = "Primary phone number of the party out of working hours", target = DocTarget.METHOD)
	public String getNightphone() {
		return nightphone;
	}

	public void setNightphone(String nightphone) {
		this.nightphone = nightphone;
	}

	//@Description(value = "Fax number of the party", target = DocTarget.METHOD)
	public String getFaxphone() {
		return faxphone;
	}

	public void setFaxphone(String faxphone) {
		this.faxphone = faxphone;
	}

	//@Description(value = "Mobile phone number of the party", target = DocTarget.METHOD)
	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	//@Description(value = "Comments or notes about the party", target = DocTarget.METHOD)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	//@Description(value = "Incorporation date if an organization, or date of birth if an individual", target = DocTarget.METHOD)
	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSkipLicense() {
		return skipLicense;
	}

	public void setSkipLicense(boolean skipLicense) {
		this.skipLicense = skipLicense;
	}

	public String getStreet() {
		return address.getStreet();
	}

	public String getHouseNumber() {
		return address.getHouseNumber();
	}

	public String getHouseNumberAddition() {
		return address.getHouseNumberAddition();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getBusinessTaxId() {
		return businessTaxId;
	}

	public void setBusinessTaxId(String businessTaxId) {
		this.businessTaxId = businessTaxId;
	}

	public String getPaymentProfileId() {
		return paymentProfileId;
	}

	public void setPaymentProfileId(String paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	@Override
	public String toString() {
		return "Party{" + "employerid: '" + employerid + '\'' + ", creatorid: '" + creatorid + '\'' + ", financeid: '" + financeid + '\''
				+ ", jurisdictionid: '" + jurisdictionid + '\'' + ", extraname: '" + extraname + '\'' + ", identitynumber: '" + identitynumber + '\''
				+ ", taxnumber: '" + taxnumber + '\'' + ", emailaddress: '" + emailaddress + '\'' + ", webaddress: '" + webaddress + '\'' + ", dayphone: '"
				+ dayphone + '\'' + ", nightphone: '" + nightphone + '\'' + ", faxphone: '" + faxphone + '\'' + ", mobilephone: '" + mobilephone + '\''
				+ ", notes: '" + notes + '\'' + ", country: '" + country + '\'' + ", language: '" + language + '\'' + ", formatdate: '" + formatdate + '\''
				+ ", formatphone: '" + formatphone + '\'' + ", birthdate: " + birthdate + ", rank: " + rank + ", password: '" + password + '\''
				+ ", postalcode: '" + postalcode + '\'' + ", usertype: '" + usertype + '\'' + ", skipLicense: " + skipLicense + ", address: " + address
				+ ", paymentProfileId: " + paymentProfileId + '}';
	}
}