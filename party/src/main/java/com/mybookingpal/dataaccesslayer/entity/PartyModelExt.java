package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.dto.HasXsl;

import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

public class PartyModelExt extends PartyModel implements HasXsl {

	// TODO : Add annotation @Description from org.apache.cxf.jaxrs.model.wadl.Description

	@XmlTransient
	private String oldstate;

	private boolean manager;
	private boolean agent;
	private String message;
	private String xsl; //NB HAS GET&SET = private

	@XmlTransient
	private List<String> roles;
	private List<String> types;

	public String getOldstate() {
		return oldstate;
	}

	public void setOldstate(String oldstate) {
		this.oldstate = oldstate;
	}

	public boolean isManager() {
		return manager;
	}

	public void setOrganization(boolean manager) {
		this.manager = manager;
	}

	public boolean isAgent() {
		return agent;
	}

	public void setAgent(boolean agent) {
		this.agent = agent;
	}

	//@Description(value = "Diagnostic message in the response", target = DocTarget.METHOD)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	//---------------------------------------------------------------------------------
	// Implements HasXsl interface
	//---------------------------------------------------------------------------------

	public String getXsl() {
		return xsl;
	}

	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	//@Description(value = "List of party types", target = DocTarget.METHOD)
	public List<String> getTypes() {
		return types;
	}

	public String getType() {
		return types == null || types.isEmpty() ? null : types.get(0);
	}

	public void setType(String type) {
		if (types == null) {
			types = new ArrayList<>();
		}
		types.add(type);
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

	@Override
	public String toString() {
		return "Party{" + "employerid: '" + employerid + '\'' + ", creatorid: '" + creatorid + '\'' + ", financeid: '" + financeid + '\''
				+ ", jurisdictionid: '" + jurisdictionid + '\'' + ", oldstate: '" + oldstate + '\'' + ", extraname: '" + extraname + '\''
				+ ", identitynumber: '" + identitynumber + '\'' + ", taxnumber: '" + taxnumber + '\'' + ", emailaddress: '" + emailaddress + '\''
				+ ", webaddress: '" + webaddress + '\'' + ", dayphone: '" + dayphone + '\'' + ", nightphone: '" + nightphone + '\'' + ", faxphone: '" + faxphone
				+ '\'' + ", mobilephone: '" + mobilephone + '\'' + ", notes: '" + notes + '\'' + ", country: '" + country + '\'' + ", language: '" + language
				+ '\'' + ", formatdate: '" + formatdate + '\'' + ", formatphone: '" + formatphone + '\'' + ", birthdate: " + birthdate + ", rank: " + rank
				+ ", password: '" + password + '\'' + ", postalcode: '" + postalcode + '\'' + ", manager: " + manager + ", agent: " + agent + ", message: '"
				+ message + '\'' + ", usertype: '" + getUsertype() + '\'' + ", xsl: '" + xsl + '\'' + ", roles: " + roles + ", types: " + types
				+ ", skipLicense: " + isSkipLicense() + ", address: " + address + ", paymentProfileId: " + paymentProfileId + '}';
	}
}