package com.mybookingpal.dataaccesslayer.entity;

//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.google.gson.annotations.SerializedName;
//import com.mybookingpal.server.PartyService;
//import com.mybookingpal.shared.BinaryRuleKey;
//import com.mybookingpal.utils.ChannelPartnerUtils;

import javax.xml.bind.annotation.XmlElement;
import java.util.Map;

public class FullChannelPartner {

	// TODO : Add PartyService

	//	private PartyService partyService = new PartyService();

	// TODO: Add @SerializedName annotation from com.google.gson.annotations.SerializedName

	//	@SerializedName("id")
	@XmlElement(name = "id")
	private Integer id;

	//	@SerializedName("pos")
	@XmlElement(name = "pos")
	private String pos;

	//	@SerializedName("channel_partner_id")
	@XmlElement(name = "channel_partner_id", nillable = true)
	private Integer channelPartnerId;

	//	@SerializedName("name")
	@XmlElement(name = "name", nillable = true)
	private String name;

	//	@SerializedName("channel_name")
	@XmlElement(name = "channel_name", nillable = true)
	private String channelName;

	//	@SerializedName("channel_abbreviation")
	@XmlElement(name = "channel_abbreviation", nillable = true)
	private String channelAbbreviation;

	//	@SerializedName("extra_name")
	@XmlElement(name = "extra_name", nillable = true)
	private String extraName;

	//	@SerializedName("email_address")
	@XmlElement(name = "email_address", nillable = true)
	private String emailAddress;

	//	@SerializedName("phone")
	@XmlElement(name = "phone", nillable = true)
	private String phone;

	//	@SerializedName("web_address")
	@XmlElement(name = "web_address", nillable = true)
	private String webAddress;

	//	@SerializedName("state")
	@XmlElement(name = "state", nillable = true)
	private String state;

	//	@SerializedName("postal_address")
	@XmlElement(name = "postal_address", nillable = true)
	private Map<String, String> postalAddress;

	//	@SerializedName("postal_code")
	@XmlElement(name = "postal_code", nillable = true)
	private String postalCode;

	//	@SerializedName("country")
	@XmlElement(name = "country", nillable = true)
	private String country;

	//	@SerializedName("parent_id")
	@XmlElement(name = "parent_id", nillable = true)
	private Integer parentId;

	//	@SerializedName("password")
	@XmlElement(name = "password", nillable = true)
	private String password;

	//	@SerializedName("type")
	@XmlElement(name = "type", nillable = true)
	private String type;

	//	@SerializedName("cp_commission")
	@XmlElement(name = "cp_commission", nillable = true)
	private Double commission;

	//	@SerializedName("guest_grotections")
	@XmlElement(name = "guest_grotections", nillable = true)
	private String guestProtections;

	//	@SerializedName("host_protections")
	@XmlElement(name = "host_protections", nillable = true)
	private String hostProtections;

	//	@SerializedName("customer_service_overview")
	@XmlElement(name = "customer_service_overview", nillable = true)
	private String customerServiceOverview;

	//	@SerializedName("generate_xml")
	@XmlElement(name = "generate_xml", nillable = true)
	private Boolean generateXml;

	//	@SerializedName("sign_up_process")
	@XmlElement(name = "sign_up_process", nillable = true)
	private String signUpProcess;

	//	@SerializedName("listing_fees")
	@XmlElement(name = "listing_fees", nillable = true)
	private String listingFees;

	//	@SerializedName("coverage")
	@XmlElement(name = "coverage", nillable = true)
	private String coverage;

	//	@SerializedName("description")
	@XmlElement(name = "description", nillable = true)
	private String description;

	//	@SerializedName("payment_process")
	@XmlElement(name = "payment_process", nillable = true)
	private String paymentProcess;

	//	@SerializedName("payouts")
	@XmlElement(name = "payouts", nillable = true)
	private String payouts;

	//	@SerializedName("responsible_party")
	@XmlElement(name = "responsible_party", nillable = true)
	private String responsibleParty;

	//	@SerializedName("contact_type")
	@XmlElement(name = "contact_type", nillable = true)
	private String contactType;

	//	@SerializedName("channel_type")
	@XmlElement(name = "channel_type", nillable = true)
	private String channelType;

	//	@SerializedName("traffic")
	@XmlElement(name = "traffic", nillable = true)
	private String traffic;

	//	@SerializedName("ftp_password")
	@XmlElement(name = "ftp_password", nillable = true)
	private String ftpPassword;

	//	@SerializedName("bp_commission")
	@XmlElement(name = "bp_commission", nillable = true)
	private Double bpCommission;

	//	@SerializedName("logo_url")
	@XmlElement(name = "logo_url", nillable = true)
	private String logoUrl;

	//	@SerializedName("callback_url")
	@XmlElement(name = "callback_url", nillable = true)
	private String callbackUrl;

	//	@SerializedName("terms_conditions")
	@XmlElement(name = "terms_conditions", nillable = true)
	private String termsConditions;

	//	@SerializedName("create_account")
	@XmlElement(name = "create_account", nillable = true)
	private Boolean createAccount;

	//	@SerializedName("privacy_policy")
	@XmlElement(name = "privacy_policy", nillable = true)
	private String privacyPolicy;

	//	@SerializedName("funds_holder")
	@XmlElement(name = "funds_holder", nillable = true)
	private Boolean fundsHolder;

	//	@SerializedName("customer_service_contact")
	@XmlElement(name = "customer_service_contact", nillable = true)
	private String customerServiceContact;

	//	@SerializedName("accounting_contact")
	@XmlElement(name = "accounting_contact", nillable = true)
	private String accountingContact;

	//	@SerializedName("adding_supply_in")
	@XmlElement(name = "adding_supply_in", nillable = true)
	private String addingSupplyIn;

	//	@SerializedName("cancellation_text")
	@XmlElement(name = "cancellation_text", nillable = true)
	private String cancellationText;

	//	@SerializedName("allow_publish_logo")
	@XmlElement(name = "allow_publish_logo", nillable = true)
	private Boolean allowPublishLogo;

	//	@SerializedName("minimum_of_required_properties")
	@XmlElement(name = "minimum_of_required_properties", nillable = true)
	private Integer minimumOfRequiredProperties;

	//	@SerializedName("allow_properties_with")
	@XmlElement(name = "allow_properties_with", nillable = true)
	private String allowPropertiesWith;

	//	@SerializedName("properties_will_not_publish")
	@XmlElement(name = "properties_will_not_publish", nillable = true)
	private String propertiesWillNotPublish;

	//	@SerializedName("included_in_total")
	@XmlElement(name = "included_in_total", nillable = true)
	private String includedInTotal;

	//	@SerializedName("send_email")
	@XmlElement(name = "send_email", nillable = true)
	private Boolean sendEmail;

	//	@SerializedName("use_channel_api")
	@XmlElement(name = "use_channel_api", nillable = true)
	private Boolean useChannelApi;

	//	@SerializedName("is_master_channel")
	@XmlElement(name = "is_master_channel", nillable = true)
	private boolean isMasterChannel;

	//TODO: Add BinaryRuleKey class from com.mybookingpal.shared.BinaryRuleKey

	//	@SerializedName("synchronization")
	//	@XmlElement(name = "synchronization", nillable = true)
	//	private List<BinaryRuleKey> synchronization;

	//	@SerializedName("payment_model")
	@XmlElement(name = "payment_model", nillable = true)
	private String paymentModel;

	//	@SerializedName("party_logo_url")
	@XmlElement(name = "party_logo_url", nillable = true)
	private String partyLogoUrl;

	//	@SerializedName("emails_logo_url")
	@XmlElement(name = "emails_logo_url", nillable = true)
	private String emailsLogoURL;

	// TODO: Add @JsonIgnore annotation from com.fasterxml.jackson.annotation.JsonIgnore;

	//@JsonIgnore
	private Long mask;

	//@JsonIgnore
	private String addressInString;

	public FullChannelPartner() {
		//		synchronization = new ArrayList<>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		//		this.pos = partyService.getEncryptedValue(String.valueOf(id));
	}

	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public String getExtraName() {
		return extraName;
	}

	public void setExtraName(String extraName) {
		this.extraName = extraName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	// TODO : Add ChannelPartnerUtils class from com.mybookingpal.utils.ChannelPartnerUtils

	//	public void setPostalAddress(Map<String, String> postalAddress) {
	//		this.postalAddress = postalAddress;
	//		addressInString = ChannelPartnerUtils.definePostalAddress(postalAddress);
	//	}
	//
	//	public Map<String, String> getPostalAddress() {
	//		postalAddress = ChannelPartnerUtils.definePostalAddressFromStaring(addressInString);
	//		return postalAddress;
	//	}
	//
	//	public void setAddressInString(String addressInString) {
	//		this.addressInString = addressInString;
	//		postalAddress = ChannelPartnerUtils.definePostalAddressFromStaring(addressInString);
	//	}

	public String getAddressInString() {
		return addressInString;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public String getGuestProtections() {
		return guestProtections;
	}

	public void setGuestProtections(String guestProtections) {
		this.guestProtections = guestProtections;
	}

	public String getHostProtections() {
		return hostProtections;
	}

	public void setHostProtections(String hostProtections) {
		this.hostProtections = hostProtections;
	}

	public String getCustomerServiceOverview() {
		return customerServiceOverview;
	}

	public void setCustomerServiceOverview(String customerServiceOverview) {
		this.customerServiceOverview = customerServiceOverview;
	}

	public Boolean getGenerateXml() {
		return generateXml;
	}

	public void setGenerateXml(Boolean generateXml) {
		this.generateXml = generateXml;
	}

	public String getSignUpProcess() {
		return signUpProcess;
	}

	public void setSignUpProcess(String signUpProcess) {
		this.signUpProcess = signUpProcess;
	}

	public String getListingFees() {
		return listingFees;
	}

	public void setListingFees(String listingFees) {
		this.listingFees = listingFees;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentProcess() {
		return paymentProcess;
	}

	public void setPaymentProcess(String paymentProcess) {
		this.paymentProcess = paymentProcess;
	}

	public String getPayouts() {
		return payouts;
	}

	public void setPayouts(String payouts) {
		this.payouts = payouts;
	}

	public String getResponsibleParty() {
		return responsibleParty;
	}

	public void setResponsibleParty(String responsibleParty) {
		this.responsibleParty = responsibleParty;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getTraffic() {
		return traffic;
	}

	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public Double getBpCommission() {
		return bpCommission;
	}

	public void setBpCommission(Double bpCommission) {
		this.bpCommission = bpCommission;
	}

	public String getTermsConditions() {
		return termsConditions;
	}

	public void setTermsConditions(String termsConditions) {
		this.termsConditions = termsConditions;
	}

	public Boolean getCreateAccount() {
		return createAccount;
	}

	public void setCreateAccount(Boolean createAccount) {
		this.createAccount = createAccount;
	}

	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public Boolean getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(Boolean fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public String getCustomerServiceContact() {
		return customerServiceContact;
	}

	public void setCustomerServiceContact(String customerServiceContact) {
		this.customerServiceContact = customerServiceContact;
	}

	public String getAccountingContact() {
		return accountingContact;
	}

	public void setAccountingContact(String accountingContact) {
		this.accountingContact = accountingContact;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getAddingSupplyIn() {
		return addingSupplyIn;
	}

	public void setAddingSupplyIn(String addingSupplyIn) {
		this.addingSupplyIn = addingSupplyIn;
	}

	public String getCancellationText() {
		return cancellationText;
	}

	public void setCancellationText(String cancellationText) {
		this.cancellationText = cancellationText;
	}

	public Boolean getAllowPublishLogo() {
		return allowPublishLogo;
	}

	public void setAllowPublishLogo(Boolean allowPublishLogo) {
		this.allowPublishLogo = allowPublishLogo;
	}

	public String getAllowPropertiesWith() {
		return allowPropertiesWith;
	}

	public void setAllowPropertiesWith(String allowPropertiesWith) {
		this.allowPropertiesWith = allowPropertiesWith;
	}

	public String getPropertiesWillNotPublish() {
		return propertiesWillNotPublish;
	}

	public void setPropertiesWillNotPublish(String propertiesWillNotPublish) {
		this.propertiesWillNotPublish = propertiesWillNotPublish;
	}

	public String getIncludedInTotal() {
		return includedInTotal;
	}

	public void setIncludedInTotal(String includedInTotal) {
		this.includedInTotal = includedInTotal;
	}

	public Boolean getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public Integer getMinimumOfRequiredProperties() {
		return minimumOfRequiredProperties;
	}

	public Boolean getUseChannelApi() {
		return useChannelApi;
	}

	public void setUseChannelApi(Boolean useChannelApi) {
		this.useChannelApi = useChannelApi;
	}

	public void setMinimumOfRequiredProperties(Integer minimumOfRequiredProperties) {
		this.minimumOfRequiredProperties = minimumOfRequiredProperties;
	}

	//	public List<BinaryRuleKey> getSynchronization() {
	//		return synchronization;
	//	}

	//	public void setSynchronization(List<BinaryRuleKey> synchronization) {
	//		this.synchronization = synchronization;
	//	}

	public Long getMask() {
		return mask;
	}

	public void setMask(Long mask) {
		this.mask = mask;
	}

	public boolean getMasterChannel() {
		return isMasterChannel;
	}

	public void setMasterChannel(boolean masterChannel) {
		isMasterChannel = masterChannel;
	}

	public String getPaymentModel() {
		return paymentModel;
	}

	public void setPaymentModel(String paymentModel) {
		this.paymentModel = paymentModel;
	}

	public String getPartyLogoUrl() {
		return partyLogoUrl;
	}

	public void setPartyLogoUrl(String partyLogoUrl) {
		this.partyLogoUrl = partyLogoUrl;
	}

	public String getEmailsLogoURL() {
		return emailsLogoURL;
	}

	public void setEmailsLogoURL(String emailsLogoURL) {
		this.emailsLogoURL = emailsLogoURL;
	}
}
