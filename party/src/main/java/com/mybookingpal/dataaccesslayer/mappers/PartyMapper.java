package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.NameStateSubState;
import com.mybookingpal.dataaccesslayer.dto.PartyWidgetItem;
import com.mybookingpal.dataaccesslayer.entity.FullChannelPartner;
import com.mybookingpal.dataaccesslayer.entity.PartyModel;
import com.mybookingpal.utils.entity.IdName;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import com.mybookingpal.utils.entity.NameStateId;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartyMapper {

	PartyModel read(String id);

	void create(PartyModel action);

	PartyModel readByEmailAddress(String emailAddress);

	void updatePassword(@Param("id") Integer id, @Param("password") String password);

	PartyModel readByAltIdAndPmsId(@Param("altId") String altId, @Param("pmsId") Integer pmsId);

	void createChannelPartnerPartyPart(FullChannelPartner action);

	void update(PartyModel action);

	PartyModel readbyemailwithinfo(String emailaddress);

	void insertChannelPatner(@Param("pmid") Integer managerId, @Param("channelid") Integer channelId);

	List<Integer> listChannelID(Integer pmId);

	void deleteChannelPartners(Integer pmId);

	PartyModel readByEmailAddressWithoutRenter(String emailAddress);

	List<PartyModel> readPartyListByEmailAddress(String emailaddress);

	PartyModel organizationread(String id);

	void insertaccounts(String organizationid);

	void insertlogo(String organizationid);

	List<NameId> credentials();

	/**
	 * Get a list of PM name and id by a part name among those PM who share their properties to all agents
	 */
	List<NameId> listByPartName(NameIdAction action);

	List<NameId> propertymanagernameid();

	List<NameStateId> getAllPropertyManagerByTravelCompanyAdmin(@Param("companyAdminId") String companyAdminId, @Param("name") String name,
			@Param("paginationLimit") Integer pagination, @Param("paginationPage") Integer paginationPage, @Param("sortingType") String sortingType,
			@Param("sortingBy") String sortingBy);

	List<NameStateId> getAllPropertyManagerByFilter(@Param("name") String name, @Param("paginationLimit") Integer pagination,
			@Param("paginationPage") Integer paginationPage, @Param("sortingType") String sortingType, @Param("sortingBy") String sortingBy);

	List<NameStateId> getAllTravelAgent(@Param("name") String name, @Param("id") Integer id);

	PartyModel getTravelEntityById(@Param("id") Integer id);

	List<NameStateId> getAllTravelAgentByCompanyAdmin(@Param("name") String name, @Param("id") Integer id, @Param("companyAdminId") String companyAdminId);

	List<NameId> type(NameIdAction action);

	PartyWidgetItem partywidget(String partyid);

	PartyWidgetItem partywidgetexists(String emailaddress);

	List<NameId> nameidwidget(@Param("organizationId") String organizationId, @Param("id") String id);

	// Foreign SQL queries
	PartyModel altread(NameId action);

	List<NameId> nameidbyid(NameIdAction action);

	PartyModel getPartyForAltIdAndAbbervationCode(NameId action);

	List<NameId> nameidbyname(NameIdAction action);

	List<PartyModel> getAllPropertyManagerFromAvailabilityCalendar();

	List<String> getAllPartyIdsPerPMSAndChannel(@Param("organizationId") String organizationId, @Param("channelId") Integer channelId);

	List<PartyModel> readAllCreatedSupportAgents();

	PartyModel readTravelAgentById(Integer travelAgentId);

	List<PartyModel> getAllTravelAgentAdmin(@Param("name") String name, @Param("id") Integer id, @Param("paginationLimit") Integer paginationLimit,
			@Param("paginationPage") Integer paginationPage);

	void updateStateAgentById(NameStateId nameStateId);

	void updateStateAndSubState(NameStateSubState nameStateSubState);

	List<PartyModel> readPartiesByEmail(@Param("email") String email);

	List<PartyModel> readByProductIds(@Param("productIds") List<Integer> productIds);

	List<PartyModel> readAllAgentsByRole(@Param("role") String role);

	void setToFinalState(@Param("id") Long id);

	PartyModel readPartyByPartnerAbbreviation(@Param("abbreviation") String abbreviation);

	PartyModel readChannelPortalUser(@Param("channelId") Integer channelId, @Param("emailAddress") String emailAddress);

	PartyModel readPropertyManager(@Param("emailAddress") String emailAddress);

	PartyModel readChannelPortalUserById(@Param("channelId") Integer channelId, @Param("id") Integer id);

	List<IdName> readChannelPortalHmcNames(@Param("channelId") Integer channelId);

	List<PartyModel> getPmListByPms(Integer pmsId);

	List<Integer> suppliersIds(Integer action);

	List<PartyModel> partylistbyids(List<String> partyids);

	PartyModel readActiveParty(String id);

	Integer getPartyIdByEmployerId(@Param("channelId") Integer channelId);

}
