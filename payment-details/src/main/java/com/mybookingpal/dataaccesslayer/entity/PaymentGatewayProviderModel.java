package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class PaymentGatewayProviderModel {
	private Integer id;
	private String name;
	private Date createDate;
	private Short fee;
	private Boolean autopay;
	private Integer profileExpiration;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Short getFee() {
		return fee;
	}

	public void setFee(Short fee) {
		this.fee = fee;
	}

	public Boolean isAutopay() {
		return autopay;
	}

	public void setAutopay(Boolean autopay) {
		this.autopay = autopay;
	}

	public Integer getProfileExpiration() {
		return profileExpiration;
	}

	public void setProfileExpiration(Integer profileExpiration) {
		this.profileExpiration = profileExpiration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ManagerToGateway [id=");
		builder.append(getId());
		builder.append(", name=");
		builder.append(name);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", fee=");
		builder.append(fee);
		builder.append(", autopay=");
		builder.append(autopay);
		builder.append("]");
		return builder.toString();
	}
}
