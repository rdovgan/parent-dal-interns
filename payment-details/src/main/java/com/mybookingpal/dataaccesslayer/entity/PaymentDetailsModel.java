package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.PaymentDetailsState;
import com.mybookingpal.dataaccesslayer.constants.PaymentType;
import com.mybookingpal.dataaccesslayer.constants.ValueType;
import com.mybookingpal.utils.entity.BigDecimalExt;

import java.time.LocalDateTime;

public class PaymentDetailsModel {
	private Long id;
	private Integer propertyManagerPartyId;
	private Integer productId;
	private PaymentType paymentType;
	private ValueType valueType;
	private BigDecimalExt firstPayment;
	private Integer balanceDueDaysFromArrival;
	private LocalDateTime created;
	private LocalDateTime version;
	private PaymentDetailsState state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPropertyManagerPartyId() {
		return propertyManagerPartyId;
	}

	public void setPropertyManagerPartyId(Integer propertyManagerPartyId) {
		this.propertyManagerPartyId = propertyManagerPartyId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public ValueType getValueType() {
		return valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public BigDecimalExt getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(BigDecimalExt firstPayment) {
		this.firstPayment = firstPayment;
	}

	public Integer getBalanceDueDaysFromArrival() {
		return balanceDueDaysFromArrival;
	}

	public void setBalanceDueDaysFromArrival(Integer balanceDueDaysFromArrival) {
		this.balanceDueDaysFromArrival = balanceDueDaysFromArrival;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getVersion() {
		return version;
	}

	public void setVersion(LocalDateTime version) {
		this.version = version;
	}

	public PaymentDetailsState getState() {
		return state;
	}

	public void setState(PaymentDetailsState state) {
		this.state = state;
	}
}
