package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.PaymentMethodTypeLabels;

import java.util.Date;

public class PaymentMethodModel {
	private Integer id;

	private Integer partyId;                    // Property Manager ID

	private String type;					/* PaymentType: receiving payment from BookingPal
												    1 - Mail Checks (mail)
													2 - PayPal (paypal)
													3 - Bank Transfer (deposit)
													4 - Wire Transfer (wire_transfer)*/

	private String paymentInfo;				/* Consists payment information depends on payment type:
												type=1 - empty;
												type=2 - PayPal account;
												type=3 - consists ACHPaymentInfo object in JSON format.*/

	private Date entryDate;                    /* Date of test payment transaction.  */
	private Double amount;                    /* Amount of test payment transaction. */
	private Date verifiedDate;                /* Date of amount verification. */

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PaymentMethodModel() {
		super();
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getVerifiedDate() {
		return verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

	public String getTypeLabel() {
		return PaymentMethodTypeLabels.getLabelByType(type);
	}

	@Override
	public String toString() {
		return "PaymentMethod [partyId=" + partyId + ", type=" + type + ", paymentInfo=" + paymentInfo + ", entryDate=" + entryDate + ", amount=" + amount
				+ ", verifiedDate=" + verifiedDate + "]";
	}
}
