package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PaymentDetailsModel;
import com.mybookingpal.dataaccesslayer.mappers.PaymentDetailsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PaymentDetailsDao {
	@Autowired
	PaymentDetailsMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentDetailsModel paymentDetailsModel) {
		if (paymentDetailsModel != null) {
			mapper.create(paymentDetailsModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentDetailsModel read(Long id) {
		if (id == null) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentDetailsModel paymentDetailsModel) {
		if (paymentDetailsModel != null) {
			mapper.update(paymentDetailsModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentDetailsModel> readByProductIdWithStateCreated(Integer productId) {
		if (productId == null) {
			return Collections.emptyList();
		}
		return mapper.readByProductIdWithStateCreated(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentDetailsModel> readByPropertyManagerIdWithStateCreated(Integer propertyManagerId) {
		if (propertyManagerId == null) {
			return Collections.emptyList();
		}
		return mapper.readByPropertyManagerIdWithStateCreated(propertyManagerId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentDetailsModel> readAllByProductId(Long productId) {
		if (productId == null) {
			return Collections.emptyList();
		}
		return mapper.readAllByProductId(productId);
	}
}
