package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PaymentGatewayProviderModel;
import com.mybookingpal.dataaccesslayer.mappers.PaymentGatewayProviderMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PaymentGatewayProviderDao {
	@Autowired
	PaymentGatewayProviderMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentGatewayProviderModel paymentGatewayProviderModel) {
		mapper.create(paymentGatewayProviderModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentGatewayProviderModel read(Integer id) {
		if (id == null) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentGatewayProviderModel readByName(String name) {
		if (StringUtils.isBlank(name)) {
			return null;
		}
		return mapper.readByName(name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentGatewayProviderModel> list() {
		return mapper.list();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentGatewayProviderModel paymentGatewayProviderModel) {
		if (paymentGatewayProviderModel != null) {
			mapper.update(paymentGatewayProviderModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (id != null) {
			mapper.delete(id);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentGatewayProviderModel> supportSplitPayment() {
		return mapper.supportSplitPayment();
	}
}
