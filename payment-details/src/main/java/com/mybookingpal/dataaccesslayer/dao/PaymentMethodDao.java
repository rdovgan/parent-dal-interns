package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PaymentMethodModel;
import com.mybookingpal.dataaccesslayer.mappers.PaymentMethodMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PaymentMethodDao {
	@Autowired
	PaymentMethodMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentMethodModel item) {
		if (item != null) {
			mapper.create(item);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentMethodModel readByPm(Integer partyId) {
		if (partyId == null) {
			return null;
		}
		return mapper.readByPm(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentMethodModel read(Integer id) {
		if (id == null) {
			return null;
		}
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentMethodModel item) {
		if (item != null) {
			mapper.update(item);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(Integer id) {
		if (id != null) {
			mapper.delete(id);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateByParty(PaymentMethodModel item) {
		if (item != null && item.getPartyId() != null) {
			mapper.updateByParty(item);
		}
	}
}
