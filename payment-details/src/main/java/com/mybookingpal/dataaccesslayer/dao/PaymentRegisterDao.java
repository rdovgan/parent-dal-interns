package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PaymentRegisterModel;
import com.mybookingpal.dataaccesslayer.mappers.PaymentRegisterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PaymentRegisterDao {
	@Autowired
	PaymentRegisterMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentRegisterModel paymentRegisterModel) {
		if (paymentRegisterModel != null) {
			mapper.create(paymentRegisterModel);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentRegisterModel read(Integer id) {
		if (id == null) {
			return null;
		}
		return mapper.read(id);
	}
}
