package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.utils.entity.BigDecimalExt;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigDecimalExtHandler extends BaseTypeHandler<BigDecimalExt> {
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int columnIndex, BigDecimalExt parameter, JdbcType jdbcType) throws SQLException {
		preparedStatement.setBigDecimal(columnIndex, parameter);
	}

	@Override
	public BigDecimalExt getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		BigDecimal value = resultSet.getBigDecimal(columnName);
		if (value == null) {
			return null;
		}
		return new BigDecimalExt(value);
	}

	@Override
	public BigDecimalExt getNullableResult(ResultSet resultSet, int i) throws SQLException {
		return new BigDecimalExt(resultSet.getString(i));
	}

	@Override
	public BigDecimalExt getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		BigDecimal value = callableStatement.getBigDecimal(columnIndex);
		if (value == null) {
			return null;
		}
		return new BigDecimalExt(value);
	}
}
