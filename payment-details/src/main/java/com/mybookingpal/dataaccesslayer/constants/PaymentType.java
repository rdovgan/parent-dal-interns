package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum PaymentType {
	FULL(1), SPLIT(2);

	private final Integer number;

	PaymentType(Integer number) {

		this.number = number;
	}

	public Integer getNumber() {
		return number;
	}

	public static PaymentType getByNumber(Integer number) {
		return Arrays.stream(values())
				.filter(p -> p.getNumber() == number)
				.findFirst()
				.orElse(FULL);
	}
}

