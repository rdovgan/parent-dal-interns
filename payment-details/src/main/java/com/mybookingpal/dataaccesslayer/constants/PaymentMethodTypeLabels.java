package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum PaymentMethodTypeLabels {
	MAIL("1", "Mail Checks"), PAYPAL("2", "PayPal"), DEPOSIT("3", "Bank Transfer"), WIRE_TRANSFER("4", "Wire Transfer"), OTHER("", "Other");

	private String type;
	private String label;

	public String getType() {
		return type;
	}

	public String getLabel() {
		return label;
	}

	PaymentMethodTypeLabels(String type, String label) {
		this.type = type;
		this.label = label;
	}

	public static PaymentMethodTypeLabels getByType(String type) {
		return Arrays.stream(PaymentMethodTypeLabels.values())
				.filter(subStateEnum -> subStateEnum.getType() != null && subStateEnum.getType()
						.equals(type))
				.findFirst()
				.orElse(OTHER);
	}

	public static String getLabelByType(String type) {
		return getByType(type).label;
	}
}
