package com.mybookingpal.dataaccesslayer.constants;

public enum PaymentDetailsState {
	CREATED, FINAL
}
