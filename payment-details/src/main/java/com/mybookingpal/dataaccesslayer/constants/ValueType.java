package com.mybookingpal.dataaccesslayer.constants;

import java.util.Arrays;

public enum ValueType {
	PERCENTAGE(1), FLAT(2), NIGHTS(3);

	private final Integer number;

	ValueType(Integer number) {

		this.number = number;
	}

	public Integer getNumber() {
		return number;
	}

	public static ValueType getByNumber(Integer number) {
		return Arrays.stream(values())
				.filter(p -> p.getNumber() == number)
				.findFirst()
				.orElse(PERCENTAGE);
	}
}