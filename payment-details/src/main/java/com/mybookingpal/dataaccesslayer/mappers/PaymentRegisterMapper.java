package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PaymentRegisterModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaymentRegisterMapper {
	void create(PaymentRegisterModel paymentRegisterModel);

	PaymentRegisterModel read(Integer id);
}
