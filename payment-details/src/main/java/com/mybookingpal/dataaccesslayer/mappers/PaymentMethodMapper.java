package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PaymentMethodModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PaymentMethodMapper {
	void create(PaymentMethodModel item);

	PaymentMethodModel readByPm(@Param("partyId") Integer partyId);

	PaymentMethodModel read(Integer id);

	void update(PaymentMethodModel item);

	void delete(Integer id);

	void updateByParty(PaymentMethodModel item);
}
