package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PaymentDetailsModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PaymentDetailsMapper {

	void create(PaymentDetailsModel paymentDetailsModel);

	PaymentDetailsModel read(Long id);

	void update(PaymentDetailsModel paymentDetailsModel);

	List<PaymentDetailsModel> readByProductIdWithStateCreated(Integer productId);

	List<PaymentDetailsModel> readByPropertyManagerIdWithStateCreated(Integer propertyManagerId);

	List<PaymentDetailsModel> readAllByProductId(Long productId);
}
