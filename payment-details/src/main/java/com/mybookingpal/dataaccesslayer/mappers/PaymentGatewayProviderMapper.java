package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PaymentGatewayProviderModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PaymentGatewayProviderMapper {
	void create(PaymentGatewayProviderModel paymentGatewayProviderModel);

	PaymentGatewayProviderModel read(Integer id);

	PaymentGatewayProviderModel readByName(String name);

	List<PaymentGatewayProviderModel> list();

	void update(PaymentGatewayProviderModel paymentGatewayProviderModel);

	void delete(Integer id);

	List<PaymentGatewayProviderModel> supportSplitPayment();
}
