package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PendingPayoutModel;
import com.mybookingpal.dataaccesslayer.dto.ReconciliationRequestDto;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PendingPayoutMapper {

	void create(PendingPayoutModel action);

	void insertList(@Param("list") List<PendingPayoutModel> list);

	void update(PendingPayoutModel action);

	PendingPayoutModel readByReservation(@Param("reservationId") Long reservationId);

	List<PendingPayoutModel> readByStatusForNotification(@Param("status") Integer status, @Param("name") String name);

	void approveNotSentPendingPayouts();

	List<PendingPayoutModel> readByReservationIds(@Param("ids") List<Integer> ids);

	List<PendingPayoutModel> getPendingPayoutReport(ReconciliationRequestDto requestDto);

	Integer getCountOfPendingPayoutForReconciliationReport(ReconciliationRequestDto reconciliationRequest);

	void updateStatusApproveDateAndAcknowledgeByPayoutIdsList(@Param("status") Integer status, @Param("acknowledgeBy") Boolean acknowledgeBy,
			@Param("approveDate") Date approveDate, @Param("list") List<Long> pendingPayoutIds);
}
