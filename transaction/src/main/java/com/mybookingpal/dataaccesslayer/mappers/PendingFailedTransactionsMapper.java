package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.FailedTransactionPayment;
import com.mybookingpal.dataaccesslayer.dto.FailedTransactionPaymentRequest;
import com.mybookingpal.dataaccesslayer.entity.PendingFailedTransactionModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PendingFailedTransactionsMapper {

	List<FailedTransactionPayment> readAllFailedTransactionPayments(@Param("request") FailedTransactionPaymentRequest request);

	void update(PendingFailedTransactionModel model);

	PendingFailedTransactionModel read(Long id);

	PendingFailedTransactionModel readPaymentsByTransactionId(Long transactionId);

	List<PendingFailedTransactionModel> readPaymentsByReservationId(Long reservationId);

	void create(PendingFailedTransactionModel pendingFailedTransaction);

	PendingFailedTransactionModel readActiveTransactionByGroupedReservationId(Long id);

	PendingFailedTransactionModel readAllActiveFailedPaymentReservationsByChannelId(Integer id);

	void markAsEmailSentToAgent(Long id);
}
