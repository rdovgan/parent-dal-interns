package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.PaymentTransactionModel;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PaymentTransactionMapper {

	void create(PaymentTransactionModel paymentTransactionModel);

	PaymentTransactionModel read(int id);

	void update(PaymentTransactionModel paymentTransactionModel);

	void delete(int id);

	List<PaymentTransactionModel> transactionToProcessRefund();

	List<PaymentTransactionModel> readByReservationId(int reservationId);

	List<PaymentTransactionModel> readAcceptedTransactionsByReservationId(int reservationId);

	PaymentTransactionModel getFailedTransactionByTransactionId(Long transactionId);

	PaymentTransactionModel getStripeFailedTransactionByReservationId(Long reservationId);

	PaymentTransactionModel getInitialTransactionByReservationId(int reservationId);

	List<PaymentTransactionModel> listLast24hours(NameIdAction action);

	PaymentTransactionModel readLastFailedTransactionForGroupedReservation(Long groupedReservationId);

	PaymentTransactionModel readLastFailedPaymentTransactionForReservation(@Param("reservationId") Integer reservationId);

	List<PaymentTransactionModel> readTransactionsByIds(List<Long> transactionIds);

	List<PaymentTransactionModel> readAllTransactionsByIds(List<Long> transactionIds);

	List<PaymentTransactionModel> readByGroupReservationIdAndTransactionType(@Param("id") Long id, @Param("type") String type);
}
