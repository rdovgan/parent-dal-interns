package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.IsHasTable;
import com.mybookingpal.dataaccesslayer.entity.PendingTransactionModel;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PendingTransactionMapper {

	void create(PendingTransactionModel pendingTransaction);

	void update(PendingTransactionModel pendingTransaction);

	PendingTransactionModel readByReservation(Integer reservationId);

	List<PendingTransactionModel> readListByReservation(Integer reservationId);

	PendingTransactionModel readActivePendingTransactionByReservationId(Integer reservationId);

	List<PendingTransactionModel> readActivePendingTransactionsByReservationId(Integer reservationId);

	List<PendingTransactionModel> readActiveTransactionsByDate(Date date);

	List<PendingTransactionModel> readMarriottFailedTransactionsByDate(Date date);

	List<PendingTransactionModel> readByTransactionIds(@Param("list") List<Long> transactionIds);

	List<PendingTransactionModel> readPendingTransactionListForVirtualCard(IsHasTable action);

	Integer getCountOfVirtualCardRecords();

	PendingTransactionModel readByTransactionId(@Param("transactionId") Long transactionId);
}
