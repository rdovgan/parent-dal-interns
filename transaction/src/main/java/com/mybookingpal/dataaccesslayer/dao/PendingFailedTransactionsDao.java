package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.FailedTransactionPayment;
import com.mybookingpal.dataaccesslayer.dto.FailedTransactionPaymentRequest;
import com.mybookingpal.dataaccesslayer.entity.PendingFailedTransactionModel;
import com.mybookingpal.dataaccesslayer.mappers.PendingFailedTransactionsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PendingFailedTransactionsDao {

	@Autowired
	private PendingFailedTransactionsMapper pendingFailedTransactionsMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FailedTransactionPayment> readAllFailedTransactionPayments(FailedTransactionPaymentRequest request) {
		return pendingFailedTransactionsMapper.readAllFailedTransactionPayments(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PendingFailedTransactionModel model) {
		if (Objects.isNull(model)) {
			return;
		}
		pendingFailedTransactionsMapper.update(model);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingFailedTransactionModel read(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return pendingFailedTransactionsMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingFailedTransactionModel readPaymentsByTransactionId(Long transactionId) {
		if (Objects.isNull(transactionId)) {
			return null;
		}
		return pendingFailedTransactionsMapper.readPaymentsByTransactionId(transactionId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingFailedTransactionModel> readPaymentsByReservationId(Long reservationId) {
		if (Objects.isNull(reservationId)) {
			return Collections.emptyList();
		}
		return pendingFailedTransactionsMapper.readPaymentsByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PendingFailedTransactionModel pendingFailedTransaction) {
		if (Objects.isNull(pendingFailedTransaction)) {
			return;
		}
		pendingFailedTransactionsMapper.create(pendingFailedTransaction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingFailedTransactionModel readActiveTransactionByGroupedReservationId(Long id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return pendingFailedTransactionsMapper.readActiveTransactionByGroupedReservationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingFailedTransactionModel readAllActiveFailedPaymentReservationsByChannelId(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}
		return pendingFailedTransactionsMapper.readAllActiveFailedPaymentReservationsByChannelId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsEmailSentToAgent(Long id) {
		if (Objects.isNull(id)) {
			return;
		}
		pendingFailedTransactionsMapper.markAsEmailSentToAgent(id);
	}
}
