package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.IsHasTable;
import com.mybookingpal.dataaccesslayer.entity.PendingTransactionModel;
import com.mybookingpal.dataaccesslayer.mappers.PendingTransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class PendingTransactionDao {

	@Autowired
	private PendingTransactionMapper pendingTransactionMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PendingTransactionModel pendingTransaction) {
		if (Objects.isNull(pendingTransaction)) {
			return;
		}
		pendingTransactionMapper.create(pendingTransaction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PendingTransactionModel pendingTransaction) {
		if (Objects.isNull(pendingTransaction)) {
			return;
		}
		pendingTransactionMapper.update(pendingTransaction);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingTransactionModel readByReservation(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return pendingTransactionMapper.readByReservation(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readListByReservation(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readListByReservation(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingTransactionModel readActivePendingTransactionByReservationId(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return pendingTransactionMapper.readActivePendingTransactionByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readActivePendingTransactionsByReservationId(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readActivePendingTransactionsByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readActiveTransactionsByDate(Date date) {
		if (Objects.isNull(date)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readActiveTransactionsByDate(date);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readMarriottFailedTransactionsByDate(Date date) {
		if (Objects.isNull(date)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readMarriottFailedTransactionsByDate(date);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readByTransactionIds(List<Long> transactionIds) {
		if (Objects.isNull(transactionIds)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readByTransactionIds(transactionIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingTransactionModel> readPendingTransactionListForVirtualCard(IsHasTable action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return pendingTransactionMapper.readPendingTransactionListForVirtualCard(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getCountOfVirtualCardRecords() {
		return pendingTransactionMapper.getCountOfVirtualCardRecords();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingTransactionModel readByTransactionId(Long transactionId) {
		if (Objects.isNull(transactionId)) {
			return null;
		}
		return pendingTransactionMapper.readByTransactionId(transactionId);
	}
}
