package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PaymentTransactionModel;
import com.mybookingpal.dataaccesslayer.mappers.PaymentTransactionMapper;
import com.mybookingpal.utils.entity.NameIdAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class PaymentTransactionDao {

	@Autowired
	private PaymentTransactionMapper paymentTransactionMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentTransactionModel paymentTransactionModel) {
		if (Objects.isNull(paymentTransactionModel)) {
			return;
		}
		paymentTransactionMapper.create(paymentTransactionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel read(int id) {
		return paymentTransactionMapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentTransactionModel paymentTransactionModel) {
		if (paymentTransactionModel == null) {
			return;
		}
		paymentTransactionMapper.update(paymentTransactionModel);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(int id) {
		paymentTransactionMapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> transactionToProcessRefund() {
		return paymentTransactionMapper.transactionToProcessRefund();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> readByReservationId(int reservationId) {
		return paymentTransactionMapper.readByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> readAcceptedTransactionsByReservationId(int reservationId) {
		return paymentTransactionMapper.readAcceptedTransactionsByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel getFailedTransactionByTransactionId(Long transactionId) {
		if (Objects.isNull(transactionId)) {
			return null;
		}
		return paymentTransactionMapper.getFailedTransactionByTransactionId(transactionId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel getStripeFailedTransactionByReservationId(Long reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return paymentTransactionMapper.getStripeFailedTransactionByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel getInitialTransactionByReservationId(int reservationId) {
		return paymentTransactionMapper.getInitialTransactionByReservationId(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> listLast24hours(NameIdAction action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return paymentTransactionMapper.listLast24hours(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel readLastFailedTransactionForGroupedReservation(Long groupedReservationId) {
		if (Objects.isNull(groupedReservationId)) {
			return null;
		}
		return paymentTransactionMapper.readLastFailedTransactionForGroupedReservation(groupedReservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PaymentTransactionModel readLastFailedPaymentTransactionForReservation(Integer reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return paymentTransactionMapper.readLastFailedPaymentTransactionForReservation(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> readTransactionsByIds(List<Long> transactionIds) {
		if (Objects.isNull(transactionIds)) {
			return Collections.emptyList();
		}
		return paymentTransactionMapper.readTransactionsByIds(transactionIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> readAllTransactionsByIds(List<Long> transactionIds) {
		if (Objects.isNull(transactionIds)) {
			return Collections.emptyList();
		}
		return paymentTransactionMapper.readAllTransactionsByIds(transactionIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentTransactionModel> readByGroupReservationIdAndTransactionType(Long id, String type) {
		if (Objects.isNull(id) || Objects.isNull(type)) {
			return Collections.emptyList();
		}
		return paymentTransactionMapper.readByGroupReservationIdAndTransactionType(id, type);
	}
}
