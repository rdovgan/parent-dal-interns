package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.PendingPayoutModel;
import com.mybookingpal.dataaccesslayer.dto.ReconciliationRequestDto;
import com.mybookingpal.dataaccesslayer.mappers.PendingPayoutMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class PendingPayoutDao {

	@Autowired
	private PendingPayoutMapper pendingPayoutMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PendingPayoutModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		pendingPayoutMapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList(List<PendingPayoutModel> list) {
		if (Objects.isNull(list)) {
			return;
		}
		pendingPayoutMapper.insertList(list);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PendingPayoutModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		pendingPayoutMapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public PendingPayoutModel readByReservation(Long reservationId) {
		if (Objects.isNull(reservationId)) {
			return null;
		}
		return pendingPayoutMapper.readByReservation(reservationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingPayoutModel> readByStatusForNotification(Integer status, String name) {
		if (Objects.isNull(status) || Objects.isNull(name)) {
			return Collections.emptyList();
		}
		return pendingPayoutMapper.readByStatusForNotification(status, name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void approveNotSentPendingPayouts() {
		pendingPayoutMapper.approveNotSentPendingPayouts();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingPayoutModel> readByReservationIds(List<Integer> ids) {
		if (Objects.isNull(ids)) {
			return Collections.emptyList();
		}
		return pendingPayoutMapper.readByReservationIds(ids);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PendingPayoutModel> getPendingPayoutReport(ReconciliationRequestDto requestDto) {
		if (Objects.isNull(requestDto)) {
			return Collections.emptyList();
		}
		return pendingPayoutMapper.getPendingPayoutReport(requestDto);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getCountOfPendingPayoutForReconciliationReport(ReconciliationRequestDto reconciliationRequest) {
		if (Objects.isNull(reconciliationRequest)) {
			return null;
		}
		return pendingPayoutMapper.getCountOfPendingPayoutForReconciliationReport(reconciliationRequest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStatusApproveDateAndAcknowledgeByPayoutIdsList(Integer status, Boolean acknowledgeBy, Date approveDate, List<Long> pendingPayoutIds) {
		if (Objects.isNull(pendingPayoutIds)) {
			return;
		}
		pendingPayoutMapper.updateStatusApproveDateAndAcknowledgeByPayoutIdsList(status, acknowledgeBy, approveDate, pendingPayoutIds);
	}
}
