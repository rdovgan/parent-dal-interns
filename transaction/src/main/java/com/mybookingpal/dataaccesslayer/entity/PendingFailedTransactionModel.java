package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.constants.Actions;
import com.mybookingpal.dataaccesslayer.constants.PaymentType;
import com.mybookingpal.dataaccesslayer.constants.Status;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;
import java.util.Date;

public class PendingFailedTransactionModel {

	private Long id;
	private Date entryDateTime;
	private Long reservationId;
	private Long channelReservationId;
	private Long transactionId;
	private Status status;
	private Date assignedDateTime;
	private Long assignedToWho;
	private Long paymentGatewayId;
	private String reason;
	private Date lastDayToCancel;
	private Long pmId;
	private PaymentType paymentType;
	private String comment;
	private Actions action;
	private Date resolved;
	private Long groupedReservationId;
	private LocalDateTime bookingComNotified;
	private Boolean sentEmailToAgent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Date entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	/**
	 *
	 * @deprecated since it13. Use {@link ReservationConfirmation#getConfirmationId()}
	 */
	@Deprecated
	public Long getChannelReservationId() {
		return channelReservationId;
	}

	/**
	 *
	 * @deprecated since it13. Use {@link ReservationConfirmation#getConfirmationId()}
	 */
	@Deprecated
	public void setChannelReservationId(Long channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getAssignedDateTime() {
		return assignedDateTime;
	}

	public void setAssignedDateTime(Date assignedDateTime) {
		this.assignedDateTime = assignedDateTime;
	}

	public Long getAssignedToWho() {
		return assignedToWho;
	}

	public void setAssignedToWho(Long assignedToWho) {
		this.assignedToWho = assignedToWho;
	}

	public Long getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(Long paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public Long getPmId() {
		return pmId;
	}

	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Actions getAction() {
		return action;
	}

	public void setAction(Actions action) {
		this.action = action;
	}

	public Date getResolved() {
		return resolved;
	}

	public void setResolved(Date resolved) {
		this.resolved = resolved;
	}

	public Long getGroupedReservationId() {
		return groupedReservationId;
	}

	public void setGroupedReservationId(Long groupedReservationId) {
		this.groupedReservationId = groupedReservationId;
	}

	public LocalDateTime getBookingComNotified() {
		return bookingComNotified;
	}

	public void setBookingComNotified(LocalDateTime bookingComNotified) {
		this.bookingComNotified = bookingComNotified;
	}

	public Boolean getSentEmailToAgent() {
		return sentEmailToAgent;
	}

	public void setSentEmailToAgent(Boolean sentEmailToAgent) {
		this.sentEmailToAgent = sentEmailToAgent;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		PendingFailedTransactionModel that = (PendingFailedTransactionModel) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.append(entryDateTime, that.entryDateTime)
				.append(reservationId, that.reservationId)
				.append(channelReservationId, that.channelReservationId)
				.append(transactionId, that.transactionId)
				.append(status, that.status)
				.append(assignedDateTime, that.assignedDateTime)
				.append(assignedToWho, that.assignedToWho)
				.append(paymentGatewayId, that.paymentGatewayId)
				.append(reason, that.reason)
				.append(lastDayToCancel, that.lastDayToCancel)
				.append(pmId, that.pmId)
				.append(paymentType, that.paymentType)
				.append(comment, that.comment)
				.append(action, that.action)
				.append(resolved, that.resolved)
				.append(groupedReservationId, that.groupedReservationId)
				.append(bookingComNotified, that.bookingComNotified)
				.append(sentEmailToAgent, that.sentEmailToAgent)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.append(entryDateTime)
				.append(reservationId)
				.append(channelReservationId)
				.append(transactionId)
				.append(status)
				.append(assignedDateTime)
				.append(assignedToWho)
				.append(paymentGatewayId)
				.append(reason)
				.append(lastDayToCancel)
				.append(pmId)
				.append(paymentType)
				.append(comment)
				.append(action)
				.append(resolved)
				.append(groupedReservationId)
				.append(bookingComNotified)
				.append(sentEmailToAgent)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("entryDateTime", entryDateTime)
				.append("reservationId", reservationId)
				.append("channelReservationId", channelReservationId)
				.append("transactionId", transactionId)
				.append("status", status)
				.append("assignedDateTime", assignedDateTime)
				.append("assignedToWho", assignedToWho)
				.append("paymentGatewayId", paymentGatewayId)
				.append("reason", reason)
				.append("lastDayToCancel", lastDayToCancel)
				.append("pmId", pmId)
				.append("paymentType", paymentType)
				.append("comment", comment)
				.append("action", action)
				.append("resolved", resolved)
				.append("groupedReservationId", groupedReservationId)
				.append("bookingComNotified", bookingComNotified)
				.append("sentEmailToAgent", sentEmailToAgent)
				.toString();
	}
}
