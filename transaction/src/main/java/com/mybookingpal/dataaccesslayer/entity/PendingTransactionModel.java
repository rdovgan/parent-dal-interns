package com.mybookingpal.dataaccesslayer.entity;

import java.math.BigDecimal;
import java.util.Date;

public class PendingTransactionModel {

	private Integer id;
	private Date entryDateTime;
	private String bookingId;
	private String pmsConfirmationId;
	private Integer paymentGatewayId;
	private Integer fundsHolder;
	private String partialIin;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private Integer partnerId;
	private Integer supplierId;
	private Date chargeDate;
	private BigDecimal chargeAmount = BigDecimal.ZERO;
	private String currency;
	private BigDecimal commission = BigDecimal.ZERO;
	private BigDecimal partnerPayment = BigDecimal.ZERO;
	private BigDecimal bookingpalPayment = BigDecimal.ZERO;
	private String gatewayTransactionId;
	private Integer status;
	private Boolean autopay;
	private Boolean netRate;
	private BigDecimal pmCommissionValue = BigDecimal.ZERO;
	private BigDecimal additionalCommissionValue = BigDecimal.ZERO;
	private BigDecimal pmsPayment = BigDecimal.ZERO;
	private BigDecimal creditCardFee = BigDecimal.ZERO;
	private Boolean virtualCard;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Date entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getPmsConfirmationId() {
		return pmsConfirmationId;
	}

	public void setPmsConfirmationId(String pmsConfirmationId) {
		this.pmsConfirmationId = pmsConfirmationId;
	}

	public Integer getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(Integer paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public Integer getFundsHolder() {
		return fundsHolder;
	}

	@Deprecated
	public void setFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public void checkAndDefineFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder == null ? 0 : fundsHolder;
	}

	public String getPartialIin() {
		return partialIin;
	}

	public void setPartialIin(String partialIin) {
		this.partialIin = partialIin;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	@Deprecated
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	@Deprecated
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public Date getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	@Deprecated
	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = new BigDecimal(chargeAmount);
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	@Deprecated
	public void setCommission(double commission) {
		this.commission = new BigDecimal(commission);
	}

	public BigDecimal getPartnerPayment() {
		return partnerPayment;
	}

	public void setPartnerPayment(BigDecimal partnerPayment) {
		this.partnerPayment = partnerPayment;
	}

	@Deprecated
	public void setPartnerPayment(double partnerPayment) {
		this.partnerPayment = new BigDecimal(partnerPayment);
	}

	public BigDecimal getBookingpalPayment() {
		return bookingpalPayment;
	}

	public void setBookingpalPayment(BigDecimal bookingpalPayment) {
		this.bookingpalPayment = bookingpalPayment;
	}

	@Deprecated
	public void setBookingpalPayment(double bookingpalPayment) {
		this.bookingpalPayment = new BigDecimal(bookingpalPayment);
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean isAutopay() {
		return autopay;
	}

	public void setAutopay(Boolean autopay) {
		this.autopay = autopay;
	}

	public Boolean isNetRate() {
		return netRate;
	}

	public void setNetRate(Boolean netRate) {
		this.netRate = netRate;
	}

	public BigDecimal getPmCommissionValue() {
		return pmCommissionValue;
	}

	public void setPmCommissionValue(BigDecimal pmCommissionValue) {
		this.pmCommissionValue = pmCommissionValue;
	}

	@Deprecated
	public void setPmCommissionValue(double pmCommissionValue) {
		this.pmCommissionValue = new BigDecimal(pmCommissionValue);
	}

	public BigDecimal getAdditionalCommissionValue() {
		return additionalCommissionValue;
	}

	public void setAdditionalCommissionValue(BigDecimal additionalCommissionValue) {
		this.additionalCommissionValue = additionalCommissionValue;
	}

	@Deprecated
	public void setAdditionalCommissionValue(double additionalCommissionValue) {
		this.additionalCommissionValue = new BigDecimal(additionalCommissionValue);
	}

	public BigDecimal getPmsPayment() {
		return pmsPayment;
	}

	public void setPmsPayment(BigDecimal pmsPayment) {
		this.pmsPayment = pmsPayment;
	}

	@Deprecated
	public void setPmsPayment(double pmsPayment) {
		this.pmsPayment = new BigDecimal(pmsPayment);
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public void setCreditCardFee(Double creditCardFee) {
		this.creditCardFee = new BigDecimal(creditCardFee);
	}

	public Boolean isVirtualCard() {
		return virtualCard;
	}

	public void setVirtualCard(Boolean virtualCard) {
		this.virtualCard = virtualCard;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PendingTransactionModel [id=");
		builder.append(getId());
		builder.append("], [autopayType=");
		builder.append(isAutopay());
		builder.append("], [BookingId=");
		builder.append(getBookingId());
		builder.append("], [BookingpalPayment=");
		builder.append(getBookingpalPayment());
		builder.append("], [ChargeAmount=");
		builder.append(getChargeAmount());
		builder.append("], [ChargeDate=");
		builder.append(getChargeDate());
		builder.append("], [Commission=");
		builder.append(getCommission());
		builder.append("], [Currency=");
		builder.append(getCurrency());
		builder.append("], [EntryDateTime=");
		builder.append(getEntryDateTime());
		builder.append("], [FundsHolder=");
		builder.append(getFundsHolder());
		builder.append("], [GatewayTransactionId=");
		builder.append(getGatewayTransactionId());
		builder.append("], [PartialIin=");
		builder.append(getPartialIin());
		builder.append("], [PartnerId=");
		builder.append(getPartnerId());
		builder.append("], [PartnerPayment=");
		builder.append(getPartnerPayment());
		builder.append("], [PaymentGatewayId=");
		builder.append(getPaymentGatewayId());
		builder.append("], [PmsConfirmationId=");
		builder.append(getPmsConfirmationId());
		builder.append("], [Status=");
		builder.append(getStatus());
		builder.append("], [SupplierId=");
		builder.append(getSupplierId());
		builder.append("], [netRate=");
		builder.append(isNetRate());
		builder.append("], [pmCommissionValue=");
		builder.append(getPmCommissionValue());
		builder.append("], [additionalCommissionValue=");
		builder.append(getAdditionalCommissionValue());
		builder.append("], [pmsPayment=");
		builder.append(getPmsPayment());
		builder.append("], [creditCardFee=");
		builder.append(getCreditCardFee());
		builder.append("]");
		return builder.toString();
	}

}

