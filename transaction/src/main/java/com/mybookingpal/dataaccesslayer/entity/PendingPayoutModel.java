package com.mybookingpal.dataaccesslayer.entity;

import java.util.Date;

public class PendingPayoutModel {

	private Long id;
	private Long reservationId;
	private Long transactionId;
	private String receiverEmail;
	private Integer status;
	private Boolean acknowledgeBy;
	private Date sentDate;
	private Date approveDate;
	private String receiverName;
	private String receiverIp;
	private Date createdDate;
	private Date version;

	public PendingPayoutModel() {
	}

	public PendingPayoutModel(Long reservationId, Long transactionId, Integer status, String pmName) {
		this.reservationId = reservationId;
		this.transactionId = transactionId;
		this.status = status;
		this.receiverName = pmName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getAcknowledgeBy() {
		return acknowledgeBy;
	}

	public void setAcknowledgeBy(Boolean acknowledgeBy) {
		this.acknowledgeBy = acknowledgeBy;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverIp() {
		return receiverIp;
	}

	public void setReceiverIp(String receiverIp) {
		this.receiverIp = receiverIp;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}
