package com.mybookingpal.dataaccesslayer.entity;

import java.math.BigDecimal;
import java.util.Date;

//TODO need to investigate
//@XmlAccessorType(XmlAccessType.NONE)
public class PaymentTransactionModel {

	protected Long id;
	protected Date createDate;
	protected Integer serverId;
	protected Integer reservationId;
	protected String pmsConfirmationId;
	protected Integer paymentMethod;
	protected String gatewayTransactionId;
	protected Integer gatewayId;
	protected Integer fundsHolder;
	protected String partialIin;
	protected String status;
	protected String message;
	protected Integer partnerId;
	protected Integer supplierId;
	protected Date chargeDate;
	protected Date invoiceDate;
	protected String currency;
	protected BigDecimal totalCommission;
	protected BigDecimal partnerPayment;
	protected BigDecimal totalBookingpalPayment;
	protected BigDecimal finalAmount;
	protected BigDecimal finalAmountUsd;
	protected BigDecimal totalAmount;
	protected BigDecimal totalAmountUsd;
	protected BigDecimal creditCardFee;
	protected String chargeType;
	protected Boolean cancelled;
	protected Boolean netRate;

	// net rate commissions
	protected BigDecimal pmsPayment;
	protected BigDecimal pmCommissionValue;

	protected BigDecimal additionalCommissionValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getPmsConfirmationId() {
		return pmsConfirmationId;
	}

	public void setPmsConfirmationId(String pmsConfirmationId) {
		this.pmsConfirmationId = pmsConfirmationId;
	}

	public Integer getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(Integer paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Integer getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(Integer gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Integer getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public String getPartialIin() {
		return partialIin;
	}

	public void setPartialIin(String partialIin) {
		this.partialIin = partialIin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public Date getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = new BigDecimal(totalAmount);
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalCommission() {
		return totalCommission;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setTotalCommission(BigDecimal totalCommission) {
		this.totalCommission = totalCommission;
	}

	public void setTotalCommission(double totalCommission) {
		this.totalCommission = new BigDecimal(totalCommission);
	}

	public BigDecimal getPartnerPayment() {
		return partnerPayment;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setPartnerPayment(BigDecimal partnerPayment) {
		this.partnerPayment = partnerPayment;
	}

	public void setPartnerPayment(double partnerPayment) {
		this.partnerPayment = new BigDecimal(partnerPayment);
	}

	public BigDecimal getTotalBookingpalPayment() {
		return totalBookingpalPayment;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setTotalBookingpalPayment(BigDecimal totalBookingpalPayment) {
		this.totalBookingpalPayment = totalBookingpalPayment;
	}

	public void setTotalBookingpalPayment(double totalBookingpalPayment) {
		this.totalBookingpalPayment = new BigDecimal(totalBookingpalPayment);
	}

	public BigDecimal getFinalAmount() {
		return finalAmount;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setFinalAmount(BigDecimal finalAmount) {
		this.finalAmount = finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = new BigDecimal(finalAmount);
	}

	public BigDecimal getFinalAmountUsd() {
		return finalAmountUsd;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setFinalAmountUsd(BigDecimal finalAmountUsd) {
		this.finalAmountUsd = finalAmountUsd;
	}

	public void setFinalAmountUsd(double finalAmountUsd) {
		this.finalAmountUsd = new BigDecimal(finalAmountUsd);
	}

	public BigDecimal getTotalAmountUsd() {
		return totalAmountUsd;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setTotalAmountUsd(BigDecimal totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	public void setTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd = new BigDecimal(totalAmountUsd);
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public void setCreditCardFee(Double creditCardFee) {
		this.creditCardFee = new BigDecimal(creditCardFee);
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public Boolean getCancelled() {
		return cancelled;
	}

	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	public Boolean getNetRate() {
		return netRate;
	}

	public void setNetRate(Boolean netRate) {
		this.netRate = netRate;
	}

	public BigDecimal getPmsPayment() {
		return pmsPayment;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setPmsPayment(BigDecimal pmsPayment) {
		this.pmsPayment = pmsPayment;
	}

	public void setPmsPayment(double pmsPayment) {
		this.pmsPayment = new BigDecimal(pmsPayment);
	}

	public BigDecimal getPmCommissionValue() {
		return pmCommissionValue;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setPmCommissionValue(BigDecimal pmCommissionValue) {
		this.pmCommissionValue = pmCommissionValue;
	}

	public void setPmCommissionValue(double pmCommissionValue) {
		this.pmCommissionValue = new BigDecimal(pmCommissionValue);
	}

	public BigDecimal getAdditionalCommissionValue() {
		return additionalCommissionValue;
	}

	//TODO need to investigate
	//@JsonIgnore
	public void setAdditionalCommissionValue(BigDecimal additionalCommissionValue) {
		this.additionalCommissionValue = additionalCommissionValue;
	}

	public void setAdditionalCommissionValue(double additionalCommissionValue) {
		this.additionalCommissionValue = new BigDecimal(additionalCommissionValue);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentTransactionModel [id=");
		builder.append(id);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", serverId=");
		builder.append(serverId);
		builder.append(", reservationId=");
		builder.append(reservationId);
		builder.append(", pmsConfirmationId=");
		builder.append(pmsConfirmationId);
		builder.append(", paymentMethod=");
		builder.append(paymentMethod);
		builder.append(", gatewayTransactionId=");
		builder.append(gatewayTransactionId);
		builder.append(", gatewayId=");
		builder.append(gatewayId);
		builder.append(", fundsHolder=");
		builder.append(fundsHolder);
		builder.append(", partialIin=");
		builder.append(partialIin);
		builder.append(", status=");
		builder.append(status);
		builder.append(", message=");
		builder.append(message);
		builder.append(", partnerId=");
		builder.append(partnerId);
		builder.append(", supplierId=");
		builder.append(supplierId);
		builder.append(", chargeDate=");
		builder.append(chargeDate);
		builder.append(", totalAmount=");
		builder.append(totalAmount);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", totalCommission=");
		builder.append(totalCommission);
		builder.append(", partnerPayment=");
		builder.append(partnerPayment);
		builder.append(", totalBookingpalPayment=");
		builder.append(totalBookingpalPayment);
		builder.append(", finalAmount=");
		builder.append(finalAmount);
		builder.append(", creditCardFee=");
		builder.append(creditCardFee);
		builder.append(", chargeType=");
		builder.append(chargeType);
		builder.append(", netRate=");
		builder.append(netRate);
		builder.append("]");
		return builder.toString();
	}
}
