package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.constants.Actions;
import com.mybookingpal.dataaccesslayer.constants.PaymentType;
import com.mybookingpal.dataaccesslayer.constants.Status;
import com.mybookingpal.dataaccesslayer.entity.PendingFailedTransactionModel;
import com.mybookingpal.dataaccesslayer.entity.PendingTransactionModel;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class PendingFailedTransactionUtils {

	public static PendingFailedTransactionModel getPendingFailedTransactionModel(Long id, Date entryDateTime, Long reservationId, Long channelReservationId,
			Long transactionId, Status status, Date assignedDateTime, Long assignedToWho, Long paymentGatewayId, String reason, Date lastDayToCancel, Long pmId,
			PaymentType paymentType, String comment, Actions action, Date resolved) {
		PendingFailedTransactionModel pendingFailedTransactionModel = new PendingFailedTransactionModel();
		pendingFailedTransactionModel.setId(id);
		pendingFailedTransactionModel.setEntryDateTime(entryDateTime);
		pendingFailedTransactionModel.setReservationId(reservationId);
		pendingFailedTransactionModel.setChannelReservationId(channelReservationId);
		pendingFailedTransactionModel.setTransactionId(transactionId);
		pendingFailedTransactionModel.setStatus(status);
		pendingFailedTransactionModel.setAssignedDateTime(assignedDateTime);
		pendingFailedTransactionModel.setAssignedToWho(assignedToWho);
		pendingFailedTransactionModel.setPaymentType(paymentType);
		pendingFailedTransactionModel.setReason(reason);
		pendingFailedTransactionModel.setLastDayToCancel(lastDayToCancel);
		pendingFailedTransactionModel.setPmId(pmId);
		pendingFailedTransactionModel.setPaymentType(paymentType);
		pendingFailedTransactionModel.setComment(comment);
		pendingFailedTransactionModel.setAction(action);
		pendingFailedTransactionModel.setResolved(resolved);
		return pendingFailedTransactionModel;
	}

	public static PendingFailedTransactionModel getPendingFailedTransactionModel(PendingFailedTransactionModel pendingFailedTransaction) {
		if (pendingFailedTransaction != null) {
			PendingFailedTransactionModel newPendingFailedTransactionModel = new PendingFailedTransactionModel();
			newPendingFailedTransactionModel.setId(pendingFailedTransaction.getId());
			newPendingFailedTransactionModel.setEntryDateTime(pendingFailedTransaction.getEntryDateTime());
			newPendingFailedTransactionModel.setReservationId(pendingFailedTransaction.getReservationId());
			newPendingFailedTransactionModel.setChannelReservationId(pendingFailedTransaction.getChannelReservationId());
			newPendingFailedTransactionModel.setTransactionId(pendingFailedTransaction.getTransactionId());
			newPendingFailedTransactionModel.setStatus(pendingFailedTransaction.getStatus());
			newPendingFailedTransactionModel.setAssignedDateTime(pendingFailedTransaction.getAssignedDateTime());
			newPendingFailedTransactionModel.setAssignedToWho(pendingFailedTransaction.getAssignedToWho());
			newPendingFailedTransactionModel.setPaymentGatewayId(pendingFailedTransaction.getPaymentGatewayId());
			newPendingFailedTransactionModel.setReason(pendingFailedTransaction.getReason());
			newPendingFailedTransactionModel.setLastDayToCancel(pendingFailedTransaction.getLastDayToCancel());
			newPendingFailedTransactionModel.setPmId(pendingFailedTransaction.getPmId());
			newPendingFailedTransactionModel.setPaymentType(pendingFailedTransaction.getPaymentType());
			newPendingFailedTransactionModel.setComment(pendingFailedTransaction.getComment());
			newPendingFailedTransactionModel.setAction(pendingFailedTransaction.getAction());
			newPendingFailedTransactionModel.setResolved(pendingFailedTransaction.getResolved());
			return newPendingFailedTransactionModel;
		}
		return null;
	}

}
