package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.dto.IsPaymentDetails;
import com.mybookingpal.dataaccesslayer.dto.IsPaymentGatewayProvider;
import com.mybookingpal.dataaccesslayer.dto.IsPropertyManagerInfo;
import com.mybookingpal.dataaccesslayer.dto.IsReservation;
import com.mybookingpal.dataaccesslayer.entity.PendingTransactionModel;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

@Component
public class PendingTransactionUtils {
	public static PendingTransactionModel getPendingTransactionModel(boolean autopay, IsReservation reservation, BigDecimal bookingpalPayment,
			BigDecimal chargeAmount, IsPropertyManagerInfo propertyManagerInfo, BigDecimal commission, String gatewayTransactionId, String partialIin,
			int partnerId, BigDecimal partnerPayment, IsPaymentGatewayProvider paymentGatewayProvider, String pmsConfirmationId, int status, int supplierId,
			IsPaymentDetails paymentDetails) throws ParseException {
		PendingTransactionModel pendingTransactionModel = new PendingTransactionModel();
		pendingTransactionModel.setAutopay(autopay);
		pendingTransactionModel.setBookingId(String.valueOf(reservation.getId()));
		pendingTransactionModel.setBookingpalPayment(bookingpalPayment);
		pendingTransactionModel.setChargeAmount(chargeAmount);
		//TODO
		//pendingTransactionModel.setChargeDate(PaymentHelper.getSecondChargeDate(reservation, propertyManagerInfo, paymentGatewayProvider, paymentDetails));
		pendingTransactionModel.setCommission(commission);
		pendingTransactionModel.setCurrency(reservation.getCurrency());
		pendingTransactionModel.setEntryDateTime(new Date());
		pendingTransactionModel.setGatewayTransactionId(gatewayTransactionId);
		pendingTransactionModel.setPartialIin(partialIin);
		pendingTransactionModel.setPartnerId(partnerId);
		pendingTransactionModel.setPartnerPayment(partnerPayment);
		pendingTransactionModel.setPaymentGatewayId(paymentGatewayProvider == null ? null : paymentGatewayProvider.getId());
		pendingTransactionModel.setPmsConfirmationId(pmsConfirmationId);
		pendingTransactionModel.setStatus(status);
		pendingTransactionModel.setSupplierId(supplierId);
		return pendingTransactionModel;
	}

	public PendingTransactionModel getPendingTransactionModel(boolean autopay, Date chargeDate, IsReservation reservation, BigDecimal bookingpalPayment,
			BigDecimal chargeAmount, BigDecimal commission, String gatewayTransactionId, String partialIin, int partnerId, BigDecimal partnerPayment,
			IsPaymentGatewayProvider paymentGatewayProvider, String pmsConfirmationId, int status, int supplierId) {
		PendingTransactionModel pendingTransactionModel = new PendingTransactionModel();
		pendingTransactionModel.setAutopay(autopay);
		pendingTransactionModel.setBookingId(String.valueOf(reservation.getId()));
		pendingTransactionModel.setBookingpalPayment(bookingpalPayment);
		pendingTransactionModel.setChargeAmount(chargeAmount);
		pendingTransactionModel.setChargeDate(chargeDate);
		pendingTransactionModel.setCommission(commission);
		pendingTransactionModel.setCurrency(reservation.getCurrency());
		pendingTransactionModel.setEntryDateTime(new Date());
		pendingTransactionModel.setGatewayTransactionId(gatewayTransactionId);
		pendingTransactionModel.setPartialIin(partialIin);
		pendingTransactionModel.setPartnerId(partnerId);
		pendingTransactionModel.setPartnerPayment(partnerPayment);
		pendingTransactionModel.setPaymentGatewayId(paymentGatewayProvider == null ? null : paymentGatewayProvider.getId());
		pendingTransactionModel.setPmsConfirmationId(pmsConfirmationId);
		pendingTransactionModel.setStatus(status);
		pendingTransactionModel.setSupplierId(supplierId);
		return pendingTransactionModel;
	}
}
