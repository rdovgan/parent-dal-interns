package com.mybookingpal.dataaccesslayer.dto;

public interface IsReservation {
	Integer getId();

	String getCurrency();
}
