package com.mybookingpal.dataaccesslayer.dto;

public class GuestDto {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
