package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.Actions;
import com.mybookingpal.dataaccesslayer.constants.PaymentType;
import com.mybookingpal.dataaccesslayer.constants.Status;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
public class FailedTransactionPayment {

	private Long id;
	private Date entryDateTime;
	private String channelReservationId;
	private Long transactionId;
	private Status status;
	private Date assignedDateTime;
	private Date resolvedDateTime;
	private Long paymentGatewayId;
	private String reason;
	private Date lastDayToCancel;
	private PaymentType paymentType;
	private String comment;
	private Actions actions;
	private GuestDto guest;
	private FailedTransactionPaymentAssignee assignee;
	private Long groupedReservationId;
	private Reservation reservation;
	private Property property;
	private Date bookingComNotified;
	private Long millisecondsRemain;
	private Long channelProductId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Date entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getAssignedDateTime() {
		return assignedDateTime;
	}

	public void setAssignedDateTime(Date assignedDateTime) {
		this.assignedDateTime = assignedDateTime;
	}

	public Date getResolvedDateTime() {
		return resolvedDateTime;
	}

	public void setResolvedDateTime(Date resolvedDateTime) {
		this.resolvedDateTime = resolvedDateTime;
	}

	public Long getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(Long paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Actions getActions() {
		return actions;
	}

	public void setActions(Actions actions) {
		this.actions = actions;
	}

	public GuestDto getGuest() {
		return guest;
	}

	public void setGuest(GuestDto guest) {
		this.guest = guest;
	}

	public FailedTransactionPaymentAssignee getAssignee() {
		return assignee;
	}

	public void setAssignee(FailedTransactionPaymentAssignee assignee) {
		this.assignee = assignee;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Date getBookingComNotified() {
		return bookingComNotified;
	}

	public void setBookingComNotified(Date bookingComNotified) {
		this.bookingComNotified = bookingComNotified;
	}

	public Long getMillisecondsRemain() {
		return millisecondsRemain;
	}

	public void setMillisecondsRemain(Long millisecondsRemain) {
		this.millisecondsRemain = millisecondsRemain;
	}

	public Long getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Long channelProductId) {
		this.channelProductId = channelProductId;
	}

	public Long getGroupedReservationId() {
		return groupedReservationId;
	}

	public void setGroupedReservationId(Long groupReservationId) {
		this.groupedReservationId = groupReservationId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("entryDateTime", entryDateTime).append("channelReservationId", channelReservationId)
				.append("channelProductId", channelProductId).append("transactionId", transactionId).append("status", status)
				.append("assignedDateTime", assignedDateTime).append("resolvedDateTime", resolvedDateTime).append("paymentGatewayId", paymentGatewayId)
				.append("reason", reason).append("lastDayToCancel", lastDayToCancel).append("paymentType", paymentType).append("comment", comment)
				.append("actions", actions).append("guest", guest).append("assignee", assignee).append("groupedReservationId", groupedReservationId)
				.append("reservation", reservation).append("property", property).append("bookingComNotified", bookingComNotified).toString();
	}
}

