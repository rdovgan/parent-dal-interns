package com.mybookingpal.dataaccesslayer.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Property {

	private String ids;
	private String names;
	private Long propertyManagerId;
	private String propertyManagerName;
	private String propertyManagerSystemName;
	private String channelId;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public Long getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Long propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public String getPropertyManagerName() {
		return propertyManagerName;
	}

	public void setPropertyManagerName(String propertyManagerName) {
		this.propertyManagerName = propertyManagerName;
	}

	public String getPropertyManagerSystemName() {
		return propertyManagerSystemName;
	}

	public void setPropertyManagerSystemName(String propertyManagerSystemName) {
		this.propertyManagerSystemName = propertyManagerSystemName;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("ids", ids).append("names", names).append("propertyManagerId", propertyManagerId)
				.append("propertyManagerName", propertyManagerName).append("propertyManagerSystemName", propertyManagerSystemName)
				.append("channelId", channelId).toString();
	}
}
