package com.mybookingpal.dataaccesslayer.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

public class Reservation {

	private String id;
	private LocalDate createdDate;
	private LocalDate fromDate;
	private LocalDate toDate;
	private String channelName;
	private String channelAbbreviation;

	public String getIds() {
		return id;
	}

	public void setIds(String ids) {
		this.id = ids;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("ids", id).append("createdDate", createdDate).append("fromDate", fromDate).append("toDate", toDate)
				.append("channelName", channelName).append("channelAbbreviation", channelAbbreviation).toString();
	}
}
