package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.dto.IsSortingParameters;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Scope(value = "prototype")
public class FailedTransactionPaymentRequest {
	private Date startDate;
	private Date endDate;
	private Long reservationId;
	private Long groupReservationId;
	private Long propertyId;
	private List<String> status;
	private Long agentId;
	private IsSortingParameters sortingParameters;
	private boolean expired;
	private Date currentServerTime = new Date();

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public Long getGroupReservationId() {
		return groupReservationId;
	}

	public void setGroupReservationId(Long groupReservationId) {
		this.groupReservationId = groupReservationId;
	}

	public Long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Long propertyId) {
		this.propertyId = propertyId;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Long getAgentId() {
		return agentId;
	}

	public IsSortingParameters getSortingParameters() {
		return sortingParameters;
	}

	public void setSortingParameters(IsSortingParameters sortingParameters) {
		this.sortingParameters = sortingParameters;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Date getCurrentServerTime() {
		return currentServerTime;
	}

	public void setCurrentServerTime(Date currentServerTime) {
		this.currentServerTime = currentServerTime;
	}
}

