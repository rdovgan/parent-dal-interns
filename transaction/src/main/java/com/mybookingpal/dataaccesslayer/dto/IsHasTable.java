package com.mybookingpal.dataaccesslayer.dto;

public interface IsHasTable {
	Integer getStartrow();

	Integer getNumrows();
}
