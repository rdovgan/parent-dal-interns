package com.mybookingpal.dataaccesslayer.dto;

public interface IsPaymentGatewayProvider {
	Integer getId();
}
