package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;
import java.util.List;

//TODO need to investigate
//@SuppressWarnings("SpellCheckingInspection")
//@XmlAccessorType(XmlAccessType.NONE)
public class ReconciliationRequestDto {

	//TODO need to investigate
	//	@ApiObjectField(description = "pm_name", name = "pm_name", required = true)
	//	@XmlElement(name = "pm_name", nillable = true)
	//	@JsonProperty("pm_name")
	private String pmName;

	//TODO need to investigate
	//	@ApiObjectField(description = "reservation_id", name = "reservation_id", required = true)
	//	@XmlElement(name = "reservation_id", nillable = true)
	//	@JsonProperty("reservation_id")
	private Long reservationId;

	//TODO need to investigate
	//	@ApiObjectField(description = "type_of_date", name = "type_of_date", required = true)
	//	@XmlElement(name = "type_of_date", nillable = true)
	//	@JsonProperty("type_of_date")
	private Integer typeOfData;

	//TODO need to investigate
	//	@ApiObjectField(description = "start_date", name = "start_date", required = true)
	//	@XmlElement(name = "start_date", nillable = true)
	//	@JsonProperty("start_date")
	private Date startDate;

	//TODO need to investigate
	//	@ApiObjectField(description = "end_date", name = "end_date", required = true)
	//	@XmlElement(name = "end_date", nillable = true)
	//	@JsonProperty("end_date")
	private Date endDate;

	//TODO need to investigate
	//	@ApiObjectField(description = "statuses", name = "statuses", required = true)
	//	@XmlElement(name = "statuses", nillable = true)
	//	@JsonProperty("statuses")
	private List<Integer> statuses;

	//TODO need to investigate
	//	@ApiObjectField(description = "limit", name = "limit", required = true)
	//	@XmlElement(name = "limit", nillable = true)
	//	@JsonProperty("limit")
	private Integer limit;

	//TODO need to investigate
	//	@ApiObjectField(description = "page", name = "page", required = true)
	//	@XmlElement(name = "page", nillable = true)
	//	@JsonProperty("page")
	private Integer page;

	//TODO need to investigate
	//	@ApiObjectField(description = "confirmation_id", name = "confirmation_id", required = true)
	//	@XmlElement(name = "confirmation_id", nillable = true)
	//	@JsonProperty("confirmation_id")
	private String confirmationId;

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getTypeOfData() {
		return typeOfData;
	}

	public void setTypeOfData(Integer typeOfData) {
		this.typeOfData = typeOfData;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Integer> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<Integer> statuses) {
		this.statuses = statuses;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}
}
