package com.mybookingpal.dataaccesslayer.constants;

public enum TransactionStateEnum {
	Initial, Accepted, Failed, Declined, Cancelled
}