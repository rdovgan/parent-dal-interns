package com.mybookingpal.dataaccesslayer.constants;

public enum PaymentType {
	Initial, Balance, Full
}
