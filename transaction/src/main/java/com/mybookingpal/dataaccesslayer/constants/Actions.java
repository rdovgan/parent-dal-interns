package com.mybookingpal.dataaccesslayer.constants;

public enum Actions {
	Confirmed, Following_up, Resolved, Cancelled
}
