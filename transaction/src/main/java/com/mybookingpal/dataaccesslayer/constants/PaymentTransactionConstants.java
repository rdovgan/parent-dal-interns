package com.mybookingpal.dataaccesslayer.constants;

public class PaymentTransactionConstants {

	public static final String FULL_CHARGE_TYPE = "Full";
	public static final String SUBSCRIPTION = "Subscription";
	public static final String DEPOSIT_CHARGE_TYPE = "Deposit";
	public static final String SECURITY_CHARGE_TYPE = "Security";
	public static final String SECURITY_AND_DPOSIT_CHARGE_TYPE = "Security and deposit";
	public static final String REFUND_CHARGE_TYPE = "Cancellation Refund";
	public static final String CANCELLED_CHARGE_TYPE = "Cancellation Fee";
	public static final String MODIFICATION = "Modification";
	public static final String SECOND_PAYMENT = "Second";
	public static final String FAILED_RESERVATION_STATE = "Unconfirmed";

}
