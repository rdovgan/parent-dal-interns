package com.mybookingpal.dataaccesslayer.constants;

public enum Status {
	Unassigned, Assigned, Confirmed, Cancelled, ClearedModified
}
