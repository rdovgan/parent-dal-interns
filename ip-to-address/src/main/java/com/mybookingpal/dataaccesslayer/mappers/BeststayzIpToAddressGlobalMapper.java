package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.BeststayzIpAddress;
import org.apache.ibatis.annotations.Param;

public interface BeststayzIpToAddressGlobalMapper {

	BeststayzIpAddress getBeststayzIpAddressByIp(@Param("ip") Long ip);

	String getCityByCountryCodeAndZip(@Param("country_code") String countryCode, @Param("zip") String zip);

}