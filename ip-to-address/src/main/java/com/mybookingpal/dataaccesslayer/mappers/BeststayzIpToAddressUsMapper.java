package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.BeststayzIpAddress;
import org.apache.ibatis.annotations.Param;

public interface BeststayzIpToAddressUsMapper {

	BeststayzIpAddress getBeststayzIpAddressByIp(@Param("ip") Long ip);

	String getCityByZip(@Param("zip") String zip);

}