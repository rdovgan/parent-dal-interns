package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.BeststayzIpAddress;
import com.mybookingpal.dataaccesslayer.mappers.BeststayzIpToAddressGlobalMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BeststayzIpToAddressGlobalDao {

	@Autowired
	private BeststayzIpToAddressGlobalMapper beststayzIpToAddressGlobalMapper;

	public BeststayzIpAddress getBeststayzIpAddressByIp(Long ip) {
		return beststayzIpToAddressGlobalMapper.getBeststayzIpAddressByIp(ip);
	}

	public String getCityByCountryCodeAndZip(String countryCode, String zip) {
		return beststayzIpToAddressGlobalMapper.getCityByCountryCodeAndZip(countryCode, zip);
	}

}
