package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.BeststayzIpAddress;
import com.mybookingpal.dataaccesslayer.mappers.BeststayzIpToAddressUsMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BeststayzIpToAddressUsDao {

	@Autowired
	private BeststayzIpToAddressUsMapper beststayzIpToAddressUsMapper;

	public BeststayzIpAddress getBeststayzIpAddressByIp(Long ip) {
		return beststayzIpToAddressUsMapper.getBeststayzIpAddressByIp(ip);
	}

	public String getCityByZip(String zip) {
		return beststayzIpToAddressUsMapper.getCityByZip(zip);
	}

}
