package com.mybookingpal.dataaccesslayer.entity;

public class BeststayzIpAddress {

	private Long id;

	private String country;

	private String city;

	private String countryCode;

	private String zip;

	public BeststayzIpAddress() {

	}

	public BeststayzIpAddress(String country, String city, String countryCode, String zip) {
		this.country = country;
		this.city = city;
		this.countryCode = countryCode;
		this.zip = zip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "BeststayzIpAddress{" + "id=" + id + ", country='" + country + '\'' + ", city='" + city + '\'' + ", countryCode='" + countryCode + '\''
				+ ", zip='" + zip + '\'' + '}';
	}

}