package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

public interface IsRoomPeriodChange {
	Long getId();

	Date getFromDate();

	Date getToDate();

	Long getProductId();

	Long getParentId();
}
