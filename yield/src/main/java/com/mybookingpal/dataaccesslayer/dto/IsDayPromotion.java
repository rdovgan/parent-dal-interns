package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.TypeOfCharge;

import java.util.Date;

public interface IsDayPromotion {
	public String getYieldName();

	public Integer getId();

	public Integer getProductId();

	public Date getDate();

	public String getName();

	public Double getAmount();

	public TypeOfCharge getTypeOfCharge();

	public String getModifier();


}
