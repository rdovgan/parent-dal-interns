package com.mybookingpal.dataaccesslayer.dto;

import com.mybookingpal.dataaccesslayer.constants.TypeOfCharge;

import java.util.Date;

public class DayPromotionDto implements IsDayPromotion {

	private String yieldName;
	private Integer id;
	private Integer productId;
	private Date date;
	private String name;
	private Double amount;
	private TypeOfCharge typeOfCharge;
	private String modifier;

	@Override
	public String getYieldName() {
		return yieldName;
	}

	public void setYieldName(String yieldName) {
		this.yieldName = yieldName;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Override
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public TypeOfCharge getTypeOfCharge() {
		return typeOfCharge;
	}

	public void setTypeOfCharge(TypeOfCharge typeOfCharge) {
		this.typeOfCharge = typeOfCharge;
	}

	@Override
	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
}
