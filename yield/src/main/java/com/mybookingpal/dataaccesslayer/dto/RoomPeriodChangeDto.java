package com.mybookingpal.dataaccesslayer.dto;

import java.util.Date;

public class RoomPeriodChangeDto implements IsRoomPeriodChange {

	private Long id;
	private Date fromDate;
	private Date toDate;
	private Long productId;
	private Long parentId;

	public void setId(Long id) {
		this.id = id;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Date getFromDate() {
		return fromDate;
	}

	@Override
	public Date getToDate() {
		return toDate;
	}

	@Override
	public Long getProductId() {
		return productId;
	}

	@Override
	public Long getParentId() {
		return parentId;
	}
}
