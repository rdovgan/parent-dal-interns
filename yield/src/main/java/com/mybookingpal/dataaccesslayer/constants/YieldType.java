package com.mybookingpal.dataaccesslayer.constants;

import com.mybookingpal.dataaccesslayer.utils.Valued;

import java.util.EnumSet;
import java.util.Set;

public enum YieldType implements Valued {
	DATE_RANGE("Date Range"), DAY_OF_WEEK("Day of Week"), GAP_FILLER("Maximum Gap Filler"), EARLY_BIRD("Early Booking Lead Time"), LAST_MINUTE(
			"Last Minute Lead Time"), LENGTH_OF_STAY("Length of Stay"), OCCUPANCY_ABOVE("Occupancy Above"), OCCUPANCY_BELOW("Occupancy Below"), WEEKEND(
			"Weekend"), WEEKLY("Weekly"),

	BASE("Basic Promotion"), SAME_DAY_DEAL("Same Day Deal"), EARLY_BOOKING_DEAL("Early Booking Deal"), FREE_NIGHTS("Free Nights"), SEASONAL_ADJUSTMENT(
			"Season adjustment"), STAYED_AT_LEAST_X_DAYS("Long-term stay adjustment"), BOOKED_WITHIN_AT_MOST_X_DAYS(
			"Last-minute discount"), BOOKED_BEYOND_AT_LEAST_X_DAYS("Booking ahead discount");

	private final String value;

	YieldType(String value) {
		this.value = value;
	}

	public static Set<YieldType> getSpecialPromotions() {
		return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS);
	}

	public static Set<YieldType> getGeneralPromotions() {
		return EnumSet.of(DATE_RANGE, WEEKEND, WEEKLY, DAY_OF_WEEK, LENGTH_OF_STAY, LAST_MINUTE, EARLY_BIRD, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
	}

	public static Set<YieldType> getYields() {
		return EnumSet.of(WEEKEND, DAY_OF_WEEK, GAP_FILLER, LENGTH_OF_STAY, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
	}

	public static Set<YieldType> getPMSSupportedYields() {
		return EnumSet.of(WEEKEND, DAY_OF_WEEK, DATE_RANGE, EARLY_BIRD, LAST_MINUTE, GAP_FILLER, LENGTH_OF_STAY, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
	}

	public static Set<YieldType> getPromotions() {
		return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS, SEASONAL_ADJUSTMENT, STAYED_AT_LEAST_X_DAYS, BOOKED_WITHIN_AT_MOST_X_DAYS,
				BOOKED_BEYOND_AT_LEAST_X_DAYS);
	}

	public static Set<YieldType> getPromotionsForRackRate() {
		return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS, WEEKEND, LENGTH_OF_STAY);
	}

	public static Set<YieldType> getAIRBNBPromotions() {
		return EnumSet.of(SEASONAL_ADJUSTMENT, STAYED_AT_LEAST_X_DAYS, BOOKED_WITHIN_AT_MOST_X_DAYS, BOOKED_BEYOND_AT_LEAST_X_DAYS);
	}

	/**
	 *  Is used to get YieldType with specified value
	 *
	 * @param value
	 * @return YieldType with specified value
	 *
	 * @throws IllegalArgumentException when there is no YieldType with such value
	 */
	public static YieldType fromValue(String value) {
		for(YieldType yieldType : YieldType.values()) {
			if(yieldType.getStringValue().equals(value)) {
				return yieldType;
			}
		}

		throw new IllegalArgumentException("Can not get correct YieldType with value " + value);
	}

	@Override
	public String getStringValue() {
		return this.value;
	}

	@Override
	public int getValue() {
		throw new UnsupportedOperationException();
	}
}

