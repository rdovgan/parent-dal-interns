package com.mybookingpal.dataaccesslayer.constants;

public class ChannelProductYieldMapConstants {
	public static final String LOS7="LOS7";
	public static final String LOS30="LOS30";
	public static final String LOS28 = "LOS28";
	public static final String WEEKEND="weekend";//weekend rates
}
