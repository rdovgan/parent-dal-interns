package com.mybookingpal.dataaccesslayer.constants;

import com.mybookingpal.dataaccesslayer.utils.Valued;

public enum TypeOfCharge implements Valued {
	DECREASE_PERCENT("Decrease Percent"),
	DECREASE_AMOUNT("Decrease Amount");

	private final String value;

	TypeOfCharge(String value) {
		this.value = value;
	}

	@Override
	public String getStringValue() {
		return this.value;
	}

	@Override
	public int getValue() {
		throw new UnsupportedOperationException();
	}
}