package com.mybookingpal.dataaccesslayer.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YieldConstants {

	public static final String MODIFIER = "modifier";
	public static final String AMOUNT = "amount";
	public static final String FROMDATE = "fromdate";
	public static final String TODATE = "todate";

	//Entity types
	public static final String PRODUCT = "Product";
	public static final String PARTY = "Party";

	// States
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String SUSPENDED = "Suspended";
	public static final String FAILED = "Failed";
	public static final String[] STATES = { INITIAL, CREATED, SUSPENDED, FINAL, FAILED };

	public static final String INCREASE_PERCENT = "Increase Percent";
	public static final String DECREASE_PERCENT = "Decrease Percent";
	public static final String INCREASE_AMOUNT = "Increase Amount";
	public static final String DECREASE_AMOUNT = "Decrease Amount";
	public static final String[] MODIFIERS = { INCREASE_PERCENT, DECREASE_PERCENT, INCREASE_AMOUNT, DECREASE_AMOUNT };

	// we only currently accept Length of Stay, Weekend, Day of Week and Channel Markup
	public static final String DATE_RANGE = "Date Range";
	public static final String DAY_OF_WEEK = "Day of Week";
	public static final String GAP_FILLER = "Maximum Gap Filler";
	public static final String EARLY_BIRD = "Early Booking Lead Time";
	public static final String LAST_MINUTE = "Last Minute Lead Time";
	public static final String LENGTH_OF_STAY = "Length of Stay";
	public static final String OCCUPANCY_ABOVE = "Occupancy Above";
	public static final String OCCUPANCY_BELOW = "Occupancy Below";
	public static final String WEEKEND = "Weekend";
	public static final String WEEKLY = "Weekly";
	public static final String STANDARD = "Standard";
	public static final String CHANNEL_MARKUP = "Channel Markup";

	public static final String[] RULES = { WEEKEND, DATE_RANGE, DAY_OF_WEEK, GAP_FILLER, EARLY_BIRD, LAST_MINUTE, LENGTH_OF_STAY, OCCUPANCY_ABOVE,
			OCCUPANCY_BELOW };

	public static final String EARLY_BOOKING_DEAL = "Early Booking Deal";
	public static final String BASIC_PROMO = "Basic Promotion";
	public static final String SAME_DAY_DEAL = "Same Day Deal";
	public static final String MOBILE_RATE = "Mobile Rate";
	public static final String BUSINESS_BOOKER = "Business Booker";
	public static final String GEO_RATE = "Geo Rate";
	public static final String FREE_NIGHT_PROMOTION = "Free Night Promotion";

	private static final String SUN = "0";
	private static final String MON = "1";
	private static final String TUE = "2";
	private static final String WED = "3";
	private static final String THU = "4";
	private static final String FRI = "5";
	private static final String SAT = "6";
	public static final String[] DAYS = { SUN, MON, TUE, WED, THU, FRI, SAT };

	public static final int DAYS_OF_WEEKEND_SAT_SUN = 0;
	public static final int DAYS_OF_WEEKEND_FRI_SAT = 1;
	public static final int DAYS_OF_WEEKEND_FRI_SAT_SUN = 2;
	public static final int DAYS_OF_WEEKEND_THU_FRI_SAT = 3;
	public static final int DAYS_OF_WEEKEND_THU_FRI_SAT_SUN = 4;

	public static final int BASIC_PROMO_PARAM = 7777;

	public static final Map<Integer, List<String>> WEEKEND_DAYS_MAP;

	static {
		WEEKEND_DAYS_MAP = new HashMap<>();
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_SAT_SUN, new ArrayList<String>() {{
			add(SAT);
			add(SUN);
		}});
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_FRI_SAT, new ArrayList<String>() {{
			add(FRI);
			add(SAT);
		}});
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_FRI_SAT_SUN, new ArrayList<String>() {{
			add(FRI);
			add(SAT);
			add(SUN);
		}});
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_THU_FRI_SAT, new ArrayList<String>() {{
			add(THU);
			add(FRI);
			add(SAT);
		}});
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_THU_FRI_SAT_SUN, new ArrayList<String>() {{
			add(THU);
			add(FRI);
			add(SAT);
			add(SUN);
		}});
	}

	public static Map<Integer, List<String>> getWeekendDaysMap() {
		return WEEKEND_DAYS_MAP;
	}

	public static String getDayOfWeek() {
		return DAY_OF_WEEK;

	}
}
