package com.mybookingpal.dataaccesslayer.constants;

public enum ModifierEnum {
	INCREASE_PERCENT("Increase Percent"),
	DECREASE_PERCENT("Decrease Percent"),
	INCREASE_AMOUNT("Increase Amount"),
	DECREASE_AMOUNT("Decrease Amount");

	private String value;

	ModifierEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static ModifierEnum getModifierEnum(String value) {
		ModifierEnum[] modifierEnumList = ModifierEnum.values();
		for (ModifierEnum modifierEnum : modifierEnumList) {
			if (modifierEnum.getValue().equalsIgnoreCase(value)) {
				return modifierEnum;
			}
		}
		return null;
	}
}