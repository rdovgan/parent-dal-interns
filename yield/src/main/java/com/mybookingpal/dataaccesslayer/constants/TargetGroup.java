package com.mybookingpal.dataaccesslayer.constants;

public enum TargetGroup {
	Public(0), SubscribersOnly(1);

	private final Integer value;

	TargetGroup(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public TargetGroup fromInteger(Integer value) {
		for(TargetGroup tg : TargetGroup.values()) {
			if(tg.getValue().equals(value)) {
				return tg;
			}
		}

		return null;
	}

}