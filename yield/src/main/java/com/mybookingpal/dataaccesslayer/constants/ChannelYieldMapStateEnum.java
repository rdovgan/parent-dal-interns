package com.mybookingpal.dataaccesslayer.constants;

public enum ChannelYieldMapStateEnum {
	CREATED("Created"),
	FINAL("Final");

	private final String value;

	ChannelYieldMapStateEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static ChannelYieldMapStateEnum getByStr(String value) {
		for (ChannelYieldMapStateEnum v : values()) {
			if (v.value.equals(value)) {
				return v;
			}
		}
		return null;
	}

	public static String getNameByStr(String value) {
		return getByStr(value).name();
	}
}
