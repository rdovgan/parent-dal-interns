package com.mybookingpal.dataaccesslayer.constants;

public enum DaysOfWeekendEnum {
	DAYS_OF_WEEKEND_SAT_SUN(0),
	DAYS_OF_WEEKEND_FRI_SAT(1),
	DAYS_OF_WEEKEND_FRI_SAT_SUN(2),
	DAYS_OF_WEEKEND_THU_FRI_SAT(3),
	DAYS_OF_WEEKEND_THU_FRI_SAT_SUN(4);

	private Integer value;

	DaysOfWeekendEnum(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static DaysOfWeekendEnum getDaysOfWeekendEnum(Integer value) {
		DaysOfWeekendEnum[] daysOfWeekendList = DaysOfWeekendEnum.values();
		for (DaysOfWeekendEnum daysOfWeekend : daysOfWeekendList) {
			if (daysOfWeekend.getValue().equals(value)) {
				return daysOfWeekend;
			}
		}
		return null;
	}
}