package com.mybookingpal.dataaccesslayer.constants;

public class YieldSupportedByPMSConstants {
	public static final String DAY_OF_WEEK = "Day of Week";
	public static final String LENGTH_OF_STAY = "Length of Stay";
	public static final String WEEKEND = "Weekend";
}
