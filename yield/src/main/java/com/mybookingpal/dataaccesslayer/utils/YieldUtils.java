package com.mybookingpal.dataaccesslayer.utils;

import com.mybookingpal.dataaccesslayer.constants.YieldType;
import com.mybookingpal.dataaccesslayer.entity.YieldModel;
import com.mybookingpal.utils.entity.BigDecimalExt;
import com.mybookingpal.utils.entity.Time;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DAYS_OF_WEEKEND_FRI_SAT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DAYS_OF_WEEKEND_FRI_SAT_SUN;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DAYS_OF_WEEKEND_SAT_SUN;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DAYS_OF_WEEKEND_THU_FRI_SAT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DAYS_OF_WEEKEND_THU_FRI_SAT_SUN;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DECREASE_AMOUNT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.DECREASE_PERCENT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.GAP_FILLER;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.INCREASE_AMOUNT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.INCREASE_PERCENT;
import static com.mybookingpal.dataaccesslayer.constants.YieldConstants.WEEKEND_DAYS_MAP;

public class YieldUtils {

	public static boolean isSpecialPromotion(String name) {
		return YieldType.getSpecialPromotions().contains(YieldType.fromValue(name));
	}

	public static boolean hasEntityType(YieldModel yield) {
		return !StringUtils.isEmpty(yield.getEntityType());
	}

	public static boolean hasEntityId(YieldModel yield) {
		return !StringUtils.isEmpty(yield.getEntityId());
	}

	public static boolean hasModifier(YieldModel yield) {
		return !StringUtils.isEmpty(yield.getModifier());
	}

	public static boolean hasModifier(YieldModel yield, String modifier) {
		return yield.getModifier() == modifier;
	}

	public static boolean hasNoModifier(YieldModel yield) {
		return !StringUtils.isEmpty(yield.getModifier());
	}

	public static boolean hasParam(YieldModel yield) {
		return yield.getParam() != null;
	}

	public static boolean hasNoParam(YieldModel yield) {
		return !hasParam(yield);
	}

	public static boolean hasParam(YieldModel yield, Integer param) {
		return yield.getParam() == param;
	}

	public static boolean hasAmount(YieldModel yield) {
		return yield.getAmount() != null;
	}

	public static Double defineAmount(YieldModel yield) {
		return yield.getAmount() != null ? yield.getAmount().doubleValue() : null;
	}

	public static boolean hasNoAmount(YieldModel yield) {
		return !hasAmount(yield);
	}

	public static boolean hasFromDate(YieldModel yield) {
		return yield.getFromDate() != null;
	}

	public static boolean hasNoFromDate(YieldModel yield) {
		return !hasFromDate(yield);
	}

	public static boolean hasToDate(YieldModel yield) {
		return yield.getToDate() != null;
	}

	public static boolean hasNoToDate(YieldModel yield) {
		return !hasToDate(yield);
	}

	public static boolean hasDate(YieldModel yield) {
		return !hasNoDate(yield);
	}

	public static boolean hasNoDate(YieldModel yield) {
		return hasNoFromDate(yield) || hasNoToDate(yield) || yield.getToDate().isBefore(yield.getFromDate());
	}

	public static boolean isInDateRange(YieldModel yield, Date date) {
		// TODO return CommonDateUtils.toLocalDate(date).compareTo(fromdate) >= 0 && CommonDateUtils.toLocalDate(date).compareTo(todate) <= 0;
		return true;
	}

	public static boolean isInDateRange(YieldModel yield, LocalDate date) {
		return !date.isBefore(yield.getFromDate()) && !date.isAfter(yield.getToDate());
	}

	public static boolean isInBookingDateRange(YieldModel yield, Date date) {
		return date.compareTo(yield.getBookingFromDate()) >= 0 && date.compareTo(yield.getBookingToDate()) <= 0;
	}

	public static boolean isWeekend(YieldModel yield, LocalDate date, Integer param) {
		List<String> daysOfWeekend = WEEKEND_DAYS_MAP.get(DAYS_OF_WEEKEND_SAT_SUN);
		if (param != null && WEEKEND_DAYS_MAP.get(param) != null) {
			daysOfWeekend = WEEKEND_DAYS_MAP.get(param);
		}
		for (String day : daysOfWeekend) {
			if (date.getDayOfWeek().getValue() == Integer.parseInt(day)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isWeekendDate(LocalDate dt, Integer param) {
		if (param != null && param == DAYS_OF_WEEKEND_SAT_SUN) {
			// sat and sun have values greater than 5
			return dt.getDayOfWeek().getValue() > 5;
		} else if (param != null && param == DAYS_OF_WEEKEND_FRI_SAT) {
			// Fri=5 & sat=6
			return dt.getDayOfWeek().getValue() == 5 || dt.getDayOfWeek().getValue() == 6;
		} else if (param != null && param == DAYS_OF_WEEKEND_FRI_SAT_SUN) {
			// Fri=5 & sat=6 & sun=7
			return dt.getDayOfWeek().getValue() > 4;
		} else if (param != null && param == DAYS_OF_WEEKEND_THU_FRI_SAT) {
			// Thu=4 & Fri=5 & sat=6
			return dt.getDayOfWeek().getValue() == 4 || dt.getDayOfWeek().getValue() == 5 || dt.getDayOfWeek().getValue() == 6;
		} else if (param != null && param == DAYS_OF_WEEKEND_THU_FRI_SAT_SUN) {
			// Thu=4 & Fri=5 & sat=6 & Sun=7
			return dt.getDayOfWeek().getValue() > 3;
		}

		return false;
	}

	public static boolean isDayOfWeek(YieldModel yield, Date date) {
		return yield.getParam() == date.getDay();
	}

	public static boolean isDayOfWeek(YieldModel yield, LocalDate date) {
		return yield.getParam() == date.getDayOfWeek().getValue();
	}

	public static boolean isEarlyBird(YieldModel yield, Date date) {
		Date deadline = Time.addDuration(new Date(), yield.getParam(), Time.DAY);
		return date.after(deadline);
	}

	public static boolean isGapFiller(YieldModel yield, boolean fillsGap, Integer stay) {
		return fillsGap && stay <= yield.getParam();
	}

	public static boolean isGapFiller(YieldModel yield) {
		return GAP_FILLER.equalsIgnoreCase(yield.getName());
	}

	public static boolean isLastMinute(YieldModel yield, Date date) {
		Date deadline = Time.addDuration(new Date(), yield.getParam(), Time.DAY);
		return date.before(deadline);
	}

	public static boolean isLengthOfStay(YieldModel yield, Integer stay) {
		return yield.getParam() <= stay;
	}

	public static boolean isOccupancyAbove(YieldModel yield, Integer occupancy) {
		return yield.getParam() < occupancy;
	}

	public static boolean isOccupancyBelow(YieldModel yield, Integer occupancy) {
		return yield.getParam() > occupancy;
	}

	public static boolean isPercentage(YieldModel yield) {
		return hasModifier(yield, DECREASE_PERCENT) || hasModifier(yield, INCREASE_PERCENT);
	}

	public static Double getValue(YieldModel yield, Double value) {
		if (yield.getModifier() == null) {
			return 0D;
		}
		if (hasModifier(yield, INCREASE_AMOUNT)) {
			return value + defineAmount(yield);
		} else if (hasModifier(yield, DECREASE_AMOUNT)) {
			return value - defineAmount(yield);
		} else if (hasModifier(yield, INCREASE_PERCENT)) {
			return value * (100.0 + defineAmount(yield)) / 100.0;
		} else if (hasModifier(yield, DECREASE_PERCENT)) {
			return value * (100.0 - defineAmount(yield)) / 100.0;
		} else
			return value;
	}

	public static BigDecimalExt getPromotionValue(YieldModel yield, BigDecimalExt value) {
		if (yield.getModifier() == null) {
			return BigDecimalExt.ZERO;
		}
		if (hasModifier(yield, INCREASE_AMOUNT)) {
			return yield.getAmount();
		} else if (hasModifier(yield, DECREASE_AMOUNT)) {
			return yield.getAmount().negate();
		} else if (hasModifier(yield, INCREASE_PERCENT)) {
			return value.multiply(yield.getAmount()).divideByHundred();
		} else if (hasModifier(yield, DECREASE_PERCENT)) {
			return value.negate().multiply(yield.getAmount()).divideByHundred();
		} else {
			return value;
		}
	}

	public static BigDecimalExt getValue(YieldModel yield, BigDecimalExt value) {
		if (yield.getModifier() == null) {
			return BigDecimalExt.ZERO;
		}
		if (hasModifier(yield, INCREASE_AMOUNT)) {
			return value.add(yield.getAmount());
		} else if (hasModifier(yield, DECREASE_AMOUNT)) {
			return value.subtract(yield.getAmount());
		} else if (hasModifier(yield, INCREASE_PERCENT)) {
			return BigDecimalExt.ONE_HUNDRED.add(yield.getAmount()).multiply(value).divideByHundred();
		} else if (hasModifier(yield, DECREASE_PERCENT)) {
			return BigDecimalExt.ONE_HUNDRED.subtract(yield.getAmount()).multiply(value).divideByHundred();
		} else {
			return value;
		}
	}

	public static boolean isSpecialPromotion(YieldModel yield) {
		return isSpecialPromotion(yield.getName());
	}

}
