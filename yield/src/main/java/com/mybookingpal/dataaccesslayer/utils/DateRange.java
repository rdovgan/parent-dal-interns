package com.mybookingpal.dataaccesslayer.utils;

import java.time.LocalDate;

public interface DateRange extends HasDateRange {
	void setFromDate(LocalDate fromDate);

	void setToDate(LocalDate toDate);

	Object clone() throws CloneNotSupportedException;
}
