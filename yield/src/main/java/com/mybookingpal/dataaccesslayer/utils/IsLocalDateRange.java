package com.mybookingpal.dataaccesslayer.utils;

import java.time.LocalDate;

public interface IsLocalDateRange {
	LocalDate getFromDate();

	LocalDate getToDate();
}
