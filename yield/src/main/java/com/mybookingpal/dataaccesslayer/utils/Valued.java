package com.mybookingpal.dataaccesslayer.utils;

public interface Valued {
	int getValue();

	String getStringValue();
}