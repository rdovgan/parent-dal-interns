package com.mybookingpal.dataaccesslayer.utils;

import java.time.LocalDate;

public interface HasDateRange {
	LocalDate getFromDate();

	LocalDate getToDate();
}
