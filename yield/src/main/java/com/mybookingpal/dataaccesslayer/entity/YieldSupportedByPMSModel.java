package com.mybookingpal.dataaccesslayer.entity;

//TODO need to investigate
//import com.thoughtworks.xstream.annotations.XStreamAlias;
//
//@XStreamAlias("YieldSupportedByPMS")
public class YieldSupportedByPMSModel {

	private Integer id;
	private Integer pmsPartyId;
	private String yieldName;
	private Integer yieldParam;

	public YieldSupportedByPMSModel() {
	}

	public YieldSupportedByPMSModel(Integer id, Integer pmsPartyId, String yieldName, Integer yieldParam) {
		this.id = id;
		this.pmsPartyId = pmsPartyId;
		this.yieldName = yieldName;
		this.yieldParam = yieldParam;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPmsPartyId() {
		return pmsPartyId;
	}

	public void setPmsPartyId(Integer pmsPartyId) {
		this.pmsPartyId = pmsPartyId;
	}

	public String getYieldName() {
		return yieldName;
	}

	public void setYieldName(String yieldName) {
		this.yieldName = yieldName;
	}

	public Integer getYieldParam() {
		return yieldParam;
	}

	public void setYieldParam(Integer yieldParam) {
		this.yieldParam = yieldParam;
	}
}
