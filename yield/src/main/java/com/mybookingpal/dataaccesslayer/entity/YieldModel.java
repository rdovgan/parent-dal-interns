package com.mybookingpal.dataaccesslayer.entity;

import com.mybookingpal.dataaccesslayer.utils.DateRange;
import com.mybookingpal.utils.entity.BigDecimalExt;

import java.time.LocalDate;
import java.util.Date;

/**
 * @see com.mybookingpal.dataaccesslayer.utils.YieldUtils for utils methods for Yield
 */
public class YieldModel implements DateRange {

	protected String id;
	protected String entityType;
	protected String entityId;
	protected String groupUnique;
	protected String name;
	protected String yieldName;
	protected String state;
	protected LocalDate fromDate;
	protected LocalDate toDate;
	protected Integer param;
	protected BigDecimalExt amount;
	protected String modifier;
	protected Integer ratePlanId;
	protected Integer minStay;
	protected Date bookingFromDate;
	protected Date bookingToDate;
	protected Integer bookStartTime;
	protected Integer bookEndTime;
	protected Integer optionValue;
	protected Integer channelId;
	protected Boolean isInternal;
	protected String additionalDates;
	protected String excludeDates;
	protected Integer targetGroup;
	protected String activeDays;
	protected Byte baseRate;
	protected Date createDate;
	protected Date version;
	protected BigDecimalExt totalPrice;
	protected Boolean creditCardRequired;

	public YieldModel() {
	}

	public YieldModel(String entityType, String entityId) {
		this.entityType = entityType;
		this.entityId = entityId;
	}

	public YieldModel(String entityType, String entityId, Date fromDate, Date toDate) {
		// TODO problem with CommonDateUtils : this(entityType, entityId, CommonDateUtils.toLocalDate(fromDate), CommonDateUtils.toLocalDate(toDate));

	}

	public YieldModel(String entityType, String entityId, LocalDate fromDate, LocalDate toDate) {
		this.entityType = entityType;
		this.entityId = entityId;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public YieldModel(YieldModel yield) {
		this.entityType = yield.getEntityType();
		this.entityId = yield.getEntityid();
		this.modifier = yield.getModifier();
		this.param = yield.getParam();
		this.amount = yield.getAmount();
		this.fromDate = yield.getFromDate();
		this.toDate = yield.getToDate();
		this.version = yield.getVersion();
		this.totalPrice = yield.getTotalPrice();
		this.bookingFromDate = yield.getBookingFromDate();
		this.bookingToDate = yield.getBookingToDate();
		this.channelId = yield.getChannelId();
		this.optionValue = yield.getOptionValue();
		this.yieldName = yield.getYieldName();
		this.name = yield.getName();
		this.state = yield.getState();
		this.minStay = yield.getMinStay();
		this.groupUnique = yield.getGroupUnique();
		this.createDate = yield.getCreateDate();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Yield [entitytype=");
		builder.append(entityType);
		builder.append(", entityId=");
		builder.append(entityId);
		builder.append(", modifier=");
		builder.append(modifier);
		builder.append(", param=");
		builder.append(param);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", fromDate=");
		builder.append(fromDate);
		builder.append(", toDate=");
		builder.append(toDate);
		builder.append(", state=");
		builder.append(state);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append(", yieldName=");
		builder.append(yieldName);
		builder.append(", ratePlanID=");
		builder.append(ratePlanId);
		builder.append(", minStay=");
		builder.append(minStay);
		builder.append(", bookingFromDate=");
		builder.append(bookingFromDate);
		builder.append(", bookingToDate=");
		builder.append(bookingToDate);
		builder.append(", optionValue=");
		builder.append(optionValue);
		builder.append(", channelID=");
		builder.append(channelId);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((modifier == null) ? 0 : modifier.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((param == null) ? 0 : param.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YieldModel other = (YieldModel) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (modifier == null) {
			if (other.modifier != null)
				return false;
		} else if (!modifier.equals(other.modifier))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (param == null) {
			if (other.param != null)
				return false;
		} else if (!param.equals(other.param))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		return true;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEntityType() {
		return entityType;
	}

	/**
	 * @deprecated use {@link #getEntityType()} instead.
	 */
	@Deprecated
	public String getEntitytype() {
		return entityType;
	}

	/**
	 * @deprecated use {@link #setEntityType(String)} instead.
	 */
	@Deprecated
	public void setEntitytype(String entityType) {
		this.entityType = entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	/**
	 * @deprecated use {@link #getEntityId()} instead.
	 */
	@Deprecated
	public String getEntityid() {
		return entityId;
	}

	/**
	 * @deprecated use {@link #setEntityId(String)}instead.
	 */
	@Deprecated
	public void setEntityid(String entityId) {
		this.entityId = entityId;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Integer getParam() {
		return param;
	}

	public void setParam(Integer param) {
		this.param = param;
	}

	@Deprecated
	public Double defineAmount() {
		return amount != null ? amount.doubleValue() : null;
	}

	public BigDecimalExt getAmount() {
		return amount;
	}

	@Deprecated
	public void setAmount(Double amount) {
		this.amount = amount != null ? new BigDecimalExt(amount) : null;
	}

	public void setAmount(BigDecimalExt amount) {
		this.amount = amount;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public BigDecimalExt getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimalExt totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Deprecated
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice != null ? new BigDecimalExt(totalPrice) : null;
	}

	@Deprecated
	public Double defineTotalPrice() {
		return totalPrice != null ? totalPrice.doubleValue() : null;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @deprecated use {@link #getCreateDate()} instead.
	 */
	@Deprecated
	public Date getCreatedDate() {
		return createDate;
	}

	/**
	 * @deprecated use {@link #setCreateDate(Date)} instead.
	 */
	@Deprecated
	public void setCreatedDate(Date createdDate) {
		this.createDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public String getYieldName() {
		return yieldName;
	}

	public void setYieldName(String yieldName) {
		this.yieldName = yieldName;
	}

	/**
	 * @deprecated use {@link #getRatePlanId()} instead.
	 */
	@Deprecated
	public Integer getRatePlanID() {
		return ratePlanId;
	}

	/**
	 * @deprecated use {@link #setRatePlanId(Integer)} instead.
	 */
	@Deprecated
	public void setRatePlanID(Integer ratePlanID) {
		this.ratePlanId = ratePlanID;
	}

	public Integer getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Integer ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Integer getMinStay() {
		return minStay;
	}

	public void setMinStay(Integer minStay) {
		this.minStay = minStay;
	}

	/**
	 * @deprecated use {@link #getMinStay()} instead.
	 */
	@Deprecated
	public Integer getMinstay() {
		return minStay;
	}

	/**
	 * @deprecated use {@link #setMinStay(Integer)} instead.
	 */
	@Deprecated
	public void setMinstay(Integer minStay) {
		this.minStay = minStay;
	}

	public Date getBookingFromDate() {
		return bookingFromDate;
	}

	public void setBookingFromDate(Date bookingFromDate) {
		this.bookingFromDate = bookingFromDate;
	}

	public Date getBookingToDate() {
		return bookingToDate;
	}

	public void setBookingToDate(Date bookingToDate) {
		this.bookingToDate = bookingToDate;
	}

	public Integer getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(Integer optionValue) {
		this.optionValue = optionValue;
	}

	/**
	 * @deprecated use {@link #getOptionValue()} instead.
	 */
	@Deprecated
	public Integer getOption() {
		return optionValue;
	}

	/**
	 * @deprecated use {@link #setOptionValue(Integer)} instead.
	 */
	@Deprecated
	public void setOption(Integer optionValue) {
		this.optionValue = optionValue;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	/**
	 * @deprecated use {@link #getChannelId()} instead.
	 */
	@Deprecated
	public Integer getChannelID() {
		return channelId;
	}

	/**
	 * @deprecated use {@link #setChannelId(Integer)} instead.
	 */
	@Deprecated
	public void setChannelID(Integer channelId) {
		this.channelId = channelId;
	}


	public boolean isInternal() {
		return isInternal;
	}

	public Boolean getIsInternal() {
		return isInternal;
	}

	public void setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
	}

	public Integer getBookStartTime() {
		return bookStartTime;
	}

	public void setBookStartTime(Integer bookStartTime) {
		this.bookStartTime = bookStartTime;
	}

	/**
	 * @deprecated use {@link #getBookStartTime()} instead.
	 */
	@Deprecated
	public Byte getBookingStartTime() {
		return bookStartTime.byteValue();
	}

	/**
	 * @deprecated use {@link #setBookStartTime(Integer)} instead.
	 */
	@Deprecated
	public void setBookingStartTime(Byte bookingStartTime) {
		this.bookStartTime = bookingStartTime.intValue();
	}

	public Integer getBookEndTime() {
		return bookEndTime;
	}

	public void setBookEndTime(Integer bookEndTime) {
		this.bookEndTime = bookEndTime;
	}

	/**
	 * @deprecated use {@link #getBookEndTime()} instead.
	 */
	public Byte getBookingEndTime() {
		return bookEndTime.byteValue();
	}

	/**
	 * @deprecated use {@link #setBookEndTime(Integer)} instead.
	 */
	public void setBookingEndTime(Byte bookingEndTime) {
		this.bookEndTime = bookingEndTime.intValue();
	}

	public String getExcludeDates() {
		return excludeDates;
	}

	public void setExcludeDates(String excludeDates) {
		this.excludeDates = excludeDates;
	}

	public Integer getTargetGroup() {
		return targetGroup;
	}

	public void setTargetGroup(Integer targetGroup) {
		this.targetGroup = targetGroup;
	}

	public String getActiveDays() {
		return activeDays;
	}

	public void setActiveDays(String activeDays) {
		this.activeDays = activeDays;
	}

	public String getAdditionalDates() {
		return additionalDates;
	}

	public void setAdditionalDates(String additionalDates) {
		this.additionalDates = additionalDates;
	}

	public Byte getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Byte baseRate) {
		this.baseRate = baseRate;
	}

	public String getGroupUnique() {
		return groupUnique;
	}

	public void setGroupUnique(String groupUnique) {
		this.groupUnique = groupUnique;
	}

	public Boolean getCreditCardRequired() {
		return creditCardRequired;
	}

	public void setCreditCardRequired(Boolean creditCardRequired) {
		this.creditCardRequired = creditCardRequired;
	}
}
