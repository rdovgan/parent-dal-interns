package com.mybookingpal.dataaccesslayer.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(value = "prototype")
public class ChannelProductYieldMapModel {
	private String id;
	private String name;
	private String productId;
	private Integer channelId;
	private String yieldRuleName;
	private String channelRateId;
	private String oldRatePlanId;
	private Integer existingChannelId;
	private List<String> list;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExistingChannelId() {
		return existingChannelId;
	}

	public void setExistingChannelId(Integer existingChannelId) {
		this.existingChannelId = existingChannelId;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getOldRatePlanId() {
		return oldRatePlanId;
	}

	public void setOldRatePlanId(String oldRatePlanId) {
		this.oldRatePlanId = oldRatePlanId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getYieldRuleName() {
		return yieldRuleName;
	}

	public void setYieldRuleName(String yieldRuleName) {
		this.yieldRuleName = yieldRuleName;
	}

	public String getChannelRateId() {
		return channelRateId;
	}

	public void setChannelRateId(String channelRateId) {
		this.channelRateId = channelRateId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((yieldRuleName == null) ? 0 : yieldRuleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		ChannelProductYieldMapModel other = (ChannelProductYieldMapModel) obj;
		if (channelId == null) {
			if (other.channelId != null) {
				return false;
			}
		} else if (!channelId.equals(other.channelId)) {
			return false;
		}
		if (productId == null) {
			if (other.productId != null) {
				return false;
			}
		} else if (!productId.equals(other.productId)) {
			return false;
		}
		if (yieldRuleName == null) {
			return other.yieldRuleName == null;
		} else {
			return yieldRuleName.equals(other.yieldRuleName);
		}
	}

}
