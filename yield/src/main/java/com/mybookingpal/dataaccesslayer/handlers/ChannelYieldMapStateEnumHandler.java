package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.dataaccesslayer.constants.ChannelYieldMapStateEnum;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class ChannelYieldMapStateEnumHandler extends EnumTypeHandler<ChannelYieldMapStateEnum> {
	public ChannelYieldMapStateEnumHandler() {
		super(ChannelYieldMapStateEnum.class);
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ChannelYieldMapStateEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	@Override
	public ChannelYieldMapStateEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String value = rs.getString(columnIndex);
		if(Objects.isNull(value)) {
			return null;
		}
		return ChannelYieldMapStateEnum.getByStr(value);
	}

	@Override
	public ChannelYieldMapStateEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		if(Objects.isNull(value)) {
			return null;
		}
		return ChannelYieldMapStateEnum.getByStr(value);
	}

	@Override
	public ChannelYieldMapStateEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		if(Objects.isNull(value)) {
			return null;
		}
		return ChannelYieldMapStateEnum.getByStr(value);
	}

}
