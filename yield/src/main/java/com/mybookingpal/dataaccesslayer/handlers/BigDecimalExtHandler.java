package com.mybookingpal.dataaccesslayer.handlers;

import com.mybookingpal.utils.entity.BigDecimalExt;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.DECIMAL)
public class BigDecimalExtHandler extends BaseTypeHandler<BigDecimalExt> {
	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, BigDecimalExt bigDecimalExt, JdbcType jdbcType) throws SQLException {
		preparedStatement.setBigDecimal(i, bigDecimalExt);
	}

	@Override
	public BigDecimalExt getNullableResult(ResultSet resultSet, String s) throws SQLException {
		BigDecimal resultValue = resultSet.getBigDecimal(s);
		if(resultValue == null) {
			return null;
		}
		return new BigDecimalExt(resultValue);
	}

	@Override
	public BigDecimalExt getNullableResult(ResultSet resultSet, int i) throws SQLException {
		BigDecimal resultValue = resultSet.getBigDecimal(i);
		if(resultValue == null) {
			return null;
		}
		return new BigDecimalExt(resultValue);
	}

	@Override
	public BigDecimalExt getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
		BigDecimal resultValue = callableStatement.getBigDecimal(i);
		if(resultValue == null) {
			return null;
		}
		return new BigDecimalExt(resultValue);
	}
}
