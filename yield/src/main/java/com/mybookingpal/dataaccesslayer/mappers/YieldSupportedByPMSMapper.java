package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.YieldSupportedByPMSModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

@Service
public interface YieldSupportedByPMSMapper {
	YieldSupportedByPMSModel readByPmId(@Param("pmId") Integer pmId);
}
