package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductYieldMapModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChannelProductYieldMapMapper {

	/**
	 * @deprecated use {@link #getChannelRateId(String productId, Integer channelId, String yieldRuleName)} instead.
	 */
	@Deprecated
	List<ChannelProductYieldMapModel> getChannelRateId(ChannelProductYieldMapModel channelProductYieldMap);

	List<ChannelProductYieldMapModel> getChannelRateId(@Param("productId") String productId, @Param("channelId") Integer channelId,
			@Param("yieldRuleName") String yieldRuleName);

	void create(ChannelProductYieldMapModel action);

	void update(ChannelProductYieldMapModel action);

	/**
	 * @deprecated use {@link #deleteByProductId(Integer channelId, String productId, String yieldRuleName)} instead.
	 */
	@Deprecated
	void deleteByProductId(ChannelProductYieldMapModel action);

	void deleteByProductId(@Param("channelId") Integer channelId, @Param("productId") String productId, @Param("yieldRuleName") String yieldRuleName);

	void deleteById(String id);

	/**
	 * @deprecated use {@link #deleteByChannelIdProductIdChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	void deleteByChannelIdProductIdChannelRateId(ChannelProductYieldMapModel channelProductYieldMap);

	void deleteByChannelIdProductIdChannelRateId(@Param("channelId") Integer channelId, @Param("productId") String productId,
			@Param("channelRateId") String channelRateId);

	/**
	 * Can cause sql exception if select will match multiple rows.
	 *
	 * @deprecated use {@link #getYieldRuleNameByChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	ChannelProductYieldMapModel getChannelRateName(ChannelProductYieldMapModel channelProductYieldMap);

	/**
	 * @deprecated use {@link #getYieldRuleNameByChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	ChannelProductYieldMapModel getYieldRuleNameByChannelRateId(ChannelProductYieldMapModel channelProductYieldMap);

	ChannelProductYieldMapModel getYieldRuleNameByChannelRateId(@Param("channelId") Integer channelId, @Param("productId") String productId,
			@Param("channelRateId") String channelRateId);

	/**
	 * @deprecated use {@link #getPromoRatesByChannelIdProductId(Integer channelId, String productId)} instead.
	 */
	@Deprecated
	List<ChannelProductYieldMapModel> getPromoRatesByChannelIdProductId(ChannelProductYieldMapModel channelProductYieldMap);

	List<ChannelProductYieldMapModel> getPromoRatesByChannelIdProductId(@Param("channelId") Integer channelId, @Param("productId") String productId);

	/**
	 * @deprecated use {@link #getByProductId(String productId)} instead.
	 */
	@Deprecated
	List<ChannelProductYieldMapModel> getByProductId(ChannelProductYieldMapModel channelProductYieldMap);

	List<ChannelProductYieldMapModel> getByProductId(@Param("productId") String productId);

	/**
	 * @deprecated use {@link #updateChannelProductYieldMapWithOldRatePlanId(String oldRatePlanId, String productId, Integer channelId)} instead.
	 */
	@Deprecated
	void updateChannelProductYieldMapWithOldRatePlanId(ChannelProductYieldMapModel action);

	void updateChannelProductYieldMapWithOldRatePlanId(@Param("oldRatePlanId") String oldRatePlanId, @Param("productId") String productId,
			@Param("channelId") Integer channelId);

	/**
	 * @deprecated use {@link #archiveChannelProductYieldMap(Integer channelId, Integer existingChannelId, List list)} instead.
	 */
	@Deprecated
	void archiveChannelProductYieldMap(ChannelProductYieldMapModel channelProductYieldMap);

	void archiveChannelProductYieldMap(@Param("channelId") Integer channelId, @Param("existingChannelId") Integer existingChannelId,
			@Param("list") List<String> list);

}
