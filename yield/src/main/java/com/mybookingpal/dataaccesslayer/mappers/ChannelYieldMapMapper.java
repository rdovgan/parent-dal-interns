package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ChannelYieldMapModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChannelYieldMapMapper {

	void create(ChannelYieldMapModel channelYieldMap);

	List<ChannelYieldMapModel> getActiveRuleGroupsByRuleId(String ruleId);

	void update(ChannelYieldMapModel channelYieldMap);

	List<ChannelYieldMapModel> getActiveByYieldId(Integer yieldId);

	ChannelYieldMapModel getActiveByYieldIdAndRuleId(@Param("yieldId") Integer yieldId, @Param("ruleId") String ruleId);
}
