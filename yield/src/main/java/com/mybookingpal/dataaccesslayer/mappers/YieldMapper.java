package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.dto.IsDayPromotion;
import com.mybookingpal.dataaccesslayer.utils.IsLocalDateRange;
import com.mybookingpal.dataaccesslayer.dto.IsRoomPeriodChange;
import com.mybookingpal.dataaccesslayer.entity.YieldModel;
import com.mybookingpal.dataaccesslayer.dto.YieldSearch;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface YieldMapper {
	//TODO @SqlUpdateMarker
	void create(YieldModel action);

	//TODO @SqlUpdateMarker
	void update(YieldModel action);

	YieldModel read(String id);

	/**
	 * @deprecated use {@link #listbyentity(String, String)} instead.
	 */
	@Deprecated
	List<YieldModel> listbyentity(YieldModel action);

	//TODO: Fix naming
	List<YieldModel> listbyentity(@Param("entityType") String entityType, @Param("entityId") String entityId);

	/**
	 * @deprecated use {@link #readByProductState(String, String, String)} instead.
	 */
	@Deprecated
	List<YieldModel> readByProductState(YieldModel action);

	List<YieldModel> readByProductState(@Param("entityType") String entityType, @Param("entityId") String entityId, @Param("state") String state);

	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void insertList(@Param("list") GenericList<YieldModel> yieldList);

	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void insertUpdateList(@Param("list") GenericList<YieldModel> yieldList);

	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void cancelYieldList(@Param("list") GenericList<YieldModel> yields, @Param("version") Date version);

	/**
	 * @deprecated use {@link #listbyproductids(List, List)} instead.
	 */
	@Deprecated
	List<YieldModel> listbyproductids(YieldSearch action);

	//TODO: Fix naming
	List<YieldModel> listbyproductids(@Param("entityTypes") List<String> entityTypes, @Param("entityIds") List<String> entityIds);

	List<Integer> readProductIdListForSpecifiedNames(@Param("list") List<String> yieldNameList, @Param("pmid") Integer pmid);

	List<YieldModel> listByEntityAndDate(YieldModel action);

	List<YieldModel> listByEntityAndDateWithFinal(YieldModel action);

	/**
	 * @deprecated use {@link #readInActiveByEntityAndVersion(String, String, Date)} instead.
	 */
	@Deprecated
	List<YieldModel> readInActiveByEntityAndVersion(YieldModel action);

	List<YieldModel> readInActiveByEntityAndVersion(@Param("entityType") String entityType, @Param("entityId") String entityId, @Param("version") Date version);

	void deleteByProductId(String productId);

	/**
	 * @deprecated use {@link #getAllBySpecifiedDateAndTypeAndEndProductId(String, String, String)} instead.
	 */
	@Deprecated
	List<YieldModel> getAllBySpecifiedDateAndTypeAndEndProductId(YieldModel yield);

	List<YieldModel> getAllBySpecifiedDateAndTypeAndEndProductId(@Param("entityId") String entityId, @Param("entityType") String entityType,
			@Param("name") String name);

	void copyYields(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	void setToFinal(@Param("idList") List<Integer> list);

	void setToFinalByUniqueAndIds(@Param("idList") List<Integer> list, @Param("uniqueKey") String uniqueKey);

	List<YieldModel> readForPeriodByProductIds(@Param("list") List<String> productIds, @Param("dateFrom") LocalDate dateFrom,
			@Param("dateTo") LocalDate dateTo);

	List<YieldModel> readByProductIdsAndName(@Param("list") List<String> productIds, @Param("name") String name);

	List<YieldModel> readForPeriodByProductIdsWithAllStates(@Param("list") List<String> productIds, @Param("dateFrom") Date dateFrom,
			@Param("dateTo") Date dateTo);

	List<YieldModel> readForBookingDatePeriodByRatePlanId(@Param("ratePlanId") Long ratePlanId, @Param("bookingFromDate") Date bookingFromDate,
			@Param("bookingToDate") Date bookingToDate);

	List<YieldModel> readForPeriodByRatePlanId(@Param("ratePlanId") Long ratePlanId, @Param("FromDate") Date fromDate, @Param("ToDate") Date toDate);

	List<IsDayPromotion> readPromotionsPerDayForPeriodByProductIds(@Param("list") List<String> productIds, @Param("dateFrom") Date dateFrom,
			@Param("dateTo") Date dateTo);

	YieldModel readById(Integer id);

	List<YieldModel> getAllYieldByUniqueKeyAndState(String uniqueKey);

	List<YieldModel> getAllYieldByUniqueKey(String groupUnique);

	List<YieldModel> getFilteringListOfPromotion(@Param("productId") Long productId, @Param("rootId") Long rootId, @Param("pmId") Long pmId,
			@Param("status") String status);

	List<YieldModel> getNewBookingComPromotion(@Param("productId") Long productId, @Param("pmId") Long pmId);

	List<YieldModel> getFilteringListOfYields(@Param("productId") Long productId, @Param("status") String status, @Param("pmId") Long pmId,
			@Param("productIds") List<Integer> productIds);

	void setToFinalByUniqueGroup(String uniqueKey);

	void updateStateOnFinalPerPm(String partyId);

	List<YieldModel> listYieldByEntityAndLastVersion(String productId);

	List<YieldModel> readByRatePlanId(Long ratePlanId);

	/**
	 * @deprecated use {@link #getPromoYieldWithinGivenDateRange(String, String, Integer)} instead.
	 */
	@Deprecated
	List<YieldModel> getPromoYieldWithinGivenDateRange(YieldModel yield);

	List<YieldModel> getPromoYieldWithinGivenDateRange(@Param("entityType") String entityType, @Param("entityId") String entityId,
			@Param("channelID") Integer channelId);

	List<YieldModel> getPromoYieldByVersionByProduct(YieldModel yield);

	List<YieldModel> getAllPromoYieldsByVersion(YieldModel yield);

	/**
	 * @deprecated use {@link #getExpiredPromoYield(String, String)} instead.
	 */
	@Deprecated
	List<YieldModel> getExpiredPromoYield(YieldModel yield);

	List<YieldModel> getExpiredPromoYield(String entityType, String entityId);

	List<YieldModel> getExpiredPromoYieldByVersion(YieldModel yield);

	YieldModel fetchYieldByEntityVersionYieldNameAndDate(YieldModel yield);

	List<YieldModel> listByEntityVersionAndDate(YieldModel yield);

	Integer getMinParamByEntityDateAndName(YieldModel yield);

	void updateYieldByGroupUnique(YieldModel yield);

	//TODO: Fix naming
	List<YieldModel> getCollissionsByDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("name") String name,
			@Param("productId") Long productId);

	List<YieldModel> readRatePlanYieldsByProduct(@Param("productId") Long productId);

	List<YieldModel> getYieldsByIds(@Param("list") List<Long> ids);

	List<YieldModel> getCreatedPromotionsForDate(LocalDateTime date);

	YieldModel readPromotionByProductAndDates(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("name") String name,
			@Param("productId") Long productId);

	List<YieldModel> readByExamples(@Param("list") List<YieldModel> yields);

	void updateStateOnFinalPerProduct(Integer productId);

	List<YieldModel> readByProductAndName(@Param("productId") Integer productId, @Param("name") String name);

	List<YieldModel> readByParamsForExactDates(@Param("productId") Integer productId, @Param("name") String name, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate, @Param("channelId") Integer channelId);

	List<YieldModel> readByEntityTypeEntityIdNameAndChannelId(@Param("entityType") String entityType, @Param("entityId") Integer entityId,
			@Param("name") String name, @Param("channelId") Integer channelId);

	/**
	 * @deprecated use {@link #copy(String)} instead.
	 */
	@Deprecated
	void copy(NameId oldnew);

	void copy(@Param("id") String id);

	/**
	 * @deprecated use {@link #countbyentity(String, String)} instead.
	 */
	@Deprecated
	Integer countbyentity(YieldModel action);

	//TODO: Fix naming
	Integer countbyentity(@Param("entityType") String entityType, @Param("entityId") String entityId);

	//TODO: Fix naming
	List<YieldModel> maximumgapfillers();

	YieldModel exists(YieldModel action);

	void cancelYieldList(List<String> yieldIdList);

	/**
	 * @deprecated use {@link #cancelversion(String, String, Date)} instead.
	 */
	@Deprecated
	void cancelversion(YieldModel action);

	//TODO: Fix naming
	void cancelversion(@Param("entityType") String entityType, @Param("entityId") String entityId, @Param("version") Date version);

	List<YieldModel> readByEntityIdAndEntityTypeOnlyFinal(YieldModel yield);

	List<YieldModel> readByEntityIdAndEntityType(YieldModel yield);

	/**
	 * @deprecated use {@link #readByIdAndstate(String, String)} instead.
	 */
	@Deprecated
	YieldModel readByIdAndstate(YieldModel action);

	//TODO: Fix naming
	YieldModel readByIdAndstate(@Param("id") String id, @Param("state") String state);

	List<YieldModel> readByEntityIdAndEntityTypeStateByVersion(YieldModel yield);

	List<YieldModel> getProductYields(YieldModel yield);

	List<YieldModel> getProductsForYieldByVersion(YieldModel yield);

	List<IsRoomPeriodChange> readYieldsForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<YieldModel> readByParamAndEntityAndBookingFromDate(YieldModel action);

	/**
	 * @deprecated use {@link #getAllPromotionYieldsByVersionAndByProduct(String, Integer, Date)} instead.
	 */
	@Deprecated
	List<YieldModel> getAllPromotionYieldsByVersionAndByProduct(YieldModel yield);

	List<YieldModel> getAllPromotionYieldsByVersionAndByProduct(@Param("entityId") String entityId, @Param("channelID") Integer channelId,
			@Param("version") Date version);

	List<YieldModel> listByEntityVersionAndDateWithoutPromotion(YieldModel yield);

	List<YieldModel> getYieldsInInitialState(YieldModel yield);

	List<YieldModel> getYieldsWithBookingDates(YieldModel action);

	List<YieldModel> getAllPromoYieldsByVersionAndState(YieldModel action);

	List<YieldModel> readForPeriodByProductIds(@Param("list") List<String> productIds, @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

	List<String> getProductsWithNightlyYieldsWithBookingInterval(@Param("productIds") List<Integer> productIds, @Param("names") List<String> nightlyYieldTypes);

	List<YieldModel> readYieldByEntityIdEntityTypeAndDates(@Param("entityId") Integer entityId, @Param("entityType") String entityType,
			@Param("list") List<IsLocalDateRange> dates);
}
