package com.mybookingpal.dataaccesslayer.mappers;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface YieldRatePlanMapMapper {

	List<Long> getRatePlanIdsForPromotion(Long yieldId);

	void insertRatePlansForPromotion(@Param("list") List<Long> ratePlanIdsList, @Param("yieldId") Long yieldId);

	void deleteRatePlanForPromotion(@Param("list") List<Long> ratePlanIdsList, @Param("yieldId") Long yieldId);

	void deleteRelationByRatePlanId(Long ratePlanId);

	void deleteRelationRatePlanByYieldId(@Param("ratePlanId") Long ratePlanId, @Param("yieldId") Long yieldId);

	List<Long> getYieldIdsForRatePlans(@Param("list") List<Long> ratePlanIds);
}
