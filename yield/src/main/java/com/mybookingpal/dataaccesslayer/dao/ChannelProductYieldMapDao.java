package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelProductYieldMapModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelProductYieldMapMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelProductYieldMapDao {

	private final ChannelProductYieldMapMapper mapper;

	public ChannelProductYieldMapDao(ChannelProductYieldMapMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * @deprecated use {@link #getChannelRateId(String productId, Integer channelId, String yieldRuleName)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getChannelRateId(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return Collections.emptyList();
		}

		return getChannelRateId(channelProductYieldMap.getProductId(), channelProductYieldMap.getChannelId(), channelProductYieldMap.getYieldRuleName());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getChannelRateId(String productId, Integer channelId, String yieldRuleName) {
		if (Objects.isNull(productId) || Objects.isNull(channelId) || Objects.isNull(yieldRuleName)) {
			return Collections.emptyList();
		}

		return mapper.getChannelRateId(productId, channelId, yieldRuleName);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelProductYieldMapModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelProductYieldMapModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		mapper.update(action);
	}

	/**
	 * @deprecated use {@link #deleteByProductId(Integer channelId, String productId, String yieldRuleName)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(ChannelProductYieldMapModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		deleteByProductId(action.getChannelId(), action.getProductId(), action.getYieldRuleName());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(Integer channelId, String productId, String yieldRuleName) {
		if (Objects.isNull(channelId) || Objects.isNull(productId) || Objects.isNull(yieldRuleName)) {
			return;
		}

		mapper.deleteByProductId(channelId, productId, yieldRuleName);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteById(String id) {
		if (Objects.isNull(id)) {
			return;
		}

		mapper.deleteById(id);
	}

	/**
	 * @deprecated use {@link #deleteByChannelIdProductIdChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByChannelIdProductIdChannelRateId(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return;
		}

		deleteByChannelIdProductIdChannelRateId(channelProductYieldMap.getChannelId(), channelProductYieldMap.getProductId(),
				channelProductYieldMap.getChannelRateId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByChannelIdProductIdChannelRateId(Integer channelId, String productId, String channelRateId) {
		if (Objects.isNull(channelId) || Objects.isNull(productId) || Objects.isNull(channelRateId)) {
			return;
		}

		mapper.deleteByChannelIdProductIdChannelRateId(channelId, productId, channelRateId);
	}

	/**
	 * Can cause sql exception if select will match multiple rows.
	 *
	 * @deprecated use {@link #getYieldRuleNameByChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductYieldMapModel getChannelRateName(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return null;
		}

		return getYieldRuleNameByChannelRateId(channelProductYieldMap.getChannelId(), channelProductYieldMap.getProductId(),
				channelProductYieldMap.getChannelRateId());
	}

	/**
	 * @deprecated use {@link #getYieldRuleNameByChannelRateId(Integer channelId, String productId, String channelRateId)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductYieldMapModel getYieldRuleNameByChannelRateId(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return null;
		}

		return getYieldRuleNameByChannelRateId(channelProductYieldMap.getChannelId(), channelProductYieldMap.getProductId(),
				channelProductYieldMap.getChannelRateId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductYieldMapModel getYieldRuleNameByChannelRateId(Integer channelId, String productId, String channelRateId) {
		if (Objects.isNull(channelId) || Objects.isNull(productId) || Objects.isNull(channelRateId)) {
			return null;
		}

		return mapper.getYieldRuleNameByChannelRateId(channelId, productId, channelRateId);
	}

	/**
	 * @deprecated use {@link #getPromoRatesByChannelIdProductId(Integer channelId, String productId)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getPromoRatesByChannelIdProductId(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return Collections.emptyList();
		}

		return getPromoRatesByChannelIdProductId(channelProductYieldMap.getChannelId(), channelProductYieldMap.getProductId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getPromoRatesByChannelIdProductId(Integer channelId, String productId) {
		if (Objects.isNull(channelId) || Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getPromoRatesByChannelIdProductId(channelId, productId);
	}

	/**
	 * @deprecated use {@link #getByProductId(String productId)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getByProductId(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return Collections.emptyList();
		}

		return getByProductId(channelProductYieldMap.getProductId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelProductYieldMapModel> getByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.getByProductId(productId);
	}

	/**
	 * @deprecated use {@link #updateChannelProductYieldMapWithOldRatePlanId(String oldRatePlanId, String productId, Integer channelId)} instead.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateChannelProductYieldMapWithOldRatePlanId(ChannelProductYieldMapModel action) {
		if (Objects.isNull(action) || action.getChannelRateId() == null) {
			return;
		}

		updateChannelProductYieldMapWithOldRatePlanId(action.getOldRatePlanId(), action.getProductId(), action.getChannelId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateChannelProductYieldMapWithOldRatePlanId(String oldRatePlanId, String productId, Integer channelId) {
		if (Objects.isNull(oldRatePlanId) || Objects.isNull(productId) || Objects.isNull(channelId)) {
			return;
		}

		mapper.updateChannelProductYieldMapWithOldRatePlanId(oldRatePlanId, productId, channelId);
	}

	/**
	 * @deprecated use {@link #archiveChannelProductYieldMap(Integer channelId, Integer existingChannelId, List list)} instead.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void archiveChannelProductYieldMap(ChannelProductYieldMapModel channelProductYieldMap) {
		if (Objects.isNull(channelProductYieldMap)) {
			return;
		}

		archiveChannelProductYieldMap(channelProductYieldMap.getChannelId(), channelProductYieldMap.getExistingChannelId(), channelProductYieldMap.getList());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void archiveChannelProductYieldMap(Integer channelId, Integer existingChannelId, List<String> list) {
		if (Objects.isNull(channelId) || Objects.isNull(existingChannelId) || Objects.isNull(list)) {
			return;
		}

		mapper.archiveChannelProductYieldMap(channelId, existingChannelId, list);
	}

}
