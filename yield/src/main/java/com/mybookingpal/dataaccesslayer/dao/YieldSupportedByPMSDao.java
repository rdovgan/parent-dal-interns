package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.YieldSupportedByPMSModel;
import com.mybookingpal.dataaccesslayer.mappers.YieldSupportedByPMSMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class YieldSupportedByPMSDao {
	private final YieldSupportedByPMSMapper mapper;

	public YieldSupportedByPMSDao(YieldSupportedByPMSMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldSupportedByPMSModel readByPmId(Integer pmId) {
		return mapper.readByPmId(pmId);
	}
}
