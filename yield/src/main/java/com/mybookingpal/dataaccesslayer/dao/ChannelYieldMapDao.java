package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ChannelYieldMapModel;
import com.mybookingpal.dataaccesslayer.mappers.ChannelYieldMapMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class ChannelYieldMapDao {

	private final ChannelYieldMapMapper mapper;

	public ChannelYieldMapDao(ChannelYieldMapMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ChannelYieldMapModel channelYieldMap) {
		if (Objects.nonNull(channelYieldMap)) {
			mapper.create(channelYieldMap);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelYieldMapModel> getActiveRuleGroupsByRuleId(String ruleId) {
		if (Objects.isNull(ruleId)) {
			return Collections.emptyList();
		}

		return mapper.getActiveRuleGroupsByRuleId(ruleId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelYieldMapModel channelYieldMap) {
		if (Objects.nonNull(channelYieldMap)) {
			mapper.update(channelYieldMap);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelYieldMapModel> getActiveByYieldId(Integer yieldId) {
		if (Objects.isNull(yieldId)) {
			return Collections.emptyList();
		}

		return mapper.getActiveByYieldId(yieldId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelYieldMapModel getActiveByYieldIdAndRuleId(Integer yieldId, String ruleId) {
		if (Objects.isNull(yieldId) || Objects.isNull(ruleId)) {
			return null;
		}

		return mapper.getActiveByYieldIdAndRuleId(yieldId, ruleId);
	}

}
