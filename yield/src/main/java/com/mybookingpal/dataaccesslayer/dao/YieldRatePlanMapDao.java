package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.mappers.YieldRatePlanMapMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class YieldRatePlanMapDao {

	private final YieldRatePlanMapMapper mapper;

	public YieldRatePlanMapDao(YieldRatePlanMapMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Long> getRatePlanIdsForPromotion(Long yieldId) {
		if (yieldId == null) {
			return Collections.emptyList();
		}

		List<Long> planIds = mapper.getRatePlanIdsForPromotion(yieldId);
		if (CollectionUtils.isEmpty(planIds)) {
			return Collections.emptyList();
		}

		return planIds;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertRatePlansForPromotion(List<Long> ratePlanIdsList, Long yieldId) {
		if (CollectionUtils.isEmpty(ratePlanIdsList) || yieldId == null) {
			return;
		}

		List<Long> filteredPlanList = ratePlanIdsList.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.insertRatePlansForPromotion(filteredPlanList, yieldId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteRatePlanForPromotion(List<Long> ratePlanIdsList, Long yieldId) {
		if (CollectionUtils.isEmpty(ratePlanIdsList) || yieldId == null) {
			return;
		}

		List<Long> filteredPlanList = ratePlanIdsList.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.deleteRatePlanForPromotion(filteredPlanList, yieldId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteRelationByRatePlanId(Long ratePlanId) {
		if (Objects.isNull(ratePlanId)) {
			return;
		}

		mapper.deleteRelationByRatePlanId(ratePlanId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteRelationRatePlanByYieldId(Long ratePlanId, Long yieldId) {
		if (Objects.isNull(ratePlanId) || Objects.isNull(yieldId)) {
			return;
		}

		mapper.deleteRelationRatePlanByYieldId(ratePlanId, yieldId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Long> getYieldIdsForRatePlans(List<Long> ratePlanIds) {
		if (CollectionUtils.isEmpty(ratePlanIds)) {
			return Collections.emptyList();
		}

		List<Long> filteredRatePlanIds = ratePlanIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.getYieldIdsForRatePlans(filteredRatePlanIds);
	}

}
