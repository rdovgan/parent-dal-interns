package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.dto.IsDayPromotion;
import com.mybookingpal.dataaccesslayer.utils.IsLocalDateRange;
import com.mybookingpal.dataaccesslayer.dto.IsRoomPeriodChange;
import com.mybookingpal.dataaccesslayer.entity.YieldModel;
import com.mybookingpal.dataaccesslayer.dto.YieldSearch;
import com.mybookingpal.dataaccesslayer.mappers.YieldMapper;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class YieldDao {

	private final YieldMapper mapper;

	public YieldDao(YieldMapper mapper) {
		this.mapper = mapper;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO @SqlUpdateMarker
	public void create(YieldModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO @SqlUpdateMarker
	public void update(YieldModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		mapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel read(String id) {
		if (Objects.isNull(id)) {
			return null;
		}

		return mapper.read(id);
	}

	/**
	 * @deprecated use {@link #listByEntity(String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	// TODO: Fix naming
	public List<YieldModel> listbyentity(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return listByEntity(action.getEntityType(), action.getEntityId());
	}

	public List<YieldModel> listByEntity(String entityType, String entityId) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId)) {
			return Collections.emptyList();
		}
		return mapper.listbyentity(entityType, entityId);
	}

	/**
	 * @deprecated use {@link #readByProductState(String, String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByProductState(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return readByProductState(action.getEntityType(), action.getEntityId(), action.getState());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByProductState(String entityType, String entityId, String state) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId) || Objects.isNull(state)) {
			return Collections.emptyList();
		}
		return mapper.readByProductState(entityType, entityId, state);
	}

	// TODO @Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void insertList(@Param("list") GenericList<YieldModel> yieldList);

	// TODO @Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void insertUpdateList(@Param("list") GenericList<YieldModel> yieldList);

	// TODO: @Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO @SqlUpdateMarker
	//TODO: GenericList can be replaced | void cancelYieldList(@Param("list") GenericList<YieldModel> yields, @Param("version") Date version);

	/**
	 * @deprecated use {@link #listByProductIds(List, List)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	// TODO: Fix naming
	public List<YieldModel> listbyproductids(YieldSearch action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return listByProductIds(action.getEntityTypes(), action.getEntityIds());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	// TODO: Fix naming
	public List<YieldModel> listByProductIds(List<String> entityTypes, List<String> entityIds) {
		if (Objects.isNull(entityTypes) || Objects.isNull(entityIds)) {
			return Collections.emptyList();
		}

		return mapper.listbyproductids(entityTypes, entityIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readProductIdListForSpecifiedNames(List<String> yieldNameList, Integer pmid) {
		if (Objects.isNull(yieldNameList) || Objects.isNull(pmid)) {
			return Collections.emptyList();
		}

		List<String> notNullYieldNameList = yieldNameList.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readProductIdListForSpecifiedNames(notNullYieldNameList, pmid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> listByEntityAndDate(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.listByEntityAndDate(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> listByEntityAndDateWithFinal(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.listByEntityAndDateWithFinal(action);
	}

	/**
	 * @deprecated use {@link #readInActiveByEntityAndVersion(String, String, Date)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readInActiveByEntityAndVersion(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return readInActiveByEntityAndVersion(action.getEntityType(), action.getEntityId(), action.getVersion());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readInActiveByEntityAndVersion(String entityType, String entityId, Date version) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId) || Objects.isNull(version)) {
			return Collections.emptyList();
		}
		return mapper.readInActiveByEntityAndVersion(entityType, entityId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		if (Objects.isNull(productId)) {
			return;
		}

		mapper.deleteByProductId(productId);
	}

	/**
	 * @deprecated use {@link #getAllBySpecifiedDateAndTypeAndEndProductId(String, String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllBySpecifiedDateAndTypeAndEndProductId(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return getAllBySpecifiedDateAndTypeAndEndProductId(yield.getEntityId(), yield.getEntityType(), yield.getName());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllBySpecifiedDateAndTypeAndEndProductId(String entityId, String entityType, String name) {
		if (Objects.isNull(entityId) || Objects.isNull(entityType) || Objects.isNull(name)) {
			return Collections.emptyList();
		}

		return mapper.getAllBySpecifiedDateAndTypeAndEndProductId(entityId, entityType, name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copyYields(Integer fromProductId, Integer toProductId) {
		if (Objects.isNull(fromProductId) || Objects.isNull(toProductId)) {
			return;
		}

		mapper.copyYields(fromProductId, toProductId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setToFinal(List<Integer> list) {
		if (Objects.isNull(list)) {
			return;
		}
		List<Integer> notNullList = list.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.setToFinal(notNullList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setToFinalByUniqueAndIds(List<Integer> list, String uniqueKey) {
		if (Objects.isNull(list) || Objects.isNull(uniqueKey)) {
			return;
		}
		List<Integer> notNullList = list.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.setToFinalByUniqueAndIds(notNullList, uniqueKey);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readForPeriodByProductIds(List<String> productIds, LocalDate dateFrom, LocalDate dateTo) {
		if (Objects.isNull(productIds) || Objects.isNull(dateFrom) || Objects.isNull(dateTo)) {
			return Collections.emptyList();
		}
		List<String> notNullProductIds = productIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readForPeriodByProductIds(notNullProductIds, dateFrom, dateTo);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByProductIdsAndName(List<String> productIds, String name) {
		if (Objects.isNull(productIds) || Objects.isNull(name)) {
			return Collections.emptyList();
		}
		List<String> notNullProductIds = productIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readByProductIdsAndName(notNullProductIds, name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readForPeriodByProductIdsWithAllStates(List<String> productIds, Date dateFrom, Date dateTo) {
		if (Objects.isNull(productIds) || Objects.isNull(dateFrom) || Objects.isNull(dateTo)) {
			return Collections.emptyList();
		}
		List<String> notNullProductIds = productIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readForPeriodByProductIdsWithAllStates(notNullProductIds, dateFrom, dateTo);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readForBookingDatePeriodByRatePlanId(Long ratePlanId, Date bookingFromDate, Date bookingToDate) {
		if (Objects.isNull(ratePlanId) || Objects.isNull(bookingFromDate) || Objects.isNull(bookingToDate)) {
			return Collections.emptyList();
		}

		return mapper.readForBookingDatePeriodByRatePlanId(ratePlanId, bookingFromDate, bookingToDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readForPeriodByRatePlanId(Long ratePlanId, Date fromDate, Date toDate) {
		if (Objects.isNull(ratePlanId) || Objects.isNull(fromDate) || Objects.isNull(toDate)) {
			return Collections.emptyList();
		}

		return mapper.readForPeriodByRatePlanId(ratePlanId, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsDayPromotion> readPromotionsPerDayForPeriodByProductIds(List<String> productIds, Date dateFrom, Date dateTo) {
		if (Objects.isNull(productIds) || Objects.isNull(dateFrom) || Objects.isNull(dateTo)) {
			return Collections.emptyList();
		}
		List<String> notNullProductIds = productIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readPromotionsPerDayForPeriodByProductIds(notNullProductIds, dateFrom, dateTo);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel readById(Integer id) {
		if (Objects.isNull(id)) {
			return null;
		}

		return mapper.readById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllYieldByUniqueKeyAndState(String uniqueKey) {
		if (Objects.isNull(uniqueKey)) {
			return Collections.emptyList();
		}

		return mapper.getAllYieldByUniqueKeyAndState(uniqueKey);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllYieldByUniqueKey(String groupUnique) {
		if (Objects.isNull(groupUnique)) {
			return Collections.emptyList();
		}

		return mapper.getAllYieldByUniqueKey(groupUnique);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getFilteringListOfPromotion(Long productId, Long rootId, Long pmId, String status) {
		return mapper.getFilteringListOfPromotion(productId, rootId, pmId, status);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getNewBookingComPromotion(Long productId, Long pmId) {
		if (Objects.isNull(productId) || Objects.isNull(pmId)) {
			return Collections.emptyList();
		}

		return mapper.getNewBookingComPromotion(productId, pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getFilteringListOfYields(Long productId, String status, Long pmId, List<Integer> productIds) {
		return mapper.getFilteringListOfYields(productId, status, pmId, productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setToFinalByUniqueGroup(String uniqueKey) {
		if (Objects.isNull(uniqueKey)) {
			return;
		}

		mapper.setToFinalByUniqueGroup(uniqueKey);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerPm(String partyId) {
		if (Objects.isNull(partyId)) {
			return;
		}

		mapper.updateStateOnFinalPerPm(partyId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> listYieldByEntityAndLastVersion(String productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.listYieldByEntityAndLastVersion(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByRatePlanId(Long ratePlanId) {
		if (Objects.isNull(ratePlanId)) {
			return Collections.emptyList();
		}

		return mapper.readByRatePlanId(ratePlanId);
	}

	/**
	 * @deprecated use {@link #getPromoYieldWithinGivenDateRange(String, String, Integer)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getPromoYieldWithinGivenDateRange(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return getPromoYieldWithinGivenDateRange(yield.getEntityType(), yield.getEntityId(), yield.getChannelId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getPromoYieldWithinGivenDateRange(String entityType, String entityId, Integer channelId) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}

		return mapper.getPromoYieldWithinGivenDateRange(entityType, entityId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getPromoYieldByVersionByProduct(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getPromoYieldByVersionByProduct(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllPromoYieldsByVersion(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getAllPromoYieldsByVersion(yield);
	}

	/**
	 * @deprecated use {@link #getExpiredPromoYield(String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getExpiredPromoYield(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return getExpiredPromoYield(yield.getEntityType(), yield.getEntityId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getExpiredPromoYield(String entityType, String entityId) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId)) {
			return Collections.emptyList();
		}

		return mapper.getExpiredPromoYield(entityType, entityId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getExpiredPromoYieldByVersion(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getExpiredPromoYieldByVersion(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel fetchYieldByEntityVersionYieldNameAndDate(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return null;
		}

		return mapper.fetchYieldByEntityVersionYieldNameAndDate(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> listByEntityVersionAndDate(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.listByEntityVersionAndDate(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getMinParamByEntityDateAndName(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return null;
		}

		return mapper.getMinParamByEntityDateAndName(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateYieldByGroupUnique(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return;
		}

		mapper.updateYieldByGroupUnique(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<YieldModel> getCollissionsByDate(Date fromDate, Date toDate, String name, Long productId) {
		return mapper.getCollissionsByDate(fromDate, toDate, name, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readRatePlanYieldsByProduct(Long productId) {
		if (Objects.isNull(productId)) {
			return Collections.emptyList();
		}

		return mapper.readRatePlanYieldsByProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getYieldsByIds(List<Long> ids) {
		if (Objects.isNull(ids)) {
			return Collections.emptyList();
		}
		List<Long> notNullIds = ids.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.getYieldsByIds(notNullIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getCreatedPromotionsForDate(LocalDateTime date) {
		if (Objects.isNull(date)) {
			return Collections.emptyList();
		}

		return mapper.getCreatedPromotionsForDate(date);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel readPromotionByProductAndDates(Date fromDate, Date toDate, String name, Long productId) {
		if (Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects.isNull(name) || Objects.isNull(productId)) {
			return null;
		}

		return mapper.readPromotionByProductAndDates(fromDate, toDate, name, productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByExamples(List<YieldModel> yields) {
		if (Objects.isNull(yields)) {
			return Collections.emptyList();
		}
		List<YieldModel> notNullYields = yields.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readByExamples(notNullYields);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStateOnFinalPerProduct(Integer productId) {
		if (Objects.isNull(productId)) {
			return;
		}

		mapper.updateStateOnFinalPerProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByProductAndName(Integer productId, String name) {
		if (Objects.isNull(productId) || Objects.isNull(name)) {
			return Collections.emptyList();
		}

		return mapper.readByProductAndName(productId, name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByParamsForExactDates(Integer productId, String name, LocalDate fromDate, LocalDate toDate, Integer channelId) {
		if (Objects.isNull(productId) || Objects.isNull(name) || Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}

		return mapper.readByParamsForExactDates(productId, name, fromDate, toDate, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByEntityTypeEntityIdNameAndChannelId(String entityType, Integer entityId, String name, Integer channelId) {
		return mapper.readByEntityTypeEntityIdNameAndChannelId(entityType, entityId, name, channelId);
	}

	/**
	 * @deprecated use {@link #copy(String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copy(NameId oldNew) {
		if (Objects.isNull(oldNew)) {
			return;
		}
		copy(oldNew.getId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void copy(String id) {
		if (Objects.isNull(id)) {
			return;
		}
		mapper.copy(id);
	}

	/**
	 * @deprecated use {@link #countByEntity(String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public Integer countbyentity(YieldModel action) {
		if (Objects.isNull(action)) {
			return null;
		}

		return countByEntity(action.getEntityType(), action.getEntityId());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer countByEntity(String entityType, String entityId) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId)) {
			return null;
		}

		return mapper.countbyentity(entityType, entityId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public List<YieldModel> maximumgapfillers() {
		return mapper.maximumgapfillers();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel exists(YieldModel action) {
		if (Objects.isNull(action)) {
			return null;
		}

		return mapper.exists(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelYieldList(List<String> yieldIdList) {
		if (Objects.isNull(yieldIdList)) {
			return;
		}

		List<String> notNullYieldIdList = yieldIdList.stream().filter(Objects::nonNull).collect(Collectors.toList());

		mapper.cancelYieldList(notNullYieldIdList);
	}

	/**
	 * @deprecated use {@link #cancelVersion(String, String, Date)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public void cancelversion(YieldModel action) {
		if (Objects.isNull(action)) {
			return;
		}

		cancelVersion(action.getEntityType(), action.getEntityId(), action.getVersion());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelVersion(String entityType, String entityId, Date version) {
		if (Objects.isNull(entityType) || Objects.isNull(entityId) || Objects.isNull(version)) {
			return;
		}

		mapper.cancelversion(entityType, entityId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByEntityIdAndEntityTypeOnlyFinal(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.readByEntityIdAndEntityTypeOnlyFinal(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByEntityIdAndEntityType(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.readByEntityIdAndEntityType(yield);
	}

	/**
	 * @deprecated use {@link #readByIdAndState(String, String)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//TODO: Fix naming
	public YieldModel readByIdAndstate(YieldModel action) {
		if (Objects.isNull(action)) {
			return null;
		}

		return readByIdAndState(action.getId(), action.getName());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public YieldModel readByIdAndState(String id, String name) {
		if (Objects.isNull(id) || Objects.isNull(name)) {
			return null;
		}

		return mapper.readByIdAndstate(id, name);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByEntityIdAndEntityTypeStateByVersion(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.readByEntityIdAndEntityTypeStateByVersion(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getProductYields(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getProductYields(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getProductsForYieldByVersion(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getProductsForYieldByVersion(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IsRoomPeriodChange> readYieldsForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date version,
			Integer channelId) {
		if (Objects.isNull(supplierId) || Objects.isNull(fromDate) || Objects.isNull(toDate) || Objects.isNull(channelId)) {
			return Collections.emptyList();
		}

		return mapper.readYieldsForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, version, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readByParamAndEntityAndBookingFromDate(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.readByParamAndEntityAndBookingFromDate(action);
	}

	/**
	 * @deprecated use {@link #getAllPromotionYieldsByVersionAndByProduct(String, Integer, Date)} instead.
	 */
	@Deprecated
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllPromotionYieldsByVersionAndByProduct(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return getAllPromotionYieldsByVersionAndByProduct(yield.getEntityId(), yield.getChannelId(), yield.getVersion());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllPromotionYieldsByVersionAndByProduct(String entityId, Integer channelId, Date version) {
		if (Objects.isNull(entityId) || Objects.isNull(channelId) || Objects.isNull(version)) {
			return Collections.emptyList();
		}

		return mapper.getAllPromotionYieldsByVersionAndByProduct(entityId, channelId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> listByEntityVersionAndDateWithoutPromotion(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.listByEntityVersionAndDateWithoutPromotion(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getYieldsInInitialState(YieldModel yield) {
		if (Objects.isNull(yield)) {
			return Collections.emptyList();
		}

		return mapper.getYieldsInInitialState(yield);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getYieldsWithBookingDates(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.getYieldsWithBookingDates(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> getAllPromoYieldsByVersionAndState(YieldModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}

		return mapper.getAllPromoYieldsByVersionAndState(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readForPeriodByProductIds(List<String> productIds, Date dateFrom, Date dateTo) {
		return mapper.readForPeriodByProductIds(productIds, dateFrom, dateTo);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> getProductsWithNightlyYieldsWithBookingInterval(List<Integer> productIds, List<String> nightlyYieldTypes) {
		return mapper.getProductsWithNightlyYieldsWithBookingInterval(productIds, nightlyYieldTypes);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<YieldModel> readYieldByEntityIdEntityTypeAndDates(Integer entityId, String entityType, List<IsLocalDateRange> dates) {
		if (Objects.isNull(entityId) || Objects.isNull(entityType) || Objects.isNull(dates)) {
			return Collections.emptyList();
		}
		List<IsLocalDateRange> notNullDates = dates.stream().filter(Objects::nonNull).collect(Collectors.toList());

		return mapper.readYieldByEntityIdEntityTypeAndDates(entityId, entityType, notNullDates);
	}

}
