package com.mybookingpal.dataaccesslayer.dao;

import com.mybookingpal.dataaccesslayer.entity.ReservationConfirmationModel;
import com.mybookingpal.dataaccesslayer.mappers.ReservationConfirmationMapper;
import com.mybookingpal.dataaccesslayer.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class ReservationConfirmationDao {

	@Autowired
	private ReservationConfirmationMapper mapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		mapper.create(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ReservationConfirmationModel read(String id) {
		return mapper.read(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ReservationConfirmationModel readByReservationId(Integer id) {
		return mapper.readByReservationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer readLatestReservationIdByConfirmationId(String id) {
		return mapper.readLatestReservationIdByConfirmationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, String> readLatestReservationIdAndProductIdByConfirmationId(String id) {
		return mapper.readLatestReservationIdAndProductIdByConfirmationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ReservationConfirmationModel readOldReservationIdByConfirmationId(String id) {
		return mapper.readOldReservationIdByConfirmationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ReservationConfirmationModel readNewReservationIdByConfirmationId(String id) {
		return mapper.readNewReservationIdByConfirmationId(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readByChannelIdConfirmationId(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return mapper.readByChannelIdConfirmationId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readByChannelIdAndLikeConfirmationId(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return mapper.readByChannelIdAndLikeConfirmationId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readListByChannelIdAndConfirmationId(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return mapper.readListByChannelIdAndConfirmationId(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return;
		}
		mapper.update(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String id) {
		mapper.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<String> readByMultipleConfirmationIds(List<String> reservationConfirmationIdList) {
		if (CollectionUtils.isEmpty(reservationConfirmationIdList)) {
			return Collections.emptyList();
		}
		return mapper.readByMultipleConfirmationIds(reservationConfirmationIdList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readByReservationIds(List<Long> reservationIds) {
		if (CollectionUtils.isEmpty(reservationIds)) {
			return Collections.emptyList();
		}
		return mapper.readByReservationIds(reservationIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readByConfirmationId(String confirmationId) {
		return mapper.readByConfirmationId(confirmationId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> list() {
		return mapper.list();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readByChannelIdAndCreatedDate(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return mapper.readByChannelIdAndCreatedDate(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReservationConfirmationModel> readDuplicatesByChannelIdAndCreatedDate(ReservationConfirmationModel action) {
		if (Objects.isNull(action)) {
			return Collections.emptyList();
		}
		return mapper.readDuplicatesByChannelIdAndCreatedDate(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteByProductId(String productId) {
		mapper.deleteByProductId(productId);
	}
}
