package com.mybookingpal.dataaccesslayer.mappers;

import com.mybookingpal.dataaccesslayer.entity.ReservationConfirmationModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ReservationConfirmationMapper {

	void create(ReservationConfirmationModel action);

	ReservationConfirmationModel read(String id);

	ReservationConfirmationModel readByReservationId(Integer id);

	Integer readLatestReservationIdByConfirmationId(String id);

	Map<String, String> readLatestReservationIdAndProductIdByConfirmationId(String id);

	ReservationConfirmationModel readOldReservationIdByConfirmationId(String id);

	ReservationConfirmationModel readNewReservationIdByConfirmationId(String id);

	List<ReservationConfirmationModel> readByChannelIdConfirmationId(ReservationConfirmationModel action);

	List<ReservationConfirmationModel> readByChannelIdAndLikeConfirmationId(ReservationConfirmationModel action);

	//TODO : This query makes the same as readByChannelIdConfirmationId(ReservationConfirmationModel action);
	/*List<ReservationConfirmationModel> findByChannelIdAndConfirmationIdLike(ReservationConfirmationModel action);*/

	List<ReservationConfirmationModel> readListByChannelIdAndConfirmationId(ReservationConfirmationModel action);

	void update(ReservationConfirmationModel action);

	void delete(String id);

	List<String> readByMultipleConfirmationIds(@Param("list") List<String> reservationConfirmationIdList);

	List<ReservationConfirmationModel> readByReservationIds(@Param("list") List<Long> reservationIds);

	List<ReservationConfirmationModel> readByConfirmationId(@Param("confirmationId") String confirmationId);

	List<ReservationConfirmationModel> list();

	List<ReservationConfirmationModel> readByChannelIdAndCreatedDate(ReservationConfirmationModel action);

	List<ReservationConfirmationModel> readDuplicatesByChannelIdAndCreatedDate(ReservationConfirmationModel action);

	void deleteByProductId(String productId);
}
